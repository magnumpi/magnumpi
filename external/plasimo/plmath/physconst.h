#ifndef H_PLMATH_PHYSCONST_H
#define H_PLMATH_PHYSCONST_H

/** \file
 *  Declarations of various mathematical and physical constants.
 */

namespace plasimo {

namespace math {

	/// The value of \f$ \pi \f$
	constexpr const double pi = 3.1415926535897932384626433833;

} // namespace math

/** This namespace contains constants to make formulae in the code more clear.
 *  Note that some constants defined here are exact as a consequence of the
 *  definition of the SI system, some are obtained from (exact) equations
 *  involving other constants, and some have approximate experimental values.
 *
 *  For the values and relations we refer to table XXXIII of \cite Mohr2016.
 *  The constants below appear in the same order as in that table, except where
 *  the dependencies dictated otherwise. In particular, the electron mass (and
 *  the proton mass) appear a bit earlier.
 *
 *  NOTE that the upcoming revision of the SI system in November 2018 is most
 *  likely going to result in some fundamental changes. In that revision, some
 *  more constants of nature will be defined to have exact values and quantities
 *  that are now exact will become inexact instead.
 *
 *  \author Kurt Garloff, Wouter Graef, Jan van Dijk
 *  \date   2001, updated to 2014 CODATA in November 2016 and January 2018
 */
namespace CODATA2016
{
	/// The speed of light in vacuum (m/s). This value is exact.
	constexpr const double c0 = 2.99792458e+08;
	constexpr const double LightSpeed = c0;
	/** The magnetic constant, also known permeability of vacuum (H/m).
	 *  This value is exact.
	 */
	constexpr const double mu0 = 4*math::pi*1e-7;
	/** The electric constant, also known as the permittivity of
	 *  vacuum (F/m). This value is exact.
	 */
	constexpr const double eps0 = 1.0/(mu0*c0*c0);
	/** The characteristic impedance of vacuum (Ohm),
	 *  \f$ Z_0 = \sqrt{ \mu_0 / \epsilon_0 } = \mu_0*c_0 \f$.
	 *  This value is exact.
	 */
	constexpr const double VacuumImpedance = mu0*c0;

	/// The Newtonian constant of gravitation (Nm^2/kg^2)
	constexpr const double GravityConstant = 6.67408e-11;

	/// The Planck constant (Js)
	constexpr const double Planck = 6.626070040e-34;
	/// The Planck constant divided by 2pi (Js)
	constexpr const double hbar = Planck/(2*math::pi);

	/// The elementary charge (C)
	constexpr const double ElementaryCharge = 1.6021766208e-19;
	/** The numerical value of 1 eV (J).
	 *  This is obtained as e*1V; as a result, the numerical
	 *  value is equal to the elementary charge unit.
	 */
	constexpr const double eV = ElementaryCharge;

	/// The electron mass (kg).
	constexpr const double ElectronMass = 9.10938356e-31;
	/// The proton mass (kg).
	constexpr const double ProtonMass = 1.672621898e-27;

	/** The fine-structure constant (dimensionless),
	 *  \f$ \alpha = e^2/(4\pi\epsilon_0\hbar c) = e^2/(2\epsilon_0 h c) \f$.
	 */
	constexpr const double alpha = ElementaryCharge*ElementaryCharge/(2*eps0*Planck*c0);
	/// The Rydberg constant (m^-1), \f$ R_{\infty} = \alpha^2 m_ec/(2h)\f$.
	constexpr const double Rinf = alpha*alpha*ElectronMass*c0/(2*Planck);
	/// The Rydberg Energy (J), \f$ E_r = R_\infty h c\f$.
	constexpr const double RydbergEnergy = Rinf*Planck*c0;
	/// The Rydberg Energy (eV)
	constexpr const double RydbergEnergyInEV = RydbergEnergy/eV;
	/// The Hartree energy (J), \f$ E_h = 2E_r \f$.
	constexpr const double Hartree = RydbergEnergy*2;
	/// The Hartree energy (eV)
	constexpr const double HartreeIneV = Hartree/eV;
	/** The Bohr radius (m),
	 *  \f$ a_0 = \alpha/(4\pi R_\infty)\f$.
	 */
	constexpr const double BohrRadius = alpha/(4*math::pi*Rinf);

	/** Avogadro's number (1).
	 *  Note that this is also the numerical value of the
	 *  Avogadro constant (1/mol).
	 */
	constexpr const double Avogadro = 6.022140857e+23;

	/** The atomic mass unit (kg).
	 *  NOTE: at some moment the definition was changed from m(O-16)/16 to
	 *  m(C-12)/12. The symbol amu refers, in principle, to the old value
	 *  but is still commonly used, referring to the new value instead.
	 *  \todo this value may change (slightly) in the 2018 version of SI.
	 *  \todo it is better to remove amu altogether.
	 */
	constexpr const double AMU = 1e-3/Avogadro;
	/** Dalton (kg). At present equal to the unified atomic mass unit u
	 *  also still called amu erroneously. This may change in the 2018
	 *  version the SI system. The value can be obtained from
	 *  \f[ Da = m(C-12)/12 = N_A m(C-12)/(12 N_A) = (12 g/mol)/(12N_A) = g/(N_A mol) \f],
	 *  or
	 *  \f[ Da = 10^{-3}/N_A (kg/mol).\f]
	 */
	constexpr const double Da = 1e-3/Avogadro;

	/// The Boltzmann constant (J/K)
	constexpr const double kB = 1.38064852e-23;
	constexpr const double Boltzmann = kB;
	// 1 eV as a temperature measure: eV/kB
	constexpr const double eVT = eV/Boltzmann;
	/// The Molar gas constant (J/K/mol), \f$ R = kN_A. \f$.
	constexpr const double GasConstant = Boltzmann*Avogadro;

	/** Stefan-Boltzmann constant (W/m^2/K^4),
	 *  \f$ \sigma = (\pi^2/60)k^4/(\hbar^3c^2)\f$.
	 */
	constexpr const double StefanBoltzmann = (math::pi*math::pi/60)*kB*kB*kB*kB/(hbar*hbar*hbar*c0*c0);

	// NOTE: The values below do not appear in table XXXIII of Mohr et al.

	/// Inch (m). This value is exact.
	constexpr const double inch = 2.54e-2;
	/// Townsend (V*m*m). This value is exact.
	constexpr const double Townsend = 1e-21;
	/// bar (Pa). This value is exact.
	constexpr const double bar = 1e5;
	/// (standard) atmosphere (Pa). This value is exact.
	constexpr const double atm = 101325.0;
	/// Torr (Pa). This value is exact.
	constexpr const double Torr = atm/760.0;
	/// Angstrom (m). This value is exact.
	constexpr const double Angstrom = 1e-10;

	/// Degree (rad). This value is exact
	constexpr const double deg = math::pi/180;


	// Time units (values in SI unit s). These values are exact.

	/// The number of seconds in one minute
	constexpr const double minute = 60.0;
	/// The number of seconds in one hour
	constexpr const double hour = 60.0*minute;
	/// The number of seconds in one day
	constexpr const double day = 24.0*hour;
	/// The number of seconds in one year
	constexpr const double year = 365.*day;

} // namespace CODATA2016

namespace constants {

using math::pi;
using namespace CODATA2016;

} // constants

} // namespace plasimo

#endif // H_PLMATH_PHYSCONST_H

