/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "plmath/clencurt.h"

namespace plasimo {
namespace clencurt {

integrator::integrator(scalar_type a, scalar_type b, unsigned lower, unsigned upper)
 : m_a(a), m_b(b), m_lower(lower), m_upper(upper)
{
	m_coeff.resize(m_upper-m_lower+1);
	//define clenshaw curtis quadratures
	for (unsigned order=m_lower;order<=m_upper;++order)
	{
		std::cout << "Setting up Clenshaw-Curtis coefficients "
			"for interval [" << a << "," << b << "], "
			"order=" << order << ", #points = " << ((1<<order)+1) << "...";
		calc_coefs(order,a,b,m_coeff[order-m_lower]);
		std::cout << " done." << std::endl;
	}
}

} // namespace clencurt
} // namespace plasimo

