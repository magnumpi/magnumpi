/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 */

/*  For reference we reproduce a MATLAB implementation by Greg von Winckel, see
 *  http://www.mathworks.com/matlabcentral/fileexchange/6911-fast-clenshaw-curtis-quadrature/content/fclencurt.m
 
	function [x,w]=fclencurt(N1,a,b)

	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	% fclencurt.m - Fast Clenshaw Curtis Quadrature
	%
	% Compute the N nodes and weights for Clenshaw-Curtis
	% Quadrature on the interval [a,b]. Unlike Gauss 
	% quadratures, Clenshaw-Curtis is only exact for 
	% polynomials up to order N, however, using the FFT
	% algorithm, the weights and nodes are computed in linear
	% time. This script will calculate for N=2^20+1 (1048577
	% points) in about 5 seconds on a normal laptop computer.
	%
	% Written by: Greg von Winckel - 02/12/2005
	% Contact: gregvw(at)chtm(dot)unm(dot)edu
	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	N=N1-1; bma=b-a;
	c=zeros(N1,2);
	c(1:2:N1,1)=(2./[1 1-(2:2:N).^2 ])'; c(2,2)=1;
	f=real(ifft([c(1:N1,:);c(N:-1:2,:)]));
	w=bma*([f(1,1); 2*f(2:N,1); f(N1,1)])/2;
	x=0.5*((b+a)+N*bma*f(1:N1,2));

*/

#include "plmath/clencurt_coefs.h"
#include "plmath/fft.h"
#include <vector>
#include <complex>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <limits>
#include <stdexcept>

namespace plasimo {
namespace clencurt {

void calc_coefs(unsigned p, double a, double b, coef_vector& cf)
{
	typedef std::vector<std::complex<double> > CV;
	const unsigned n1=1<<p;
	const unsigned n=n1+1;
	/** \todo Can we also use something akin to GSL's gsl_fft_halfcomplex_inverse
	 *        here? That should be more efficient, since we know that the result
	 *        of the IFFT is going to be real.
	 */
	const double bma=b-a;
	CV c0(n);
	CV c1(n);
	c0[0]=2;
	for (unsigned i=0; i<n; i+=2)
	{
		c0[i]=2./(1.-i*i);
	}
	c1[1]=1;
	CV f0(n1*2);
	CV f1(n1*2);
	for (unsigned i=0; i!=n1; ++i)
	{
		f0[i] = c0[i];
		f1[i] = c1[i];
	}
	for (unsigned i=0; i!=n1; ++i)
	{
		f0[n1+i] = c0[n-i-1];
		f1[n1+i] = c1[n-i-1];
	}
	plasimo::math::ifft_complex(f0);
	plasimo::math::ifft_complex(f1);

	cf.resize(n);
	for (unsigned i=0; i!=cf.size(); ++i)
	{
		const double xi = 0.5*((b+a) + n1*bma*f1[i].real());
		const double wi = bma*( (i==0 || i==n-1) ? f0[i].real() : 2*f0[i].real() ) /2;
		cf[i].pos = xi;
		cf[i].weight = wi;
	}
}

void write_coefs(unsigned p, double a, double b, std::ostream& os)
{
	// save the state of the stream, so we can restore it later
	// (undo the precision modification, etc.)
	std::ios state(NULL);
	state.copyfmt(os);

	coef_vector cf;
	calc_coefs(p,a,b,cf);

	os	<< "# Clenshaw-Curtis positions and weights" << std::endl
		<< "# p = " << p << " (n = " << (1<<p)+1 << ')' << std::endl
		<< "# [a,b] = [" << a << ',' << b << "]." << std::endl;
	os << std::scientific;
	const unsigned precision=std::numeric_limits<double>::digits10 + 1;
	os << "# position:" << std::string(precision+5,' ') << "weight:" << std::endl;
	os.precision(precision);
	for (const auto& i : cf)
	{
		os << i.pos << '\t' << i.weight << std::endl;
	}

	// restore the state of the stream
	os.copyfmt(state);
}

void write_coefs(unsigned p, double a, double b, const std::string& fname)
{
	std::ofstream ofs(fname);
	if (!ofs)
	{
		throw std::runtime_error("Could not open file '" + fname + "' for writing.");
	}
	write_coefs(p,a,b,ofs);
}

} // namespace clencurt
} // namespace plasimo
