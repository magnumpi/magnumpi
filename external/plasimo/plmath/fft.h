/** \file
 *
 * Copyright (C) 2008-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "plmath/physconst.h"
#include <cmath>
#include <complex>
#include <cstdlib>
#include <stdexcept>

namespace plasimo {
namespace math {

namespace impl {

/** A basic implementation of the Fast Fourier Transform (for DoIFFT==false)
 *  and its inverse (for DoIFFT==true). The pointer \a vec must point to an
 *  array of \a size complex values, \a size must by an integral power of
 *  two (or a std::runtime_error is thrown). On return, the elements of \a vec
 *  have been overwritten by the FFT of the data (for DoIFFT==false), or the
 *  inverse DFT of the data (for DoIFFT==true).
 *
 *  For an explanation of the data layout and the algorithm, see page 508 of
 *  "Numerical recipes in C" (second edition) \cite Press1992.
 *
 *  \sa fft_complex
 *  \sa ifft_complex
 *
 *  \author Bart Hartgers
 *  \date   August 2008
 */
template <bool DoIFFT,typename T>
void do_fft_complex(std::complex<T>* const vec, const std::size_t size)
{
	using size_type = std::size_t;
	using scalar_type = T;
	if (size & (size-1))
	{
		throw std::runtime_error("fft: vector size must be a power of 2.");
	}
	for(size_type i=0,j=0; i<size ; ++i)
	{
		if (j>i)
		{
			std::swap(vec[j],vec[i]);
		}
		size_type m=size/2;
		while(m>=1 && j>=m)
		{
			j-=m;
			m/=2;
		}
		j+=m;
	}

	size_type mmax=1;
	size_type n=size;
	scalar_type theta = DoIFFT ? -math::pi : math::pi;
	scalar_type sin_theta = 0;
	while (n>mmax)
	{
		const size_type istep = mmax * 2;
		const scalar_type wpi = sin_theta;
		sin_theta = std::sin(0.5*theta);
		const scalar_type wpr = -2.0 * sin_theta * sin_theta;
		const std::complex<T> wp{wpr,wpi};
		std::complex<T> w{1};
		for(size_type m=0; m<mmax; ++m)
		{
			for(size_type i=m; i<n; i+=istep)
			{
				const size_type j = i + mmax;
				const std::complex<T> tmp{w*vec[j]};
				vec[j]	= vec[i] - tmp;
				vec[i] += tmp;
			}
			w += w*wp;
		}
		mmax *= 2;
		theta *= 0.5;
	}
	if (DoIFFT)
	{
		for (auto* v=vec; v!=vec+size; ++v)
		{
			*v /= size;
		}
	}
}

} // namespace impl

/** Calc the FFT of the vector [ \a vec, \a vec + \a size ).
 *  If \a size is not a power of 2, a std::runtime_error is thrown.
 *
 *  \author Bart Hartgers
 *  \date   August 2008
 */
template <typename T>
void fft_complex(std::complex<T>* const vec, const std::size_t size)
{
	impl::do_fft_complex<false>(vec,size);
}

/** Calc the inverse FFT of the vector [ \a vec, \a vec + \a size ).
 *  If \a size is not a power of 2, a std::runtime_error is thrown.
 *
 *  \author Bart Hartgers
 *  \date   August 2008
 */
template <typename T>
void ifft_complex(std::complex<T>* const vec, const std::size_t size)
{
	impl::do_fft_complex<true>(vec,size);
}

/** Calc the FFT of the vector \a vec. It is assumed that the elements
 *  of \a vec are in a contiguous memory block that starts at vec.data()
 *  and that vec.size() can be used to query vector size.
 *  If \a size is not a power of 2, a std::runtime_error is thrown.
 *
 *  \author Bart Hartgers
 *  \date   August 2008
 */
template <typename VectorT>
void fft_complex(VectorT& vec)
{
	fft_complex(vec.data(),vec.size());
}

/** Calc the inverse FFT of the vector \a vec. It is assumed that the elements
 *  of \a vec are in a contiguous memory block that starts at vec.data()
 *  and that vec.size() can be used to query vector size.
 *  If \a size is not a power of 2, a std::runtime_error is thrown.
 *
 *  \author Bart Hartgers
 *  \date   August 2008
 */
template <typename VectorT>
void ifft_complex(VectorT& vec)
{
	ifft_complex(vec.data(),vec.size());
}

/** This function shuffles the FFT vector \a vec such that the value
 *  that corresponds to frequency zero is centered.
 *
 *  \author Bart Hartgers
 *  \date   August 2008
 */
template <typename VectorT>
void fftshift(VectorT& vec)
{
      typename VectorT::iterator n = vec.begin();
      typename VectorT::iterator p = vec.begin() + vec.size()/2;
      while (p!=vec.end())
      {
	      std::swap(*n++,*p++);
      }
}

} // namespace math
} // namespace plasimo
