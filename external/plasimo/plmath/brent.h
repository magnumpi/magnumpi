/** \file
 *
 * Copyright (C) 2008-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_PLMATH_BRENT_H
#define H_PLMATH_BRENT_H

#include <stdexcept>
#include <limits>
#include <cassert>
#include <cmath>
#include <sstream>

namespace plasimo {
namespace math {

/** Finds the root of the function (object) \a func on the interval [\a x1, \a x2]
 *  using Brent's algorithm. The maximum number of iterations is \a iter_max.
 *  The interval [\a x1, \a x2] must be such that the function has different signs
 *  in the end points.
 *  Func must provide a type-definition value_type (a floating point type) and
 *  a function-call operator with signature 'value_type operator()(value_type x) const'.
 *
 *  \todo At this moment, value_type=double is assumed, float, long double
 *  etc. should also be supported.
 *
 *  \author Colin Johnston and Jan van Dijk
 *  \date   August 2008
 */
template <class Func>
inline double brent(const Func& func, double x1, double x2, unsigned iter_max=1000)
{
	using std::abs;

	double a = x1;
	double b = x2;
	double c = x2;
	double d = 0.0;

	double e = 0.0;

	double fa = func(a);
	double fb = func(b);

	// make sure endpoints straddle y=0
	if (fa*fb>0.0)
	{
		std::stringstream ss;
		ss << "Brent algorithm: End points do not straddle y=0: f("
			<< a << ") = " << fa << ", f(" << b << ") = " << fb << ".";
		throw std::runtime_error(ss.str());
	}

	double fc = fb;

	// the main event
	for(unsigned iter = 0; iter != iter_max; ++iter)
	{
		if(( fb > 0.0 && fc > 0.0)||(fb < 0.0 && fc < 0.0))
		{
			//adjust bounding interval
			c = a;
			fc = fa;
			d = b - a;
			e = b - a;
		}
		if(abs(fc) < abs(fb))
		{
			a = b;
			b = c;
			c = a;
			fa = fb;
			fb = fc;
			fc =fa;
		}
		const double EPS= std::numeric_limits<double>::epsilon();
		// convergence check - from GSL's brent.c
		const double tol1 = 0.5*EPS*abs(b);
		// Numerical Recipes check, only works
		// in limited cirmcumstances
		//tol1 = 2.0*EPS*abs(b)+0.5*1e-5;
		const double xm = 0.5*(c-b);
		if(abs(xm) <= tol1 || fb == 0.0)
		{
			// Success
			return b;
		}
		// use bisection
		if (abs (e) < tol1 || abs (fa) <= abs (fb))
		{
			d = xm;
			e = xm;
		}
		// inverse quadratic interpolation
		else
		{
			double p,q;
			const double s = fb/fa;
			if(a == c)
			{
				p = 2.0*xm*s;
				q = 1.0-s;
			}
			else
			{
				q = fa/fc;
				const double r = fb/fc;
				p = s*(2.0*xm*q*(q-r)-(b-a)*(r-1.0));
				q = (q-1.0)*(r-1.0)*(s-1.0);
			}

			if(p > 0.0)
				q = -q;

			p=abs(p);
			const double min1 = 3.0*xm*q-abs(tol1*q);
			const double min2 = abs(e*q);
			// accept interpolation?
			if(2.0*p < ((min1 < min2) ? min1 : min2))
			{
				e = d;
				d = p/q;
			}
			else
			{
				// interpolation failed - use bisection
				d = xm;
				e = xm;
			}
		}
		// move last best guess to a
		a = b;
		fa = fb;
		// evaluate new trial root
		if(abs(d) > tol1)
			b += d;
		else
			b += (xm >=0 ? (tol1>=0 ? tol1 : -tol1) : (tol1>=0 ? -tol1 : tol1));

		fb = func(b);
	}
	throw std::runtime_error("Brent: Maximum number of iterations exceeded.");
}


/** This helper class template can be constructed with a pointer to a
 *  function of type Func and a pointer to a constant object of type
 *  Params. Func must be a binary function with signature
 *  double ()(double,const Params* params). The object can be called
 *  as a unary function, which will return the result of calling the
 *  provided function, passing the parameter pointer as second argument.
 *
 *  \author Colin Johnston and Jan van Dijk
 *  \date   August 2008
 */
template <class Func, class Params>
struct brent_helper
{
	brent_helper( Func* func, const Params* params)
	 : m_func(func), m_params(params)
	{
	}
	double operator()(double x) const
	{
		return (*m_func)(x,m_params);
	}
	Func* m_func;
	const Params* m_params;
};

/** Find the root 'x' of the function (*\a func)(x,\a params) using Brent's
 *  algorithm. This constructs a brent_helper object from the provided function
 *  pointer and parameter pointer, and passes that to the 4-argument overload of the
 *  brent function along with the parameters \a x1, \a x2 and \a iter_max.
 *
 *  \author Colin Johnston and Jan van Dijk
 *  \date   August 2008
 */
template <class Func, class Params>
inline double brent(Func* func, const Params* params, double x1, double x2, unsigned iter_max=1000)
{
	return brent(brent_helper<Func,Params>(func,params), x1, x2, iter_max);
}

} // namespace math
} // namespace plasimo


#endif // H_PLMATH_BRENT_H

