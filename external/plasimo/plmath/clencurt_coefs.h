/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * This files contains functions for calculating coefficients for
 * Clenshaw-Curtis quadrature.
 */

#ifndef H_PLMATH_CLENCURT_COEFS_H
#define H_PLMATH_CLENCURT_COEFS_H

#include <vector>
#include <string>
#include <iostream>

namespace plasimo {
namespace clencurt {

struct coef_type
{
	using value_type = double;
	value_type pos;
	value_type weight;
};

using coef_vector = std::vector<coef_type>;

/** Calculation of the positions \a x and weights \a w of the (2^p+1)-point
 *  Clenshaw-Curtis quadrature rule for integrating a function over the interval
 *  [\a a, \a b]. The positions and weights are stored in vector \a cf,
 *  which is resized to size 2^p+1 when necessary.
 *
 *  The code uses an inverse fast fourier transform (IFFT) for the efficient and
 *  accurate evaluation of the positions and weights; the algorithm is described
 *  in the references \cite Waldvogel2006 and \cite Trefethen2008 and has also
 *  been implemented in a MATLAB script (Greg von Winckel, 2005), see MathWorks,
 *  http://www.mathworks.com/matlabcentral/fileexchange/6911-fast-clenshaw-curtis-quadrature/content/fclencurt.m
 *
 *  \author Jan van Dijk
 *  \date   May 2015
 */
void calc_coefs(unsigned p, double a, double b, coef_vector& cf);


/** Write the positions and weights of the (2^p+1)-point Clenshaw-Curtis
 *  quadrature rule for integrating a function over the interval [\a a, \a b]
 *  to stream \a os.  See the overload that takes a reference to the coefficient
 *  vector for further information.
 *
 *  \author Jan van Dijk
 *  \date   May 2015
 */
void write_coefs(unsigned p, double a, double b, std::ostream& os);

/** Write the positions and weights of the (2^p+1)-point Clenshaw-Curtis
 *  quadrature rule for integrating a function over the interval [\a a, \a b]
 *  to file \a fname.  See the overload that takes a reference to the
 *  coefficient vector for further information.
 *
 *  \author Jan van Dijk
 *  \date   May 2015
 */
void write_coefs(unsigned p, double a, double b, const std::string& fname);

} // namespace clencurt
} // namespace plasimo

#endif // H_PLMATH_CLENCURT_COEFS_H

