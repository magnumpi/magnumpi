#ifndef H_PLUTIL_SPLIT_STRING_H
#define H_PLUTIL_SPLIT_STRING_H

#include <string>
#include <utility>

namespace plasimo {

/** Split the string \a s in fields, delimited by the character \a d and
 *  appends the results (without the delimiters) in the container \a v.
 *  The container must provide a member push_back that accepts a string.
 *  If \a s is empty, no strings will be added to \a v.
 *
 *  Note that empty strings will be added to \a v in case the delimiter \a d
 *  occurs in the beginning or at the end of \a s, or in the case of two
 *  consecutive delimiters.
 *
 *  As an example, let d be the delimiter character. Then
 *  \verbatim
      ""      -> { }
      "XdYdZ" -> {"X", "Y", "Z"}
      "XdddZ" -> {"X", "",  "",  "Z"}
      "dXYZd" -> {"", "XYZ", ""} \endverbatim
 *
 *  \author Jan van Dijk
 *  \date   May 2009
 */
template <class StringContainerT>
inline void split_string(StringContainerT& v, const std::string& s, char d)
{
	v.clear();
	if (s.empty())
	{
		return;
	}
	std::string::size_type pb=0;
	while (1)
	{
		std::string::size_type pe = s.find(d,pb);
		if (pe!=std::string::npos)
		{
			v.push_back(std::string(s.begin()+pb,s.begin()+pe));
			pb=++pe;
		}
		else
		{
			v.push_back(std::string(s.begin()+pb,s.end()));
			break;
		}
	}
}

template <class StringContainerT>
inline StringContainerT split_string(const std::string& s, char d)
{
	StringContainerT v;
	split_string<StringContainerT>(v,s,d);
	return v;
}

} // namespace plasimo

#endif // H_PLUTIL_SPLIT_STRING_H

