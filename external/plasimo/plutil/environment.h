#ifndef H_PLUTIL_ENVIRONMENT_H
#define H_PLUTIL_ENVIRONMENT_H

#include <cctype>
#include <cstdlib>
#include <stdexcept>
#include "plutil/string_util.h"

namespace plasimo {

/** If the environment variable \a envvar exists, return its
 *  value as a std::string. If the variable is not set, and
 *  \a required is false, return an empty string is. If the
 *  variable is not set and \a required is true, throw a
 *  std::runtime_error.
 *   
 *  \author Jan van Dijk
 *  \date   May 2013
 */
inline std::string get_env_var(const std::string& envvar, bool required)
{
	const char* value = std::getenv(envvar.c_str());
	if (required && !value)
	{
		throw std::runtime_error( "Environment variable '"
			+ envvar + "' required, but not set.");
	}
	return value ? value : "";
}

} // namespace plasimo
#include <iostream>
namespace plasimo {

/** Resolves environment variables in \a name. If \a env_required
 *  is true, a runtime_error is thrown if an environment variables that
 *  is used in \a name is not set. The function returns true if any
 *  substiutions were made, false otehrwise.
 *
 *  An environment variable is of the form $(variable). A runtime_error
 *  is thrown if an empty variable name is encountered, as in "$()".
 *
 *  Note that this function correctly handles nested environment
 *  assignments. As an example, given the environment variables
	\verbatim
	A=$(B)
	B=$(C)
	C=value, \endverbatim
 *  this function changes the string "$(A)" into "value", not into "$(B)".
 *
 *  If an error occurs, partial substitution may result.
 *
 *  \author Bart Hartgers and Jan van Dijk
 *  \date   May 2013
 */
inline bool substitute_env_vars(std::string& name, bool env_required)
{
  try {
	std::string::size_type ndx = name.find("$(");
	if (ndx==name.npos)
	{
		// no opening $( found, there is nothing to be substituted
		return false;
	}
	const std::string::size_type n1=ndx;
	// OK, there is one. Locate the matching ')'
	while (++ndx != name.size())
	{
		if ( name[ndx]==')' )
		{
			break;
		}
	}
	if (ndx==name.size())
	{
		throw std::runtime_error("Unmatched '('.");
	}
	// also remove the $( and )
	const std::string::size_type len=ndx-n1-2;
	const std::string envvar(name.substr(n1+2,len));

	check_envvar_name(envvar);
	const char* envval(std::getenv(envvar.c_str()));
	if (env_required && !envval)
	{
		throw std::runtime_error("Variable '" + envvar + "' required, but not set.");
	}
	search_and_replace(name, "$("+envvar+")", envval ? envval : "");
	// recurse: needed in case multiple environment variables are used in
	//          the string, or the environment variables are defined in terms
	//          of other environment variables.
	substitute_env_vars(name,env_required);
	return true;
  }
  catch (std::exception& exc)
  {
	throw std::runtime_error(
		"While substituting environment variables in '" + name +
			"': " + std::string(exc.what()));
  }
}

/** This function is similar to substitute_env_vars, but modifies and returns
 *  a copy of the string \a name, rather than \a name itself.
 *
 *  \author Bart Hartgers and Jan van Dijk
 *  \date   May 2013
 */
inline std::string substitute_env_vars_copy(const std::string& name, bool env_required)
{
	std::string copy(name);
	substitute_env_vars(copy,env_required);
	return copy;
}

} // namespace plasimo

#endif // H_PLUTIL_ENVIRONMENT_H
