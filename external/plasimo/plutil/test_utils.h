#ifndef H_PLUTIL_TEST_UTILS_H
#define H_PLUTIL_TEST_UTILS_H

#include <iostream>
#include <exception>
#include <limits>
#include <cmath>
#include <complex>

inline bool are_almost_equal(
	double a,
	double b,
	double tol=10*std::numeric_limits<double>::epsilon())
{
	return a==0.0 && b==0.0
		? true
		: std::abs(a-b)<=std::max(std::abs(a),std::abs(b))*tol;
}

inline bool are_almost_equal(
	std::complex<double> a,
	std::complex<double> b,
	double tol=10*std::numeric_limits<double>::epsilon())
{
	return     are_almost_equal(a.real(),b.real())
		&& are_almost_equal(a.imag(),b.imag());
}

template <class Iter1, class Iter2>
inline bool are_almost_equal_range(
	Iter1 b,
	Iter1 e,
	Iter2 b2,
	double tol=10*std::numeric_limits<double>::epsilon())
{
	bool res = true;
	while (b!=e)
	{
		res = res && are_almost_equal(*b,*b2,tol);
		++b; ++b2;
	}
	return res;
}

#define test_almost_equal(q1,q2) test_expr(are_almost_equal(q1,q2));

inline int& test_ntests() { static int ntests=0; return ntests; }
inline int& test_nerrors() { static int nerrors=0; return nerrors; }
inline int& test_nxfails() { static int nxfails=0; return nxfails; }

#define report_failed std::cout << "TEST FAILED (" << __FILE__ << ':' << __LINE__ << "):" << std::endl;

#define begin_test \
try { \
  ++test_ntests();	\
  {

#define end_test(expr,should_throw,xfails) \
  }	\
  if (should_throw)	\
  {	\
	if (xfails) \
	{	\
		++test_nxfails();	\
		std::cout << " * OK: XFAIL (known failure): test should generate an exception. Expression was: '" << #expr << "'." << std::endl;	\
	}	\
	else	\
	{	\
		++test_nerrors();	\
		report_failed \
		std::cout << " * test should generate an exception. Expression was: '" << #expr << "'." << std::endl;	\
	}	\
  }	\
  else	\
  {	\
  }	\
} catch (std::exception& exc)	\
{	\
  if (should_throw)	\
  {	\
	if (xfails) \
	{	\
		++test_nxfails();	\
		report_failed \
		std::cout << " * This test passed, but was marked XFAIL: an exception was expected: '" \
			     << exc.what() << "'" << std::endl;	\
	}	\
	else \
	{	\
		std::cout << " * OK: an exception was expected: '" \
			     << exc.what() << "'" << std::endl;	\
	}	\
  }	\
  else	\
  {	\
	if (xfails) \
	{	\
		++test_nxfails();	\
		std::cout << " * OK: XFAIL (known failure): test generated an unexpected exception. Expression was: '" << #expr \
				<< "', it raised: '" << exc.what() << "'." << std::endl;	\
	}	\
	else \
	{	\
		++test_nerrors();	\
		report_failed \
		std::cout << " * test generated an unexpected exception. Expression was: '" << #expr \
				<< "', it raised: '" << exc.what() << "'." << std::endl;	\
	} \
  }	\
}

#define do_test_expr( expr, xfails) \
  if ( (xfails) ^ ((expr)) )	\
  {	\
	if (xfails) \
	{	\
		++test_nxfails();	\
		std::cout << " * OK: XFAIL (known failure): Expression '" << #expr << "'." << std::endl;	\
	}	\
	else \
		std::cout << " * OK: Expression '" << #expr << "'." << std::endl;	\
  }	\
  else	\
  {	\
	report_failed \
	std::cout << " * assert failed: '" << #expr << "'" << std::endl;	\
	++test_nerrors();	\
  }

/** Test that \a expr can be executed without throwing an exception.
 *  If it does anyway, an error is flagged.
 */
#define test_code(expr)	\
begin_test	\
expr;	\
std::cout << " * OK: code '" << #expr << "'." << std::endl;	\
end_test(expr,false,false)

/** Execute the code \a expr and verify that it throws an exception.
 *  If it does not, an error is flagged.
 */
#define test_code_throws(expr)	\
begin_test	\
expr;	\
end_test(expr,true,false)

/** Execute the code \a expr and verify that it throws an exception.
 *  If it does not, an xfail is flagged.
 *  This is for code that is known to have a defficiency.
 */
#define test_code_throws_xfail(expr)	\
begin_test	\
expr;	\
end_test(expr,true,true)

#define test_expr_xfail(expr)	\
begin_test	\
do_test_expr(expr,true)	\
end_test(expr,false,false)

#define test_expr_throws(expr)	\
begin_test	\
do_test_expr(expr,false)	\
end_test(expr,true,false)

#define test_expr_throws_xfail(expr)	\
begin_test	\
do_test_expr(expr,false)	\
end_test(expr,true,true)

#define test_expr(expr)	\
begin_test	\
do_test_expr(expr,false)	\
end_test(expr,false,false)

#define test_static_assert(expr)	\
begin_test	\
static_assert(expr,#expr); \
do_test_expr(expr,false)	\
end_test(expr,false,false)

#define test_expr_else(expr,code, penalty) \
  if (! (expr))	\
  {	\
	report_failed \
	std::cout << " * aborting: assert failed: '" << #expr << "'" << std::endl;	\
	test_nerrors()+=penalty;	\
	code;	\
  }	\
  else	\
  {	\
	std::cout << " * OK: Expression '" << #expr << "'." << std::endl;	\
  }

#define test_report \
std::cout << "TEST REPORT: " << std::endl;	\
std::cout << " - " << test_ntests()  << " tests were done" << std::endl;	\
std::cout << " - " << test_nxfails() << " known failures occurred." << std::endl;		\
std::cout << " - " << test_nerrors() << " errors occured" << std::endl;


#endif // H_PLUTIL_TEST_UTILS_H

