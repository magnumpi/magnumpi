/** \file
 *
 * Copyright (C) 2028-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_VECTOR_SORT_H
#define H_VECTOR_SORT_H

#include <vector>
#include <algorithm>
#include <numeric>

namespace tk {

/** \todo Document me, add references, explain interface and purpose.
 *
 *  \author Chris Schoutrop
 *  \date   October 2018
 */
class vector_sort
{
	public:
	/*
	Sort vector based on the permutations needed to sort another vector.
	Example:
        //Input x-y data
		std::vector<double> m_x{-2, 8, 3, 5};
        std::vector<double> m_y{ 3, 9, 0,-4};

		//Setup a permutation vector based on one of the vectors
		//Sorted in ascending order, since std::less is used here
        vector_sort v(m_x,std::less<double>());

        v.permute(m_x);
        v.permute(m_y);

		//After the v.permute the arrays become:
		//m_x={-2, 3, 5, 8}
		//m_y={ 3, 0,-4, 9}
		//Note that the x-y points are still coupled.

	Based on:
	https://stackoverflow.com/questions/17074324/how-can-i-sort-two-vectors-in-the-same-way-with-criteria-that-uses-only-one-of
	*/
	template<typename T,typename Compare>
	vector_sort(const std::vector<T>& vec,Compare compare)
	{
		/*
		IN:
			vec: Vector to sort based on a sorting rule set by compare

			compare: function object used in the comparison e.g.
			std::less<double> for ascending order sort
			std::greater<double> for descending order sort
			or a lambda can be used.

		Side effect:
			m_permutation_vector is set to a vector containing the permutations
			needed to transform vec into a sorted vector.
		*/
		m_permutation_vector.resize(vec.size());
		std::iota(m_permutation_vector.begin(), m_permutation_vector.end(), 0);
		std::sort(m_permutation_vector.begin(), m_permutation_vector.end(),
			[&](std::size_t i, std::size_t j){ return compare(vec[i], vec[j]); });
	}

	template<typename T>
	void permute(std::vector<T>& vec) const
	{
		/*
		Sort the input vector vec based on the permutation vector m_permutation_vector
		*/
		const size_t vec_size=vec.size();
		std::vector<bool> done(vec_size,false);
		for(std::size_t i = 0; i < vec_size; ++i)
		{
			//Perform the permutations in-place to avoid copies
			if(done[i])
			{
				continue;
			}
			done[i] = true;
			std::size_t prev_j = i;
			std::size_t j = m_permutation_vector[i];
			while(i != j)
			{
				std::swap(vec[prev_j], vec[j]);
				done[j] = true;
				prev_j = j;
				j = m_permutation_vector[j];
			}
		}
	}

	private:
	std::vector<std::size_t> m_permutation_vector;
};

} // namespace tk

#endif // H_VECTOR_SORT_H
