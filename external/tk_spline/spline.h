/* This file is based on the following code:
 *
 * spline.h
 *
 * simple cubic spline interpolation library without external
 * dependencies
 *
 * ---------------------------------------------------------------------
 * Copyright (C) 2011, 2014 Tino Kluge (ttk448 at gmail.com)
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * ---------------------------------------------------------------------
 *
 */

#ifndef H_TK_SPLINE_H
#define H_TK_SPLINE_H

#include <vector>
#include "boost/math/interpolators/quintic_hermite.hpp"
#include <memory>

namespace tk
{

/** \todo It appears that this project is still alive at
 *  https://github.com/ttk592/spline/  Check this and decide
 *  if we should move to a newer version. This code is also
 *  better placed in the support/ directory, like the other
 *  external code.
 */
class spline
{
public:
	//Set the interpolation points
	void set_points(const std::vector<double>& x, const std::vector<double>& y);
	void set_points(const std::vector<double>& x, const std::vector<double>& y,
		const std::vector<double>& dydx);
	void set_points(const std::vector<double>& x, const std::vector<double>& y,
		const std::vector<double>& dydx, const std::vector<double>& d2ydx2);

	//Interpolated value at x
	double operator() (double x) const;
	//Interpolated first derivative at x
	double deriv1(double x) const;
	//Interpolated second derivative at x
	double deriv2(double x) const;
private:
	//Coordinates of the left and right-most nodes
	std::pair<double, double> m_x;
	//Values at the left and right-most nodes
	std::pair<double, double> m_y;
	//Estimate of the derivative at the left and right-most nodes
	std::pair<double, double> m_dydx;
	//Estimate of the second derivative the left and right-most nodes
	std::pair<double, double> m_d2ydx2;
	//Spline object from boost
	std::shared_ptr<boost::math::interpolators::quintic_hermite<std::vector<double>>> m_spline;

	void construct_spline(std::vector<double> x, std::vector<double> y, std::vector<double> dydx,
		std::vector<double> d2ydx2);

	void compute_derivatives(const std::vector<double>& x, const std::vector<double>& y,
		std::vector<double>& dydx) const;
	double compute_derivative(const double x, const double x0, const double x1, const double x2,
		const double y0, const double y1, const double y2) const;
};

} // namespace tk

#endif // H_TK_SPLINE_H
