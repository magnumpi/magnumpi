#include "tk_spline/spline.h"
#include "plutil/test_utils.h"
#include <cmath>
#include <iostream>
#include <iostream>
#include <vector>
#include <iomanip>
#include <algorithm>

bool interpolate_exact()
{
	//Set up the interpolating spline
	tk::spline spline;
	auto f = [](double x)
	{
		return x * x + 1.0;
	};
	auto dfdx = [](double x)
	{
		return 2.0 * x;
	};
	auto d2fdx2 = [](double x)
	{
		return 2.0;
	};

	//The interpolator should work with non-sorted inputs
	std::vector<double> x_i{-2.9, 1.7, 3.8, 0.0, -1.7, -2.8};
	std::vector<double> y_i;
	std::for_each(x_i.begin(), x_i.end(), [&](double x)
	{
		y_i.push_back(f(x));
	});

	spline.set_points(x_i, y_i);

	//The interpolator should work with non-sorted outputs
	//The interpolator should be able to extrapolate.
	std::vector<double> x_test{-4.0, 3.5, 2.0, 0.0, 5.0};

	//For parabolas as input, the interpolation and extrapolation should be exact.
	for (auto x : x_test)
	{
		if (std::abs(spline(x) - f(x)) > 1e-12)
		{
			return false;
		}

		if (std::abs(spline.deriv1(x) - dfdx(x)) > 1e-12)
		{
			return false;
		}

		if (std::abs(spline.deriv2(x) - d2fdx2(x)) > 1e-12)
		{
			return false;
		}
	}

	//Everything went OK, return true
	return true;
}

int main()
{
	test_expr(interpolate_exact());

	// report test results and return the numer of errors.
	test_report;
	return test_nerrors();
}

