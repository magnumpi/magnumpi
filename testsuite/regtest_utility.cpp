/** \file
 *
 * Copyright (C) 2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/utility.h"
#include "plutil/test_utils.h"
#include <limits>

void test_make_samples()
{
	using namespace magnumpi;

	std::vector<double> res;
	const double tol = 3*std::numeric_limits<double>::epsilon();

	// [-1,2], 4 samples: samples at -1,0,1,2
	test_code( res = make_samples_lin(-1,2,4) );
	test_expr( res.size()==4 );
	test_expr( std::abs(res[0]+1)<tol );
	test_expr( std::abs(res[1]-0)<tol );
	test_expr( std::abs(res[2]-1)<tol );
	test_expr( std::abs(res[3]-2)<tol );

	// [1e-1,1e2], 1 sample per decade: samples at 1e-1,1,1e1,1e2
	test_code( res = make_samples_log(-1,2,1) );
	test_expr( res.size()==4 );
	test_expr( std::abs(res[0]-1e-1)<tol );
	test_expr( std::abs(res[1]-1   )<tol );
	test_expr( std::abs(res[2]-1e+1)<tol );
	test_expr( std::abs(res[3]-1e+2)<tol );
}

void test_make_range()
{
	using namespace magnumpi;

	std::vector<double> res;
	const double tol = 50*std::numeric_limits<double>::epsilon();

	// [-1,2], 4 samples: samples at -1,0,1,2
	test_code( res = make_range(-1,2,4,false) );
	test_expr( res.size()==4 );
	test_expr( std::abs(res[0]+1)<tol );
	test_expr( std::abs(res[1]-0)<tol );
	test_expr( std::abs(res[2]-1)<tol );
	test_expr( std::abs(res[3]-2)<tol );

	// [1e-1,1e2], 1 sample per decade: samples at 1e-1,1,1e1,1e2
	test_code( res = make_range(1e-1,1e2,4,true) );
	test_expr( res.size()==4 );
	test_expr( std::abs(res[0]-1e-1)<tol );
	test_expr( std::abs(res[1]-1   )<tol );
	test_expr( std::abs(res[2]-1e+1)<tol );
	test_expr( std::abs(res[3]-1e+2)<tol );

	// [-1e-1,-1e2], 1 sample per decade: samples at -1e-1,-1,-1e1,-1e2
	test_code( res = make_range(-1e-1,-1e2,4,true) );
	test_expr( res.size()==4 );
	test_expr( std::abs(res[0]-(-1e-1))<tol );
	test_expr( std::abs(res[1]-(-1   ))<tol );
	test_expr( std::abs(res[2]-(-1e+1))<tol );
	test_expr( std::abs(res[3]-(-1e+2))<tol );

	// size must be at least 2
	test_code_throws(make_range(1,2,0,false) );
	test_code_throws(make_range(1,2,1,false) );
	// first, last must be non-zero and have the same sign
	// for logarithmic ranges
	test_code_throws(make_range(-1,0,4,true) );
	test_code_throws(make_range(0,1,4,true) );
	test_code_throws(make_range(-1,1,4,true) );
}

int main()
{
	// the old interface:
	test_make_samples();
	// the new interface:
	test_make_range();

	// report test results and return the numer of errors.
        test_report;
        return test_nerrors();
}

