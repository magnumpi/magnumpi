#include "magnumpi/units.h"
#include "magnumpi/consts.h"
#include "plutil/test_utils.h"
#include <sstream>

void conversion_tests()
{
	using namespace magnumpi;

	// only 'false' and 'true' are recognized as boolean strings
	test_expr( is_bool("false")==true );
	test_expr( is_bool("true")==true );
	test_expr( is_bool("0")==false );
	test_expr( is_bool("1")==false );

	test_expr( is_integer("-1")==true );
	test_expr( is_integer("1")==true );
	test_expr( is_integer("0x2A")==true );
	test_expr( is_integer("0.")==false );
	test_expr( is_integer("1e0")==false );
	test_expr( is_integer("1e0")==false );
	test_expr( is_integer("true")==false );
	test_expr( is_integer("false")==false );

	test_expr( is_number("-1.")==true );
	test_expr( is_number("1.")==true );
	test_expr( is_number("0x2A")==true );
	test_expr( is_number("0.")==true );
	test_expr( is_number("1e1")==true );
	test_expr( is_number("1e-1")==true );
	test_expr( is_number("-1e1")==true );
	test_expr( is_number("-1e-1")==true );
	test_expr( is_number("42x")==false );
	test_expr( is_number("x42x")==false );

	test_expr( convert_to<int>("42")==42 );
	test_expr( convert_to<int>("0x2A")==42 );
	test_code_throws( convert_to<int>("42."); );
	test_code_throws( convert_to<int>("42 "); );
	test_code_throws( convert_to<int>("42x"); );
	test_code_throws( convert_to<int>("x42x"); );

	test_expr( convert_to<unsigned long>("42")==42 );
	test_expr( convert_to<unsigned long>("0x2A")==42 );
	test_code_throws( convert_to<unsigned long>("42."); );
	test_code_throws( convert_to<unsigned long>("42 "); );
	test_code_throws( convert_to<unsigned long>("42x"); );
	test_code_throws( convert_to<unsigned long>("x42x"); );

	test_expr( convert_to<long>("42")==42 );
	test_expr( convert_to<long>("0x2A")==42 );
	test_code_throws( convert_to<long>("42."); );
	test_code_throws( convert_to<long>("42 "); );
	test_code_throws( convert_to<long>("42x"); );
	test_code_throws( convert_to<long>("x42x"); );
	// the following test relies on long being larger than int,
	// which is not guaranteed by the standard (and may not be
	// the case in particular on 32-bit systems).
	if (sizeof(long)!=sizeof(int))
	{
		std::stringstream lmax; lmax << std::numeric_limits<long>::max();
		test_code_throws( convert_to<int>(lmax.str()); );
		std::stringstream llow; llow << std::numeric_limits<long>::lowest();
		test_code_throws( convert_to<int>(llow.str()); );
	}

	test_expr( convert_to<double>("42")==42 );
	test_expr( convert_to<double>("0x2A")==42 );
	test_code_throws( convert_to<double>("42.0."); );
	test_code_throws( convert_to<double>("42 "); );
	test_code_throws( convert_to<double>("42x"); );
	test_code_throws( convert_to<double>("x42x"); );

	test_expr( convert_to<bool>("true")==true );
	test_expr( convert_to<bool>("false")==false );
	test_code_throws( convert_to<bool>("0"); );
	test_code_throws( convert_to<bool>("1"); );
	test_code_throws( convert_to<bool>("x"); );
}

void unit_tests()
{
	namespace constants = magnumpi::constants;
	using namespace magnumpi::units;

	test_expr( unit_in_si("kg",mass)==1.0 );
	test_expr( unit_in_si("amu",mass)==constants::AMU );
	test_expr( quantity_in_si("1*kg",mass)==1.0 );
	test_expr( quantity_in_si("1*amu",mass)==constants::AMU );
	test_expr( quantity_in_si("3*amu",mass)==3*constants::AMU );
	test_expr( in_units(constants::AMU,"amu",mass)=="1*amu" );

	test_expr( unit_in_si("m",length)==1.0 );
	test_expr( unit_in_si("a0",length)==constants::BohrRadius );
	test_expr( quantity_in_si("1*m",length)==1.0 );
	test_expr( quantity_in_si("1*a0",length)==constants::BohrRadius );
	test_expr( quantity_in_si("3*a0",length)==3*constants::BohrRadius );
	test_expr( in_units(constants::BohrRadius,"a0",length)=="1*a0" );

	// qualify, since time is also a C function in the global namespace
	test_expr( unit_in_si("s",magnumpi::units::time)==1.0 );

	test_expr( unit_in_si("K",temperature)==1.0 );
	test_expr( unit_in_si("eVT",temperature)==constants::eVT );
	test_expr( quantity_in_si("1*K",temperature)==1.0 );
	test_expr( quantity_in_si("1*eVT",temperature)==constants::eVT );
	test_expr( quantity_in_si("3*eVT",temperature)==3*constants::eVT );
	test_expr( in_units(constants::eVT,"eVT",temperature)=="1*eVT" );

	test_expr( unit_in_si("J",energy)==1.0 );
	test_expr( unit_in_si("eV",energy)==constants::eV );
	test_expr( quantity_in_si("1*J",energy)==1.0 );
	test_expr( quantity_in_si("1*eV",energy)==constants::eV );
	test_expr( quantity_in_si("3*eV",energy)==3*constants::eV );
	test_expr( in_units(constants::eV,"eV",energy)=="1*eV" );

	test_expr( unit_in_si("rad",angle)==1.0 );
	test_expr( unit_in_si("deg",angle)==constants::deg );
	test_expr( quantity_in_si("1*rad",angle)==1.0 );
	test_expr( quantity_in_si("1*deg",angle)==constants::deg );
	test_expr( quantity_in_si("3*deg",angle)==3*constants::deg );
	test_expr( in_units(constants::deg,"deg",angle)=="1*deg" );

	test_expr( unit_in_si("m^2",area)==1.0 );
	test_expr( unit_in_si("m^3/s",m3_s)==1.0 );
	test_expr( quantity_in_si("1*m^2",area)==1.0 );
	test_expr( quantity_in_si("1*m^3/s",m3_s)==1.0 );

	test_expr( unit_in_si("1/m",reciprocal_length)==1.0 );
	test_expr( unit_in_si("1/a0",reciprocal_length)==1/constants::BohrRadius );
	test_expr( unit_in_si("1/Bohr",reciprocal_length)==1/constants::BohrRadius );

	test_code_throws( quantity_in_si("",length) );
	test_code_throws( quantity_in_si("1",length) );
	test_code_throws( quantity_in_si("1*kg",length) );
	test_code_throws( quantity_in_si("*m",length) );
	test_code_throws( quantity_in_si("1*",length) );
	test_code_throws( quantity_in_si("1*2*m",length) );
	test_code_throws( quantity_in_si("1.2.3*m",length) );
	test_code_throws( quantity_in_si("1*m*1",length) );
}

int main()
{
	conversion_tests();
	unit_tests();

	// report test results and return the numer of errors.
        test_report;
        return test_nerrors();
}
