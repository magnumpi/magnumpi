#include "magnumpi/json.h"
#include "plutil/test_utils.h"

const char* test_data =
R"({
 "subsection1": {
  "subsection1a": {
   "test_key_1a": "test_value_1a"
  },
  "test_key": "test_value"
 },
 "test_key_1": "test_value_1",
 "test_key_2": "\ttest value 2  "
})";

void input_parser_tests()
{
	using namespace magnumpi;

	test_code_throws(
		const std::string str("#no hash-comments in JSON files.");
		json_type cnf(json_type::parse(str));
	);
	test_code_throws(
		const std::string str("{ \"value_without_key\" }");
		json_type cnf(json_type::parse(str));
	);
	test_code_throws(
		const std::string str("{ \"key_without_value\": }");
		json_type cnf(json_type::parse(str));
	);
	test_code_throws(
		const std::string str("=");
		json_type cnf(json_type::parse(str));
	);
}

void access_tests()
{
	using namespace magnumpi;

	// check that an exception is thrown when the ipnut file does not exist
	test_code_throws( read_json_from_file("non_existing_file.txt") );

	// construct json objects with test input data.
	json_type tmp, cnf, fragment;
	
	try {
		tmp = json_type::parse(test_data);
		write_json_to_file(tmp,"regtest_json.json");
		cnf = read_json_from_file("regtest_json.json");
		fragment = read_json_from_file("regtest_json.json#.subsection1.subsection1a.test_key_1a");
	}
	catch(std::exception& exc)
	{
		std::cerr << exc.what() << std::endl;
		test_nerrors() += 100;
		return;
	}
	// after writing and reading the object should be the same
	test_expr( tmp==cnf );
	// test that reading only the fragment worked
	test_expr( fragment=="test_value_1a" );

	// key-value tests (also for whitespace handling)
	test_expr( cnf.at("test_key_1")=="test_value_1" );
	// whitespace handling. Leading and trailing whitespace in values is kept as-is
	test_expr( cnf.at("test_key_2")=="\ttest value 2  " );

	// section access
	test_code_throws( cnf.at("non_existing_subsection") );
	// subsection access
	test_code( cnf.at("subsection1").at("subsection1a") );
	test_expr( cnf.at("subsection1").at("subsection1a").at("test_key_1a")=="test_value_1a" );

	// integer parsing tests
	test_expr( json_type::parse("2")==2 );
	// NOTE: get<int> etc. allow double -> int conversions, just like C++ does.
	// so even while:
	test_expr( json_type::parse("3.5").is_number_integer()==false );
	// ... the following will pass:
	test_expr( json_type::parse("3.5").get<int64_t>()==3 );

	// double parsing tests
	test_expr( json_type::parse("3.0")==3.0 );
	test_expr( json_type::parse("3.0e0")==3.0 );
	// bad double, will give a parse error
	test_expr_throws( json_type::parse("3.0.0")==3.0 );

	// comparison
	test_expr( cnf.at("subsection1").at("subsection1a")==json_type::parse("{ \"test_key_1a\": \"test_value_1a\" }") );
}

void path_tests()
{
	using namespace magnumpi;
	json_path path;
	test_expr( path.size()==0 );
	test_code( path = json_path("."); );
	test_expr( path.size()==0 );
	test_code( path = json_path(".section"); );
	test_expr( path.size()==1 );
	test_code( path = json_path(".section.subsection"); );
	test_expr( path.size()==2 );
	test_code_throws( path = json_path("section" ); );
	test_code_throws( path = json_path(".." ); );
	test_code_throws( path = json_path(".section." ); );
	test_code_throws( path = json_path("..subsection." ); );

	json_type cnf;
	try {
		const std::string fname = "$(MAGNUMPI_INPUTDATA_DIR)/testsuite/regtest_json.json";
		cnf = read_json_from_file(fname);
	}
	catch(std::exception& exc)
	{
		std::cerr << exc.what() << std::endl;
		test_nerrors() += 100;
		return;
	}
	test_code( path = json_path(".subsection1.subsection1a.test_key_1a"); );
	test_expr( path.get_element(cnf)=="test_value_1a" );
	test_code( path.get_element(cnf)="new_test_value_1a" );
	test_expr( path.get_element(cnf)=="new_test_value_1a" );
}

int main()
{
	input_parser_tests();
	access_tests();
	path_tests();

	// report test results and return the numer of errors.
        test_report;
        return test_nerrors();
}
