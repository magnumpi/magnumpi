/** \file
 *
 * Copyright (C) 2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * This file test Clenshaw-Curtis integration over the interval (-1,1)
 * for various test functions, and check the results against analytical
 * expressions.
 *
 * \author Jan van Dijk
 * \date   April 2021
 */

#include <cmath>
#include <iostream>
#include <vector>
#include <functional>
#include <cassert>
#include "plmath/clencurt.h"

#include "plutil/test_utils.h"

/* This wraps a function object that provides operator()(double y, unsigned p)
 * in an object that is used for testing. The list of test parameter values
 * p is passed to the constructor and stored. The number of parameters and the
 * values can be retrieved through embers param(i) amd size().
 *
 * Objects of this type can be used to test the array interface of the
 * clencurt::integrator class.
 */
template <class F>
class int_func_array
{
  public:
	typedef double result_type;
	typedef double argument_type;
	int_func_array(const std::initializer_list<unsigned>& s)
	 : m_s(s)
	{
	}
	unsigned size() const { return m_s.size(); }
	unsigned param(unsigned i) const { return m_s[i]; }
	void operator () (argument_type y, std::vector<double>& h) const
	{
		assert(h.size()==size());
		for(unsigned ndx=0;ndx<h.size();++ndx)
		{
			unsigned s = m_s[ndx];
			h[ndx]=m_f(y,s);
		}
	}
  private:
	const std::vector<unsigned> m_s;
	F m_f;
};

/* This wraps a function object that provides operator()(double y, unsigned p)
 * in an object that is used for testing. The Fixed test parameter value is
 * passed to the constructor.
 *
 * Objects of this type can be used to test the scalar interface of the
 * clencurt::integrator class.
 */
template <class F>
class int_func_array_element : private F
{
  public:
	typedef double result_type;
	typedef double argument_type;
	int_func_array_element(int s)
	 : m_s(s)
	{
	}
	//return function integrand
	result_type operator() (argument_type y) const
	{
		return m_f(y,m_s);
	}
  private:
	const int m_s;
	F m_f;
};

/* Call test_expr to test near-equality of arguments numerical and analytical,
 * given a prescribed tolerance.
 */
template <typename V, typename T>
void test_integration_result(const V& numerical, const V& analytical, T tolerance)
{
	const T mean_abs = (std::abs(numerical)+std::abs(analytical))/2;
	const T ref = mean_abs < tolerance ? 1 : mean_abs;
#if 1
	std::cout << "numerical = " << numerical
		<< ", analytical = " << analytical
		<< ", error = " << (numerical-analytical)
		<< ", mean_abs = " << mean_abs
		<< ", ref = " << ref
		<< ", error/ref = " << std::abs((numerical-analytical)/ref) << std::endl;
#endif
	test_expr(std::abs((numerical-analytical)/ref)<tolerance);
}

/* Test that an integrand that is represented by function objects of type F
 * yields value 'analytical', when integrated with the integrator cc. (Note
 * that cc defines the integration interval). Use the prescribed tolerance
 * when comparing the result.
 */
template <typename F>
void test_integration_single(
	const plasimo::clencurt::integrator& cc,
	plasimo::clencurt::integrator::scalar_type tolerance,
	const typename F::result_type& analytical)
{
	const F int_func;
	const typename F::result_type numerical  = cc.integrate(int_func,tolerance);
	test_integration_result(numerical,analytical,tolerance);
}

/* Test that the parameterized integrand that is represented by function objects
 * of type F yields the values that are produces by function object 'analytical'
 * for all parameter values that are defined by params argument. (Note that
 * that cc defines the integration interval). This tests both the array interface
 * and the scalar interface of clencurt::integrator.
 */
template <typename F>
void test_integration_array(
	const plasimo::clencurt::integrator& cc,
	plasimo::clencurt::integrator::scalar_type tolerance,
	const std::initializer_list<unsigned> params,
	std::function<double(unsigned)> an)
{
	int_func_array<F> int_func(params);
	// 1. Test the array interface
	unsigned dim=int_func.size();
	std::vector<double> result(dim);
	cc.integrate_md(int_func,dim,tolerance,result);
	for (unsigned i=0; i!=int_func.size();++i)
	{
		const typename F::result_type numerical  = result[i];
		const typename F::result_type analytical = an(int_func.param(i));
		test_integration_result(numerical,analytical,tolerance);
	}
	// 2. For all parameter values, test the scalar interface
	for (unsigned i=0; i!=int_func.size();++i)
	{
		int_func_array_element<F> el_func(int_func.param(i));
		const typename F::result_type numerical= cc.integrate(el_func,tolerance);
		const typename F::result_type analytical = an(int_func.param(i));
		test_integration_result(numerical,analytical,tolerance);
	}
}

/* TEST 1: f(x) = (s+1)*x^s => F(x) = x^(s+1) +C
 *         F(1)-F(1) = 0 (odd s) or 1 (even s)
 */
class test_func_powx
{
  public:
	typedef double result_type;
	typedef double argument_type;

	result_type operator()(argument_type y, unsigned s) const
	{
		return (s+1)*std::pow(y,s);
	}
};

void test_powx()
{
	enum { lower_cc=4, upper_cc=14 };
	const double tolerance=1e-6;
	const plasimo::clencurt::integrator cc(-1.0, 1.0, lower_cc,upper_cc);
	test_integration_array<test_func_powx>(
		cc,
		tolerance,
		{0,1,2,3,4,5,6,7,8,9},
		[](unsigned s) -> double { return (s+1)%2 ? 2:0;}
	);
}

/* TEST 2: f(x) = log(1+x) => F(x) = (1+x)log(1+x) -x + C
 *         F(1)-F(-1) = 2*log(2)-2. (Note:(0_+)*log(0_+)=0.)
 */
class test_func_log
{
  public:
	typedef double result_type;
	typedef double argument_type;

	result_type operator()(argument_type y) const
	{
		return std::log(1+y);
	}
};

void test_log()
{
	enum { lower_cc=4, upper_cc=14 };
	const double tolerance=1e-6;
	const plasimo::clencurt::integrator cc(-1.0, 1.0, lower_cc,upper_cc);
	test_integration_single<test_func_log>(
		cc,
		tolerance,
		( 2*std::log(2)-1 ) - (0.0+1)
	);
}

/* TEST 3: f(x) = x^(s+1)*exp(-x) => F(infty)-F(0) = Gamma(s+2) = (s+1)!.
 * This is handled by splitting the integration interval in
 * (0,s+1) and (s+1,infty) and a suitable coordinate transform,
 * see \cite OHara1970 section 2.3. In this testcase we assume Q(E)=1.
 */
class test_func_gamma
{
  public:
	typedef double result_type;
	typedef double argument_type;

	result_type operator()(argument_type y, unsigned s) const
	{
		return f1(y,s)+f2(y,s);
	}
  private:
	result_type f(double x, unsigned s) const
	{
		return std::pow(x,s+1)*std::exp(-x);
	}
	result_type f1(argument_type y,unsigned s) const
	{
		const double x1 = (s+1)/2.0*(1.0+y);
		return (s+1.0)/2.0*f(x1,s);
	}
	result_type f2(argument_type y,unsigned s) const
	{
		if (y<=0)
		{
			return 0.0;
		}
		const double x2 = (s+1)/y;
		// note: x2*x2*f(x2,s) = f(x2,s+2)
		return 1/(s+1.0)*f(x2,s+2);
	}
};

void test_gamma()
{
	enum { lower_cc=4, upper_cc=14 };
	const double tolerance=1e-6;
	const plasimo::clencurt::integrator cc(-1.0, 1.0, lower_cc,upper_cc);
	// f(x)=x^(s+1)exp(-x) over [0,infinity)
	test_integration_array<test_func_gamma>(
		cc,
		tolerance,
		{2,5,6},
		[](unsigned s) -> double { return std::tgamma(s+2);}
	);
}

int main()
{
	test_powx();
	test_log();
	test_gamma();

	// report test results and return the numer of errors.
        test_report;
        return test_nerrors();
}
