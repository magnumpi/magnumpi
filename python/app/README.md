# Scripts that use the Python Bindings for MagnumPI

This directory contains scripts that do calculations using
the python bindings for the MagnumPI code.

NOTES

 * Source set-magnumpi-local (which reside in the top-level build directory),
   that will set relevant environment variables. It will also append the
   python/ subdirectory of the build directory to the PYTHONPATH environment
   variable.

 * The Python3 script `get_scat.py` requires one input file name,
   for example `input/scat_rscp_colonna.json`. It calculates
   the scattering angle chi(b,E) for user-specified impact parameter
   (b) and energy (E) ranges. It calls the C++ function
   `magnumpi::get_scattering_angle`. The resulting JSON object
   is written to the standard output stream (by Pyton code).

 * The Python3 script `get_pot.py` requires one input file name,
   for example `input/HH.json`. It prints a table of values of the
   potential V(r) and its first and second derivatives for various
   values of the inter-molecular separation r.

 * The Python3 script `get_cs.py` requires one input file name,
   for example `input/cs.in`. It prints the requested cross sections
   `Q_l(eps)` for user-selected values `l` in `[0,l_max)` as a function
   of a set of user-specified energy values.

As an example, from the build directory the `get_scat.py` script can be
invoked as (everything on one line):

```
    python3 python/app/get_scat.py ../input/scat_rscp_colonna.json
```

