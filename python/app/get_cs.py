import sys
import json
from math import log10

from magnumpi.calc_cs import calc_cs

if len(sys.argv) != 2:
    print('usage: python3 get_cs <input file>')
    exit(1)

with open(sys.argv[1]) as f:
    request = json.load(f)

cs_data = calc_cs(request)
print(json.dumps(cs_data, indent=2))
