from magnumpi.calc_cs import calc_cs
from magnumpi.calc_omega import calc_omega
from magnumpi.dsutils import ds2json


def calc_cs_omega(request):

    cs_data = calc_cs(request)
    omega_data = calc_omega(request, cs_data)
    return {'cs': cs_data, 'omega': ds2json(omega_data)}
