# Python Bindings for MagnumPI

This directory contains python bindings for some of the interfaces
of MagnumPI and some associated native Python code.

NOTES

 * the Python bindings in backend.cpp are compiled into a
   shared library, which dynamically links against the
   MagnumPI library. The bindings use the pybind11 library.

 * For autotools build, the shared library that contains the bindings is
   produced in the `.libs/` subdirectory of the ideas/jvdijk/python build
   directory.
