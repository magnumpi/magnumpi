import magnumpi.backend as backend
from magnumpi.dsutils import ds2json


def calc_cs(request):

    cs_data = backend.calc_cross_sec(request)
    return ds2json(cs_data)
