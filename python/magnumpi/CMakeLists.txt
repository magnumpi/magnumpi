include_directories(
	${Python3_INCLUDE_DIRS}
	${CMAKE_SOURCE_DIR}/external
	${CMAKE_SOURCE_DIR}/external/plasimo)
add_library(backend MODULE
	${CMAKE_SOURCE_DIR}/pymagnumpi/src/magnumpi/libmagnumpi_py.cpp
)
target_compile_options(backend PRIVATE -DMAGNUMPI_MODULE_NAME=backend )
#suppress the lib prefix (on Unix)
SET_TARGET_PROPERTIES(backend PROPERTIES PREFIX "")
target_link_libraries(backend
	magnumpi
	pthread
	${pybind11_LIBRARIES}
)

configure_file(calc_cs.py calc_cs.py COPYONLY)
configure_file(calc_cs_omega.py calc_cs_omega.py COPYONLY)
configure_file(calc_omega.py calc_omega.py COPYONLY)
configure_file(dsutils.py dsutils.py COPYONLY)
configure_file(potential_builder.py potential_builder.py COPYONLY)
