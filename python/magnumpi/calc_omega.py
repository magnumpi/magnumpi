import json
from math import log10

import magnumpi.backend as backend
from magnumpi.potential_builder import build_potential


def calc_omega(request, cs_data):

    reduced_mass = backend.in_si(request['interaction']['reduced_mass'],
                                 backend.Dimension.MASS)
    tolerance = request['tolerance']
    l_max = request['l_max']
    ci = request['ci']
    s_max = ci['s_max']
    ci_lower_cc = ci['cc']['lower']
    ci_upper_cc = ci['cc']['upper']
    tr = ci['temperature_range']
    tmin = backend.in_si(tr['min'], backend.Dimension.TEMPERATURE)
    tmax = backend.in_si(tr['max'], backend.Dimension.TEMPERATURE)
    tdel = backend.in_si(tr['step'], backend.Dimension.TEMPERATURE)

    q = backend.CrossSectionFromDatasetLog(cs_data)
    ls_pairs = backend.construct_ls_pairs(l_max, s_max)
    oc = backend.OmegaCalculator(ls_pairs,
                                 ci_lower_cc, ci_upper_cc,
                                 tolerance)
    omega_data = oc.integrate(q, reduced_mass, tmin, tmax, tdel)

    return omega_data
