dnl ***************************************************************************
dnl
dnl     Part 1. Initialize and configure autoconf, automake and libtool.
dnl
dnl ***************************************************************************

AC_PREREQ(1.0)

dnl Arguments: package name, package version, email for bugs, tar name
AC_INIT([magnumpi],
        [m4_esyscmd_s([cat VERSION])],
        [bugs@plasimo.phys.tue.nl],
        magnumpi)

dnl This is an autoconf safety check, The argument must be a file
dnl that is actually present in (this) source directory. See the
dnl documentation of AC_CONFIG_SRCDIR in the autoconf documentation
dnl for more details.
AC_CONFIG_SRCDIR([Makefile.am])

dnl put as much autotools-related stuff as possible in the autodir
dnl subdirectory, to keep the toplevel source directory as clean
dnl as possible. Also see autodir/README
AC_CONFIG_AUX_DIR([autodir])
AC_CONFIG_MACRO_DIR([autodir])

dnl autoheader and autoconf usually create a file config.h that
dnl contains defines that describe the result of some of the tests
dnl that it performs (HAVE_XXX macros).
dnl To allow for the presence and interpretation of multiple such files
dnl in a single compilation unit (a .c or .cpp file), we rename the
dnl file in magnumpi-config.h and prefix all defines with PI_.
dnl This means that you have to test for PI_HAVE_CXXABI_H, for
dnl example, instead of for HAVE_CXXABI_H, in a file that includes
dnl magnum-config.h. (For completeness sake: note that you should
dnl include piconfig.h in your code: that will include magnum-config.h
dnl and provide some additional stuff.) See piconfig.h for details.
AC_CONFIG_HEADERS(config.h)
AX_PREFIX_CONFIG_H([magnumpi-config.h], [MAGNUMPI])

AM_INIT_AUTOMAKE([1.6.3 foreign subdir-objects])

dnl Initialize libtool. The options ensure that by default we build
dnl only shared libraries, and that Windows dll files can be created
dnl in case we are configuring a build for that platform. That also
dnl requires that -no-undefined is pass when linking such library.
dnl The win32-dll option has no effect when configuring on another
dnl platform such as Linux of OSX.
LT_INIT([disable-static win32-dll])
AC_SUBST(LIBTOOL_DEPS)
dnl MAGNUMPI supports only shared-library builds. This is made the default
dnl by the LT_INIT options, see above. Generate an error if the user
dnl passed --disable-dshared or --enable-static to configure.
if test x$enable_shared = xno || test x$enable_static = xyes ; then
  AC_MSG_ERROR([Only shared library versions of the code are supported.])
fi

dnl ***************************************************************************
dnl
dnl     Check the host type, see if we are cross-compiling
dnl
dnl When (cross-)compiling for Windows, set PL_WIN32=yes, when compiling for
dnl OSX, set PL_OSX=yes. The values PL_WIN32 and PL_OSX are used later in this
dnl configure script.
dnl
dnl ***************************************************************************


AC_CANONICAL_HOST

PL_WIN32=no;
PL_OSX=no;
dnl Are we doing windows or apple?
case $host in
  *mingw32*) PL_WIN32=yes ;;
  *apple*)   PL_OSX=yes   ;;
  *)                      ;;
esac

AM_CONDITIONAL(WIN32_HOST, test "$PL_WIN32" = "yes")
AM_CONDITIONAL(OSX_HOST, test "$PL_OSX" = "yes")

dnl Checks for programs.
AC_PROG_CXX
AC_PROG_CC
AC_PROG_CPP
AC_PROG_F77
AC_PROG_FC

dnl Do some windows sanity checking...

if test "$PL_WIN32" = "yes" ; then
  dnl Use -mthreads for compiler and linker to make exception handling thread-safe
  MAGNUMPI_GCC_CFLAGS("-mthreads $CFLAGS")
  MAGNUMPI_GCC_CXXFLAGS("-mthreads $CXXFLAGS")
  LDFLAGS="-mthreads $LDFLAGS"
  dnl Enable automatic DLL import/export
  LDFLAGS="-Wl,--enable-runtime-pseudo-reloc $LDFLAGS"
fi

dnl ***************************************************************************
dnl
dnl     Python support.
dnl
dnl ***************************************************************************

AC_LANG_PUSH(C++)

PYTHON_CONFIG=
PYTHON_INCLUDE=
PYTHON_LDFLAGS=
PYTHON_LIBS=
PYTHON_SUBDIR=
AC_ARG_WITH(python,
	[  --with-python
			  create Python3 bindings ],
      	with_python="${withval}", with_python="no")
if test "x$with_python" = "xyes" ; then
  AM_PATH_PYTHON([3.6])
  PYTHON_CONFIG="$PYTHON-config"
  PYTHON_INCLUDE="`$PYTHON_CONFIG --cflags`"
  PYTHON_LDFLAGS="`$PYTHON_CONFIG --ldflags`"
  PYTHON_LIBS="`$PYTHON_CONFIG --libs`"
  PYTHON_SUBDIR=python
  AC_SUBST(PYTHON_CONFIG)
  AC_SUBST(PYTHON_INCLUDE)
  AC_SUBST(PYTHON_LDFLAGS)
  AC_SUBST(PYTHON_LIBS)
  AC_SUBST(PYTHON_SUBDIR)
  TMP_CPPFLAGS="$CPPFLAGS"
  CPPFLAGS="$CPPFLAGS $PYTHON_INCLUDE"
  AC_CHECK_HEADER(pybind11/pybind11.h, ,AC_MSG_ERROR([The pybind11.h header was not found. Please install this or configure --without-python]))
  CPPFLAGS="$TMP_CPPFLAGS"
fi

AC_LANG_POP(C++)

dnl ***************************************************************************
dnl
dnl     Java support
dnl
dnl ***************************************************************************

dnl AM_PROG_GCJ will set GCJ to gcj
AM_PROG_GCJ

AC_ARG_WITH(java,
  	[  --with-java		  compile Java support],
	WITH_JAVA="${withval}", WITH_JAVA="no")

AC_MSG_CHECKING(--with-java)
if test "$WITH_JAVA" = "no"; then
  GCJ=""
  AC_MSG_RESULT(no)
  JAVA_SUBDIR=""
else
  JAVA_SUBDIR="java"
  AC_MSG_RESULT(javac)
  if test "x$WITH_JAVA" = "xjavac"; then
	GCJ="javac"
	JAVA_JNI_INCLUDE_BASE=`which javac | xargs readlink -m  | xargs dirname | sed -e 's/bin/include/'`
	JAVA_JNI_INCLUDEDIR="-I$JAVA_JNI_INCLUDE_BASE -I$JAVA_JNI_INCLUDE_BASE/linux"
  fi
  if test "x$WITH_JAVA" = "xgcj"; then
	GCJ="gcj"
	JAVA_JNI_INCLUDEDIR=""
  fi
fi
AC_SUBST(JAVA_SUBDIR)

dnl Compile Java components?
AM_CONDITIONAL(USE_JAVA, test "x$JAVA_SUBDIR" = "xjava")

dnl The Requested java (gcj or javac)
AM_CONDITIONAL(HAVE_JAVAC, test "$GCJ" = "javac")

if test "x$GCJ" != "x" ; then
	echo "JNI preprocessor flags: $JAVA_JNI_INCLUDEDIR"
	CPPFLAGS="$CPPFLAGS $JAVA_JNI_INCLUDEDIR"
	AC_CHECK_HEADER(jni.h,,AC_MSG_ERROR([The jni.h header was not found. Install the Java SDK and re-configure or configure --without-java]))
fi

MAGNUMPI_GCC_CXXFLAGS("-Wall $CXXFLAGS")
MAGNUMPI_GCC_CXXFLAGS("-Werror=overloaded-virtual $CXXFLAGS")
MAGNUMPI_GCC_CXXFLAGS("-Werror=non-virtual-dtor $CXXFLAGS")
MAGNUMPI_GCC_CXXFLAGS("-Werror=unused-variable $CXXFLAGS")
MAGNUMPI_GCC_CXXFLAGS("-Werror=ignored-qualifiers $CXXFLAGS")
dnl disabled, beast_post.cpp violates this at present:
dnl MAGNUMPI_GCC_CXXFLAGS("-Werror=zero-as-null-pointer-constant $CXXFLAGS")
MAGNUMPI_GCC_CXXFLAGS("-Werror=misleading-indentation $CXXFLAGS")
MAGNUMPI_GCC_CXXFLAGS("-Werror=reorder $CXXFLAGS")
dnl disabled, notsupported by gcc 7.5.0:
dnl MAGNUMPI_GCC_CXXFLAGS("-Werror=cast-function-type $CXXFLAGS")
MAGNUMPI_GCC_CXXFLAGS("-Werror=type-limits $CXXFLAGS")
MAGNUMPI_GCC_CXXFLAGS("-Werror=missing-field-initializers $CXXFLAGS")

dnl ***************************************************************************
dnl
dnl     Optimization
dnl
dnl ***************************************************************************

AC_ARG_ENABLE(optimize,
  	[  --enable-optimize	  compile optimized -O2 code],
  	USE_OPTIMIZE="${enableval}", USE_OPTIMIZE="yes" )

AC_MSG_CHECKING(--enable-optimize)
if test "$USE_OPTIMIZE" = "yes"; then
  MAGNUMPI_GCC_CFLAGS("-O2 $CFLAGS")
  MAGNUMPI_GCC_CXXFLAGS("-O2 -funroll-loops $CXXFLAGS")
  MAGNUMPI_GCC_FFLAGS("-O2 $FFLAGS")
  MAGNUMPI_GCC_FCFLAGS("-O2 $FCFLAGS")
  AC_MSG_RESULT(yes)
  ENABLE_ASSERT="no"
else
  AC_MSG_RESULT(no)
  ENABLE_ASSERT="yes"
dnl autoconf makes -O2 a default. DO NOT REMOVE THIS!!!
  MAGNUMPI_REMOVE_CFLAG(-O2)
  MAGNUMPI_REMOVE_CXXFLAG(-O2)
  MAGNUMPI_REMOVE_FFLAG(-O2)
  MAGNUMPI_REMOVE_FCFLAG(-O2)
fi

dnl ***************************************************************************
dnl
dnl     Debug symbol generation
dnl
dnl ***************************************************************************

AC_ARG_ENABLE(debug,
  	[  --enable-debug	  compile with -g flag],
  	USE_DEBUG="${enableval}", USE_DEBUG="no" )

AC_MSG_CHECKING(--enable-debug)
if test "$USE_DEBUG" = "yes"; then
   MAGNUMPI_GCC_CXXFLAGS("-g $CXXFLAGS")
   MAGNUMPI_GCC_CFLAGS("-g $CFLAGS")
   AC_MSG_RESULT(yes)
else
   AC_MSG_RESULT(no)
   MAGNUMPI_REMOVE_CXXFLAG(-g)
   MAGNUMPI_REMOVE_CFLAG(-g)
fi

dnl ***************************************************************************
dnl
dnl     Compile-time asserts
dnl
dnl ***************************************************************************

AC_ARG_ENABLE(assert,
  	[  --enable-assert	  enable assert when optimizing],
  	ENABLE_ASSERT="${enableval}")

AC_MSG_CHECKING(--enable-assert)
if test "$ENABLE_ASSERT" = "yes"; then
  AC_MSG_RESULT(yes)
else
  AC_MSG_RESULT(no)
  CPPFLAGS="$CPPFLAGS -DNDEBUG"
fi

dnl ***************************************************************************
dnl
dnl     Architecture-specific optimizations
dnl
dnl ***************************************************************************

AC_ARG_ENABLE(opt-arch,
  	[  --enable-opt-arch	  CPU specific optimizations ],
  	USE_OPT_ARCH="${enableval}", USE_OPT_ARCH="no" )
AC_MSG_CHECKING(--enable-opt-arch)
if test "$USE_OPT_ARCH" = "yes"; then
  MAGNUMPI_GCC_CFLAGS("$CFLAGS -march=native -mtune=native")
  MAGNUMPI_GCC_CXXFLAGS("$CXXFLAGS -march=native -mtune=native")
  MAGNUMPI_GCC_FFLAGS("$FFLAGS -march=native -mtune=native")
  MAGNUMPI_GCC_FCFLAGS("$FCFLAGS -march=native -mtune=native")
  AC_MSG_RESULT([Compiling C, C++ and Fortran code with -march=native -mtune=native])
else
  AC_MSG_RESULT(no)
fi

dnl ***************************************************************************
dnl
dnl     C++ standard version
dnl
dnl Check that the compiler, with the provided or calculated compiler options,
dnl is running in C++2017 (or later) mode.
dnl
dnl ***************************************************************************

AC_LANG_PUSH(C++)

AC_TRY_COMPILE(
  [ ],
  [ static_assert(__cplusplus>=201703L, "No C++17 support."); ],
  [ AC_MSG_NOTICE([C++17 detected. No futher action needed.]) ],
  [
    dnl The C++17 test failed. Add the -stc++17 flag and try again.
    AC_MSG_WARN([No C++17 support. I will try again with compiler flag -std=c++17.])
    CXXFLAGS="$CXXFLAGS -std=c++17"
    AC_TRY_COMPILE(
      [ ],
      [ static_assert(__cplusplus>=201703L, "No C++17 support."); ],
      [ ],
      [
        AC_MSG_ERROR([C++17 or later is required. Reconfigure with appropriate CXXFLAGS, or update your compiler.]) ]
    )
  ]
)

AC_LANG_POP(C++)

dnl ***************************************************************************
dnl
dnl     Compiler feature tests
dnl
dnl ***************************************************************************

dnl Check for the __builtin_expect keyword. The result is needed
dnl to define the pl_likely and pl_unlikely macros in plconfig.h

AC_LANG_PUSH(C++)

AC_MSG_CHECKING(if the C++ compiler has __builtin_expect)
  AC_TRY_COMPILE([
	int fn (int a, int b )
	{
	 	if (__builtin_expect(a==b,0))
		   return 322;
		else
		   return a+b;
	}],
	[ fn(1,2); ],
	[ pl_cv_cc_builtin_expect="yes" ],
	[ pl_cv_cc_builtin_expect="no" ]
  )

  if test x$pl_cv_cc_builtin_expect=xyes; then
	AC_DEFINE(HAVE_BUILTIN_EXPECT,1,[__builtin_expect function])
  fi
AC_MSG_RESULT([$pl_cv_cc_builtin_expect])

AC_LANG_POP(C++)

dnl ***************************************************************************
dnl
dnl     libc feature tests
dnl
dnl ***************************************************************************

AC_HEADER_STDC
AC_CHECK_HEADERS(unistd.h)
AC_CHECK_HEADERS([sys/types.h])

dnl Check for the presence of the exp function in the C math library libm.
dnl This will always succeed. It is important, though, to do this, and do this
dnl first, because the AC_CHECK_FUNCS checks below will then link against
dnl libm when it is testing for the presence of a function. Without this call,
dnl tests for functions that are in libm (when present at all), such as
dnl feenableexcept,  will fail.
AC_CHECK_LIB(m,exp)

dnl check for the headers and functions related to the management of shared
dnl libraries.
AC_CHECK_HEADERS(dlfcn.h)
AC_CHECK_LIB(dl,dlopen)
AC_CHECK_FUNCS(dlopen dlerror dladdr)

dnl Check for the availability of FPU management functions
AC_CHECK_HEADERS([fenv.h ieeefp.h])
AC_CHECK_FUNCS(fesettrapenable fpsetmask)
dnl If present in the first place, feenableexcept  will only be exposed by
dnl fenv.h if the macro _GNU_SOURCE is defined before including that header.
dnl We therefore temporarily add -D_GNU_SOURCE to the preprocessor flags that
dnl are used by configure, then test for that function.
dnl NOTE: do not forget to also define _GNU_SOURCE in a module that decides to
dnl use feenableexcept, based on the fact that PL_HAVE_FEENABLEEXCEPT is defined
dnl in magnumpi-config.h. Otherwise the compilation of that module will still fail.
OLD_CPPFLAGS="$CPPFLAGS"
CPPFLAGS="$CPPFLAGS -D_GNU_SOURCE"
AC_CHECK_FUNCS(feenableexcept)
CPPFLAGS="$OLD_CPPFLAGS"

dnl ***************************************************************************
dnl
dnl     C++ standard library feature tests
dnl
dnl ***************************************************************************

AC_LANG_PUSH(C++)

dnl cxxabi.h is a non-standard header that provides functions to mangle/demangle
dnl C++ function names. See plregistry/demangle.h and the accompanying source
dnl file for more details.
AC_CHECK_HEADERS([cxxabi.h])

dnl ***************************************************************************
dnl
dnl     The Eigen headers and libraries
dnl
dnl ***************************************************************************
AC_ARG_WITH(eigen,
	[  --with-eigen(=eigen prefix)
			  use the Eigen library],
      	with_eigen="${withval}", with_eigen="auto")
AC_MSG_CHECKING(--with-eigen)
AC_MSG_RESULT($with_eigen)
EIGEN_MATVEC_LIBS=
if test "$with_eigen" != "no" ; then
  dnl Testsuites that rely on eigen
  if test "$with_eigen" = "auto" ; then
    AC_CHECK_HEADER(eigen3/Eigen/Core,with_eigen=yes, with_eigen=no)
  else
    EIGEN_CPPFLAGS="-I$with_eigen"
    CPPFLAGS="$CPPFLAGS $EIGEN_CPPFLAGS"
    AC_CHECK_HEADER(eigen3/Eigen/Core,with_eigen=yes,AC_MSG_ERROR(The eigen3 headers were not found. Please install these and re-configure.))
  fi
  dnl Subdirs for eigen-specific stuff
  if test "$with_eigen" = "yes"; then
    EIGEN_SUBDIR="eigen"
    EIGEN_MATVEC_LIBS="$(pwd)/plmath/lib/eigen/libpleigen_lapack.la $LAPACK_LIBS"
    AC_DEFINE_UNQUOTED([HAVE_EIGEN],1,[Define if we have the Eigen matrix vector library])
  fi
fi
AM_CONDITIONAL(USE_EIGEN, test "x$with_eigen" = "xyes")

AC_SUBST(EIGEN_CPPFLAGS)
AC_SUBST(EIGEN_SUBDIR)
AC_SUBST(EIGEN_MATVEC_LIBS)

dnl ***************************************************************************
dnl
dnl     The C++ threads library
dnl
dnl ***************************************************************************

AC_ARG_ENABLE(std_threads,
	[  --enable-std_threads  Enable std::thread support],
	WITH_STD_THREADS="${enableval}", WITH_STD_THREADS="auto" )

if test "x$WITH_STD_THREADS" = "xauto"; then
  AUTO_THREAD_SUPPORT="(result of automatic detection)"
  AC_TRY_COMPILE(
	[ #include <thread> ],
	[ return std::thread::hardware_concurrency(); ],
        [ WITH_STD_THREADS="yes" ],
	[ WITH_STD_THREADS="no" ]
  )
elif test "x$WITH_STD_THREADS" = "xmingw"; then
  AUTO_THREAD_SUPPORT="(mingw std::thread emulation requested)"
else
  AUTO_THREAD_SUPPORT=""
fi

AC_MSG_CHECKING(--enable-std_threads)
if test "$WITH_STD_THREADS" = "yes"; then
   MAGNUMPI_GCC_CXXFLAGS("$CXXFLAGS -pthread")
   AC_DEFINE(USE_STD_THREADS,1, [Enable C++11 thread support.])
   AC_MSG_RESULT([yes $AUTO_THREAD_SUPPORT])
elif test "x$WITH_STD_THREADS" = "xmingw"; then
   AC_DEFINE(USE_STD_THREADS,1, [Enable C++11 thread support.])
   AC_DEFINE(USE_MINGW_STD_THREADS,1, [mingw emulation of C++11 thread support.])
   AC_MSG_RESULT([yes $AUTO_THREAD_SUPPORT])
else
   AC_MSG_RESULT([no $AUTO_THREAD_SUPPORT])
fi
AM_CONDITIONAL(USE_STD_THREADS, test "x$WITH_STD_THREADS" = "xyes")

dnl ***************************************************************************
dnl
dnl     The Boost headers and libraries
dnl
dnl ***************************************************************************

BOOST_REQUIRE(1.70, AC_MSG_RESULT([WARNING - Boost version 1.70 not found. Networking facilities will not be available.]))
CPPFLAGS="$CPPFLAGS $BOOST_CPPFLAGS"
dnl BOOST_SYSTEM()
dnl BOOST_FILESYSTEM()
dnl BOOST_LDFLAGS="$BOOST_FILESYSTEM_LDFLAGS $BOOST_SYSTEM_LDFLAGS"
BOOST_LDFLAGS=""

dnl TODO unlike BOOST_LIBS, we set this globally.
LDFLAGS="$LDFLAGS $BOOST_LDFLAGS"
dnl BOOST_LIBS="$BOOST_FILESYSTEM_LIBS $BOOST_SYSTEM_LIBS"
BOOST_LIBS=""

AC_SUBST(BOOST_LDFLAGS)
AC_SUBST(BOOST_LIBS)
AC_SUBST(BOOST_SUBDIR)

dnl AC_CHECK_HEADER(boost/beast/core.hpp,,AC_MSG_ERROR([Could not find Boost header boost/beast/core.hpp.  Please install Boost version 1.66 or later. If Boost is present in a non-standard location: reconfigure with --with-boost=<boost root>.]))

dnl ***************************************************************************

AC_LANG_POP(C++)

dnl ***************************************************************************
dnl
dnl Documentation configuration
dnl
dnl   Handle the --with-doc flag, test for necessary components
dnl
dnl ***************************************************************************
CONFIGURE_DOCUMENTATION()

dnl ***************************************************************************
dnl
dnl     Report the most important configure results to the user
dnl
dnl ***************************************************************************

echo "Compile flags     : $CXXFLAGS"
echo "Options are"
echo "Debug             : $USE_DEBUG"
echo "Optimize          : $USE_OPTIMIZE"

echo "Doc generation    : $MAKE_IN_DOC"
if test "$MAKE_IN_DOC" = "yes"; then
	echo "Doxygen generation: $WITH_DOXYGEN"
	echo "Latex2html generation: $WITH_LATEX2HTML"
	echo "htlatex generation: $WITH_HTLATEX"
fi

FULL_SRC=`cd ${srcdir} && pwd`
FULL_BLD=`pwd`
AC_SUBST(FULL_SRC)
AC_SUBST(FULL_BLD)

if test "$PL_WIN32" = "no" ; then
  MAGNUMPI_EXTRA_FLAGS
fi

AC_CONFIG_COMMANDS([reconfigure],[
  echo "${MAGNUMPI_RECONFIG_SCRIPT}" > ./reconfigure
  chmod a+x ./reconfigure
  ],[
  MAGNUMPI_RECONFIG_SCRIPT="#!${SHELL}
exec ${0} $ac_configure_args \"\\\$[@]\""
])

AC_CONFIG_COMMANDS([gitignore],[
  echo "/*" > .gitignore
])

AC_CONFIG_FILES([
Makefile
app/Makefile
doc/Makefile
doc/doxygen/Makefile
doc/doxygen/magnumpi.doxy
examples/Makefile
ideas/Makefile
ideas/cschoutrop/Makefile
ideas/cschoutrop/recursive_integration/Makefile
ideas/cschoutrop/interpolation_test/Makefile
ideas/cschoutrop/concept_code/Makefile
ideas/cschoutrop/concept_code/vector_sort/Makefile
ideas/dcs/Makefile
ideas/jvdijk/Makefile
ideas/jvdijk/boost/Makefile
ideas/jvdijk/curl/Makefile
ideas/jvdijk/web/Makefile
ideas/scat_vl/Makefile
input/Makefile
lib/Makefile
magnumpi/Makefile
python/Makefile
python/magnumpi/Makefile
python/app/Makefile
python/tests/Makefile
set-magnumpi-local
set-magnumpi
testsuite/Makefile
])

AC_OUTPUT
