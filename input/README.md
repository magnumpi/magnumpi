# MagnumPI Input Files

This directory contains four types of input files:

 * Files with names of the form `*_int.json` describe interactions and contain
   a single element `interaction`. An `interaction` element contains two
   elements, a `potential` and a `reduced_mass`. The latter is needed for
   the calculation of collision integrals.

 * Files with names of the form `*_pot.json` describe potentialsand contain a
   single element `potential`. These suffice for the calculation of all other
   quantities: scattering angles, cross sections, reduced cross sections and
   reduced collision integrals.

 * Configuration files: these have names `*_scat.json`, `*_cs.json`,
   `*_cs_star.json`, `*_omega.json` and `*_omega_star.json`. These contain the
   numerical parameters that are needed to do a calculation of scattering
   angles, cross sections, reduced cross sections, collision integrals and
   reduced collision integrals. Typically a single configuration file can be
   used for multiple studies by combining it with different potential or
   interaction files.

 * Files with extension `_lut.dat`: these are old-style unannotated lookup
   tables that are referred to by some potential files.

Potential and interaction files do not necessarily describe a particular
real-world case: a Lennard Jones interaction file may use dummy parameter
values such as 1 Angstrom, 1 eV and 1 amu, and are used mostly for testing.
When such file describes a real interaction between two particles of a
particular type, this is made clear by the name of the file. As an example,
the file `heplus_he_ungerade_int.json` describes the potential for ungerade
interaction of a helium atom and a helium ion. In all cases, the files
contain annotation elements that describe the origin and purpose of the
file contents.

