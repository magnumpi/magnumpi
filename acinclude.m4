dnl Add extra functionality

dnl Copies $1 to $2 if $2 does not yet exist
dnl If $2 is already there, we check the creation dates. If $1 (the source)
dnl is newer, the user may need to (manually) update $2.
AC_DEFUN([MAGNUMPI_CREATE_LOCAL_CNF],
[
  if test -e $2; then
	dnl $2 already exists.

	dnl Check if $1 and $2 are different.
	dnl (note that 0==true in bash. If diff returns 0 the if-branch is taken.)
	if diff $1 $2 > /dev/null; then
        	dnl If $1 and $2 are the same, nothing needs to be done.
		AC_MSG_NOTICE(config file $2 unaltered)
	else
		dnl $1 and $2 both exist and are different.
  		if test $1 -nt $2; then
			dnl If the target file is older than the source file,
			dnl we move the local file to $2.old and install
			dnl a new config file.
			mv $2 $2.old
			cp $1 $2
			AC_MSG_WARN(Saved your old config file $2 into $2.old,)
			AC_MSG_WARN(Created new config file $2.)
		else
			dnl If the target file is newer than the source file,
			dnl local changes were made, but no new source file
			dnl arrived. Just keep the local changes.
			AC_MSG_NOTICE($2 not changed: it is more recent than $1)
		fi
	fi
  else
        dnl create the file by copying $1 to $2
	dnl if a file $2.old is present, tell the user to remove/rename it
	dnl (perhaps after moving over some changes to $2).
        cp $1 $2
	AC_MSG_NOTICE(copied $1 to $2)
  fi
])

AC_DEFUN([MAGNUMPI_EXTRA_FLAGS],
[
CFLAGS="$CFLAGS \$(EXTRA_CFLAGS)"
CXXFLAGS="$CXXFLAGS \$(EXTRA_CXXFLAGS)"
CPPFLAGS="$CPPFLAGS \$(EXTRA_CPPFLAGS)"
FFLAGS="$FFLAGS \$(EXTRA_FFLAGS)"
DEFS="$DEFS \$(EXTRA_DEFS)"
LDFLAGS="$LDFLAGS \$(EXTRA_LDFLAGS)"
LIBS="$LIBS \$(EXTRA_LIBS)"
])

AC_DEFUN([MAGNUMPI_GCC_CFLAGS],
[AC_REQUIRE([AC_PROG_CC])
test "$GCC" = "yes" && CFLAGS=$1])

AC_DEFUN([MAGNUMPI_GCC_CXXFLAGS],
[AC_REQUIRE([AC_PROG_CXX])
test "$GXX" = "yes" && CXXFLAGS=$1])

AC_DEFUN([MAGNUMPI_GCC_FFLAGS],
[AC_REQUIRE([AC_PROG_F77])
test "$GCC" = "yes" && FFLAGS=$1])

AC_DEFUN([MAGNUMPI_GCC_FCFLAGS],
[AC_REQUIRE([AC_PROG_FC])
test "$GCC" = "yes" && FCFLAGS=$1])

AC_DEFUN([MAGNUMPI_REMOVE_CFLAG],
[ CFLAGS=`echo " $CFLAGS " | sed -e 's/ $1 / /'`])

AC_DEFUN([MAGNUMPI_REMOVE_CXXFLAG],
[ CXXFLAGS=`echo " $CXXFLAGS " | sed -e 's/ $1 / /'`])

AC_DEFUN([MAGNUMPI_REMOVE_FFLAG],
[ FFLAGS=`echo " $FFLAGS " | sed -e 's/ $1 / /'`])

AC_DEFUN([MAGNUMPI_REMOVE_FCFLAG],
[ FCFLAGS=`echo " $FCFLAGS " | sed -e 's/ $1 / /'`])


dnl do we have doxygen installed?
AC_DEFUN([PROGRAM_DOXYGEN],
[
AC_CHECK_PROG(DOXYGEN, doxygen, doxygen, "")
if ! test "$DOXYGEN" ; then
  AC_MSG_WARN(doxygen not found; source code documentation will not be built)
fi
])

dnl do we have the dot program for creating dependency graphs?
dnl the word YES or NO will be subsituted in the doxygen input file
dnl on the line HAVE_DOT = @HAVE_DOT_YESNO@
AC_DEFUN([PROGRAM_DOT],
[
HAVE_DOT_YESNO="YES"
AC_CHECK_PROG(DOT, dot, dot, "")
if ! test "$DOT" ; then
 HAVE_DOT_YESNO="NO"
 AC_MSG_WARN(dot utility not found; source code documentation will be built without dependency graphs.)
fi
AM_CONDITIONAL(HAVE_DOT, test "$DOT")
AC_SUBST(HAVE_DOT_YESNO)
])

dnl do we have Python installed?
AC_DEFUN([PROGRAM_PYTHON],
[
AC_CHECK_PROG(PYTHON, python, python, "")
if ! test "$PYTHON" ; then
  AC_MSG_WARN(python not found; class to html mappings will not be generated)
fi
AM_CONDITIONAL(HAVE_PYTHON, test "$PYTHON")
])

dnl do we have the convert utility?
AC_DEFUN([PROGRAM_CONVERT],
[
HAVE_CONVERT=
AC_CHECK_PROG(CONVERT, convert, convert, "")
if ! test "$CONVERT" ; then
  AC_MSG_WARN(convert not found; the manual will not be built)
fi
AM_CONDITIONAL(HAVE_CONVERT, test "$CONVERT")
])

dnl do we have the pdflatex program?
dnl to do: check for version 7.3.7 or later
AC_DEFUN([PROGRAM_PDFLATEX],
[
HAVE_PDFLATEX=
AC_CHECK_PROG(PDFLATEX, pdflatex, pdflatex, "")
if ! test "$PDFLATEX" ; then
  AC_MSG_WARN(pdflatex not found; pdf version of the manual will not be built)
fi
AM_CONDITIONAL(HAVE_PDFLATEX, test "$PDFLATEX")
])

dnl do we have htlatex installed?
AC_DEFUN([PROGRAM_HTLATEX],
[
AC_CHECK_PROG(HTLATEX, htlatex, htlatex, "")
if ! test "$HTLATEX" ; then
  AC_MSG_WARN(htlatex not found; xml/mathml version of the documentation will not be built)
fi
])


dnl Always create the documentation? 
dnl Note that this is quite time-consuming. If you actively develop the code,
dnl say no here. You can always do a `cd doc; make' manually.
AC_DEFUN([CHECK_MAKE_SUBDIR_DOC],
[
AC_ARG_WITH(doc,
	[  --with-doc		  always generate documentation],
      	MAKE_IN_DOC="${withval}", MAKE_IN_DOC="no")

AC_MSG_CHECKING(--with-doc             )
if test "$MAKE_IN_DOC" = "yes"; then
  AC_MSG_RESULT(yes)
  DOC_SUBDIR="$DOC_SUBDIR doc"
else
  AC_MSG_RESULT(no)
fi
AC_SUBST(DOC_SUBDIR)
])

AC_DEFUN([CHECK_WITH_DOXYGEN],
[
AC_ARG_WITH(doxygen,
	[  --with-doxygen	  always generate doxygen documentation],
		WITH_DOXYGEN="${withval}", WITH_DOXYGEN="yes")
AC_MSG_CHECKING(--with-doxygen         )
if test "$WITH_DOXYGEN" = "yes"; then
  AC_MSG_RESULT(yes)
else
  AC_MSG_RESULT(no)
fi
if ! test "$DOXYGEN"; then
  WITH_DOXYGEN="no"
fi
AM_CONDITIONAL(MAKE_DOXYGEN, test "$WITH_DOXYGEN" = "yes")
])

AC_DEFUN([CHECK_WITH_HTLATEX],
[
AC_ARG_WITH(htlatex,
	[  --with-htlatex     always generate htlatex documentation],
		WITH_HTLATEX="${withval}", WITH_HTLATEX="yes")
AC_MSG_CHECKING(--with-htlatex      )
if test "$WITH_HTLATEX" = "yes"; then
  AC_MSG_RESULT(yes)
else
  AC_MSG_RESULT(no)
fi
if ! test "$HTLATEX"; then
  WITH_HTLATEX="no"
fi
AM_CONDITIONAL(MAKE_HTLATEX, test "$WITH_HTLATEX" = "yes")
])

AC_DEFUN([CONFIGURE_DOCUMENTATION],
[
dnl Always enter the doc subdir when make is called?
CHECK_MAKE_SUBDIR_DOC()
dnl Now check which of the tools needed for building the documentation
dnl are present
PROGRAM_DOXYGEN()
PROGRAM_DOT()
PROGRAM_PYTHON()
PROGRAM_CONVERT()
PROGRAM_PDFLATEX()
PROGRAM_HTLATEX()
dnl Check whether doxygen documentation is to be created
CHECK_WITH_DOXYGEN()
dnl Check whether htlatex documentation is to be created
CHECK_WITH_HTLATEX()
])

dnl JvD - Taken from the ac-archive at
dnl http://ac-archive.sourceforge.net/Miscellaneous/ax_prefix_config_h.html

dnl @synopsis AX_PREFIX_CONFIG_H [(OUTPUT-HEADER [,PREFIX [,ORIG-HEADER]])]
dnl
dnl This is a new variant from ac_prefix_config_ this one will use a
dnl lowercase-prefix if the config-define was starting with a
dnl lowercase-char, e.g. "#define const", "#define restrict", or
dnl "#define off_t", (and this one can live in another directory, e.g.
dnl testpkg/config.h therefore I decided to move the output-header to
dnl be the first arg)
dnl
dnl takes the usual config.h generated header file; looks for each of
dnl the generated "#define SOMEDEF" lines, and prefixes the defined name
dnl (ie. makes it "#define PREFIX_SOMEDEF". The result is written to
dnl the output config.header file. The PREFIX is converted to uppercase
dnl for the conversions.
dnl
dnl Defaults:
dnl
dnl   OUTPUT-HEADER = $PACKAGE-config.h
dnl   PREFIX = $PACKAGE
dnl   ORIG-HEADER, from AM_CONFIG_HEADER(config.h)
dnl
dnl Your configure.ac script should contain both macros in this order,
dnl and unlike the earlier variations of this prefix-macro it is okay to
dnl place the AX_PREFIX_CONFIG_H call before the AC_OUTPUT invokation.
dnl
dnl Example:
dnl
dnl   AC_INIT(config.h.in)        # config.h.in as created by "autoheader"
dnl   AM_INIT_AUTOMAKE(testpkg, 0.1.1)    # makes #undef VERSION and PACKAGE
dnl   AM_CONFIG_HEADER(config.h)          # prep config.h from config.h.in
dnl   AX_PREFIX_CONFIG_H(mylib/_config.h) # prep mylib/_config.h from it..
dnl   AC_MEMORY_H                         # makes "#undef NEED_MEMORY_H"
dnl   AC_C_CONST_H                        # makes "#undef const"
dnl   AC_OUTPUT(Makefile)                 # creates the "config.h" now
dnl                                       # and also mylib/_config.h
dnl
dnl if the argument to AX_PREFIX_CONFIG_H would have been omitted then the
dnl default outputfile would have been called simply "testpkg-config.h", but
dnl even under the name "mylib/_config.h" it contains prefix-defines like
dnl
dnl   #ifndef TESTPKG_VERSION
dnl   #define TESTPKG_VERSION "0.1.1"
dnl   #endif
dnl   #ifndef TESTPKG_NEED_MEMORY_H
dnl   #define TESTPKG_NEED_MEMORY_H 1
dnl   #endif
dnl   #ifndef _testpkg_const
dnl   #define _testpkg_const _const
dnl   #endif
dnl
dnl and this "mylib/_config.h" can be installed along with other
dnl header-files, which is most convenient when creating a shared
dnl library (that has some headers) where some functionality is
dnl dependent on the OS-features detected at compile-time. No
dnl need to invent some "mylib-confdefs.h.in" manually. :-)
dnl
dnl Note that some AC_DEFINEs that end up in the config.h file are
dnl actually self-referential - e.g. AC_C_INLINE, AC_C_CONST, and the
dnl AC_TYPE_OFF_T say that they "will define inline|const|off_t if the
dnl system does not do it by itself". You might want to clean up about
dnl these - consider an extra mylib/conf.h that reads something like:
dnl
dnl    #include <mylib/_config.h>
dnl    #ifndef _testpkg_const
dnl    #define _testpkg_const const
dnl    #endif
dnl
dnl and then start using _testpkg_const in the header files. That is
dnl also a good thing to differentiate whether some library-user has
dnl starting to take up with a different compiler, so perhaps it could
dnl read something like this:
dnl
dnl   #ifdef _MSC_VER
dnl   #include <mylib/_msvc.h>
dnl   #else
dnl   #include <mylib/_config.h>
dnl   #endif
dnl   #ifndef _testpkg_const
dnl   #define _testpkg_const const
dnl   #endif
dnl
dnl @version $Id$
dnl @author  Guiodo Draheim <guidod@gmx.de>
dnl
AC_DEFUN([AX_PREFIX_CONFIG_H],[AC_REQUIRE([AC_CONFIG_HEADERS])
AC_CONFIG_COMMANDS([ifelse($1,,$PACKAGE-config.h,$1)],[dnl
AS_VAR_PUSHDEF([_OUT],[ac_prefix_conf_OUT])dnl
AS_VAR_PUSHDEF([_DEF],[ac_prefix_conf_DEF])dnl
AS_VAR_PUSHDEF([_PKG],[ac_prefix_conf_PKG])dnl
AS_VAR_PUSHDEF([_LOW],[ac_prefix_conf_LOW])dnl
AS_VAR_PUSHDEF([_UPP],[ac_prefix_conf_UPP])dnl
AS_VAR_PUSHDEF([_INP],[ac_prefix_conf_INP])dnl
m4_pushdef([_script],[conftest.prefix])dnl
m4_pushdef([_symbol],[m4_cr_Letters[]m4_cr_digits[]_])dnl
_OUT=`echo ifelse($1, , $PACKAGE-config.h, $1)`
_DEF=`echo _$_OUT | sed -e "y:m4_cr_letters:m4_cr_LETTERS[]:" -e "s/@<:@^m4_cr_Letters@:>@/_/g"`
_PKG=`echo ifelse($2, , $PACKAGE, $2)`
_LOW=`echo _$_PKG | sed -e "y:m4_cr_LETTERS-:m4_cr_letters[]_:"`
_UPP=`echo $_PKG | sed -e "y:m4_cr_letters-:m4_cr_LETTERS[]_:"  -e "/^@<:@m4_cr_digits@:>@/s/^/_/"`
_INP=`echo "ifelse($3,,,$3)" | sed -e 's/ *//'`
if test ".$_INP" = "."; then
   for ac_file in : $CONFIG_HEADERS; do test "_$ac_file" = _: && continue
     case "$ac_file" in
        *.h) _INP=$ac_file ;;
        *)
     esac
     test ".$_INP" != "." && break
   done
fi
if test ".$_INP" = "."; then
   case "$_OUT" in
      */*) _INP=`basename "$_OUT"`
      ;;
      *-*) _INP=`echo "$_OUT" | sed -e "s/@<:@_symbol@:>@*-//"`
      ;;
      *) _INP=config.h
      ;;
   esac
fi
if test -z "$_PKG" ; then
   AC_MSG_ERROR([no prefix for _PREFIX_PKG_CONFIG_H])
else
  if test ! -f "$_INP" ; then if test -f "$srcdir/$_INP" ; then
     _INP="$srcdir/$_INP"
  fi fi
  AC_MSG_NOTICE(creating $_OUT - prefix $_UPP for $_INP defines)
  if test -f $_INP ; then
    echo "s/@%:@undef  *\\(@<:@m4_cr_LETTERS[]_@:>@\\)/@%:@undef $_UPP""_\\1/" > _script
    echo "s/@%:@undef  *\\(@<:@m4_cr_letters@:>@\\)/@%:@undef $_LOW""_\\1/" >> _script
    echo "s/@%:@def[]ine  *\\(@<:@m4_cr_LETTERS[]_@:>@@<:@_symbol@:>@*\\)\\(.*\\)/@%:@ifndef $_UPP""_\\1 \\" >> _script
    echo "@%:@def[]ine $_UPP""_\\1 \\2 \\" >> _script
    echo "@%:@endif/" >>_script
    echo "s/@%:@def[]ine  *\\(@<:@m4_cr_letters@:>@@<:@_symbol@:>@*\\)\\(.*\\)/@%:@ifndef $_LOW""_\\1 \\" >> _script
    echo "@%:@define $_LOW""_\\1 \\2 \\" >> _script
    echo "@%:@endif/" >> _script
    # now executing _script on _DEF input to create _OUT output file
    echo "@%:@ifndef $_DEF"      >$tmp/pconfig.h
    echo "@%:@def[]ine $_DEF 1" >>$tmp/pconfig.h
    echo ' ' >>$tmp/pconfig.h
    echo /'*' $_OUT. Generated automatically at end of configure. '*'/ >>$tmp/pconfig.h

    sed -f _script $_INP >>$tmp/pconfig.h
    echo ' ' >>$tmp/pconfig.h
    echo '/* once:' $_DEF '*/' >>$tmp/pconfig.h
    echo "@%:@endif" >>$tmp/pconfig.h
    if cmp -s $_OUT $tmp/pconfig.h 2>/dev/null; then
      AC_MSG_NOTICE([$_OUT is unchanged])
    else
      ac_dir=`AS_DIRNAME(["$_OUT"])`
      AS_MKDIR_P(["$ac_dir"])
      rm -f "$_OUT"
      mv $tmp/pconfig.h "$_OUT"
    fi
    cp _script _configs.sed
  else
    AC_MSG_ERROR([input file $_INP does not exist - skip generating $_OUT])
  fi
  rm -f conftest.*
fi
m4_popdef([_symbol])dnl
m4_popdef([_script])dnl
AS_VAR_POPDEF([_INP])dnl
AS_VAR_POPDEF([_UPP])dnl
AS_VAR_POPDEF([_LOW])dnl
AS_VAR_POPDEF([_PKG])dnl
AS_VAR_POPDEF([_DEF])dnl
AS_VAR_POPDEF([_OUT])dnl
],[PACKAGE="$PACKAGE"])])

AC_DEFUN([MAGNUMPI_INFOFILE],
[AC_CONFIG_COMMANDS([doc/magnumpi-info.txt],
[( echo "This is MAGNUMPI version $VERSION"
echo "configured by $WHO on $DATE"
echo "--"
echo "CPPFLAGS: $CPPFLAGS"
echo "CFLAGS  : $CFLAGS"
echo "CXXFLAGS: $CXXFLAGS"
echo "CXX     : $CXX"
echo "Compiler version: $CXXVERSION"
echo "Linker version: $LDVERSION"
echo "WX version: $WXVERSION"
echo "--" ) > doc/magnumpi-info.txt],
[VERSION='$VERSION'
WHO='$USER@$HOSTNAME'
DATE="`date`"
CPPFLAGS='$CPPFLAGS'
CXXFLAGS='$CXXFLAGS'
CFLAGS='$CFLAGS'
CXX='$CXX'
CXXVERSION="`$CXX -v 2>&1`"
LDVERSION="`$LD -V`"
WXVERSION="`$WXCONFIG --version`"])])
