/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/potential.h"
#include "magnumpi/viehland_common.h"
#include "magnumpi/consts.h"
#include "plmath/clencurt.h"
#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

namespace magnumpi {

double r0_test(const potential& pot, double energy,double b,double r)
{
	const double b_r=b/r;
	return 1.0-b_r*b_r-pot.value(r)/energy;
}

void adjust_b(const potential& pot, double energy, double* b, double r0)
{
	using std::pow;
	/** \todo declare b_mid more locally. Also fmid?
	 *  \todo explain hardcoded numbers, replace with constants if possible:
	 *        for example: 5e-16 is probably related to the machine epsilon.
	 *  \todo document (in words) what this algorithm does and what pre and
	 *        postconditions are. If this is (not much more than) bisection,
	 *        make a call to a bisection routine (or use Brent?)
	 */
	double b_left,b_right,b_mid,fmid;
	//initialize
	fmid=r0_test(pot,energy,*b,r0);
	//remove increasingly larger chuncks of b to guarantee a positive result for 1-b^2/r^2-V/E
	if (fmid<0.0)
	{
		int count=-15;
		b_right=*b;
		b_left= *b-pow(10,count);
		//find b_left that gives a positive result
		while (r0_test(pot,energy,b_left,r0)<0.0)
		{
			count++;
			b_left=*b-pow(10,count);

			if (b_left<0)
			{
				b_left=0.0;
				break;
			}
		}
	}
	else if (fmid>0.0)
	{
		int count=-15;
		b_left=*b;
		b_right= *b+pow(10,count);
		//find b_right that gives a negative result
		while (r0_test(pot,energy,b_right,r0)>0.0)
		{
			count++;
			b_right= *b+pow(10,count);
		}
	}
	else
	{
		return;
	}

	//bisection
	double res=1.0;
	while (res>5.0e-16)
	{
		b_mid=(b_left+b_right)/2.0;
		fmid=r0_test(pot,energy,b_mid,r0);

		if (fmid<0.0)
		{
			b_right=b_mid;
		}
		else if (fmid>0.0)
		{
			b_left=b_mid;
		}
		else //fmid==0
		{
			b_right=b_mid;
			b_left=b_mid;
		}
		res=(b_right-b_left)/b_right;
	}
	//pick b_left to ensure that 1-b^2/r^2-V/E >0
	*b=b_left;
	//pick b_right to ensure that r0 is found in the correct area
	//*b=b_right;
}


triplet_data::triplet_data(const potential* pot, double rmin, double rmax, double dr, double E_low)
 : m_pot(pot), m_rmin(rmin), m_rmax(rmax)
{
	std::vector<peak> top;
	std::vector<peak> bottom;
	//double delta_r=1e-15;
	Ed=1e20;
	Ec=0.0;
	int increasing=1;
	int index=0;
	double r_prev=rmax;
	int num_orbit=0;
	//indices n and p refer to next and previous
	double Vn,Vp=0.0;
	std::cout << "making triplets" << std::endl;

	//verify whether orbiting occurs at energies below E0(rmax)
	double phi=m_pot->value(rmax);
	double dVdr=m_pot->dVdr(rmax);
	double d2Vdr2=m_pot->d2Vdr2(rmax);
	//eq14
	double E0=phi+0.5*rmax*dVdr;
	double eq15=d2Vdr2+3.0/rmax*dVdr;
	//check if orbiting conditions are met at higher energies as well
	if (E0>0 && dVdr>0 && eq15<0)
	{
		m_orbiting=1;
		double r_test=rmax;
		double r_test_prev=rmax;
		//make sure E0 is smaller than energy_low to guarantee that the best method is chosen for integration
		while (E0>E_low && dVdr>0 && eq15<0)
		{
			//store previous result
			r_test_prev=r_test;
			//calculate orbiting energy for higher energy
			r_test *= 1.2;
			phi=m_pot->value(r_test);
			dVdr=m_pot->dVdr(r_test);
			d2Vdr2=m_pot->d2Vdr2(r_test);
			//eq14
			E0=phi+0.5*r_test*dVdr;
			eq15=d2Vdr2+3.0/r_test*dVdr;
		}
		//make sure E0 is correct: Should E0 be determined more accurately?
		if (E0>E_low)
		{
			phi=m_pot->value(r_test_prev);
			dVdr=m_pot->dVdr(r_test_prev);
			//eq14
			E0=phi+0.5*r_test_prev*dVdr;
			r_test=r_test_prev;
		}
		//add to orbiting vectors
		Ed=E0;
		E_orbit.push_back(Range(E0,E0));
		b_orbit.push_back(Range(0.0,0.0));
		r_orbit.push_back(Range(r_test,r_test));
	}
	else
	{
		//initialize orbiting parameter
		m_orbiting=0;
	}

	//evaluate orbiting criteria, Viehland eq 9-15
	for (double r=rmax;r>=rmin;r=r-dr)
	{
		//check next potential value
		Vn=m_pot->value(r-dr);
		//store previous result
		Vp=phi;
		//calculate new potential
		phi=m_pot->value(r);

		if (r<rmax)
		{
			//determine peaks
			if (phi>Vp && phi>Vn)
			{
				//peak found
				top.push_back(peak(r,phi));
			}
			else if (phi<Vp && phi<Vn)
			{
				//well found
				bottom.push_back(peak(r,phi));
			}
		}

		dVdr=m_pot->dVdr(r);
		d2Vdr2=m_pot->d2Vdr2(r);

		//eq14
		E0=phi+0.5*r*dVdr;
		eq15=d2Vdr2+3.0/r*dVdr;

		//evaluate orbiting criteria
		//note that Veff'=0, which indicates that V'>0
		if (E0>0 && dVdr>0 && eq15<0)
		{
			const double b=sqrt(pow(r,3)*dVdr/2.0/E0);
			//verify whether orbiting really occurs
			/*
			if (std::abs(1.0-pow(b/r,2)-phi/E0)>1e-14)
			{
			std::cout << 1.0-pow(b/r,2)-phi/E0 << std::endl;
			}
			// */

			if (E0>Ec)
			{
				Ec=E0;
				bc=b;
				m_rc=r;
			}
			if (E0<Ed)
			{
				Ed=E0;
				bd=b;
			}
			//*
			//check for multiple orbiting energies
			if (m_orbiting==0)
			{
				//initialize: first orbiting interval
				E_orbit.push_back(Range(E0,E0));
				b_orbit.push_back(Range(0.0,0.0));
				r_orbit.push_back(Range(r,r));
				m_orbiting=1;
				Ed=E0;
				Ec=E0;
			}
			else if (increasing==1 && E0>E_orbit[index].high)
			{
				//adjust upper boundary of energy of current orbiting interval
				E_orbit[index].high=E0;
				r_orbit[index].high=r;
				num_orbit++;
			}
			else if (increasing==0 && E0<E_orbit[index].low)
			{
				//adjust lower boundary of current orbiting interval (energy)
				E_orbit[index].low=E0;
				r_orbit[index].low=r;
				num_orbit++;
			}
			else if (increasing==1 && E0<E_orbit[index].high)
			{
				//energy lower than upper boundary, first decrease
				increasing=0;

				if (num_orbit>0 && E_orbit[index].low!=E_orbit[index].high)
				{
					//valid energy interval with multiple points
					//add new interval: use previous E_high as first value of decreasing interval
					E_orbit.push_back(Range(E_orbit[index].high,E_orbit[index].high));
					r_orbit.push_back(Range(r_orbit[index].high,r_orbit[index].high));
					b_orbit.push_back(Range(0.0,0.0));
					index++;
				}
				else
				{
					//previous interval was invalid, it did not contain multiple points
					E_orbit[index].low=E0;
					//reset Ehigh if Ehigh was not obtained from the previous r
					if (r_prev-r>1.5*dr)
					{
						//overwrite previous value
						E_orbit[index].high=E0;
						r_orbit[index].high=r;
					}
					r_orbit[index].low=r;
					//reset Ed and Ec if necessary
					if (E_orbit.size()==1)
					{
						Ed=E0;
						Ec=E0;
					}
				}
				num_orbit=0;
			}
			else if (increasing==0 && E0>E_orbit[index].low)
			{
				//energy higher than lower boundary, first increase
				increasing=1;
				if (num_orbit>0 && E_orbit[index].low!=E_orbit[index].high)
				{
					//valid energy interval with multiple points
					//add new interval: use previous E_low as first value of increasing interval
					E_orbit.push_back(Range(E_orbit[index].low,E_orbit[index].low));
					r_orbit.push_back(Range(r_orbit[index].low,r_orbit[index].low));
					b_orbit.push_back(Range(0.0,0.0));
					index++;
				}
				else
				{
					//previous interval was invalid, it did not contain multiple points
					if (r_prev-r>1.5*dr)
					{
						//overwrite previous value
						E_orbit[index].low=E0;
						r_orbit[index].low=r;
					}
					E_orbit[index].high=E0;
					r_orbit[index].high=r;

					//reset Ed and Ec if necessary
					if (E_orbit.size()==1)
					{
						Ed=E0;
						Ec=E0;
					}
				}
				num_orbit=0;
			}
			/*
			//if (E0>4e-3 && E0<6e-3)
			{
			std::cout << E0 << "\t" << r << "\t" << increasing << "\t" << E_orbit[index-1].low
			<< "\t" << E_orbit[index-1].high << std::endl;
			}
			// */
			//evaluate validity of current interval
			if (r_prev-r>1.5*dr)
			{
				num_orbit=0;
			}
			//remember last r for which orbiting occurred
			r_prev=r;
		}
	}

	if (m_orbiting==1)
	{
		//remove orbiting parameters that have an r_orbit that displays a relative difference of less than rel_tol
		double rel_tol =1e-4;
		for (unsigned i=1; i<r_orbit.size();++i)
		{
			if (std::abs(r_orbit[i].low-r_orbit[i-1].low)/r_orbit[i].low<rel_tol
				&& std::abs(r_orbit[i].high-r_orbit[i-1].high)/r_orbit[i].high < rel_tol)
			{
				//remove orbiting parameters
				r_orbit.erase(r_orbit.begin()+i);
				E_orbit.erase(E_orbit.begin()+i);
				b_orbit.erase(b_orbit.begin()+i);
			}
		}

		//assign values to b_orbit
		for (unsigned i=0;i<E_orbit.size();i++)
		{
			b_orbit[i].low=r_orbit[i].low*sqrt(1.0-m_pot->value(r_orbit[i].low)/E_orbit[i].low);
			adjust_b(*m_pot,E_orbit[i].low,&b_orbit[i].low,r_orbit[i].low);
			b_orbit[i].high=r_orbit[i].high*sqrt(1.0-m_pot->value(r_orbit[i].high)/E_orbit[i].high);
			adjust_b(*m_pot,E_orbit[i].high,&b_orbit[i].high,r_orbit[i].high);
		}
	}

	//write found peaks to screen
	if (top.size()==0)
	{
		std::cout << "No peaks found" << std::endl;
	}
	else
	{
		std::cout << "Peaks found" << std::endl;
		for (unsigned i=0;i<top.size();i++)
		{
			std::cout << top[i].r << "m\t" << top[i].V/constants::eV <<"eV" << std::endl;
		}
	}
	//write found wells to screen
	if (bottom.size()==0)
	{
		std::cout << "No minimum found" << std::endl;
	}
	else
	{
		std::cout << "Minima found"<< std::endl;
		for (unsigned i=0;i<bottom.size();i++)
		{
			std::cout << bottom[i].r << "m\t" << bottom[i].V/constants::eV << "eV" << std::endl;
		}
	}

	//write orbiting energies to screen
	if (m_orbiting==1)
	{
		std::cout << "Orbiting occurs between the following energy intervals" << std::endl;
		for (unsigned i=0;i<E_orbit.size();i++)
		{
			std::cout << E_orbit[i].low << "\t" << E_orbit[i].high << std::endl;
		}
		std::cout << std::endl;
		std::cout << "Ed\t" << Ed/constants::eV << "eV" << std::endl;
		std::cout << "Ec\t" << Ec/constants::eV << "eV" << std::endl;
	}
	else
	{
		std::cout << "No orbiting detected" << std::endl << std::endl;
	}
}

void triplet_data::get_orbiting_b(std::vector<double>* b_vec,std::vector<double>* r_vec,double energy) const
{

	for (unsigned i=0;i<E_orbit.size();i++)
	{
		if (E_orbit[i].low<=energy && E_orbit[i].high>=energy)
		{
			//determine r: eq 14
			double r1=r_orbit[i].low;
			double r2=r_orbit[i].high;

			double rmid,Emid,dVdr;

			double res=1.0;
			/// \todo Motivate the value of tol
			double tol=5e-16;
			//bisection
			while(res>tol)
			{
				rmid=(r2+r1)/2.0;
				dVdr=m_pot->dVdr(rmid);
				//eq 14
				Emid=m_pot->value(rmid)+dVdr/2.0*rmid;

				if (Emid-energy<0)
				{
					//below E0
					r1=rmid;
				}
				else
				{
					//above E0
					r2=rmid;
				}
				res=std::abs(r2-r1)/r2;
			}

			//correct b to prevent 1-(b/r)^2-V/E<0
			double b=sqrt(dVdr/2.0*pow(rmid,3)/Emid);
			adjust_b(*m_pot,energy,&b,rmid);

			//determine b: eq 13
			if (dVdr>0.0)
			{
				b_vec->push_back(b);
				r_vec->push_back(rmid);
			}
		}
	}

	//reorder b_vec in ascending order if multiple b's are present
	if ( b_vec->size()>1)
	{
		unsigned start=0,index;
		double temp;
		while (start<b_vec->size()-1)
		{
			index=start;
			for (unsigned i=start+1;i<b_vec->size();i++)
			{
				//look for minimum value between start and size
				if ( (*b_vec)[i]<(*b_vec)[index] )
				{
					index=i;
				}
			}
			//execute swap for b
			temp=(*b_vec)[start];
			(*b_vec)[start]=(*b_vec)[index];
			(*b_vec)[index]=temp;
			//execute swap for r
			temp=(*r_vec)[start];
			(*r_vec)[start]=(*r_vec)[index];
			(*r_vec)[index]=temp;

			start++;
		}
		//verify whether orbiting impact parameter is valid
		//if b_i<b_i+1 and r_i+1<r_i orbiting is invalid (point can not be reached)
		int ind=1;
		while (ind>0)
		{
			//if no false orbiting case is detected the loop exits
			ind=-1;
			if (b_vec->size()>1)
			{
				for (unsigned i=1;i<b_vec->size();i++)
				{
					if ( (*r_vec)[i-1]>(*r_vec)[i] )
					{
						ind=i;
						//erase orbiting vectors
						b_vec->erase(b_vec->begin()+ind);
						r_vec->erase(r_vec->begin()+ind);
						break;
					}
				}
			}
		}
	}
}


double triplet_data::min_range(const std::vector<Range>& X_in) const
{
	double min=1e10;
	if (X_in.size()>0)
	{
		for (unsigned i=0; i<X_in.size(); ++i)
		{
			min=std::min(min,X_in[i].low);
		}
	}
	return min;
}

double triplet_data::max_range(const std::vector<Range>& X_in) const
{
	double max=0;
	if (X_in.size()>0)
	{
		for (unsigned i=0; i<X_in.size(); ++i)
		{
			max=std::max(max,X_in[i].high);
		}
	}
	return max;
}

void triplet_data::get_rctilde_r0tilde(double* rc_tilde,double* r0_tilde,double energy) const
{
	*rc_tilde=m_rmax;
	*r0_tilde=m_rmin;
	double res=1.0;
	double fmid,rlower,rupper;
	//double delta_r;
	//int index=-1;

	//calculate r0_tilde: V(r0_tilde)=energy, use bisection
	res=1.0;
	//verify lower boundary
	rlower=m_rmin;
	while (m_pot->value(rlower)<energy)
	{
		rlower=rlower-1e-11;
	}
	//verify upper boundary
	rupper=m_rmax;
	while (m_pot->value(rupper)>energy)
	{
		rupper=rupper+1e-9;
	}
	//apply bisection
	while (res>5e-16)
	{
		*r0_tilde=(rlower+rupper)/2.0;
		fmid=m_pot->value(*r0_tilde);
		if (fmid>energy)
		{
			rlower=*r0_tilde;
		}
		else if(fmid<energy)
		{
			rupper=*r0_tilde;
		}
		else
		{
			//fmid==energy
			rlower=*r0_tilde;
			rupper=*r0_tilde;
		}
		res=std::abs(rupper-rlower)/rupper;
	}

	//calculate rc_tilde: b(rc_tilde)=rc_tilde
	rlower=*r0_tilde;
	rupper=*r0_tilde*1.1;
	double f2=pow(rupper,2)*(1.0-m_pot->value(rupper)/energy)-pow(m_rc,2);

	//verify input
	while (f2<=0.0)
	{
		rupper=rupper+*r0_tilde*0.1;
		f2=pow(rupper,2)*(1.0-m_pot->value(rupper)/energy)-pow(m_rc,2);
	}

	//use bisection
	res=1.0;
	while (res>5e-16)
	{
		*rc_tilde=(rlower+rupper)/2.0;
		fmid=pow(*rc_tilde,2)*(1.0-m_pot->value(*rc_tilde)/energy)-pow(m_rc,2);

		if (fmid<0.0)
		{
			rlower=*rc_tilde;
		}
		else
		{
			rupper=*rc_tilde;
		}
		res=(rupper-rlower)/rupper;
	}
}

triplet_data::case_data::case_data(const triplet_data& triplets, double energy)
{
	// we ensure that b_split is set when needed by default-initiaizing
	// to a NaN, so a failure to properly initialize will be detected.
	b_split = std::numeric_limits<double>::quiet_NaN();

	// this is what was previously found in calc_cs.cpp and calc_scat.cpp
	if (triplets.m_orbiting==0)
	{
		//no orbiting: Viehland case 3
		case_id = 3;
		b_split=2.0*triplets.m_rmin; //arbitrary value, does this splitting always work?
	}
	else if (energy< triplets.Ed)
	{
		//Viehland case 3
		case_id = 3;
		b_split=triplets.bd;
	}
	else if (energy<triplets.Ec)
	{
		case_id = 1;
		//Viehland case 1
		//determine number of orbiting impact parameters
		triplets.get_orbiting_b(&b_vec, &r_vec, energy);
		for (unsigned i=0; i<b_vec.size();++i)
		{
			std::cout << "orbiting r\t " << r_vec[i] << "\tb\t" << b_vec[i] << std::endl;
		}
	}
	else if (energy<3*triplets.Ec)
	{
		//Viehland case 2
		case_id = 2;
		//get rc_tilde and r0_tilde
		triplets.get_rctilde_r0tilde(&rc_tilde,&r0_tilde,energy);
	}
	else // if (energy>=3*Ec)
	{
		//Viehland case 3
		case_id = 3;
		b_split=triplets.bc;
	}
}

} // namespace magnumpi
