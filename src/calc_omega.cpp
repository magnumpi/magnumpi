/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/calc_omega.h"
#include "magnumpi/consts.h"
#include "magnumpi/data_set.h"
#include <algorithm>
#include <iostream>
#include <cmath>

namespace magnumpi {

class int_func_ci
{
  public:
	using result_type = double;
	using argument_type = double;

	int_func_ci(
		const calc_omega::ls_pairs_type& ls,
		double T,
		const cross_section& Q)
	:	m_T(T),
		m_ls(ls),
		m_Q(Q)
	{
		for (const auto& i : ls)
		{
			if (!m_Q.supports_l(i.l))
			{
				throw std::runtime_error("Omega calculation: cross section does not support required l-value.");
			}
		}
	}

	// return function integrand (single dimension)
	// this function can not be made multi dimensional since a given y
	// corresponds to different x because s is different
	void operator()(argument_type y, std::vector<double>& h) const
	{
		assert(h.size()==m_ls.size());
		for (unsigned ind=0;ind<m_ls.size();++ind)
		{
			const unsigned l = m_ls[ind].l;
			const unsigned s = m_ls[ind].s;
			h[ind]=f1(y,l,s)+f2(y,l,s);
		}
	}
  private:
	result_type f(double x, unsigned l, unsigned s) const
	{
		const double E=x*constants::Boltzmann*m_T;
		const double Q=m_Q(l,E);
		return std::pow(x,s+1)*std::exp(-x)*Q;
	}
	result_type f1(argument_type y,unsigned l,unsigned s) const
	{
		const double x1 = (s+1)/2.0*(1+y);
		return (s+1)/2.0*f(x1,l,s);
	}
	result_type f2(argument_type y,unsigned l,unsigned s) const
	{
		if (y<=0)
		{
			return 0.0;
		}
		const double x2=(s+1)/y;
		// note: (s+1)*f(x2,l,s)/y^2
		//     = (s+1)^-1*f(x2,l,s)*x2^2
		//     = (s+1)^-1f(x2,l,s+2)
		return 1/(s+1.0)*f(x2,l,s+2);
	}
  private:
	const double m_T;
	const calc_omega::ls_pairs_type& m_ls;
	const cross_section& m_Q;
};

calc_omega::calc_omega(
		const ls_pairs_type& ls_pairs,
		int lower_cc,
		int upper_cc,
		double tol)
 :	m_ls_pairs(ls_pairs),
	m_integrator(-1.0,1.0,lower_cc,upper_cc),
	m_tolerance(tol)
{
	std::cout << "Requesting " << m_ls_pairs.size() << " collision integrals" << std::endl;
}

std::vector<double> calc_omega::integrate(const cross_section& Q, double reduced_mass, double T) const
{
	const int_func_ci func(m_ls_pairs,T,Q);
	std::vector<double> omega_ls(m_ls_pairs.size(),0.0);
	m_integrator.integrate_md(func,m_ls_pairs.size(),m_tolerance,omega_ls);
	const double v_8 = std::sqrt(8*constants::Boltzmann*T/constants::pi/reduced_mass)/8;
	for (auto& value : omega_ls)
	{
		value *= v_8;
	}
	return omega_ls;
}

data_set calc_omega::integrate(
		const cross_section& Q,
		double reduced_mass,
		double Tmin,
		double Tmax,
		double Tdel) const
{
	data_set omega_data(
		"Omega_ls(T)", units::m3_s, "m^3/s",
		{"T", units::temperature, "K"},
		{"(l,s)"} );
	{
		for (auto& pair: m_ls_pairs)
		{
			omega_data.add_column("("+std::to_string(pair.l)+","+std::to_string(pair.s)+")");
		}
	}

	// result buffer
	std::vector<double> omega_ls(m_ls_pairs.size(),0.0);
	unsigned num_T=(Tmax-Tmin)/Tdel+1;
	for (unsigned i=0;i!=num_T;++i)
	{
		const double T=Tmin+i*Tdel;
		const int_func_ci func{m_ls_pairs,T,Q};
		m_integrator.integrate_md(func,m_ls_pairs.size(),m_tolerance,omega_ls);
		const double v_8 = std::sqrt(8*constants::Boltzmann*T/constants::pi/reduced_mass)/8;
		for (auto& value : omega_ls)
		{
			value *= v_8;
		}
		omega_data.add_row(T,omega_ls);
	}
	/// \todo use/enable move semantics
	return omega_data;
}

data_set calc_omega::calculate_omega_star(
		const cross_section& Q,
		double eps,
		double Qhs,
		double Tstarmin,
		double Tstarmax,
		double Tstardel) const
{
	data_set omega_data(
		"Omega_ls*(T*)", units::scalar, "1",
		{"T*", units::scalar, "1"},
		{"(l,s)"} );
	{
		for (auto& pair: m_ls_pairs)
		{
			omega_data.add_column("("+std::to_string(pair.l)+","+std::to_string(pair.s)+")");
		}
	}

	// result buffer
	std::vector<double> omega_star_ls(m_ls_pairs.size(),0.0);
	unsigned num_Tstar=(Tstarmax-Tstarmin)/Tstardel+1;
	for (unsigned i=0;i!=num_Tstar;++i)
	{
		const double Tstar=Tstarmin+i*Tstardel;
		const double T = Tstar*eps/constants::Boltzmann;
		const int_func_ci func{m_ls_pairs,T,Q};
		m_integrator.integrate_md(func,m_ls_pairs.size(),m_tolerance,omega_star_ls);
		for (std::size_t i=0; i!= omega_star_ls.size(); ++i)
		{
			unsigned l = m_ls_pairs[i].l;
			unsigned s = m_ls_pairs[i].s;
			const double ls_factor = 2.*(l+1.)/( std::tgamma(s+2)*(2*l + (l%2 ? 2 : 0)) );
			omega_star_ls[i] *= ls_factor/Qhs;
		}
		omega_data.add_row(Tstar,omega_star_ls);
	}
	/// \todo use/enable move semantics
	return omega_data;
}

calc_omega::ls_pairs_type
construct_ls_pairs(unsigned lmax,unsigned smax)
{
	calc_omega::ls_pairs_type ls;
	for (unsigned l=1;l<=lmax;++l)
	{
		for (unsigned s=l;s<=smax;++s)
		{
			ls.emplace_back(l,s);
		}
	}
	return ls;
}

data_set calculate_ci(
		int lower_cc,
		int upper_cc,
		double reduced_mass,
		double Tmin,
		double Tdel,
		double Tmax,
		int l_max,
		int s_max,
		double tol,
		const cross_section& Q)
{
	//make collision integral object
	std::cout << "l_max\t" << l_max << "\ts_max\t" << s_max << std::endl;
	const calc_omega::ls_pairs_type ls(construct_ls_pairs(l_max,s_max));
	const calc_omega ci(ls,lower_cc,upper_cc,tol);
	std::cout << lower_cc << std::endl;
	std::cout << upper_cc << std::endl;
	std::cout << reduced_mass << std::endl;
	std::cout << l_max << std::endl;
	std::cout << s_max << std::endl;
	std::cout << tol << std::endl;
	//perform integration
	return ci.integrate(Q,reduced_mass,Tmin,Tmax,Tdel);
}

data_set calculate_ci(
		const cross_section& Q,
		const json_type& cnf,
		unsigned l_max,
		double reduced_mass,
		double tolerance)
{
	const int s_max = cnf.at("s_max");
	const int ci_lower_cc=cnf.at("cc").at("lower");
	const int ci_upper_cc=cnf.at("cc").at("upper");
	const double Tmin=in_si(cnf.at("temperature_range").at("min"),units::temperature);
	const double Tmax=in_si(cnf.at("temperature_range").at("max"),units::temperature);
	const double Tdel=in_si(cnf.at("temperature_range").at("step"),units::temperature);

	std::cout << "l_max\t" << l_max << "\ts_max\t" << s_max << std::endl;
	const calc_omega::ls_pairs_type ls(construct_ls_pairs(l_max,s_max));
	const calc_omega ci(ls,ci_lower_cc, ci_upper_cc, tolerance);
	return ci.integrate(Q,reduced_mass,Tmin,Tmax,Tdel);
}

data_set calculate_ci_star(
		const cross_section& Q,
		double eps,
		double Qhs,
		const json_type& cnf,
		unsigned l_max,
		double tolerance)
{
	const int s_max = cnf.at("s_max");
	const int ci_lower_cc=cnf.at("cc").at("lower");
	const int ci_upper_cc=cnf.at("cc").at("upper");
	const double Tstarmin=in_si(cnf.at("temperature_star_range").at("min"),units::scalar);
	const double Tstarmax=in_si(cnf.at("temperature_star_range").at("max"),units::scalar);
	const double Tstardel=in_si(cnf.at("temperature_star_range").at("step"),units::scalar);

	std::cout << "l_max\t" << l_max << "\ts_max\t" << s_max << std::endl;
	const calc_omega::ls_pairs_type ls(construct_ls_pairs(l_max,s_max));
	const calc_omega ci(ls,ci_lower_cc, ci_upper_cc, tolerance);
	return ci.calculate_omega_star(Q,eps,Qhs,Tstarmin,Tstarmax,Tstardel);
}

} // namespace magnumpi
