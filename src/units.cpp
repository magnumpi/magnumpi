/** \file
 *
 * Copyright (C) 2018-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/units.h"
#include "magnumpi/consts.h"
#include <string>
#include <cstdlib>
#include <vector>
#include <sstream>
#include <stdexcept>
#include <algorithm>

namespace magnumpi {

template <>
unsigned long convert_to<unsigned long>(const std::string& value)
{
	char* endptr;
	unsigned long res = std::strtoul(value.c_str(),&endptr,0);
	if (endptr!=value.c_str()+value.size())
	{
		throw std::runtime_error("Error converting '"
			+ value + "' to an unsigned long integer value.");
	}
	return res;
}

template <>
long convert_to<long>(const std::string& value)
{
	char* endptr;
	long res = std::strtol(value.c_str(),&endptr,0);
	if (endptr!=value.c_str()+value.size())
	{
		throw std::runtime_error("Error converting '"
			+ value + "' to a long integer value.");
	}
	return res;
}

template <>
int convert_to<int>(const std::string& value)
{
	long res = convert_to<long>(value);
	// check if the result is in the range of int values,
	// throw an error otherwise.
	if (res<static_cast<long>(std::numeric_limits<int>::lowest())
	  ||res>static_cast<long>(std::numeric_limits<int>::max()))
	{
		throw std::runtime_error("Error converting '"
			+ value + "' to an integer: value out of range.");
	}
	return res;
}

template <>
double convert_to<double>(const std::string& value)
{
	char* endptr;
	double res = std::strtod(value.c_str(),&endptr);
	if (endptr!=value.c_str()+value.size())
	{
		throw std::runtime_error("Error converting '"
			+ value + "' to a double value.");
	}
	return res;
}

template <>
bool convert_to<bool>(const std::string& value)
{
	if (value=="true")
	{
		return true;
	}
	else if (value=="false")
	{
		return false;
	}
	throw std::runtime_error("Error converting '" + value
		+ "' to a boolean value: expected 'true' or 'false'.");
}

bool is_integer(const std::string& value)
{
	try { convert_to<int>(value); return true; } catch(...) { };
	return false;
}

bool is_number(const std::string& value)
{
	try { convert_to<int>(value); return true; } catch(...) { };
	try { convert_to<double>(value); return true; } catch(...) { };
	return false;
}

bool is_bool(const std::string& value)
{
	try { convert_to<bool>(value); return true; } catch(...) { };
	return false;
}

namespace units {

struct unit_def { std::string symbol; double value; };
using unit_table = std::vector<unit_def>;

std::string name(dimension d)
{
	switch(d)
	{
		case scalar:
			return "scalar";
		break;
		case mass:
			return "mass";
		break;
		case length:
			return "length";
		break;
		case time:
			return "time";
		break;
		case temperature:
			return "temperature";
		break;
		case energy:
			return "energy";
		break;
		case angle:
			return "angle";
		break;
		case area:
			return "m^2";
		break;
		case m3_s:
			return "m^3/s";
		break;
		case Jm6:
			return "J*m^6";
		break;
		case reciprocal_length:
			return "reciprocal_length";
		break;
		default:
			throw std::logic_error("Illegal dimension argument.");
		break;
	};
}

double get_unit_si_value(dimension d, const unit_table& table, const std::string& unit_symbol)
{
	auto result_iterator = std::find_if(table.begin(),table.end(),
		[&unit_symbol](const unit_def& unit_from_table)
	{
		return unit_from_table.symbol == unit_symbol;
	});

	if (result_iterator != table.end())
	{
		return result_iterator->value;
	}
	else
	{
		throw std::runtime_error("Unsupported " + name(d) + " unit '" + unit_symbol + "'.");
	}

}

double unit_in_si(const std::string& unit_symbol, dimension d)
{
	switch (d)
	{
		case scalar:
			return get_unit_si_value(d,
			{
				{ "1", 1.0 }
			},
			unit_symbol);
		break;
		case mass:
			return get_unit_si_value(d,
			{
				{ "kg", 1.0 },
				{ "amu", constants::AMU }
			},
			unit_symbol);
		break;
		case length:
			return get_unit_si_value(d,
			{
				{ "m", 1.0 },
				{ "a0", constants::BohrRadius },
				{ "Bohr", constants::BohrRadius }
			},
			unit_symbol);
		break;
		case time:
			return get_unit_si_value(d,
			{
				{ "s", 1.0 }
			},
			unit_symbol);
		break;
		case temperature:
			return get_unit_si_value(d,
			{
				{ "K", 1.0 },
				{ "eVT", constants::eV/constants::Boltzmann }
			},
			unit_symbol);
		break;
		case energy:
			return get_unit_si_value(d,
			{
				{ "J", 1.0 },
				{ "eV", constants::eV },
				{ "Hartree", constants::Hartree }
			},
			unit_symbol);
		break;
		case angle:
			return get_unit_si_value(d,
			{
				{ "rad", 1.0 },
				{ "deg", constants::deg }
			},
			unit_symbol);
		break;
		case area:
			return get_unit_si_value(d,
			{
				{ "m^2", 1.0 }
			},
			unit_symbol);
		break;
		case m3_s:
			return get_unit_si_value(d,
			{
				{ "m^3/s", 1.0 }
			},
			unit_symbol);
		break;
		case Jm6:
			return get_unit_si_value(d,
			{
				{ "J*m^6", 1.0 },
				{ "eV*m^6", constants::eV }
			},
			unit_symbol);
		break;
		case reciprocal_length:
			return get_unit_si_value(d,
			{
				{ "1/m", 1.0 },
				{ "1/a0", 1/constants::BohrRadius },
				{ "1/Bohr", 1/constants::BohrRadius }
			},
			unit_symbol);
		break;
		default:
			throw std::logic_error("Illegal dimension argument.");
		break;
	}
}

double quantity_in_si(const std::string& q, dimension d)
{
	try {
		std::string::size_type star_pos = q.find('*');
		if (star_pos==std::string::npos || star_pos==0 || star_pos==q.size()-1)
		{
			throw std::runtime_error("Expected 'value*unit'.");
		}
		const std::string value_str = q.substr(0,star_pos);
		const std::string unit_str = q.substr(star_pos+1);
		if (unit_str.find('*')!=std::string::npos)
		{
			throw std::runtime_error("Expected 'value*unit'.");
		}
		return convert_to<double>(value_str) * unit_in_si(unit_str,d);
	}
	catch(std::exception& exc)
	{
		throw std::runtime_error("Error parsing " + name(d) +" '" + q + "': "
			+ std::string(exc.what()));
	}
}

std::string in_units(double si_value, const std::string& unit, dimension d)
{
	std::stringstream ss;
	ss << (si_value/unit_in_si(unit,d)) << '*' << unit;
	return ss.str();
}

double in_si(const json_type& q, dimension d)
{
	if (!q.is_object())
	{
		throw std::runtime_error("Error converting '" + q.dump() + "': expected value/unit object.");
	}
	return q.at("value").get<double>() * unit_in_si(q.at("unit").get<std::string>(), d);
}

} // namespace units

} // namespace magnumpi
