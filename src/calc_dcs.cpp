/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/potential.h"
#include "magnumpi/calc_dcs.h"
#include "magnumpi/consts.h"
#include "magnumpi/scat_colonna.h"
#include "magnumpi/scat_viehland.h"
#include "magnumpi/utility.h"
#include "plmath/clencurt.h"
#include "external/tk_spline/spline.h"
#include <vector>
#include <cmath>
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>

namespace magnumpi {

calc_dcs::calc_dcs(int points_per_degree,
		unsigned lower_cc,
		unsigned upper_cc,
		double E_low,
		double rmin,double rmax,double dr,
		int use_scat_Col,
		int interpol,
		double tol,
		const potential* pot)
 : m_points_per_degree(points_per_degree),
   m_resolution(1.0 / m_points_per_degree),
   m_dchi(m_resolution / 180.0 * constants::pi),
   m_lower(lower_cc),
   m_upper(upper_cc),
   m_integrator(-1.0, 1.0, m_lower, m_upper),
   m_rmin(rmin),m_rmax(rmax),
   m_use_scat_Colonna(use_scat_Col),
   m_interp(interpol),
   m_tolerance(tol),
   m_pot(*pot),
   m_triplets(pot, rmin, rmax, dr, E_low)
{
	unsigned num_points = 180 * m_points_per_degree;
	m_angles.resize(num_points);
	m_angles_rad.resize(num_points);
	for (unsigned i=0; i<num_points; ++i)
	{
		m_angles[i] = m_resolution / 2 + m_resolution * i;
		m_angles_rad[i] = m_angles[i] / 180.0 * constants::pi;
	}
}

double calc_dcs::integrate_DCS_fraction(std::vector<double>& DCS_temp, unsigned l) const
{
	double sigma = 0;
	//Note that [1 - (cos chi)^l] corresponds to the momentum transfer cross section
	for (unsigned i=0; i<DCS_temp.size(); ++i)
	{
		sigma += DCS_temp[i] * 2.0 * constants::pi * std::sin(m_angles_rad[i]) * (1.0 - std::pow(std::cos(m_angles_rad[i]),l)) * m_dchi;
	}
	return sigma;
}

void calc_dcs::update_DCS(std::vector<double>& DCS, std::vector<double>& DCS_temp) const
{
	for (unsigned i=0; i<DCS_temp.size(); ++i)
	{
		DCS[i] += DCS_temp[i];
	}
}

void calc_dcs::update_DCS_fraction(std::vector<double>& DCS_temp, std::vector<double>& b_vec, std::vector<double>& chi_vec, bool reset) const
{
	//debug mode
	const bool debug = false;
	//print inputs
	const bool print = false;
	if (print)
	{
		for (unsigned i=0; i<b_vec.size(); ++i)
		{
			std::cout << b_vec[i] << "\t" << chi_vec[i] << std::endl;
		}
	}
	if (reset)
	{
		DCS_temp.assign(DCS_temp.size(), 0.0);
	}
	//check monotonic sections
	unsigned low = 0;
	unsigned high = low;
	double sign = chi_vec[1] - chi_vec[0];
	for (unsigned i=1; i<chi_vec.size(); ++i)
	{
		if (sign * (chi_vec[i] - chi_vec[i-1]) > 0)
		{
			continue;
		}
		else
		{
			if (std::abs(sign) > 0)
			{
				//copy vector segments
				high = i - 1;
				std::vector<double> b_temp(high - low + 1, 0);
				std::vector<double> chi_temp(high - low + 1, 0);
				for (unsigned j=low; j<=high; ++j)
				{
					b_temp[j-low] = b_vec[j];
					chi_temp[j-low] = chi_vec[j];
				}
				//skip section when the size is smaller than 4 (needed by spline fit)
				//and db is smaller than 1*pm: this is most likely just noise
				if (chi_temp.size() > 4)
				{
					//call DCS update on vector segments
					if (debug)
					{
						std::cout << b_temp[0] << "\t" << b_temp[b_temp.size()-1] << "\t";
					}
					update_DCS_monotonic_fraction(DCS_temp, b_temp, chi_temp);
					if (debug)
					{
						double sigma = integrate_DCS_fraction(DCS_temp, 1);
						std::cout << sigma << std::endl;
					}
				}
				//update settings for next monotonic section
				sign = chi_vec[i] - chi_vec[i-1];
				low = i - 1;
				high = low;
			}
			else
			{
				//skip this part
				sign = chi_vec[i] - chi_vec[i-1];
				low = i - 1;
				high = low;
			}
		}
	}
	//also add last segment
	{
		//copy vector segments
		high = chi_vec.size()-1;
		std::vector<double> b_temp(high - low + 1, 0);
		std::vector<double> chi_temp(high - low + 1, 0);
		for (unsigned j=low; j<=high; ++j)
		{
			b_temp[j-low] = b_vec[j];
			chi_temp[j-low] = chi_vec[j];
		}
		//skip section when the size is smaller than 4 (needed by spline fit)
		//and db is smaller than 1*pm: this is most likely just noise
		if (chi_temp.size() > 4)
		{
			//call DCS update on vector segments
			update_DCS_monotonic_fraction(DCS_temp, b_temp, chi_temp);
		}
	}
}

void calc_dcs::update_DCS_monotonic_fraction(std::vector<double>& DCS_temp, std::vector<double>& b_vec, std::vector<double>& chi_vec) const
{
	//check whether there are zeros in the angle. When there are multiple zeros we remove all but the first one
	bool modify = false;
	const bool debug = false;
	unsigned index = chi_vec.size()-1;
	unsigned i=index - 1;
	while (1)
	{
		if (chi_vec[i+1] == 0 && chi_vec[i] == 0)
		{
			index = i;
		}
		else
		{
			break;
		}
		if (i == 0)
		{
			break;
		}
		--i;
	}
	if (index < chi_vec.size()-1)
	{
		modify = true;
	}
	//make spline fit
	tk::spline spline;
	if (!modify)
	{
		spline.set_points(chi_vec, b_vec);
	}
	else
	{
		std::vector<double>::const_iterator first = chi_vec.begin();
		std::vector<double>::const_iterator last = chi_vec.begin() + index + 1;
		std::vector<double> chi_vec_temp(first, last);

		std::vector<double>::const_iterator first_b = b_vec.begin();
		std::vector<double>::const_iterator last_b = b_vec.begin() + index + 1;
		std::vector<double> b_vec_temp(first_b, last_b);

		spline.set_points(chi_vec_temp, b_vec_temp);
	}
	//determine maximum and minimum angles
	double max_chi = std::max(chi_vec[0], chi_vec[chi_vec.size()-1]);
	double min_chi = std::min(chi_vec[0], chi_vec[chi_vec.size()-1]);
	if (std::isinf(min_chi))
	{
		throw std::runtime_error("Found scattering angle equal to -Inf or Inf");
	}
	//loop over all intervals of 0 - 180 until all angles have been added
	int min_mult;
	if (min_chi < 0)
	{
		min_mult = - std::floor(std::abs(min_chi) / constants::pi);
	}
	else
	{
		min_mult = std::floor(min_chi / constants::pi);
	}
	int max_mult;
	if (max_chi < 0)
	{
		max_mult = - std::ceil(std::abs(max_chi) / constants::pi);
	}
	else
	{
		max_mult = std::ceil(max_chi / constants::pi);
	}
	for (int mult = min_mult; mult <= max_mult; ++mult)
	{
		int angle1 = mult * constants::pi;
		int angle2 = (mult + 1) * constants::pi;
		bool flip = (std::abs(angle1) % 2*constants::pi) >= constants::pi;
		int min_angle = min_chi >= angle1 ? min_chi : angle1;
		int max_angle = max_chi >= angle2 ? angle2 : max_chi;
		for (unsigned i=0; i<m_angles_rad.size(); ++i)
		{
			if ((m_angles_rad[i] >= min_angle - angle1) && (m_angles_rad[i] <= max_angle - angle1))
			{
				double b_temp = spline(m_angles_rad[i]);
				double db_dchi = spline.deriv1(m_angles_rad[i]);
				double dcs_temp = std::abs(b_temp / std::sin(m_angles_rad[i]) * db_dchi);
				if (debug)
				{
					std::cout << m_angles[i] << "\t" << b_temp << "\t" << db_dchi << "\t" << dcs_temp << "\t" << b_vec[0]
						<< "\t" << b_vec[b_vec.size()-1] << "\t" << chi_vec[0] * 180 / constants::pi << "\t"
						<< chi_vec[chi_vec.size()-1] * 180 / constants::pi << std::endl;
				}
				//skip entry when predicted b is not in the b range
				if (b_temp < std::min(b_vec[0],b_vec[b_vec.size()-1]) || b_temp > std::max(b_vec[0],b_vec[b_vec.size()-1]))
				{
					continue;
				}
				if (flip == false)
				{
					DCS_temp[i] += std::abs(dcs_temp);
				}
				else
				{
					//remember: 181 degrees now corresponds to 179 on a grid of 0-180 degrees
					DCS_temp[DCS_temp.size() - 1 - i] += std::abs(dcs_temp);
				}
			}
		}
	}
}

void calc_dcs::Viehland_DCS_case_1(std::vector<double>& DCS, double energy, const triplet_data::case_data& data) const
{
	std::vector<double> DCS_temp(DCS.size(),0);
	unsigned order;
	//write b vs chi vector to b_chi.txt
	bool debug = true;

	//part 1: 0 <= b <= b1
	order = m_lower;
	double residue = 1;
	double sigma = 0;
	double sigma_old = 0;
	while (order <= m_upper && residue > m_tolerance)
	{
		sigma_old = sigma;
		unsigned num_points = std::pow(2, order) + 1;
		std::vector<double> b_vec(num_points, 0);
		std::vector<double> chi_vec(num_points, 0);
		for (unsigned i = 0; i<num_points; ++i)
		{
			double y = m_integrator.cc_position(order, i);
			b_vec[i] = data.b_vec[0] * std::cos(constants::pi * (y + 1.0)/4.0);
			if (m_use_scat_Colonna == 1)
			{
				chi_vec[i] = scat_Colonna(&m_pot, b_vec[i], energy, m_tolerance, m_interp);
			}
			else
			{
				// initialize with lowest r0 value r_vec[0]
				// note: this also adjusts b
				const double r0=scat_viehland::find_r0_case_1(m_pot, energy, &b_vec[i], data.r_vec[0]);
				chi_vec[i] = scat_viehland::scat1(m_integrator, m_pot, energy, b_vec[i], r0, m_tolerance);
			}
		}
		update_DCS_fraction(DCS_temp, b_vec, chi_vec, true);
		sigma = integrate_DCS_fraction(DCS_temp, 1);
		if (sigma > 0)
		{
			residue = std::abs(sigma - sigma_old) / sigma;
		}
		++order;
		if ((order >= m_upper || residue <= m_tolerance) && debug)
		{
			std::ofstream fid("b_chi.txt");
			for (unsigned i=0; i<b_vec.size(); ++i)
			{
				fid << std::setprecision(10) << b_vec[i] << "\t" << chi_vec[i] << std::endl;
			}
		}
	}
	update_DCS(DCS, DCS_temp);
	DCS_temp.assign(DCS_temp.size(), 0.0);
	//part 2: bi <= b <= bi+1
	for (unsigned k=0; k!=data.b_vec.size()-1; ++k)
	{
		order = m_lower;
		residue = 1;
		sigma = 0;
		sigma_old = 0;
		//average radius, eq 26
		double r_avr=(data.r_vec[k] + data.r_vec[k+1])/2.0;
		while (order <= m_upper && residue > m_tolerance)
		{
			sigma_old = sigma;
			unsigned num_points = std::pow(2, order) + 1;
			std::vector<double> b_vec(num_points, 0);
			std::vector<double> chi_vec(num_points, 0);
			//for debugging purposes
			std::vector<double> b_vec1, chi_vec1;
			//part 1:
			for (unsigned i = 0; i<num_points; ++i)
			{
				double y = m_integrator.cc_position(order, i);
				double r1, x1;
				const double xi1=4.0/constants::pi*std::asin(data.r_vec[k]/r_avr)-1.0;
				if (y==-1)
				{
					/* shortcut. The result of the else branch is the
					 * same when evaluated for y==-1, which would boil
					 * down to r_avr*sin(asin(r_vec[i]/r_avr)).
					 */
					x1=xi1;
					r1=data.r_vec[k];
				}
				else
				{
					// equation 28
					x1=( (1.0-xi1)*y+xi1+1.0)/2.0;
					// equation 27
					r1=r_avr*sin(constants::pi/4.0*(x1+1.0));
					/** \todo Here we calculate a sine, below (eq 34)
					 *  we need the sin of twice that angle. See if
					 *  this can be optimized. (Then we may also not
					 *  need to keep x1, except for the calculation of
					 *  that sine.)
					 */
				}
				// equation 24
				double temp=1.0-m_pot.value(r1)/energy;
				double b;
				if (temp >= 0)
				{
					b=r1*std::sqrt(temp);
				}
				else
				{
					b = 0;
				}
				b_vec[i] = b;
				if (m_use_scat_Colonna)
				{
					chi_vec[i] = scat_Colonna(&m_pot, b, energy, m_tolerance, m_interp);
				}
				else
				{
					adjust_b(m_pot, energy,&b,r1);
					chi_vec[i] = scat_viehland::scat1(m_integrator, m_pot, energy, b, r1, m_tolerance);
				}
			}
			update_DCS_fraction(DCS_temp, b_vec, chi_vec, true);
			if (debug)
			{
				b_vec1 = b_vec;
				chi_vec1 = chi_vec;
			}
			b_vec.assign(b_vec.size(), 0);
			chi_vec.assign(chi_vec.size(), 0);
			//part 2:
			for (unsigned i = 0; i<num_points; ++i)
			{
				double y = m_integrator.cc_position(order, i);
				double r2, x2;
				// equation 32
				const double xi2=4.0/constants::pi*std::acos(r_avr/ data.r_vec[k+1])-1.0;
				if (y==1)
				{
					// shortcut. see the comments on 'if (y==-1)'
					// for the first piece.
					x2=xi2;
					r2=data.r_vec[k+1];
				}
				else
				{
					// equation 31
					x2=( (1.0+xi2)*y+xi2-1.0)/2.0;
					// equation 30
					r2=data.r_vec[k+1]*cos(constants::pi/4.0*(x2+1.0));
				}
				double temp=1.0-m_pot.value(r2)/energy;
				double b;
				if (temp >= 0)
				{
					b=r2*std::sqrt(temp);
				}
				else
				{
					b = 0;
				}
				b_vec[i] = b;
				if (m_use_scat_Colonna)
				{
					chi_vec[i] = scat_Colonna(&m_pot, b, energy, m_tolerance, m_interp);
				}
				else
				{
					adjust_b(m_pot, energy,&b,r2);
					chi_vec[i] = scat_viehland::scat1(m_integrator, m_pot, energy, b, r2, m_tolerance);
				}
			}
			update_DCS_fraction(DCS_temp, b_vec, chi_vec, false);
			sigma = integrate_DCS_fraction(DCS_temp, 1);
			if (sigma > 0)
			{
				residue = std::abs(sigma - sigma_old) / sigma;
			}
			//for debugging
			if ((order >= m_upper || residue <= m_tolerance) && debug)
			{
				std::ofstream fid("b_chi.txt", std::ofstream::app);
				for (unsigned i=0; i<b_vec1.size(); ++i)
				{
					fid << std::setprecision(10) << b_vec1[i] << "\t" << chi_vec1[i] << std::endl;
				}
				for (unsigned i=0; i<b_vec.size(); ++i)
				{
					fid << std::setprecision(10) << b_vec[i] << "\t" << chi_vec[i] << std::endl;
				}
			}
			++order;
		}
		update_DCS(DCS, DCS_temp);
		DCS_temp.assign(DCS_temp.size(), 0.0);
	}
	//part 3: bi <= b <= infinity
	order = m_lower;
	residue = 1;
	sigma = 0;
	sigma_old = 0;
	while (order <= m_upper && residue > m_tolerance)
	{
		sigma_old = sigma;
		unsigned num_points = std::pow(2, order) + 1;
		std::vector<double> b_vec(num_points, 0);
		std::vector<double> chi_vec(num_points, 0);
		for (unsigned i = 0; i<num_points; ++i)
		{
			double y = m_integrator.cc_position(order, i);
			// eqn 41
			const double x3=(1+y)/2;
			// eqn 39
			double r3=data.r_vec[data.r_vec.size()-1]/std::sin((constants::pi/2.0)*x3);
			double temp = 1.0-m_pot.value(r3)/energy;
			double b;
			if (temp >= 0)
			{
				b=r3*std::sqrt(temp);
			}
			else
			{
				b = 0;
			}
			b_vec[i] = b;
			if (m_use_scat_Colonna)
			{
				if (std::isinf(b))
				{
					chi_vec[i] = 0;
				}
				else
				{
					chi_vec[i]=scat_Colonna(&m_pot, b, energy, m_tolerance, m_interp);
				}
			}
			else
			{
				adjust_b(m_pot, energy, &b, r3);
				chi_vec[i]=scat_viehland::scat1(m_integrator, m_pot, energy, b, r3, m_tolerance);
			}
		}
		update_DCS_fraction(DCS_temp, b_vec, chi_vec, true);
		sigma = integrate_DCS_fraction(DCS_temp, 1);
		if (sigma > 0)
		{
			residue = std::abs(sigma - sigma_old) / sigma;
		}
		//for debugging
		if ((order >= m_upper || residue <= m_tolerance) && debug)
		{
			std::ofstream fid("b_chi.txt", std::ofstream::app);
			for (unsigned i=0; i<b_vec.size(); ++i)
			{
				fid << std::setprecision(10) << b_vec[i] << "\t" << chi_vec[i] << std::endl;
			}
		}
		++order;
	}
	update_DCS(DCS, DCS_temp);
}

void calc_dcs::Viehland_DCS_case_2(std::vector<double>& DCS, double energy, const triplet_data::case_data& data) const
{
	std::vector<double> DCS_temp(DCS.size(),0);
	unsigned order;
	const double rc_tilde = data.rc_tilde;
	const double r0_tilde = data.r0_tilde;
	//write b vs chi vector to b_chi.txt
	bool debug = true;

	//part 1:
	order = m_lower;
	double residue = 1;
	double sigma = 0;
	double sigma_old = 0;
	while (order <= m_upper && residue > m_tolerance)
	{
		sigma_old = sigma;
		unsigned num_points = std::pow(2, order) + 1;
		std::vector<double> b_vec(num_points, 0);
		std::vector<double> chi_vec(num_points, 0);
		for (unsigned i = 0; i<num_points; ++i)
		{
			double y = m_integrator.cc_position(order, i);
			const double r4=((rc_tilde-r0_tilde)*y+rc_tilde+r0_tilde)/2.0;
			const double temp = 1.0-m_pot.value(r4)/energy;
			double b4;
			if (temp >= 0)
			{
				b4=r4*std::sqrt(temp);
			}
			else
			{
				b4 = 0;
			}
			b_vec[i] = b4;
			if (m_use_scat_Colonna==1)
			{
				chi_vec[i]=scat_Colonna(&m_pot, b4, energy, m_tolerance, m_interp);
			}
			else
			{
				adjust_b(m_pot, energy, &b4, r4);
				chi_vec[i]=scat_viehland::scat1(m_integrator, m_pot, energy, b4, r4, m_tolerance);
			}
		}
		update_DCS_fraction(DCS_temp, b_vec, chi_vec, true);
		sigma = integrate_DCS_fraction(DCS_temp, 1);
		if (sigma > 0)
		{
			residue = std::abs(sigma - sigma_old) / sigma;
		}
		//for debugging
		if ((order >= m_upper || residue <= m_tolerance) && debug)
		{
			std::ofstream fid("b_chi.txt");
			//write in reversed order (y first = 1 and y last = -1)
			int i = b_vec.size()-1;
			while (i >= 0)
			{
				fid << std::setprecision(10) << b_vec[i] << "\t" << chi_vec[i] << std::endl;
				--i;
			}
		}
		++order;
	}
	update_DCS(DCS, DCS_temp);
	DCS_temp.assign(DCS_temp.size(), 0.0);
	//part 2:
	order = m_lower;
	residue = 1;
	sigma = 0;
	sigma_old = 0;
	while (order <= m_upper && residue > m_tolerance)
	{
		sigma_old = sigma;
		unsigned num_points = std::pow(2, order) + 1;
		std::vector<double> b_vec(num_points, 0);
		std::vector<double> chi_vec(num_points, 0);
		for (unsigned i = 0; i<num_points; ++i)
		{
			double y = m_integrator.cc_position(order, i);
			const double r5=rc_tilde/std::sin(constants::pi*(1.0+y)/4.0);
			double temp=1.0-m_pot.value(r5)/energy;
			double b5;
			if (temp >= 0)
			{
				b5=r5*std::sqrt(temp);
			}
			else
			{
				b5 = 0;
			}
			b_vec[i] = b5;
			if (m_use_scat_Colonna==1)
			{
				if (std::isinf(b5))
				{
					chi_vec[i] = 0;
					//also adjust b input
					double y2 = m_integrator.cc_position(order,i-1);
					double y_avr = (-1 + y2)/2;
					b_vec[i] = rc_tilde/std::sin(constants::pi*(1.0+y_avr)/4.0);
				}
				else
				{
					chi_vec[i]=scat_Colonna(&m_pot, b5, energy, m_tolerance, m_interp);
				}
			}
			else
			{
				adjust_b(m_pot, energy, &b5, r5);
				chi_vec[i]=scat_viehland::scat1(m_integrator, m_pot, energy, b5, r5, m_tolerance);
			}
		}
		update_DCS_fraction(DCS_temp, b_vec, chi_vec, true);
		sigma = integrate_DCS_fraction(DCS_temp, 1);
		if (sigma > 0)
		{
			residue = std::abs(sigma - sigma_old) / sigma;
		}
		//for debugging
		if ((order >= m_upper || residue <= m_tolerance) && debug)
		{
			std::ofstream fid("b_chi.txt", std::ofstream::app);
			for (unsigned i=0; i<b_vec.size(); ++i)
			{
				fid << std::setprecision(10) << b_vec[i] << "\t" << chi_vec[i] << std::endl;
			}
		}
		++order;
	}
	update_DCS(DCS, DCS_temp);
}

void calc_dcs::Viehland_DCS_case_3(std::vector<double>& DCS, double energy, const triplet_data::case_data& data) const
{
	std::vector<double> DCS_temp(DCS.size(),0);
	unsigned order;
	//write b vs chi vector to b_chi.txt
	bool debug = true;

	//part 1:
	order = m_lower;
	double residue = 1;
	double sigma = 0;
	double sigma_old = 0;
	while (order <= m_upper && residue > m_tolerance)
	{
		sigma_old = sigma;
		unsigned num_points = std::pow(2, order) + 1;
		std::vector<double> b_vec(num_points, 0);
		std::vector<double> chi_vec(num_points, 0);
		for (unsigned i = 0; i<num_points; ++i)
		{
			double y = m_integrator.cc_position(order, i);
			const double b4 = data.b_split*(y + 1.0) / 2.0;
			b_vec[i] = b4;
			if (m_use_scat_Colonna==1)
			{
				chi_vec[i]=scat_Colonna(&m_pot, b4, energy, m_tolerance, m_interp);
			}
			else
			{
				const double r0_p1=scat_viehland::find_r0_case_3(m_pot, m_rmin, m_rmax, energy,&b4);
				chi_vec[i]=scat_viehland::scat1(m_integrator, m_pot, energy, b4, r0_p1, m_tolerance);
			}
		}
		update_DCS_fraction(DCS_temp, b_vec, chi_vec, true);
		sigma = integrate_DCS_fraction(DCS_temp, 1);
		if (sigma > 0)
		{
			residue = std::abs(sigma - sigma_old) / sigma;
		}
		//for debugging
		if ((order >= m_upper || residue <= m_tolerance) && debug)
		{
			std::ofstream fid("b_chi.txt");
			//write in reversed order
			int i=b_vec.size() - 1;
			while (i >= 0)
			{
				fid << std::setprecision(10) << b_vec[i] << "\t" << chi_vec[i] << std::endl;
				--i;
			}
		}
		++order;
	}
	update_DCS(DCS, DCS_temp);
	DCS_temp.assign(DCS_temp.size(), 0.0);
	//part 2:
	order = m_lower;
	residue = 1;
	sigma = 0;
	sigma_old = 0;
	while (order <= m_upper && residue > m_tolerance)
	{
		sigma_old = sigma;
		unsigned num_points = std::pow(2, order) + 1;
		std::vector<double> b_vec(num_points, 0);
		std::vector<double> chi_vec(num_points, 0);
		for (unsigned i = 0; i<num_points; ++i)
		{
			double y = m_integrator.cc_position(order, i);
			const double b5 = data.b_split/(y + 1.0) * 2;
			b_vec[i] = b5;
			if (m_use_scat_Colonna==1)
			{
				if (std::isinf(b5))
				{
					chi_vec[i] = 0;
				}
				else
				{
					chi_vec[i]=scat_Colonna(&m_pot, b5, energy, m_tolerance, m_interp);
				}
			}
			else
			{
				const double r0_p2=scat_viehland::find_r0_case_3(m_pot, m_rmin, m_rmax, energy,&b5);
				chi_vec[i]=scat_viehland::scat1(m_integrator, m_pot, energy, b5, r0_p2, m_tolerance);
			}
		}
		update_DCS_fraction(DCS_temp, b_vec, chi_vec, true);
		sigma = integrate_DCS_fraction(DCS_temp, 1);
		if (sigma > 0)
		{
			residue = std::abs(sigma - sigma_old) / sigma;
		}
		//for debugging
		if ((order >= m_upper || residue <= m_tolerance) && debug)
		{
			std::ofstream fid("b_chi.txt", std::ofstream::app);
			for (unsigned i=0; i<b_vec.size(); ++i)
			{
				fid << std::setprecision(10) << b_vec[i] << "\t" << chi_vec[i] << std::endl;
			}
		}
		++order;
	}
	update_DCS(DCS, DCS_temp);
}

std::vector<double> calc_dcs::integrate(double energy) const
{
	std::vector<double> DCS(m_angles.size(),0);
	const triplet_data::case_data data(m_triplets, energy);
	/*
	 * Use information from triplets to split the integral in smaller pieces
	 * In each piece we start with a given number of points and then we double the number of points until
	 * the total CS for that piece converges
	 */
	switch (data.case_id)
	{
		case 1:
			//Viehland case 1: Ed<=E<=Ec
			Viehland_DCS_case_1(DCS, energy, data);
			break;
		case 2:
			//Viehland case 2: Ec <= E <= 3*Ec
			Viehland_DCS_case_2(DCS, energy, data);
			break;
		case 3:
			//Viehland case 3: E > 3*Ec or E < Ed
			Viehland_DCS_case_3(DCS, energy, data);
			break;
	}
	return DCS;
}

data_set calc_dcs::integrate(const std::vector<double>& energy_vals) const
{
	data_set dcs_data(
		"dcs ", units::area, "m^2",
		{"angle", units::angle, "deg"},
		{"E"} );
	dcs_data.row_axis().set_display_unit("deg");
	//add column titles
	for (unsigned i=0; i<energy_vals.size(); ++i)
	{
		dcs_data.add_column(std::to_string(energy_vals[i]));
	}

	std::cout << "Starting calculation of differential cross section" << std::endl;

	//add x-values (angles)
	for (unsigned r=0; r<m_angles.size(); ++r)
	{
		dcs_data.add_row(m_angles[r]);
	}
	//add y-values
	for (unsigned c=0; c<energy_vals.size(); ++c)
	{
		std::cout << "Energy: " << energy_vals[c] / constants::eV << "*eV" << std::endl;
		std::vector<double> DCS = integrate(energy_vals[c]);
		//determine cross section size
		double sigma = integrate_DCS_fraction(DCS, 1);
		std::cout << "cs\t" << sigma << "*m^2" << std::endl;
		//write output data
		for (unsigned r=0; r<m_angles.size(); ++r)
		{
			dcs_data(r,c) = DCS[r];
		}
	}
	return dcs_data;
}

data_set calc_dcs::integrate(double logE_low, double logE_high, unsigned logE_pdecade) const
{
	std::vector<double> energy_vals = make_samples_log(logE_low,logE_high,logE_pdecade);
	return integrate(energy_vals);
}

data_set calc_diff_cross_sec(int points_per_degree, unsigned lower_cc, unsigned upper_cc, double logE_low,double logE_high,int logE_pdecade,
	double rmin,double rmax,double dr,int use_scat_Col,int interpol,double tol,const potential* pot)
{
	//make cross section object
	calc_dcs m_dcs(points_per_degree, lower_cc, upper_cc, std::pow(10,logE_low),
		rmin,rmax,dr,use_scat_Col,interpol,tol,pot);
	//perform integration
	return m_dcs.integrate(logE_low,logE_high,logE_pdecade);
}

data_set calc_diff_cross_sec(const json_type& cnf, const potential* pot)
{
	//perform integration
	std::cout << "Energy range min: ";
	const double logE_low=std::log10(in_si(cnf.at("energy_range").at("min"),units::energy));
	std::cout << std::pow(10,logE_low) / constants::eV << "*eV" << std::endl;
	std::cout << "Energy range max: ";
	const double logE_high=std::log10(in_si(cnf.at("energy_range").at("max"),units::energy));
	std::cout << std::pow(10,logE_high) / constants::eV << "*eV" << std::endl;
	std::cout << "Energy range points per decade: ";
	const double logE_pdecade=cnf.at("energy_range").at("points_per_decade");
	std::cout << logE_pdecade << std::endl;
	std::cout << "DCS points per degree: ";
	const int points_per_degree=in_si(cnf.at("dcs").at("points_per_degree"),units::scalar);
	std::cout << points_per_degree << std::endl;
	std::cout << "cc lower: ";
	const unsigned lower_cc=cnf.at("cc").at("lower");
	std::cout << lower_cc << std::endl;
	std::cout << "cc upper: ";
	const unsigned upper_cc=cnf.at("cc").at("upper");
	std::cout << upper_cc << std::endl;
	std::cout << "r, rmin: ";
	const double rmin=in_si(cnf.at("r").at("rmin"), units::length);
	std::cout << rmin << "*m" << std::endl;
	std::cout << "r, rmax: ";
	const double rmax=in_si(cnf.at("r").at("rmax"), units::length);
	std::cout << rmax << "*m" << std::endl;
	std::cout << "r, del: ";
	const double dr=in_si(cnf.at("r").at("del"), units::length);
	std::cout << dr << "*m" << std::endl;
	//should this be made configurable?
	const bool use_scat_Colonna = true;
	//should this be made configurable?
	const int interpol=3;
	std::cout << "tolerance: ";
	const double tolerance=cnf.at("tolerance");
	std::cout << tolerance << std::endl;
	return calc_diff_cross_sec(points_per_degree, lower_cc, upper_cc,logE_low,logE_high,logE_pdecade,
		rmin, rmax, dr, use_scat_Colonna, interpol, tolerance, pot);
}

data_set calc_diff_cross_sec(const json_type& cnf, const json_type& pot)
{
	std::cout << "Setting up potential" << std::endl;
	const std::unique_ptr<const potential> p{potential::create(pot)};
	std::cout << "Calculating DCS ..." << std::endl;
	return calc_diff_cross_sec(cnf,p.get());
}

data_set calc_diff_cross_sec(const json_type& cnf)
{
	return calc_diff_cross_sec(cnf,cnf.at("interaction").at("potential"));
}

} //namespace mangumpi
