/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/potential.h"
#include "magnumpi/calc_cs.h"
#include "magnumpi/consts.h"
#include "magnumpi/scat_colonna.h"
#include "magnumpi/scat_viehland.h"
#include "magnumpi/utility.h"
#include "plmath/clencurt.h"
#include <vector>
#include <cmath>
#include <string>
#include <iostream>

namespace magnumpi {

/** Case 1 matches \cite Viehland2010 section 5.1
 */
class calc_cs::Viehland_case_1_sub
{
public:
	using result_type = double;
	using argument_type = double;
	Viehland_case_1_sub(const calc_cs& cs, double E, const triplet_data::case_data& data);
	void operator()(double y,std::vector<double>& h) const;
private:
	const calc_cs& m_cs;
	const double m_energy;
	const triplet_data::case_data& m_data;
};

calc_cs::Viehland_case_1_sub::Viehland_case_1_sub(const calc_cs& cs, double E, const triplet_data::case_data& data)
 : m_cs(cs), m_energy(E), m_data(data)
{
#if 0
	//write orbiting parameters
	for (unsigned i=0;i<m_b_vec.size();++i)
	{
		const double b = m_data.b_vec[i];
		const double r = m_data.r_vec[i];
		std::cout << "b\t" << b
			<< "\tr\t" << r
			//<< "\t" << scat_Col_get_r(b,m_energy,m_cs.m_tolerance,m_cs.interp)
			<< "\t" << (1.0-pow(b/r,2)-m_cs.m_pot.value(r)/m_energy)
			<< std::endl;
	}
#endif
}

void calc_cs::Viehland_case_1_sub::operator()(double y, std::vector<double>& h) const
{
	/// \todo Here, and in all other integrand implementations:
	// using std::pow
	// and perhaps (or qualify these):
	// using std::asin
	// using std::sin
	// using std::cos
	assert(h.size() == m_cs.m_l_max);
	// for brevity of the implementation below:
	const std::vector<double>& b_vec = m_data.b_vec;
	const std::vector<double>& r_vec = m_data.r_vec;

	assert(b_vec.size()>0);
	assert(b_vec.size()==r_vec.size());

	//*************************PART 1**********************************
	//calculate impact parameter and scattering angle for part 1 of case 1
	if (y>-1 && y<1)
	{
		//discretization of impact parameter
		// equation 21
		double b = b_vec[0] *cos(constants::pi *(y+1.0)/4.0);
		double scat;
		if (m_cs.m_use_scat_Colonna==1)
		{
			scat=scat_Colonna(&m_cs.m_pot,b,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			// initialize with lowest r0 value r_vec[0]
			// note: this also adjusts b
			const double r0=scat_viehland::find_r0_case_1(m_cs.m_pot,m_energy,&b,r_vec[0]);
			scat=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b,r0,m_cs.m_tolerance);
		}

		//add contribution to output
		const double aux = pow(constants::pi,2)/4.0*pow(b_vec[0],2)*sin(constants::pi*(y+1.0)/2.0);
		for (unsigned l=1; l<=m_cs.m_l_max;++l)
		{
			// equation 23
			h[l-1]=aux*(1.0-pow(cos(scat),l));
		}
	}
	else
	{
		for (unsigned l=1; l<=m_cs.m_l_max;++l)
		{
			h[l-1]=0.0;
		}
	}
	//**************************PART 2*********************************
	//part 2: multiple orbiting b_i<=b<=b_{i+1}, eq 24-36
	//        this loop is visited if b_vec.size()>1.
	for (unsigned i=0;i!=b_vec.size()-1;++i)
	{
		// average radius, eq 26
		double r_avr=(r_vec[i]+r_vec[i+1])/2.0;
		/* The integration in the r-domain (equation 25) is
		 * split in parts [r_i,r_avg] and [r_avg,r_{i+1}].
		 * For ech interval a transformation is done that maps
		 * that interval to y in [-1,1]. The result is that
		 * this integrand consists of two terms (see eqn 34),
		 * each representing one of these pieces. In the special
		 * cases of y==-1 and y==1, only one term is present,
		 * as expressed by eqns. 35 and 36, respectively.
		 */
		if (y<1)
		{
			double r1,x1;
			//part r1
			// equation 29
			const double xi1=4.0/constants::pi*asin(r_vec[i]/r_avr)-1.0;
			if (y==-1)
			{
				/* shortcut. The result of the else branch is the
				 * same when evaluated for y==-1, which would boil
				 * down to r_avr*sin(asin(r_vec[i]/r_avr)).
				 */
				x1=xi1;
				r1=r_vec[i];
			}
			else
			{
				// equation 28
				x1=( (1.0-xi1)*y+xi1+1.0)/2.0;
				// equation 27
				r1=r_avr*sin(constants::pi/4.0*(x1+1.0));
				/** \todo Here we calculate a sine, below (eq 34)
				 *  we need the sin of twice that angle. See if
				 *  this can be optimized. (Then we may also not
				 *  need to keep x1, except for the calculation of
				 *  that sine.)
				 */
			}
			// equation 24
			const double b=r1*sqrt(1.0-m_cs.m_pot.value(r1)/m_energy);
			/// \todo Why is m_cs.m_use_scat_Colonna==0 not supported here?
			double scat;

			//if (m_cs.m_use_scat_Colonna==1)
			//{
				scat=scat_Colonna(&m_cs.m_pot,b,m_energy,m_cs.m_tolerance,m_cs.m_interp);
			//}
			//else
			//{
			//adjust_b(m_energy,&b,r1);
					//std::cout << y << "\t" << r1 << "\t" << b << "\t"
			//	<< m_cs.m_pot.value(r1)<< "\t" << 1.0-pow(b/r1,2) - m_cs.m_pot.value(r1)/m_energy << std::endl;
			//scat=m_cs.m_scat_viehland.scat2(m_cs.m_pot,m_energy,b,r_avr,r1,coeff);
			//scat=m_cs.m_scat_viehland.scat1(m_cs.m_pot,m_energy,b,r1,coeff);
			//}

			const double dVdr=m_cs.m_pot.dVdr(r1);

			/*
			//check whether number is positive
			if (1.0-m_cs.m_pot.value(r1)/m_energy-r1/2.0*dVdr/m_energy < 0)
			{
				std::cout << "case 2 1" << y << "\t" << 1.0-m_cs.m_pot.value(r1)/m_energy-r1/2.0*dVdr/m_energy << std::endl;
			}
			*/
			const double aux =
				pow(constants::pi,2)/8.0*pow(r_avr,2)*(1.0-xi1)
				*(1.0-m_cs.m_pot.value(r1)/m_energy-r1/2.0*dVdr/m_energy)
				*sin(constants::pi*(1.0+x1)/2.0);
			for (unsigned l=1; l<=m_cs.m_l_max;++l)
			{
				// euation 34 (first term)
				h[l-1] += aux*(1.0-pow(cos(scat),l));
			}
		}

		if (y>-1)
		{
			double x2,r2;
			// equation 32
			const double xi2=4.0/constants::pi*acos(r_avr/ r_vec[i+1])-1.0;
			if (y==1)
			{
				// shortcut. see the comments on 'if (y==-1)'
				// for the first piece.
				x2=xi2;
				r2=r_vec[i+1];
			}
			else
			{
				// equation 31
				x2=( (1.0+xi2)*y+xi2-1.0)/2.0;
				// equation 30
				r2=r_vec[i+1]*cos(constants::pi/4.0*(x2+1.0));
			}
			const double b=r2*sqrt(1.0-m_cs.m_pot.value(r2)/m_energy);
			/// \todo Why is m_cs.m_use_scat_Colonna==0 not supported here?
			double scat;

			//if (m_cs.m_use_scat_Colonna==1)
			//{
				scat=scat_Colonna(&m_cs.m_pot,b,m_energy,m_cs.m_tolerance,m_cs.m_interp);
			//}
			//else
			//{
			//	adjust_b(m_energy,&b,r2);
			//std::cout << y << "\t" << r2 << "\t" << b << "\t" << 1.0-pow(b/r2,2) - m_cs.m_pot.value(r2)/m_energy << std::endl;
			//	scat=m_cs.m_scat_viehland.scat1(m_cs.m_pot,m_energy,b,r2,coeff);
			//}

			const double dVdr=m_cs.m_pot.dVdr(r2);

			/*
			//check whether number is positive
			if (1.0-m_cs.m_pot.value(r2)/m_energy-r2/2.0*dVdr/m_energy < 0)
			{
				std::cout << "case 2 2" << y << "\t" << 1.0-m_cs.m_pot.value(r2)/m_energy-r2/2.0*dVdr/m_energy << std::endl;
			}
			*/
			const double aux =
				pow(constants::pi,2)/8.0*pow(r_vec[i+1],2)*(1.0+xi2)
				*(1.0-m_cs.m_pot.value(r2)/m_energy-r2/2.0*dVdr/m_energy)
				*sin(constants::pi*(1.0+x2)/2.0);
			for (unsigned l=1; l<=m_cs.m_l_max;++l)
			{
				// euation 34 (second term)
				h[l-1] += aux*(1.0-pow(cos(scat),l));

			}
		}
	}
	//**********************PART 3******************************************
	//part 3: b_last<=b<=infinity, eq 37-46
	if (y>-1 && y<1)
	{
		// eqn. 41
		const double x3=(1+y)/2;
		// eqn. 39:
		double r3=r_vec[r_vec.size()-1]/sin((constants::pi/2.0)*x3);
		double b=r3*sqrt(1.0-m_cs.m_pot.value(r3)/m_energy);
		double scat;

		if (m_cs.m_use_scat_Colonna==1)
		{
			scat=scat_Colonna(&m_cs.m_pot,b,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			/** \todo this appears to be eqn. 38. But the comment of adjust_b says
			 *  that that implements eqn. 24. Is that the same function, just called
			 *  with different parameters?
			 */
			adjust_b(m_cs.m_pot,m_energy,&b,r3);
			scat=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b,r3,m_cs.m_tolerance);
		}
		const double dVdr=m_cs.m_pot.dVdr(r3);
		/*
		//check whether number is positive
		if (1.0-m_cs.m_pot.value(r3)/m_energy-r3/2.0*dVdr/m_energy < 0 )
		{
			std::cout << "case 3 " << y << "\t" << r3 << "\t" << 1.0-m_cs.m_pot.value(r3)/m_energy-r3/2.0*dVdr/m_energy
				<< "\t" << 1.0-pow(b/r3,2)-m_cs.m_pot.value(r3)/m_energy << std::endl;
		}
		*/
		// equation 43
		const double aux =
			pow(constants::pi,2)/2.0*pow(r_vec[r_vec.size()-1],2)
			*(1.0-m_cs.m_pot.value(r3)/m_energy-r3/2.0*dVdr/m_energy)
			*pow(r3/r_vec[r_vec.size()-1],3)
			*cos(constants::pi*(1.0+y)/4.0);
		if (1.0-m_cs.m_pot.value(r3)/m_energy-r3/2.0*dVdr/m_energy >= 0)
		{
			for (unsigned l=1; l<=m_cs.m_l_max;++l)
			{
				h[l-1] += aux*(1.0-pow(cos(scat),l));
			}

		}
	}
}

class calc_cs::Viehland_case_2_sub
{
public:
	using result_type = double;
	using argument_type = double;
	Viehland_case_2_sub(const calc_cs& cs, double E, const triplet_data::case_data& data);
	void operator()(double y,std::vector<double>& h) const;
private:
	const calc_cs& m_cs;
	const double m_energy;
	const triplet_data::case_data& m_data;
};

calc_cs::Viehland_case_2_sub::Viehland_case_2_sub(const calc_cs& cs, double E, const triplet_data::case_data& data)
 : m_cs(cs), m_energy(E), m_data(data)
{
}

void calc_cs::Viehland_case_2_sub::operator()(double y, std::vector<double>& h) const
{
	const double rc_tilde = m_data.rc_tilde;
	const double r0_tilde = m_data.r0_tilde;

	//***************PART 1*************************************************
	if (y>-1)
	{
		//discretization of the distance of closest approach
		const double r4=((rc_tilde-r0_tilde)*y+rc_tilde+r0_tilde)/2.0;
		double b4=r4*sqrt(1.0-m_cs.m_pot.value(r4)/m_energy);

		double scat;
		if (m_cs.m_use_scat_Colonna==1)
		{
			scat=scat_Colonna(&m_cs.m_pot,b4,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			adjust_b(m_cs.m_pot,m_energy,&b4,r4);
			scat=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b4,r4,m_cs.m_tolerance);
		}
		const double dVdr=m_cs.m_pot.dVdr(r4);

		const double aux = constants::pi*(rc_tilde-r0_tilde)
			*(1.0-m_cs.m_pot.value(r4)/m_energy-r4/2.0*dVdr/m_energy)*r4;
		for (unsigned l=1;l<=m_cs.m_l_max;l++)
		{
			// equation 53
			h[l-1] = aux*(1.0-pow(cos(scat),l));
		}
	}
	else
	{
		const double r4=r0_tilde;
		const double dVdr=m_cs.m_pot.dVdr(r4);
		/** \todo See how to present this. scat is unused because cos(scat)=-1
		 *        is used directly in the code below.
		 */
		//const double scat=constants::pi;
		for (unsigned l=1;l<=m_cs.m_l_max;l++)
		{
			//eq 54
			h[l-1] = constants::pi*r4*(rc_tilde-r0_tilde)*(1.0-pow(-1,l))*(-r4/2.0*dVdr/m_energy);
		}
	}
	//************************PART 2************************************
	//part 2: r5, eq 51
	if (y>-1 && y<1)
	{
		const double r5=rc_tilde/sin(constants::pi*(1.0+y)/4.0);
		double b5=r5*sqrt(1.0-m_cs.m_pot.value(r5)/m_energy);
		double scat;
		if (m_cs.m_use_scat_Colonna==1)
		{
			scat=scat_Colonna(&m_cs.m_pot,b5,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			adjust_b(m_cs.m_pot,m_energy,&b5,r5);
			scat=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b5,r5,m_cs.m_tolerance);
		}

		const double dVdr=m_cs.m_pot.dVdr(r5);
		const double aux =
			0.5*pow(constants::pi,2)*pow(rc_tilde,2)
			*(1.0-m_cs.m_pot.value(r5)/m_energy-r5/2.0*dVdr/m_energy)
			*pow(r5/rc_tilde,3)*cos(constants::pi*(1.0+y)/4.0);
		for (unsigned l=1;l<=m_cs.m_l_max;l++)
		{
			// equation 53
			h[l-1] += aux*(1.0-pow(cos(scat),l));
		}
	}
}

/** Implement equation 59 of Viehland \cite Viehland2010
 */
class calc_cs::Viehland_case_3_sub
{
public:
	using result_type = double;
	using argument_type = double;
	Viehland_case_3_sub(const calc_cs& cs, double E, const triplet_data::case_data& data);
	void operator()(double y,std::vector<double>& h) const;
private:
	const calc_cs& m_cs;
	const double m_energy;
	const triplet_data::case_data& m_data;
};

calc_cs::Viehland_case_3_sub::Viehland_case_3_sub(const calc_cs& cs, double E, const triplet_data::case_data& data)
 : m_cs(cs), m_energy(E), m_data(data)
{
}

void calc_cs::Viehland_case_3_sub::operator()(double y, std::vector<double>& h) const
{
	if (y>-1)
	{
		const double yt = (y+1.0)/2;
		const double b4=m_data.b_split*yt;
		const double b5=m_data.b_split/yt;
		double chi4, chi5;
		if (m_cs.m_use_scat_Colonna==1)
		{
			chi4=scat_Colonna(&m_cs.m_pot,b4,m_energy,m_cs.m_tolerance,m_cs.m_interp);
			chi5=scat_Colonna(&m_cs.m_pot,b5,m_energy,m_cs.m_tolerance,m_cs.m_interp);
		}
		else
		{
			const double r0_p1=scat_viehland::find_r0_case_3(m_cs.m_pot,m_cs.m_rmin,m_cs.m_rmax,m_energy,&b4);
			const double r0_p2=scat_viehland::find_r0_case_3(m_cs.m_pot,m_cs.m_rmin,m_cs.m_rmax,m_energy,&b5);
			chi4=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b4,r0_p1,m_cs.m_tolerance);
			chi5=scat_viehland::scat1(m_cs.m_integrator,m_cs.m_pot,m_energy,b5,r0_p2,m_cs.m_tolerance);
		}
		// The expression below is equation 59 of Viehland, with the following
		// notational changes:
		//  - 1/2 of the first term is combined with (y+1) to form yt
		//  - the factor pi*bs^2 has been taken out of the parentheses
		//  - we used b5/bs = 1/yt (bs is bc in Viehland's paper, or
		//    bd for the low-energy no-orbiting case, see the text below
		//    equation 60).
		// The present notation makes it easy to see that when y=1 we
		// have yt=1, b4=b5=bs and chi4=chi5, resulting in Viehland's
		// equation 60.
		// Note: Eqn. 59 as written by Viehland mixes y and b5(y).
		for (unsigned l=1; l<=m_cs.m_l_max;++l)
		{
			h[l-1]  = constants::pi*pow(m_data.b_split,2)*(
				(1.0-pow(cos(chi4),l))*yt
				+
				(1.0-pow(cos(chi5),l))/pow(yt,3)
			);
		}
	}
	else
	{
		// y=-1 -> h = 0 (Viehland, text between eqns. 59 and 60).
		/** \todo Show this, not 100% trivial. It relies on the
		 *  fact that (1-cos(chi(b5))) tends to zero faster than
		 *  (1+y)^3 when y->-1. This requires an expression for
		 *  the asymptotic behaviour of chi for large impact
		 *  parameters.
		 */
		for (unsigned l=1; l<=m_cs.m_l_max;++l)
		{
			h[l-1] = 0.0;
		}
	}
}

calc_cs::calc_cs(int lower, int upper,
		double E_low,
		unsigned l_max,
		double rmin,double rmax,double dr,
		int use_scat_Col,
		int interpol,
		double tol,
		const potential* pot)
 : m_integrator(-1.0,1.0,lower,upper),
   m_rmin(rmin),m_rmax(rmax),
   m_use_scat_Colonna(use_scat_Col),
   m_interp(interpol),
   m_tolerance(tol),
   m_pot(*pot),
   m_l_max(l_max),
   m_triplets(pot, rmin, rmax, dr, E_low)
{
}

std::unique_ptr<const calc_cs> calc_cs::create(const json_type& cnf, const potential* pot)
{
	//consider these clenshaw curtis quadratures
	const int lower_cc=cnf.at("cc").at("lower");
	const int upper_cc=cnf.at("cc").at("upper");

	//set range for calculating cross section as a function of energy [log]
	/// \todo pass around the energy itself, in SI units, around internally
	const double logE_low=std::log10(in_si(cnf.at("energy_range").at("min"),units::energy));
	//const double logE_high=std::log10(in_si(cnf.at("energy_range").at("max"),units::energy));
	//const double E_points_decade=cnf.at("energy_range").at("points_per_decade");

	// max l for which the cross section is calculated
	const unsigned l_max = cnf.at("l_max");

	double rmin=in_si(cnf.at("r").at("min"),units::length);
	double rmax=in_si(cnf.at("r").at("max"),units::length);
	const double dr=in_si(cnf.at("r").at("del"),units::length);

	// adjust rmin,rmax if necessary
	const double Vmin=in_si(cnf.at("r").at("Vmin"),units::energy);
	const double Vmax=in_si(cnf.at("r").at("Vmax"),units::energy);
	rmin_input_evaluation(rmin,pot,Vmin);
	rmax_input_evaluation(rmax,pot,Vmax);

	/** \todo Make use_scat_Colonna user-configurable?
	 */
	//determine whether scat_Colonna or scat_Viehland is used for estimating scattering angles
	const int use_scat_Colonna=1;
	//set tolerance and interpolation order
	/** \todo Make interp user-configurable?
	 */
	const int interpol=3;
	const double tolerance = cnf.at("tolerance");

	return std::make_unique<const calc_cs>(lower_cc,upper_cc,std::pow(10,logE_low),l_max,
		rmin,rmax,dr,use_scat_Colonna,interpol,tolerance,pot);
}

std::vector<double> calc_cs::integrate(double energy) const
{
	const triplet_data::case_data data{m_triplets,energy};

	std::vector<double> Q(m_l_max,0.0);

	switch (data.case_id)
	{
		case 1:
			//integrate cross section case 1: Ed<=E<=Ec
			m_integrator.integrate_md(Viehland_case_1_sub(*this,energy,data),m_l_max,m_tolerance,Q);
		break;
		case 2:
			//integrate cross section case 2: Ec<E<=3*Ec
			m_integrator.integrate_md(Viehland_case_2_sub(*this,energy,data),m_l_max,m_tolerance,Q);
		break;
		case 3:
			//integrate cross section case 3: E<Ed || E>3 Ec
			m_integrator.integrate_md(Viehland_case_3_sub(*this,energy,data),m_l_max,m_tolerance,Q);
		break;
	}

	//print results to screen
	std::cout << energy/constants::eV << "\t";
	for (unsigned l=0;l!=Q.size();++l)
	{
		std::cout << Q[l] << "\t";
	}
	std::cout << std::endl;

	return Q;
}

data_set calc_cs::integrate(const std::vector<double>& energy_vals) const
{
	data_set sigma_data(
		"sigma_l(eps)", units::area, "m^2",
		{"epsilon", units::energy, "J"},
		{"l"} );
	// while we use SI internally, in output we prefer to write eV for the energies
	sigma_data.row_axis().set_display_unit("eV");
	for (unsigned l=1; l<=m_l_max; ++l)
	{
		sigma_data.add_column(std::to_string(l));
	}

	std::cout << "Starting calculation of cross section" << std::endl;

	for (auto energy : energy_vals)
	{
		std::vector<double> Q = integrate(energy);
		sigma_data.add_row(energy,Q);
	}
	return sigma_data;
}

data_set calc_cs::integrate(double logE_low, double logE_high, unsigned logE_pdecade) const
{
	std::vector<double> energy_vals = make_samples_log(logE_low,logE_high,logE_pdecade);
	return integrate(energy_vals);
}

data_set calc_cross_sec(int lower_cc, int upper_cc, double logE_low,double logE_high,int logE_pdecade,unsigned l_max,
	double rmin,double rmax,double dr,int use_scat_Col,int interpol,double tol,const potential* pot)
{
	//make cross section object
	calc_cs m_cs(lower_cc,upper_cc,std::pow(10,logE_low),l_max,
		rmin,rmax,dr,use_scat_Col,interpol,tol,pot);
	//perform integration
	return m_cs.integrate(logE_low,logE_high,logE_pdecade);
}

data_set calc_cross_sec(const json_type& cnf, const potential* pot)
{
	//make cross section object
	const auto cs = calc_cs::create(cnf,pot);
	//perform integration
	const double logE_low=std::log10(in_si(cnf.at("energy_range").at("min"),units::energy));
	const double logE_high=std::log10(in_si(cnf.at("energy_range").at("max"),units::energy));
	const double logE_pdecade=cnf.at("energy_range").at("points_per_decade");
	return cs->integrate(logE_low,logE_high,logE_pdecade);
}

data_set calc_cross_sec(const json_type& cnf, const json_type& pot)
{
	const std::unique_ptr<const potential> p{potential::create(pot)};
	return calc_cross_sec(cnf,p.get());
}

data_set calc_cross_sec(const json_type& cnf)
{
	return calc_cross_sec(cnf,cnf.at("interaction").at("potential"));
}

} // namespace magnumpi
