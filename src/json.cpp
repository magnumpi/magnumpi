/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/json.h"
#include "plutil/environment.h"
#include "plutil/split_string.h"

#include <fstream>
#include <iostream>
#include <stdexcept>

namespace magnumpi {

json_path::json_path()
{
}

json_path::json_path(const std::string& s)
try
 : m_path(plasimo::split_string<string_list>(s,'.'))
{
	// handle this separately, so we do not need to deal with
	// { "", "" } in the test code below.
	if (s==".")
	{
		m_path.clear();
		return;
	}
	if (m_path.begin()==m_path.end())
	{
		throw std::runtime_error("Empty JSON Path specified.");
	}
	// for ".aap.noot.mies", the list contains { "", "aap", "noot", "mies }
	// first we check that the first element is empty and we remove it
	if (!m_path.front().empty())
	{
		throw std::runtime_error("JSON Path must start with a '.'");
	}
	m_path.erase(m_path.begin());
	// now check that all remaining elements are non-empty
	for (const auto& e : m_path)
	{
		if (e.empty())
		{
			throw std::runtime_error("JSON Path: empty section "
				"name encountered.");
		}
	}
}
catch(std::exception& exc)
{
	throw std::runtime_error("JSON Path: while parsing string '" + s + "': "
		+ std::string(exc.what())); 
}

const json_type& json_path::get_element(const json_type& json) const
{
	const json_type* res = &json;
	
	for (const auto& e : m_path)
	{
		try {
			res = &res->at(e);
		}
		catch (std::exception& exc)
		{
			throw std::runtime_error("JSON Path: could not find "
				"subsection '" + e + "'.");
		}
	}
	return *res;
}

json_type& json_path::get_element(json_type& json) const
{
	// call get_element on the constified json argument,
	// remove the constness from the result.
	const json_type& res = get_element(const_cast<const json_type&>(json));
	return const_cast<json_type&>(res);
}

json_type read_json_from_stream(std::istream& is)
{
	return json_type::parse(is);
}

json_type read_json_from_file(const std::string& src)
{
	std::string fname, pstr;
	const std::string::size_type hash = src.find_last_of("#");
	if (hash==std::string::npos)
	{
		fname = src;
	}
	else
	{
		fname = src.substr(0,hash);
		pstr = src.substr(hash+1);
		// std::cout << "fname = " << fname << ", path = " << pstr << std::endl;
	}
	std::ifstream is(plasimo::substitute_env_vars_copy(fname,true));
	if (!is)
	{
		throw std::runtime_error("Could not open file '"
			+ fname + "' for reading.");
	}
	try {
		if (pstr.empty())
		{
			return json_type::parse(is);
		}
		else
		{
			const json_path path(pstr);
			const auto obj = read_json_from_stream(is);
			return path.get_element(obj);
		}
	}
	catch (std::exception& exc)
	{
		throw std::runtime_error("Error reading file '"
			+ fname + "':\n" + std::string(exc.what()));
	}
}

void write_json_to_file(const json_type& data, const std::string& fname, int indent)
{
	std::ofstream os(plasimo::substitute_env_vars_copy(fname,true));
	if (!os)
	{
		throw std::runtime_error("Could not open file '"
			+ fname + "' for writing.");
	}
	try {
		os << data.dump(indent) << std::endl;
	}
	catch (std::exception& exc)
	{
		throw std::runtime_error("Error writing JSON file '"
			+ fname + "':\n" + std::string(exc.what()));
	}
}

}; // namespace magnumpi
