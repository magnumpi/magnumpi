/** \file
 *
 * Copyright (C) 2016-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/interpolation.h"
#include <vector>
#include <iostream>
#include <algorithm>

namespace magnumpi {

/*
#include <math.h>
#include <stdio.h>
*/
//Based on http://www.paulinternet.nl/?page=bicubic
// transform u=(x-x1)/(x-x2)
// f(u)=a u^3+ b u^2 + c u + d
// f(x1)=d
// f(x2)=a+b+c+d
// f'(x1)=c/(x2-x1)
// f'(x2)=(3a+2b+c)/(x2-x1)
// invert
// a=2 (f(x1)-f(x2))+(x2-x1)*(f'(x2)+f'(x1)) = 1/2*p3-3/2*p2+3/2*p1-1/2*p0
// b=3 (f(x2)-f(x1))-(x2-x1)(f'(x1)+2f'(x2)) = -1/2*p3+2*p2-5/2*p1+p0
// c=f'(x1)*(x2-x1)=(p2-p0)/2
// d=f(x1)=p1

/*
//return entire row of y with size width
void cubic_interp1(const double* x1,const double* y1,double x,double* y,int length,int width)
{
    //assume x and y are monotonically ascending.
    double p[4],u;
    int index=length;
    
    //check if x exceeds the range of x1
    if (x<*(x1))
    {
        //return first row
        for (int i=0;i<width;i++)
        {
            *(y+i)=*(y1+i);
        }
        return;
    }
    else if (x>*(x1+length-1))
    {
        for (int i=0;i<width;i++)
        {
            *(y+i)= *(y1+(length-1)*width+i);
        }
        return;
    }
    
    //find first value in x1 that exceeds x
    for (int i=0;i<length;i++)
    {
        if ( *(x1+i)>x)
        {
            index=i;
            break;
        }
    }
    
    //allow interpolation of multicolumns having the same x vector
    for (int i=0;i<width;i++)
    {
        //prepare p values for interpolating
        if (index==1)
        {
            //copy first value to p[0]
            p[0]=*(y1+(index-1)*width+i);
            p[1]=*(y1+(index-1)*width+i);
            p[2]=*(y1+index*width+i);
            p[3]=*(y1+(index+1)*width+i);
        }
        else if (index==length-1)
        {
            //copy last value to p[3]
            p[0]=*(y1+(index-2)*width+i);
            p[1]=*(y1+(index-1)*width+i);
            p[2]=*(y1+index*width+i);
            p[3]=*(y1+index*width+i);
        }
        if (index>1 && index<length-2)
        {
            p[0]=*(y1+(index-2)*width+i);
            p[1]=*(y1+(index-1)*width+i);
            p[2]=*(y1+index*width+i);
            p[3]=*(y1+(index+1)*width+i);
        }
        //u=(x-x1)/(x2-x1)
        u=(x-*(x1+index-1))/(*(x1+index)- *(x1+index-1));
        //calculate y
        *(y+i)=p[1]+ u*(p[2]-p[0]+u*(2.0*p[0]-5.0*p[1]+4.0*p[2]-p[3]+u*(3.0*(p[1]-p[2])+p[3]-p[0])));
    }    
}
*/
/*
//return a single element pos from the row
void single_cubic_interp1(const double* x1,const double* y1,double x,double* y,int length,int width,int pos)
{
    //assume x and y are monotonically ascending. Based on http://www.paulinternet.nl/?page=bicubic
    double p[4],u;
    int index=length;
    
    //check if x exceeds the range of x1
    if (x<*(x1))
    {
        //return first row
        for (int i=0;i<width;i++)
        {
            *(y+i)=*(y1+i);
        }
        return;
    }
    else if (x>*(x1+length-1))
    {
        for (int i=0;i<width;i++)
        {
            *(y+i)= *(y1+(length-1)*width+i);
        }
        return;
    }
    
    //find first value in x1 that exceeds x
    for (int i=0;i<length;i++)
    {
        if ( *(x1+i)>x)
        {
            index=i;
            break;
        }
    }
    
    //allow interpolation of multicolumns having the same x vector
    for (int i=pos;i<=pos;i++)
    {
        //prepare p values for interpolating
        if (index==1)
        {
            //copy first value to p[0]
            p[0]=*(y1+(index-1)*width+i);
            p[1]=*(y1+(index-1)*width+i);
            p[2]=*(y1+index*width+i);
            p[3]=*(y1+(index+1)*width+i);
        }
        else if (index==length-1)
        {
            //copy last value to p[3]
            p[0]=*(y1+(index-2)*width+i);
            p[1]=*(y1+(index-1)*width+i);
            p[2]=*(y1+index*width+i);
            p[3]=*(y1+index*width+i);
        }
        if (index>1 && index<length-2)
        {
            p[0]=*(y1+(index-2)*width+i);
            p[1]=*(y1+(index-1)*width+i);
            p[2]=*(y1+index*width+i);
            p[3]=*(y1+(index+1)*width+i);
        }
        //u=(x-x1)/(x2-x1)
        u=(x-*(x1+index-1))/(*(x1+index)- *(x1+index-1));
        //calculate y
        std::cout << u << "\t"<< p[3] << "\t" << p[2] << "\t" << p[1] << "\t" << p[0] << std::endl;
        *(y)=p[1]+0.5*u*(p[2]-p[0]+u*(2.0*p[0]-5.0*p[1]+4.0*p[2]-p[3]+u*(3.0*(p[1]-p[2])+p[3]-p[0])));
    }    
}
*/

//return a single element pos from the row
void single_linear_interp1(
	const std::vector<double>& x1,
	const std::vector<std::vector<double>>& y1,
	double x_param,
	double* y_param,
	int column)
{
    //assume x is monotonically ascending.
    double p[2];
    double px[2];
    
    //check if x exceeds the range of x1
    if (x_param<x1[0])
    {
        //return first row
        *y_param=y1[0][column];
        return;
    }
    else if (x_param>x1[x1.size()-1])
    {
        *y_param= y1[x1.size()-1][column];
        return;
    }
    
    auto upper = std::upper_bound(x1.begin(), x1.end(), x_param);
    int index = std::distance(x1.begin(), upper);
    
    //determine points
    p[0]=y1[index-1][column];
    p[1]=y1[index][column];
        
    px[0]=x1[index-1];
    px[1]=x1[index];
        
    //calculate y
    *y_param=p[0]+(p[1]-p[0])*(x_param-px[0])/(px[1]-px[0]);
}

void vector_linear_interp1(
	const std::vector<double>& x1,
	const std::vector<double>& y1,
	const std::vector<double>& x_param,
	std::vector<double>& y_param,
	unsigned int mult,
	unsigned int last)
{
	//start with count=1, otherwise count-1 is smaller than zero
	//assuming that x1 and x_param are monotonic
	unsigned int count= mult;
	unsigned int n = 0;
	int ind;
	for (unsigned int j=0; j<x_param.size()/last; ++j)
	{
		for (unsigned int i=0;i<last;i+=mult)
		{
			ind = j*last + i;
			//find index that is smaller than x1(count)
			while (count< x1.size() && x1[count]<x_param[ind])
			{	
				if (count+mult > (n+1) * last)	{
					++n;
					count = n*last+mult;
				}
				else
					count+=mult;
			}
			//interpolation
			y_param[ind]=y1[count-mult]+(y1[count]-y1[count-mult])/(x1[count]-x1[count-mult])*(x_param[ind]-x1[count-mult]);
		}
	}
}

} // namespace magnumpi
