/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/scat_viehland.h"
#include "magnumpi/potential.h"
#include "magnumpi/viehland_common.h"
#include "magnumpi/consts.h"
#include "plmath/clencurt.h"

#include <vector>
#include <cmath>
#include <iostream>

namespace magnumpi {

class scat_viehland::scat1_integrand 
{
    public:
    typedef double result_type;
    typedef double argument_type;
    
    scat1_integrand(double E,double b_impact, double r_null,const potential& pot)
	: energy(E),b(b_impact),r0(r_null),m_pot(pot)
	{}

    result_type operator () (double y) const
    {
        result_type f_out;
        if (y==1)
        {
            f_out=constants::pi/2.0*(1.0-b/r0);
        }
        else if (y<1.0 && y>-1)
        {
            const double r6=r0/cos(constants::pi*(y+1.0)/4.0);
            /// \todo this statement should be guaranteed by adjusting b and r0
            if (1.0-pow(b/r6,2)-m_pot.value(r6)/energy>0.0)
            {
                //eq 63
                f_out=constants::pi/2.0*(1.0-b/r0*sin(constants::pi*(y+1.0)/4.0)/sqrt(1.0-pow(b/r6,2)-m_pot.value(r6)/energy));
            }
            else
            {
                /// \todo improve this statement, if a negative value for 1-b^2/r^2-V/E is detected r0 is not determined correctly
                //let's neglect these contributions for now
                f_out=0.0;//constants::pi/2.0*1.0/0.0;
                /*
                std::cout<< "Error: square root of negative argument in scat1_integrand for y=" << y << std::endl;
                std::cout<< "r6\t" <<  r6 << "\tb\t" << b << "\tE\t" << energy << "\tr0\t" << r0 << "\t" 
                         << 1.0-pow(b/r6,2)-m_pot.value(r6)/energy << "\t"<< 1.0-pow(b/r0,2)-m_pot.value(r0)/energy<< std::endl;
                */
            }
        }
        else if (y==-1)
        {
            const double dVdr=m_pot.dVdr(r0);
            
            if (1.0-pow(r0,3)/2.0/energy/pow(b,2)*dVdr>0)
            {
                //eq 64
                f_out=constants::pi/2.0*(1.0-1.0/sqrt(1.0-pow(r0,3)/2.0/energy/pow(b,2)*dVdr));
            }
            else
            {
                //improve this statement
                f_out=0;
                //std::cout << "Error: square root of negative argument in scat1_integrand for y=-1 \tb\t"<< b << std::endl;
            }
        }
	//if (y<-9.99999705e-1)
	//std::cout << "E\t" << energy << "\tb\t" << b << "\tr0\t" << r0 << "\tscat\t" << f_out << std::endl;
        return f_out;
    }
  private:
    const double energy,b,r0;
    const potential& m_pot;
};

class scat_viehland::scat2_integrand
{
    public:
    typedef double result_type;
    typedef double argument_type;

    scat2_integrand(double E,double b_impact,double r_null,double r_tild,const potential& pot):
        energy(E),b(b_impact),r0(r_null),r_tilde(r_tild),m_pot(pot) {}

    result_type operator () (double y) const
    {
        result_type f_out;
        double r7,r8,dVdr;
        if (y==1)
        {
            //eq 71
            //delta_r=1e-8*r0;
            //dVdr=(m_pot.value(r0+delta_r)-m_pot.value(r0-delta_r))/2.0/delta_r;
            dVdr=m_pot.dVdr(r0);
            if (1.0-pow(r0,3)/2.0/energy/pow(b,2)*dVdr > 0.0)
                f_out=constants::pi/2.0* (1.0-b/r_tilde-acos(r0/r_tilde)/sqrt(1.0-pow(r0,3)/2.0/energy/pow(b,2)*dVdr) );
            else
                f_out=0.0;
        }
        else if (y<1.0 && y>-1)
        {
            r7=r_tilde/cos(constants::pi/4.0*(1.0+y));
            r8=r0/cos(acos(r0/r_tilde)*cos(constants::pi*(y+1.0)/4.0) );
            //eq 70
            //part 1
            if (1.0-pow(b/r7,2)-m_pot.value(r7)/energy >0.0 )
                f_out=constants::pi/2.0* (1.0-b/r_tilde*sin(constants::pi*(y+1.0)/4.0)/sqrt(1.0-pow(b/r7,2)-m_pot.value(r7)/energy) );
            else
                f_out=0.0;
            //part 2
            if(1.0-pow(b/r8,2)-m_pot.value(r8)/energy>0)
            {
                f_out += constants::pi/2.0*(  -b/r0*acos(r0/r_tilde)*sin(acos(r0/r_tilde)*cos(constants::pi/4.0*(y+1.0)))*
                    sin(constants::pi*(y+1.0)/4.0)/sqrt(1.0-pow(b/r8,2)-m_pot.value(r8)/energy) );
            }
                    
            //check whether nan's occur
            /*
            if( 1.0-pow(b/r7,2)-m_pot.value(r7)/energy <=0 || 1.0-pow(b/r8,2)-m_pot.value(r8)/energy<=0 )
            {
                std::cout << "F(r7)=0 or F(r8)=0\t" << 1.0-pow(b/r7,2)-m_pot.value(r7)/energy << "\t" << 
                            1.0-pow(b/r8,2)-m_pot.value(r8)/energy << std::endl;
            }
            // */
        }
        else if (y==-1)
        {
            //eq 72
            f_out=constants::pi/2.0;
        }
        return f_out;
    }
  private:
	const double energy,b,r0,r_tilde;
	const potential& m_pot;
};

double scat_viehland::scat1(const integrator_type& integrator,
		const potential& pot, double energy,double b,double r0, double tolerance)
{   
	return integrator.integrate(scat1_integrand(energy,b,r0,pot),tolerance);
}

double scat_viehland::scat2(const integrator_type& integrator,
		const potential& pot, double energy,double b,double r_tilde,double r0, double tolerance)
{        
	return integrator.integrate(scat2_integrand(energy,b,r0,r_tilde,pot),tolerance);
}

//search for r0 use previous r0 as first guess for lower boundary
//Since b (y=1 -> b=0, y=-1 -> b=b1) is increasing in case 1 part 1 we know that r0 also increasing
double scat_viehland::find_r0_case_1(const potential& pot, double energy,double* b,double r0_prev)
{
	double r_upper,r_lower=r0_prev;
	int count=0,found_upper=0,max_step=20;
	double tol_r0=5e-16;

	//make sure that r0_test(r_lower) is negative (should always be true)
	while (r0_test(pot,energy,0.0,r_lower)>0.0)
	{
		r_lower=r_lower*0.95;
	}
	//look for first r_upper that gives a positive value
	//take steps of 0.1*r0_lower upto r_upper=(max_step/step+1)*r_lower
	while (count<max_step)
	{
		count++;
		r_upper=r_lower+count*1.0/max_step*r_lower;
		if (r0_test(pot,energy,0.0,r_upper)>0.0)
		{
			found_upper=1;
			break;
		}
	}
	//search further than r_upper=(max_step/step+1)*r_lower if necessary, use bigger steps
	if (found_upper==0)
	{
		while (r0_test(pot,energy,0.0,r_upper))
		{
			r_upper=r_upper*1.2;
		}
	}

	//apply bisection to find r0
	double res=1.0;
	double fmid,rmid;

	//initialization ensures that r0_test(r_lower)<0 && r0_test(r_upper)>0
	while (res>tol_r0)
	{
		rmid=(r_upper+r_lower)/2.0;
		fmid=r0_test(pot,energy,*b,rmid);
		if (fmid>0)
		{
			r_upper=rmid;
		}
		else if (fmid<0)
		{
			r_lower=rmid;
		}
		else //fmid==0
		{
			r_upper=rmid;
			r_lower=rmid;
		}
		res=(r_upper-r_lower)/r_upper;
	}

	//adjust b to match with the new r0
	adjust_b(pot,energy,b,r_upper);

	//return r_upper to ensure that 1-b^2/r^2-V/E>0
	return r_upper;
}

//high energy, r0 is practically obtained from 1-b^2/r^2=0 or from 1-V(r)/E=0 for small b's
//no fancy techniques required
double scat_viehland::find_r0_case_3(const potential& pot, double rmin, double rmax, double energy,const double* b)
{
	const double tol_r0=5e-16;

	//first estimate of boundaries
	double r_upper = rmax;
	double r_lower = rmin;
	//verify upper boundary
	while (r0_test(pot,energy,*b,r_upper)<0.0)
	{
		r_upper=r_upper*10.0;
	}
	//verify lower boundary
	while (r0_test(pot,energy,*b,r_lower)>0.0)
	{
		r_lower=r_lower/10.0;
	}

	//apply bisection to find r0
	double res=1.0;
	double fmid,rmid;

	//initialization ensures that r0_test(r_lower)<0 && r0_test(r_upper)>0
	while (res>tol_r0)
	{
		rmid=(r_upper+r_lower)/2.0;
		fmid=r0_test(pot,energy,*b,rmid);
		if (fmid>0)
		{
			r_upper=rmid;
		}
		else if (fmid<0)
		{
			r_lower=rmid;
		}
		else //fmid==0
		{
			r_upper=rmid;
			r_lower=rmid;
		}
		res=std::abs(r_upper-r_lower)/r_upper;
	}

	//return r_upper to ensure that 1-b^2/r^2-V/E>0
	//std::cout << r0_test(energy,*b,rmid) << std::endl;
	return r_upper;
}


} // namespace magnumpi

