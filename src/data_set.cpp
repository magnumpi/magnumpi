/** \file
 *
 * Copyright (C) 2017-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/data_set.h"
#include "magnumpi/units.h"
#include <stdexcept>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cassert>

namespace magnumpi {

data_info::data_info(std::string name, units::dimension d, std::string unit)
 : m_name(name), m_dimension(d), m_unit(unit), m_display_unit(unit), m_conversion_factor(1.0), m_precision(0)
{
}

data_info::~data_info()
{
}

void data_info::set_display_unit(std::string dunit)
{
	m_conversion_factor = units::unit_in_si(m_unit,m_dimension)/units::unit_in_si(dunit,m_dimension);
	m_display_unit = dunit;
}

std::string data_info::full_name() const
{
	std::string res = name();
	if (unit()!="1")
		res += " [" + unit() + "]";
	return res;
}

std::string data_info::display_full_name() const
{
	std::string res = name();
	if (display_unit()!="1")
		res += " [" + display_unit() + "]";
	return res;
}


data_set::axis_type::axis_type(const std::string& name, units::dimension d, const std::string& unit)
 : data_info(name,d,unit), m_type(numeric)
{
}

data_set::axis_type::axis_type(const std::string& name)
 : data_info(name,units::scalar, "1"), m_type(labeled)
{
}

data_set::axis_type& data_set::axis_type::operator<<(const std::string& value)
{
	if (m_type==numeric)
	{
		throw std::runtime_error("Cannot add string value '"
			+ value + "' to a numeric axis.");
	}
	m_data.push_back({value,double{}});
	return *this;
}

data_set::axis_type& data_set::axis_type::operator<<(double value)
{
	if (m_type==labeled)
	{
		std::stringstream ss;
		ss << "Cannot add double value '" << value
			<< "' to a labeled axis.";
		throw std::runtime_error(ss.str());
	}
	m_data.push_back({std::string{},value});
	return *this;
}

std::string data_set::axis_type::label_str(size_type ndx) const
{
	if (m_type==labeled)
	{
		return m_data[ndx].first;
	}
	else
	{
		/// \todo avoid the intermediate string
		std::stringstream ss;
		if (this->precision())
		{
			 ss << std::setprecision(this->precision());
		}
		ss << display_value(ndx);
		return ss.str();
	}
}

json_type data_set::axis_type::write_json() const
{
	json_type res;
	res["title"] = name();
	if (is_numeric())
	{
		json_type values = json_type::array();
		for (size_type i=0; i!= size(); ++i)
		{
			values.push_back(m_data[i].second*conversion_factor());
		}
		res["data"]["unit"] = display_unit();
		res["data"]["values"] = values;
	}
	else
	{
		json_type labels = json_type::array();
		for (size_type i=0; i!= size(); ++i)
		{
			labels.push_back(m_data[i].first);
		}
		res["labels"] = labels;
	}
	return res;
}

data_set::data_set(
		const std::string& dname, units::dimension dd, const std::string& dunit,
		const std::string& rname, units::dimension rd, const std::string& runit,
		const std::string& cname, units::dimension cd, const std::string& cunit)
 : data_info(dname,dd,dunit),
   m_row_axis(rname,rd,runit),
   m_col_axis(cname,cd,cunit),
   m_write_gnu3d(false)
{
}

data_set::data_set(
		const std::string& dname, units::dimension dd, const std::string& dunit,
		const axis_type& row_axis,
		const axis_type& col_axis)
 : data_info(dname,dd,dunit),
   m_row_axis(row_axis),
   m_col_axis(col_axis),
   m_write_gnu3d(false)
{
}

void data_set::add_row(const std::string& value, const array_type& values)
{
	if (values.size()!=0 && values.size()!=col_axis().size())
	{
		throw std::runtime_error("Add row: provided row data has wrong size.");
	}
	m_row_axis << value;
	for (size_type c=0; c!=col_axis().size(); ++c)
	{
		m_data[c].push_back(values.size() ? values[c] : double{});
	}
}

void data_set::add_row(double value, const array_type& values)
{
	if (values.size()!=0 && values.size()!=col_axis().size())
	{
		throw std::runtime_error("Add row: provided row data has wrong size.");
	}
	m_row_axis << value;
	for (size_type c=0; c!=col_axis().size(); ++c)
	{
		m_data[c].push_back(values.size() ? values[c] : double{});
	}
}

void data_set::add_column(const std::string& value, const array_type& values)
{
	if (values.size()!=0 && values.size()!=row_axis().size())
	{
		throw std::runtime_error("Add col: provided column data has wrong size.");
	}
	m_col_axis << value;
	m_data.push_back(values.size() ? values : array_type(row_axis().size()));
}

void data_set::add_column(double value, const array_type& values)
{
	m_col_axis << value;
	if (values.size()!=0 && values.size()!=row_axis().size())
	{
		throw std::runtime_error("Add col: provided column data has wrong size.");
	}
	m_data.push_back(values.size() ? values : array_type(row_axis().size()));
}

void data_set::write_legend(std::ostream& os) const
{
	const size_type nc = this->col_axis().size();
	os << "# - ";
	const size_type offset=2;
	if (nc==1)
	{
		os << "column " << offset;
	}
	else
	{
		os << "columns [" << offset << ':' << ((offset+nc)-1) << ']';
	}
	os << ": data set '"
		<< this->name() << "' [" << (this->display_unit()=="1" ? "-" : this->display_unit())
		<< "] (size=" << this->col_axis().size() << ")\n";
	for (size_type c = 0; c != this->col_axis().size(); ++c)
	{
		os << "# - column " << (offset+c) << ": '[";
		if (!this->col_axis().name().empty())
		{
			os << this->col_axis().name() << " ";
		}
		os << col_axis().label_str(c);
		os << "]'\n";
	}
}

void data_set::write_row(std::ostream& os, size_type r) const
{
	assert(r<row_axis().size());
	os << row_axis().label_str(r);
	for (size_type c = 0; c != this->col_axis().size(); ++c)
	{
		os << '\t'; write_display_value(os,r,c);
	}
	os << '\n';
}

void data_set::write_2d(std::ostream& os, const std::string& header) const
{
	if (!header.empty())
	{
		os << header << std::endl;
	}
	os << "# - column " << 1 << ": '"
		<< this->row_axis().name()
		<< "' [" << (this->row_axis().display_unit()=="1" ? "-" : this->row_axis().display_unit()) << "]\n";
	write_legend(os);
	os << '#' << row_axis().name() << '[' << row_axis().display_unit() << ']';
	for (size_type c = 0; c != this->col_axis().size(); ++c)
	{
		os << '\t';
		os << col_axis().label_str(c);
	}
	os << '\n';
	for (size_type r=0; r != row_axis().size(); ++r)
	{
		write_row(os,r);
	}
	os << "\n\n";
	os.flush();
}

void data_set::write_3d(std::ostream& os, const std::string& header) const
{
	if (!header.empty())
	{
		os << header << std::endl;
	}
	const size_type nr = this->row_axis().size();
	const size_type nc = this->col_axis().size();
	for (size_type c=0; c!=nc; ++c)
	{
		for (size_type r=0; r!=nr; ++r)
		{
			os << this->col_axis().value(c);
			os << '\t' << this->row_axis().value(r);
			os << '\t'; write_display_value(os,r,c);
			os << '\n';
		}
		os << '\n';
	}
	os << "\n\n";
	os.flush();
}

void data_set::write(std::ostream& os, const std::string& header) const
{
	if (GetWriteGnu3d())
	{
		write_3d(os,header);
	}
	else
	{
		write_2d(os,header);
	}
}

void data_set::write(const std::string& fname, const std::string& header) const
{
	std::ofstream os(fname);
	if (!os)
	{
		throw std::runtime_error("Could not open file '" + fname + "' for writing.");
	}
	os << std::scientific;
	write(os,header);
}

json_type data_set::write_json(const std::string& header) const
{
	json_type res;
	res["title"] = this->name();
	res["unit"] = this->unit();
	res["row_axis"] = this->row_axis().write_json();
	res["column_axis"] = this->col_axis().write_json();
	/** The present JSON format assumes that the data are
	 *  stored as a collection of rows, not columns.
	 *  If we want to keep it that way, element 'columns'
	 *  should be renamed 'rows', such that data['rows'][r]
	 *  if the r'th row.
	 */
	json_type& rows = res["columns"] = json_type::array();
	const size_type nr = this->row_axis().size();
	const size_type nc = this->col_axis().size();
	for (size_type r=0; r!=nr; ++r)
	{
		json_type row = json_type::array();
		for (size_type c=0; c!=nc; ++c)
		{
			row.push_back( (*this)(r,c) );
		}
		rows.push_back(row);
	}
	return res;
}

void data_set::write_display_value(std::ostream& os, size_type r, size_type c) const
{
	if (this->precision())
	{
		 os << std::setprecision(this->precision());
	}
	// note: column-wise storage, select the column first
	os << (*this)(r,c);
}


} // namespace magnumpi

