/** \file
 *
 * Copyright (C) 2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *
 * This program calculates trajectories for a number of impact parameters
 * and energies.
 *
 * \todo Discuss details COM frame vs. laboratory frame.
 * \todo Introduce options -com, -lab, the latter visualizing both particles?
 *
 * Example invocation:
 *
 *   ./example_trajectories example_trajectories.in ../../../input/colonna_fig4a_lj_pot.json.json "lj-"
 *
 * In this case results are written to files:
 *
 *  - hh-deflection_angles.dat: theta(E,b), grouped per E with an empty line
 *    between E-values,
 *  - hh-<i>-trajectories.dat, where i=0...Ne-1, the number of energy values:
 *    files containing (x,y)(t) and more information. One file is created per
 *    E value, two empty lines are written between the trajectories (b-values).
 *
 * To plot the results using GNUPLOT:
 *
 *   gnuplot
 *     # angles(b) for all E:
 *     plot "hh-deflection_angles.dat" w l
 *     # trajectories(b) for first E value (file index 0):
 *     plot "hh-0-trajectories.dat" u 4:5:8 w l
 *
 * \author Jan van Dijk
 * \date   May 2021
 */

#include "magnumpi/consts.h"
#include "magnumpi/json.h"
#include "magnumpi/potential.h"
#include "magnumpi/units.h"
#include "magnumpi/utility.h"
#include <boost/numeric/odeint.hpp>

#include <vector>
#include <array>
#include <iostream>
#include <fstream>
#include <cmath>

class scattering_problem
{
public:
	using vector_type = std::array<double,2>;

	scattering_problem(const magnumpi::json_type& cnf, const magnumpi::potential& V)
	 : m_V(V), m_m(in_si(cnf.at("mass"),magnumpi::units::mass))
	{}
	/** Calculate the acceleration \a a of a particle at position \a x
	 *  with velocity \a v at time \a t. In the present case we have a
	 *  radial force vector vec(F) = F_r*vec(e_r) = (F_r/r)*vec(x), so the
	 *  acceleration is given by vec(a) = vec(F)/m = (F_r/m/r)*vec(x).
	 *  The radial force component is obtained by calling to force_r(r).
	 */
	void operator()(const vector_type& x, const vector_type& v, vector_type &a, double t) const
	{
		double r = std::sqrt(x[0]*x[0]+x[1]*x[1]);
		double a_mr = force_r(r)/m_m/r;
		a[0] = x[0]*a_mr;
		a[1] = x[1]*a_mr;
	}
	double force_r(double r) const
	{
		return -m_V.dVdr(r);
	}
	double V(double r) const
	{
		return m_V.value(r);
	}
private:
	const magnumpi::potential& m_V;
	const double m_m;
};

void calculate_trajectory(
	const magnumpi::json_type& cnf,
	const magnumpi::potential& V,
	double b,
	double E,
	std::ostream& ts,
	std::ostream& ds)
{
	using vector_type = scattering_problem::vector_type;

	const double m = in_si(cnf.at("mass"),magnumpi::units::mass);
	const double y0 = in_si(cnf.at("y0"),magnumpi::units::length);
	
	vector_type x{ b, y0 };
	vector_type v{ 0.0, std::sqrt(2*E/m) };
	
	double t = 0.0;
	double r0=std::sqrt(x[0]*x[0]+x[1]*x[1]);
	double r=r0;
	boost::numeric::odeint::velocity_verlet< vector_type > stepper;
	scattering_problem sys(cnf,V);
	do
	{
		const double a_r = sys.force_r(r)/m;
		const double dx=std::max(1e-13,r);
		const double speed=std::sqrt(v[0]*v[0]+v[1]*v[1]);
		double dt=std::min(dx/speed,speed/std::abs(a_r))/1000;
/// \todo Remember why this clipping was needed, remove it.
#define MAGNUMPI_TRAJECTORY_CLIP_DT
#ifdef MAGNUMPI_TRAJECTORY_CLIP_DT
		const double dt_max=1e-23;
		dt=std::max(dt,dt_max);
#endif
		stepper.do_step(sys, std::make_pair(std::ref(x),std::ref(v)), t, dt);
		t += dt;
		r=std::sqrt(x[0]*x[0]+x[1]*x[1]);

		ts	<< t
			<< '\t' << dt
			<< '\t' << r
			<< '\t' << x[0] << '\t' << x[1]
			<< '\t' << v[0] << '\t' << v[1]
			<< '\t' << (sys.V(r)+0.5*m*speed*speed)/E
			<< std::endl;
	}
	while (r<r0);
	/** \todo handle the case of orbiting, in which case r->r_min
	 *  asymptotically, and the particles never separate again.
	 *  That would results in an infinite loop, were it not that
	 *  round-off errors prevent that from happening.
	 */
	/** \todo see \cite Newton equation 5.13 for a slightly different
	 *  definition, where theta is in the range [0,pi] --- corresponidng to
	 *  the absolute value of the result below, I believe. Carefully document
	 *  what we (want to) provide.
	 */
	// the scattering angle is defined up to a term n*2pi and must be in the range [-pi,pi].
	// atan2 does The Right Thing.
	ds << b << '\t' << std::atan2(v[0],v[1]) << std::endl;
}

void calculate_trajectories(const magnumpi::json_type& cnf, const magnumpi::json_type& cnf_pot, const std::string& prefix)
{
	using namespace magnumpi;
	const std::unique_ptr<const magnumpi::potential> V(potential::create(cnf_pot.at("potential")));
	const auto bvals = make_range(cnf.at("impact_parameter"),magnumpi::units::length);
	const auto evals = make_range(cnf.at("energy"),magnumpi::units::energy);
	const std::string dfname(prefix+"deflection_angles.dat");
	std::ofstream ds(dfname);
	if (!ds)
	{
		throw std::runtime_error("Could not open file '" + dfname + "' for writing.");
	}
	ds << "#b/m\tchi/rad" << std::endl;
	unsigned endx=0;
	for (double E : evals)
	{
		ds << "#energy = " << E << "*J" << std::endl;
		const std::string tfname(prefix+std::to_string(endx++)+"-trajectories.dat");
		std::ofstream ts(tfname);
		if (!ts)
		{
			throw std::runtime_error("Could not open file '" + tfname + "' for writing.");
		}
		ts << "#energy = " << E << "*J" << std::endl;
		ts << "#t/s\tdt/s\tr/m\tx/m\ty/m\tvx/(m/s)\tvy/(m/s)\tEinit/J\tEtot/J" << std::endl;
		for (double b : bvals)
		{
			ts << "#b = " << b << "/m" << std::endl;
			calculate_trajectory(cnf,*V,b,E,ts,ds);
			ts << std::endl << std::endl;
		}
		ds << std::endl << std::endl;
	}
}

int main(int argc, const char* argv[])
try
{
	if (argc!=4)
	{
		throw std::runtime_error("Usage: example_trajectories <settings file> <potential file>");
	}
	const magnumpi::json_type cnf(magnumpi::read_json_from_file(argv[1]));
	const magnumpi::json_type pot(magnumpi::read_json_from_file(argv[2]));
	calculate_trajectories(cnf,pot,argv[3]);
	return 0;
}
catch(std::exception& exc)
{
	std::cerr << exc.what() << std::endl;
	return 1;
}
