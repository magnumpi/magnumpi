#include <iostream>
#include "magnumpi/beast_post.h"

int main(int argc, char** argv)
{
	// Check command line arguments.
	if (argc != 5 && argc != 6)
	{
		std::cerr <<
			"Usage:\n  http-client-async <host> <port> <target>"
				" <body> [<HTTP version: 1.0 or 1.1(default)>]\n"
			"Example:\n  "
			"./example_beast_post localhost 3000"
				" /test/magnumpi {\\\"id\\\":2}\n"
		;
		return 1;
	}
	auto const host = argv[1];
	auto const port = argv[2];
	auto const target = argv[3];
	auto const body = argv[4];
	int version = argc == 6 && !std::strcmp("1.0", argv[5]) ? 10 : 11;
	const auto res = magnumpi::post_request(host,port,target,body,version);
	std::cout << "RESPONSE:\n" << res.dump(2) << std::endl;
	return 0;
}
