#ifndef H_MAGNUM_MAGMUM_PARSE_URI_FILE_H
#define H_MAGNUM_MAGMUM_PARSE_URI_FILE_H

#include "plutil/environment.h"
#include "magnumpi/json.h"
#include <string>
#include <stdexcept>

namespace magnumpi {

/** Extract and return a filename from a file URI that refers to
 *  a file on the local computer.
 *  The format of \a uri must be either file:/path or file:///path.
 *  The (legal) form file://<hostname>/path is not supported, although
 *  we could accept this when '<hostname>' is equal to 'localhost'.
 *
 *  \author Jan van Dijk
 *  \date   April 2019
 */
inline std::string parse_file_uri(std::string uri)
{
	const std::string fmt_msg("expected: 'file:<path> or file://<path>', "
			" where <path> is of the form /a/b/...'");
	const std::string::size_type protocol_str_length
		= std::string{"file:"}.length();
	if (uri.length() <= protocol_str_length)
	{
		throw std::runtime_error(fmt_msg);
	}
	// strip the leading string "file:"
	uri = uri.substr(protocol_str_length);
	// what is left can be a path /a/b/... or ///a/b/...
	if (uri.substr(0,2)=="//")
	{
		// strip the leading //
		uri = uri.substr(2);
	}
	if (uri.empty() || uri[0]!='/')
	{
		throw std::runtime_error(fmt_msg);
	}
	return uri;
}

/** Creates and returns a JSON object based from the provided \a uri.
 *  This \a uri must refer to a local filename that is relative to
 *  the MagnumPI source or installation directory. As an example,
 *  the uri file:/input/He+_He_ungerade.json will be resolved as
 *  <magnum_dir>/input/He+_He_ungerade.json, where <magnum_dir> is
 *  the value of the environment variable MAGNUMPI_INPUTDATA_DIR.
 *
 *  A web-aware application or a future version of the 'local' C++
 *  application may implement this member to take more advanced
 *  actions that reading from a local file.
 *
 *  \author Jan van Dijk
 *  \date   April 2019
 */
inline json_type create_json_from_uri(const std::string& uri)
{
	return read_json_from_file(
		plasimo::get_env_var("MAGNUMPI_INPUTDATA_DIR",true)
		+ parse_file_uri(uri));
}

} // namespace magnumpi

#endif // H_MAGNUM_MAGMUM_PARSE_URI_FILE_H
