#include <iostream>
#include <stdexcept>
#include "magnumpi/data_set.h"

int main(int argc, const char* argv[])
{
	try {
		using namespace magnumpi;

		// a data set with numeric row labels and numeric column labels:
		// 'a matrix'.
		data_set chi1("chi (He He+)",units::angle,"rad",{"b",units::length,"m"}, {"E",units::energy,"J"} );
		chi1.add_column(1e-19);
		chi1.add_column(2e-19);
		chi1.add_column(3e-19);
		chi1.add_column(4e-19);
		chi1.add_row(1e-10);
		chi1.add_row(2e-10);
		chi1.add_row(3e-10);
		for (data_set::size_type r=0; r!=chi1.row_axis().size(); ++r)
		for (data_set::size_type c=0; c!=chi1.col_axis().size(); ++c)
		{
			chi1(r,c) = r*10+c;
		}
		chi1.write(std::cout);

		// a data set with numeric row labels and labeled column:
		// 'y_i(x)'.
		data_set chi2("chi",units::angle,"rad",{"b",units::length,"m"}, {"Species pair"} );
		chi2.add_column("He-He");
		chi2.add_column("He-He+");
		chi2.add_column("Ar-He");
		chi2.add_column("Ar-He+");
		chi2.add_row(1e-10);
		chi2.add_row(2e-10);
		chi2.add_row(3e-10);
		for (data_set::size_type r=0; r!=chi2.row_axis().size(); ++r)
		for (data_set::size_type c=0; c!=chi2.col_axis().size(); ++c)
		{
			chi2(r,c) = r*10+c;
		}
		chi2.write(std::cout);

		return 0;
	}
	catch (std::exception& exc)
	{
		std::cerr << exc.what() << std::endl;
		return 1;
	}
	return 0;
}
