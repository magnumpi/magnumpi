#include "parse_uri.h"
#include "magnumpi/json.h"

/*  Example invocation:
 *  ./example_json_uri file:///input/He+_He_ungerade.json
 */
int main(int argc, const char* argv[])
{
	if (argc!=2)
	{
		std::cerr << "Usage: example_file_uri <file-uri>" << std::endl;
		return 1;
	}
	const magnumpi::json_type j{ magnumpi::create_json_from_uri(argv[1]) };
	std::cout << j.dump(2) << std::endl;
	return 0;
}
