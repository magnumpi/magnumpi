/* 
 * Based on an example by Jean-Philippe Barrette-LaPierre>
 * that is part of the curlpp project.
 */

#include <cerrno>
#include <sstream>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>

#include "magnumpi/json.h"

int main(int argc, char *argv[])
{
	if (argc!=3)
	{
		std::cerr << "Usage: example_curlpp_post <url> <json-string>" << std::endl;
		return 1;
	}
	
	const std::string url{argv[1]};
	const std::string body{argv[2]};

	std::cout << "URL: \"" << url << "\"." << std::endl;
	std::cout << "Body: \"" << body << "\"." << std::endl;
	
	try {
		curlpp::Cleanup cleaner;
		curlpp::Easy request;
		
		request.setOpt(new curlpp::options::Url(url)); 
		request.setOpt(new curlpp::options::Verbose(true)); 
		
		std::list<std::string> header; 
		header.push_back("Content-Type: application/json"); 
		
		request.setOpt(new curlpp::options::HttpHeader(header)); 
		
		request.setOpt(new curlpp::options::PostFields(body));
		request.setOpt(new curlpp::options::PostFieldSize(body.length()));

		std::stringstream response;
		request.setOpt(new curlpp::options::WriteStream(&response));
		
		request.perform(); 

		std::cout << " *** response begin *** " << std::endl;
		std::cout << response.str() << std::endl;
		std::cout << " *** response end *** " << std::endl;

		magnumpi::json_type potential_data(magnumpi::read_json_from_stream(response));
		std::cout << "I created the following JSON object." << std::endl;
		std::cout << potential_data << std::endl;
	}
	catch ( std::exception& e )
	{
		std::cout << e.what() << std::endl;
		return 1;
	}

	return 0;
}
