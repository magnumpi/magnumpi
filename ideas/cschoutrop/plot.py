import matplotlib.pyplot as plt
import numpy as np
import re
import sys


class struct(object):
    pass


def process_metadata(raw_metadata):
	'''
	This aims to transform
	# - column 1: 'epsilon' [J]
	# - columns [2:6]: data set 'sigma_l(eps)' [m^2] (size=5)
	# - column 2: '[l 0]'
	# - column 3: '[l 1]'
	# - column 4: '[l 2]'
	# - column 5: '[l 3]'
	# - column 6: '[l 4]'
	#epsilon[J]	0	1	2	3	4
	into a a struct named metadata with fields:
	metadata.xlabel="$epsilon$ [J]"
	metadata.ylabel="$sigma_l(eps)$ [m^2]"
	metadata.column_label=["l 0","l 1","l 2","l 3","l 4","l 5"]}

	Current assumptions:
	1. last row is irrelevant
	2. first row is the x-info
	3. 2nd row contains y-info
	4. subsequent rows contain column labels
	'''
	metadata = struct()

	#Transform the first column:
	#From: 	# - column 1: 'epsilon' [J]
	#To: 	'epsilon' [J]
	p1 = re.compile("\'.*?\]")
	col1_match = p1.findall(raw_metadata[0])
	metadata.xlabel = re.sub("'", "$", col1_match[0])
	#Transform the second column:
	#From: 	# - columns [2:6]: data set 'sigma_l(eps)' [m^2] (size=5)
	#To 	'sigma_l(eps)' [m^2]
	col2_match = p1.findall(raw_metadata[1])
	metadata.ylabel = re.sub("'", "$", col2_match[0])

	#Transform the rest of the columns
	#From for example: 	# - column 6: '[l 4]'
	#To: 			l 4
	p2 = re.compile("\[(.*?)\]")
	column_label = []
	for i in range(2, len(raw_metadata)-1):
		column_match = p2.findall(raw_metadata[i])
		column_label.append(re.sub("'", "$", column_match[0]))
	metadata.column_label = column_label
	return metadata


def load_file(filename):
	#load input file, skip everything with #, return numpy array
	lines_to_skip = 0
	raw_metadata = []
	for line in open(filename):
		li = line.strip()
		if li.startswith("#"):
			lines_to_skip = lines_to_skip+1
			raw_metadata.append(li)
	#This could be more efficient
	#And a parser for the header could be used to deduce labels
	data = np.loadtxt(filename, skiprows=lines_to_skip)
	if len(raw_metadata) is 0:
		metadata = struct()
	else:
		metadata = process_metadata(raw_metadata)
	#Replace extensions with .pdf
	metadata.plotname = re.sub("\.(.*?)$", ".pdf", filename)
	return data, metadata


def plot_columns(data, metadata):
	#Plot each column as function of the first column
	figure_1 = plt.figure()
	if hasattr(metadata, "column_label") and len(metadata.column_label)+1 == np.size(data, 1):
		for i in range(1, np.size(data, 1)):
			plt.loglog(data[:, 0], data[:, i], label=metadata.column_label[i-1])
	else:
		for i in range(1, np.size(data, 1)):
			plt.loglog(data[:, 0], data[:, i], label="Col "+str(i))

	if hasattr(metadata, 'xlabel'):
		plt.xlabel(metadata.xlabel)
	if hasattr(metadata, 'ylabel'):
		plt.ylabel(metadata.ylabel)

	plt.show()
	plt.legend()
	figure_1.savefig(metadata.plotname, bbox_inches='tight')


if __name__ == "__main__":
	#It expects the input name as an argument
	filename = sys.argv[1]

	data, metadata = load_file(filename)
	print("Plotting: "+filename)
	plot_columns(data, metadata)
