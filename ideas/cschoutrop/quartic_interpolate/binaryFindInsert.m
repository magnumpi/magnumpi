function [L,R] = binaryFindInsert(ArrayIn, ValueIn)
%{
ArrayIn is sorted input array,
this returns two indices in between which the value is located in the
array.
If value<min(ArrayIn) L=0, R=1
If value>=max(ArrayIn) L=length(ArrayIn), R=length(ArrayIn)+1
%}
assert(length(ValueIn)==1,'Input value must be a scalar')
n=length(ArrayIn);
assert(n>0,'Input array must be non-empty')
L=1;
R=n;
if ArrayIn(1)>ValueIn
    %check if it's smaller than the first value
    %indexValue=[0 1];
    L=0;
    R=1;
    return
end
if ArrayIn(n)<ValueIn
    %check if it's smaller than the last value
    L=n;
    R=n+1;
    return
end
isFound=false;
while isFound==false%L<R
    m=ceil((L+R)/2);            %chose pivot halfway through interval
    ArrayValuem=ArrayIn(m);     %store value of array there as temp
    if ArrayValuem<=ValueIn      %if the value is less than the wanted value, it is not in the lower half
        %L=m+1;
        L=m;
    elseif ArrayValuem>ValueIn %if the value is more than the wanted value, it is not in the upper half
        %R=m-1;
        R=m;    %m-1 helps to speed up for discrete case, not if the value is not in the array
    else
        error('Invalid input array or input value')
    end
    if (R-L)==1
        %isFound=true;
		return
    end
end
%Can't be found if it does not return the indexValue.
%indexValue=-1;
%error('Requested element not found in Array')

end