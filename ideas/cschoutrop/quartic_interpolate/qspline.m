
classdef qspline <handle
    properties (Access=public)
        %None
    end
    properties (Access=public) %private
        m_x %input x-values
        m_s %input values
        
        m_y %Estimates derivative of input values
        
        m_N
        %x,y coordinates of points
    end
    properties (Access=private,Constant)
        I=[-1/2 1/2 1/10 -1/10;...
            -1/2 1/2 -1/10 1/10;...
            -1/10 1/10 0 -1/60;...
            1/10 -1/10 1/60 0];
        %{
        I=[ I0000 I0001 I0010 I0011;...
            I0100 I0101 I0110 I0111;...
            I1000 I1001 I1010 I1011;...
            I1100 I1101 I1110 I1111;...
            ]
            %}
            I0000 = -1/2; I0010 = 1/10; I0001 = 1/2; I0011 = -1/10;
            I1000 = -1/10; I1010 = 0; I1001 = 1/10; I1011 = -1/60;
            I0100 = -1/2; I0110 = -1/10; I0101 = 1/2; I0111 = 1/10;
            I1100 = 1/10; I1110 = 1/60; I1101 = -1/10; I1111 = 0;
    end
    properties (Access=private)
        m_A
        m_b
        m_xh
        m_yh
    end
    methods (Access=public)
        function obj=qspline()
            %ctor
        end
        function obj=set_points(obj,vec_x,vec_s)
            sz=size(vec_x);
            if sz(2)>sz(1)
                vec_x=transpose(vec_x);
            end
            sz=size(vec_s);
            if sz(2)>sz(1)
                vec_s=transpose(vec_s);
            end
            obj.m_x=vec_x;
            obj.m_N=length(obj.m_x);
            obj.m_xh=obj.calc_xh();
            
            obj.m_s=vec_s;
            obj.m_y=obj.calc_y();
            obj.m_yh=obj.calc_yh();
        end       

        function out=get(obj,xv)
            [is_lower,ix_lower,ix_upper]=obj.upper_lower_interval(xv);
            if is_lower
                out=obj.gL(xv,ix_lower,0);
            else
                out=obj.gR(xv,ix_lower,0);
            end
        end
        function out=deriv1(obj,xv)
            [is_lower,ix_lower,ix_upper]=obj.upper_lower_interval(xv);
            if is_lower
                out=obj.gL(xv,ix_lower,1);
            else
                out=obj.gR(xv,ix_lower,1);
            end            
        end
        function out=deriv2(obj,xv)
            [is_lower,ix_lower,ix_upper]=obj.upper_lower_interval(xv);
            if is_lower
                out=obj.gL(xv,ix_lower,2);
            else
                out=obj.gR(xv,ix_lower,2);
            end            
        end
    end
    methods (Access=private)
%         function out=calc_y(obj)
%             %Estimate the derivative of the input using quadratic
%             %polynomials
%             out=zeros(obj.m_N,1);
%             p=polyfit(obj.m_x(1:3),obj.m_s(1:3),2);
%             p_der=polyder(p);
%             out(1)=polyval(p_der,obj.m_x(1));
%             out(2)=polyval(p_der,obj.m_x(2));
%             for i=3:obj.m_N-1
%                 p=polyfit(obj.m_x(i-1:i+1),obj.m_s(i-1:i+1),2);
%                 p_der=polyder(p);
%                 out(i)=polyval(p_der,obj.m_x(i));
%             end
%             out(obj.m_N)=polyval(p_der,obj.m_x(obj.m_N));
%         end  
        function out=calc_y(obj)
            %Estimate the derivative of the input using cubic polynomials
            %This is better if one needs the first 2 derivatives to be
            %accurate.
            out=zeros(obj.m_N,1);
            p=polyfit(obj.m_x(1:4),obj.m_s(1:4),3);
            p_der=polyder(p);
            out(1)=polyval(p_der,obj.m_x(1));
            out(2)=polyval(p_der,obj.m_x(2));
            for i=3:obj.m_N-2
                p=polyfit(obj.m_x(i-1:i+2),obj.m_s(i-1:i+2),3);
                p_der=polyder(p);
                out(i)=polyval(p_der,obj.m_x(i));
            end
            p=polyfit(obj.m_x(end-3:end),obj.m_s(end-3:end),3);
            p_der=polyder(p);            
            out(obj.m_N-1)=polyval(p_der,obj.m_x(obj.m_N-1));
            out(obj.m_N)=polyval(p_der,obj.m_x(obj.m_N));
        end         
        function out=calc_xh(obj)
            dx=obj.m_x(2:end)-obj.m_x(1:end-1);
            obj.m_xh=dx./2+obj.m_x(1:end-1);
            out=[obj.m_x(1)-dx(1)/2; obj.m_xh; obj.m_x(end)+dx(end)/2];
        end
        function out=calc_yh(obj)
            A_diag=[0.5;NaN(obj.m_N-1,1);0.5];
            A_super=[0.5;NaN(obj.m_N-1,1)];
            A_sub=[NaN(obj.m_N-1,1);0.5];
            for ix=1:obj.m_N-1
                A_diag(ix+1)=obj.Av2(ix);
                A_super(ix+1)=obj.Av3(ix);
                A_sub(ix)=obj.Av1(ix);
            end
            
            obj.m_A=spdiags(A_sub,-1,obj.m_N+1,obj.m_N+1);
            obj.m_A=obj.m_A+spdiags(A_diag,0,obj.m_N+1,obj.m_N+1);
            obj.m_A=obj.m_A+spdiags([NaN; A_super],1,obj.m_N+1,obj.m_N+1);
            %This NaN is needed to make spdiags work, not a bug.
            
            
            obj.m_b=[obj.m_y(1);NaN(obj.m_N-1,1);obj.m_y(obj.m_N)];
            for ix=1:obj.m_N-1
                obj.m_b(ix+1)=obj.bv(ix);
            end
            
            out=obj.m_A\obj.m_b;
        end
        function out=bv(obj,ix)
            out=(obj.m_s(ix+1)-obj.m_s(ix))+...
                (...
                -qspline.sigma00(obj.m_xh(ix),obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1))...
                +0.5.*qspline.sigma11(obj.m_xh(ix),obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1))...
                +0.5.*qspline.sigma10(obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1),obj.m_xh(ix+2))...
                )...
                .*obj.m_y(ix)+...
                (...
                -0.5.*qspline.sigma11(obj.m_xh(ix),obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1))...
                -qspline.sigma01(obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1),obj.m_xh(ix+2))...
                -0.5.*qspline.sigma10(obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1),obj.m_xh(ix+2))...
                ).*obj.m_y(ix+1);
        end
        function out=Av1(obj,ix)
            out=-0.5.*qspline.sigma10(obj.m_xh(ix),obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1));
        end
        function out=Av2(obj,ix)
            out=...
                qspline.sigma01(obj.m_xh(ix),obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1))...
                +0.5.*qspline.sigma10(obj.m_xh(ix),obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1))...
                -0.5.*qspline.sigma11(obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1),obj.m_xh(ix+2))...
                +qspline.sigma00(obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1),obj.m_xh(ix+2));
        end
        function out=Av3(obj,ix)
            out=0.5.*qspline.sigma11(obj.m_x(ix),obj.m_xh(ix+1),obj.m_x(ix+1),obj.m_xh(ix+2));
        end
        function [is_lower,ix_lower,ix_upper]=upper_lower_interval(obj,xv)
            %This can be done with std::lower_bound() for same complexity
            [ix_lower, ix_upper]=binaryFindInsert(obj.m_x,xv);
            x_low=obj.m_x(ix_lower);
            x_high=obj.m_x(ix_upper);
            x_half=(x_low+x_high)/2;
            if xv<=(x_half)
                is_lower=true;
            else
                is_lower=false;
            end
        end
        function out=qixplushalf(obj,ix)
            out=0.5.*(obj.m_y(ix+1)-obj.m_y(ix));
        end
        function out=qix(obj,ix)
            out=0.5.*(obj.m_yh(ix+1)-obj.m_yh(ix));
        end
        function out=uL(obj,xv,ix,d)
            if d==0
                out=(xv-obj.m_x(ix))./(obj.m_xh(ix+1)-obj.m_x(ix));
            elseif d==1
                out=1./(obj.m_xh(ix+1)-obj.m_x(ix));
            else
                out=0;
            end
        end
        function out=uR(obj,xv,ix,d)
            if d==0
                out=(xv-obj.m_xh(ix+1))./(obj.m_x(ix+1)-obj.m_xh(ix+1));
            elseif d==1
                out=1./(obj.m_x(ix+1)-obj.m_xh(ix+1));
            else
                out=0;
            end
        end
        
        function out=gL(obj,xv,ix,d)
            u=obj.uL(xv,ix,0);
            if d==0
                out=obj.m_s(ix);
            else
                out=0;
            end
            interp_value=(obj.m_xh(ix+1)-obj.m_x(ix))...
            .*(...
                obj.m_y(ix).*qspline.G00(u,d)+...
                obj.m_yh(ix+1).*qspline.G01(u,d)+...
                obj.qix(ix).*qspline.G10(u,d)+...
                obj.qixplushalf(ix).*qspline.G11(u,d)...
                );
            if d==1
                interp_value=interp_value.*obj.uL(xv,ix,1);
            elseif d==2
                interp_value=interp_value.*obj.uL(xv,ix,1).^2;
            end
            out=out+interp_value;
        end
        function out=gR(obj,xv,ix,d)
            u=obj.uR(xv,ix,0);
            if d==0
                out=obj.gL(obj.m_xh(ix+1),ix,d);
            else
                out=0;
            end
            
             interp_value=(obj.m_x(ix+1)-obj.m_xh(ix+1))...
                .*(...
                obj.m_yh(ix+1).*qspline.G00(u,d)+...
                obj.m_y(ix+1).*qspline.G01(u,d)+...
                obj.qixplushalf(ix).*qspline.G10(u,d)+...
                obj.qix(ix+1).*qspline.G11(u,d)...                
                );
            if d==1
                interp_value=interp_value.*obj.uR(xv,ix,1);
            elseif d==2
                interp_value=interp_value.*obj.uR(xv,ix,1).^2;
            end
            out=out+interp_value;
        end        
    end
    methods (Access=private,Static)        
        function out=pL(xR,xLL)
            out=0.5.*(xR-xLL);
        end
        function out=pR(xL,xRR)
            out=0.5.*(xRR-xL);
        end
        function out=sigma00(xLL,xL,xR,xRR)
            out=xL.*qspline.I0000 + xR.*qspline.I0001 + qspline.pL(xR, xLL).*qspline.I0010 + qspline.pR(xL, xRR).*qspline.I0011;
        end
        function out=sigma01(xLL,xL,xR,xRR)
            out=xL.*qspline.I0100 + xR.*qspline.I0101 + qspline.pL(xR, xLL).*qspline.I0110 + qspline.pR(xL, xRR).*qspline.I0111;
        end
        function out=sigma10(xLL,xL,xR,xRR)
            out=xL.*qspline.I1000 + xR.*qspline.I1001 + qspline.pL(xR, xLL).*qspline.I1010 + qspline.pR(xL, xRR).*qspline.I1011;
        end
        function out=sigma11(xLL,xL,xR,xRR)
            out=xL.*qspline.I1100 + xR.*qspline.I1101 + qspline.pL(xR, xLL).*qspline.I1110 + qspline.pR(xL, xRR).*qspline.I1111;
        end
        %G are the Hermite base functions
        function G=G00(u,d)
            %u(1-u^2-u^3/2)
            %u^4/2-u^3+u
            p=[1/2,-1,0,1,0];
            for i=1:d
                p=polyder(p);
            end            
            G=polyval(p,u);            
        end
        function G=G01(u,d)
            %u^3*(1-u/2)
            %-u^4/2+u^3
            p=[-1/2,1,0,0,0];
            for i=1:d
                p=polyder(p);
            end            
            G=polyval(p,u);            
        end
        function G=G10(u,d)
            %u^2*(3u^2-8u+6)/12
            %(3/12)u^4-(2/3)u^3+(1/2)u^2
            p=[1/4,-2/3,1/2,0,0];
            for i=1:d
                p=polyder(p);
            end            
            G=polyval(p,u);            
        end
        function G=G11(u,d)
            %u^3*(3u-4)/12
            %(1/4)u^4-(1/3)u^3
            p=[1/4,-1/3,0,0,0];
            for i=1:d
                p=polyder(p);
            end            
            G=polyval(p,u);
        end
        
        
    end
end