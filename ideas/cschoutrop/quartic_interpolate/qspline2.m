 
classdef qspline2 <handle
    properties (Access=public)
        %None
    end
    properties (Access=private)
        m_vec_x
        m_vec_y
        %x,y coordinates of points
        
        m_vec_a
        m_vec_b
        m_vec_c
        %spline coefficients
        
        m_b0
        m_c0
        %bd_type?
        m_left_value
        m_right_value
        
    end
    properties (Access=private,Constant)
        I=[-1/2 1/2 1/10 -1/10;...
            -1/2 1/2 -1/10 1/10;...
            -1/10 1/10 0 -1/60;...
            1/10 -1/10 1/60 0];
        %{
        I=[ I0000 I0001 I0010 I0011;...
            I0100 I0101 I0110 I0111;...
            I1000 I1001 I1010 I1011;...
            I1100 I1101 I1110 I1111;...
            ]
        %}
    end
    methods (Access=public)
        function obj=qspline()
            %ctor
        end
        function obj=set_points(obj,vec_x,vec_y)
            %{
            x 
            y Locations of control points
            s Locations of input points
            %}
            %Input points have to be sorted to allow for O(lg(n)) lookups
            [obj.m_vec_x,ix]=sort(vec_x);
            obj.m_vec_y=vec_y(ix);
            N=length(obj.m_vec_x);
            
            matrix_bands=zeros(N+1,3);
            rhs=zeros(N+1,1);
            
            %{
                boundary points
                counter-symmetric boundary condition is used, since it acts
                like a linear extrapolation. 
            %}
            %Left boundary i=1 and i=2
            %Left ghost point:
            x_left_ghost =obj.m_vec_x(1)-(obj.m_vec_x(2)+obj.m_vec_x(1))/2         %x_{1/2}
            x_right_ghost=obj.m_vec_x(end)+(obj.m_vec_x(end)-obj.m_vec_x(end-1))/2    %x_{end+1/2}
            
            y_left_ghost =obj.m_vec_y(1)-(obj.m_vec_y(2)-obj.m_vec_y(1))
            y_right_ghost=obj.m_vec_y(N)+(obj.m_vec_y(N)-obj.m_vec_y(N-1))
            %loop intervals [x_i, x_{i+1}]
            for i=1:N-1
                %{
                Last part of section 2.4
                %}
                %Lower interval [x_i,x_{i+1/2}]
                if i==1
                    xLL=x_left_ghost;                           %x_{1/2}
                else
                    xLL=(obj.m_vec_x(i)+obj.m_vec_x(i-1))/2;    %x_{i-1/2}
                end
                xL =obj.m_vec_x(i);                             %x_i
                xR =(obj.m_vec_x(i)+obj.m_vec_x(i+1))/2;        %x_{i+1/2}
                xRR=obj.m_vec_x(i+1);                           %x_{i+1}
                pL=(xR-xLL)/2;
                pR=(xRR-xL)/2;                
                sigma_left=qspline.I*[xL;xR;pL;pR];             %=[sigma00;sigma01;sigma10;sigma11]
                
                %Upper inverval [x_{i+1/2},x_i]
                xLL=obj.m_vec_x(i);                             %x_i
                xL =(obj.m_vec_x(i)+obj.m_vec_x(i+1))/2;        %x_{i+1/2}
                xR =obj.m_vec_x(i+1);                           %x_{i+1}
                if i==N-1
                    xRR=x_right_ghost;                          %x_{end+1/2}
                else
                    xRR=(obj.m_vec_x(i+1)+obj.m_vec_x(i+2))/2;  %x_{i+3/2}
                end
                pL=(xR-xLL)/2;
                pR=(xRR-xL)/2;                
                sigma_right=qspline.I*[xL;xR;pL;pR];            %=[sigma00;sigma01;sigma10;sigma11]                
                
                adjusted_area=obj.m_vec_y(i+1)-obj.m_vec_y(i);  %equation 13

                matrix_bands(i+1,1)=-sigma_left(3)/2
                matrix_bands(i+1,2)=sigma_left(2)+sigma_left(3)/2+sigma_right(1)-sigma_right(4)/2;
                matrix_bands(i+1,3)=sigma_right(4)/2
                y_i=obj.m_vec_y(i);
                y_iP=obj.m_vec_y(i+1);
                rhs_temp=adjusted_area-(sigma_left(1).*y_i+sigma_left(4)*(y_iP-y_i)/2);
                rhs(i+1,1)=rhs_temp-(sigma_right(2)*y_iP+sigma_right(3)*(y_iP-y_i)/2);
            end
            

                        
            
            %solve tridiagonal system for half-y points
            matrix_bands
            A=transpose(spdiags(matrix_bands,[-1,0,1],N+1,N+1))
            
            
            %Force ghost point boundaries ?
             A(1,1)=1;
%             A(1,2)=0;
             A(end,end)=1;
%             A(end,end-1)=0;
             rhs(1)=y_left_ghost
             rhs(end)=y_right_ghost
            
            plot(A\rhs)
            y_half=A\rhs
            rhs
            full(A)
        end
        function obj=get(obj,x)
            
        end
        function obj=deriv1(obj,x)
            
        end
        function obj=deriv2(obj,x)
            
        end
    end
    methods (Access=private)
        function u=u_in_interval(obj,x)
            %This can be done with std::lower_bound() for same complexity
            [ix_lower, ix_upper]=binaryFindInsert(obj.m_vec_x,x);
            x_low=obj.m_vec_x(ix_lower);
            x_high=obj.m_vec_x(ix_upper);
            x_half=(x_low+x_high)/2;
            if x<(x_half)
                u=(x-x_low)./(x_high-x_low); 
                %TODO:
                %Why not (x-x_low)./(x_half-x_low) ?
            else
                u=(x-x_half)./(x_high-x_half);
            end
        end
    end
    methods (Access=private,Static)
        %G are the Hermite base functions
        function G=G00(u)
            %u(1-u^2-u^3/2)
            %-u^4/2-u^3+u
            G=polyval([-1/2,-1,0,1,0],u);
        end
        function G=G01(u)
            %u^3*(1-u/2)
            %-u^4/2+u^3
            G=polyval([-1/2,+1,0,0,0],u);
        end
        function G=G10(u)
            %u^2*(3u^2-8u+6)/12
            %(3/12)u^4-(2/3)u^3+(1/2)u^2
            G=polyval([1/4,-2/3,1/2,0,0],u);
        end
        function G=G11(u)
            %u^3*(3u-4)/12
            %(1/4)u^4-(1/3)u^3
            G=polyval([1/4,-1/3,0,0,0],u);
        end
        
    end
end
