#include <iostream>

unsigned method1(unsigned m_l_max, unsigned m_s_max)
{
	unsigned m_dim = 0;

	for (unsigned l = 1; l <= m_l_max; ++l)
	{
		m_dim += m_s_max - l + 1;
		//This overflows if m_l_max>m_s_max
	}

	return m_dim;
}

unsigned method2(unsigned m_l_max, unsigned m_s_max)
{
	unsigned m_dim = 0;
	m_dim = ((m_l_max + 2 * m_l_max * m_s_max) - m_l_max * m_l_max) / 2;
	return m_dim;
}

int main()
{
	unsigned error_count = 0;

	for (unsigned m_l_max = 0; m_l_max < 40; ++m_l_max)
	{
		for (unsigned m_s_max = m_l_max; m_s_max < 40; ++m_s_max)
		{
			if (method1(m_l_max, m_s_max) != method2(m_l_max, m_s_max))
			{
				std::cout << m_l_max << "\n";
				std::cout << m_s_max << "\n";
				std::cout << method1(m_l_max, m_s_max) << "\n";
				std::cout << method2(m_l_max, m_s_max) << "\n";
				error_count++;
			}
		}
	}

	if (error_count == 0)
	{
		std::cout << "Methods are equal for tested parameters\n";
	}
	else
	{
		std::cout << "Methods are NOT equal for tested parameters\n";
	}
}
