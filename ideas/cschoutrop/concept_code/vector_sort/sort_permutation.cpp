#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <algorithm>
#include <numeric>
/*
	Concept version for functionality needed of the the vector sorting class vector_sort
	based on
	https://stackoverflow.com/questions/17074324/how-can-i-sort-two-vectors-in-the-same-way-with-criteria-that-uses-only-one-of
*/

template<typename T>
void print(std::vector<T> v)
{
	for(auto a:v){
		std::cout<<a<<" ";
	}
	std::cout<<"\n";
}

double fRand(double fMin, double fMax)
{
	double f = (double)rand() / RAND_MAX;
	return fMin + f * (fMax - fMin);
}

template <typename T>
std::vector<T> apply_permutation(const std::vector<T>& vec,const std::vector<std::size_t>& p)
{
	std::vector<T> sorted_vec(vec.size());
	std::transform(p.begin(), p.end(), sorted_vec.begin(),
		[&](std::size_t i){ return vec[i]; });
	return sorted_vec;
}
template <typename T>
void apply_permutation_in_place(std::vector<T>& vec,const std::vector<std::size_t>& p)
{
	std::vector<bool> done(vec.size());
	for (std::size_t i = 0; i < vec.size(); ++i)
	{
		if (done[i])
		{
			continue;
		}
		done[i] = true;
		std::size_t prev_j = i;
		std::size_t j = p[i];
		while (i != j)
		{
			std::swap(vec[prev_j], vec[j]);
			done[j] = true;
			prev_j = j;
			j = p[j];
		}
	}
}

template <typename T, typename Compare>
std::vector<std::size_t> sort_permutation(const std::vector<T>& vec,Compare compare)
{
	std::vector<std::size_t> p(vec.size());
	std::iota(p.begin(), p.end(), 0);
	std::sort(p.begin(), p.end(),
		[&](std::size_t i, std::size_t j){ return compare(vec[i], vec[j]); });
	return p;
}
int main()
{
	srand(0);
	int N=10;
	std::vector<double> m_x;
	std::vector<double> m_y;
	for(int i=0;i<N;++i){
		m_x.push_back(fRand(-10.0,10.0));
		m_y.push_back(fRand(-10.0,10.0));
	}
	print(m_x);
	print(m_y);

	auto permutation_vector = sort_permutation(m_x,
		[](double const& a, double const& b){ return b>a; });

	m_x = apply_permutation(m_x, permutation_vector);
	m_y = apply_permutation(m_y, permutation_vector);

	print(m_x);
	print(m_y);
}
