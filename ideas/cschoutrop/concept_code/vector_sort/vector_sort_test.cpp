#include <vector>
#include <random>
#include <iostream>
#include <chrono>
#include <iomanip>
#include "ideas/cschoutrop/concept_code/vector_sort/vector_sort.h"
/*
	Manual test for the vector sorting class vector_sort
*/

std::vector<double> random_double_vector(size_t size_of_vector, double min=-10.0,double max=+10.0)
{
	std::vector<double> vec(size_of_vector);
	std::uniform_real_distribution<double> unif(min,max);
	std::random_device rd;
	uint64_t random_seed;
	if(rd.entropy()!=0)
	{
		random_seed=rd();
	}
	else
	{
		using namespace std::chrono;
		auto point_in_time=high_resolution_clock::now().time_since_epoch();
		random_seed=point_in_time.count();
	}
	std::default_random_engine re(random_seed);
	for(auto &a:vec)
	{
		a=unif(re);
	}
	return vec;
}

template<typename T>
void print_vector(std::vector<T> vec)
{
	for(auto a:vec){
		std::cout<<std::setw(9)<<a<<" ";
	}
	std::cout<<"\n";
}

int main()
{
	{
		std::cout<<"Randomly generated vector test (ascending order)\n";
		int N=10;
		std::vector<double> m_x=random_double_vector(N);
		std::vector<double> m_y=random_double_vector(N);
		std::cout<<"Before sorting based on 1st vector: \n";
		print_vector(m_x);
		print_vector(m_y);

		vector_sort v(m_x,std::less_equal<double>());
		v.permute(m_x);
		v.permute(m_y);
		std::cout<<"After sorting based on 1st vector: \n";
		print_vector(m_x);
		print_vector(m_y);

	}
	std::cout<<"\n";
	{
		std::cout<<"Constant vector test (ascending order)\n";
		std::vector<double> m_x{-2.4,8,3.9,5.0};
		std::vector<double> m_y{3.4,9.1,0.6,-4.7};
		std::cout<<"Before sorting based on 1st vector: \n";
		print_vector(m_x);
		print_vector(m_y);
		vector_sort v(m_x,std::less_equal<double>());
		v.permute(m_x);
		v.permute(m_y);
		std::cout<<"After sorting based on 1st vector: \n";
		print_vector(m_x);
		print_vector(m_y);
	}
	std::cout<<"\n";
	{
		std::cout<<"Lambda based sorter test (descending order)\n";
		int N=9;
		std::vector<double> m_x=random_double_vector(N);
		std::vector<double> m_y=random_double_vector(N);
		std::cout<<"Before sorting based on 1st vector: \n";
		print_vector(m_x);
		print_vector(m_y);
		auto L=[](double a, double b){return a>b;};
		vector_sort v(m_x,L);
		v.permute(m_x);
		v.permute(m_y);
		std::cout<<"After sorting based on 1st vector: \n";
		print_vector(m_x);
		print_vector(m_y);

	}
}
