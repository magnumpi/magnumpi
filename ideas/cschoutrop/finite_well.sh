#!/bin/bash

script_path=$MAGNUMPI_DIR/../examples/
#Which scripts to run to generate the output needed
scripts=(		\
finite_well_cs.sh	\
finite_well_dcs.sh	\
finite_well_scat.sh	\
)

#Which output files to plot
output_files=(		\
"cs.txt"		\
"DCS.txt"		\
"scat_vs_b.new"		\
)

echo "Starting apps"
# run processes in background/parallel and store pids in array
pids=""
RESULT=0
for i in ${scripts[*]}; do
	$script_path/${i} &
	pids="$pids $!"
done
#Wait for all the scripts to finish running
for pid in $pids; do
	wait $pid || let "RESULT=1"
done
#If a script fails, then stop executing
if [ "$RESULT" == "1" ];
	then
		exit 1
fi
echo "Apps finished"

#Plot the generated output files
echo "Plotting"
DIR="$(dirname $0)"
#plot.py is for now located in the same folder as this .sh
for i in ${output_files[*]}; do
	#Loop over the output files, pass each to the plotting script
	python3 "$DIR/plot.py" ${i}
done




