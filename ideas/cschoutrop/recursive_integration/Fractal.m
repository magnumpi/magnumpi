classdef Fractal <handle
    %{
    Recursive method with coefficients from 9-point Gauss Kronrod method
    Here the error is estimated using the bisection method.
    %}
    properties (Constant)
        is_embedded=true;
                
        scheme_order=3;
        name='Fractal';
    end
    
    methods (Static)
        function [S1,S2] = integrate(a,b,fn)
            
            %Trapezoidal part
            S1=(fn(b)+fn(a)).*(b-a)./2;
            %Combined midpoint and trapezoidal rules
            S2=S1./2+fn((a+b)./2)*(b-a)./2;            
            
        end

    end
end

