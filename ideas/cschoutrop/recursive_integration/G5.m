classdef G5 <handle
    %{
    Recursive method with coefficients from 9-point Gauss Kronrod method
    Here the error is estimated using the bisection method.
    %}
    properties (Constant)
        is_embedded=false;
        
        %points
        x=[...
            -sqrt(245+14*sqrt(70))./21;...
            -sqrt(245-14*sqrt(70))./21;...
            0;...
            sqrt(245-14*sqrt(70))./21;...
            sqrt(245+14*sqrt(70))./21;...
            ];
        %weights
        w_G=[...
            (322-13.*sqrt(70))./900;...
            (322+13.*sqrt(70))./900;...
            128./225;...
            (322+13.*sqrt(70))./900;...
            (322-13.*sqrt(70))./900;...
            ];
        
        scheme_order=5*2+1;
        name='G5';
    end
    
    methods (Static)
        function [S1,S2,partial_lower,partial_upper] = integrate(a,b,fn,previous_estimate)
            if isempty(previous_estimate)
                S1=G5.integrate_interval(a,b,fn);
            else
                %{
                After a bisection the fine estimate becomes the course
                estimate for the next level down the recursion tree.
                This saves at best 33% of function evaluations.
                %}
                S1=previous_estimate;
            end
            partial_lower=G5.integrate_interval(a,(a+b)/2,fn);
            partial_upper=G5.integrate_interval((a+b)/2,b,fn);
            S2=partial_lower+partial_upper;
        end
        function S =integrate_interval(a,b,fn)
            %Transform to the interval [-1,1]
            rm=(b-a)/2;
            rp=(b+a)/2;
            
            n=length(G5.w_G);
            fn_vals=zeros(n,1);
            for i=1:n
                fn_vals(i)=fn(rm*G5.x(i)+rp);
            end
            I2=0;
            
            for i=1:n
                I2=I2+fn_vals(i)*G5.w_G(i);
            end
            S=I2*rm;
            
        end
    end
end

