function [N,val]=int_Fractal(a,b,fn,tol,N)
%Recursive method with coefficients from Colonna 2008
    I1=get_I1(a,b,fn);
    I2=get_I2(a,b,fn,I1);
    error=abs(I2-I1);
    if error>tol
        %Split interval in half if local error estimate is large
        [N_lower,lower_subdivision]=int_Fractal(a,(a+b)./2,fn,tol,N);
        [N_upper,higher_subdivision]=int_Fractal((a+b)./2,b,fn,tol,N);
        val=lower_subdivision+higher_subdivision;
        N=N+N_lower+N_upper;
    else
        val=I2;
    end
end
function I1=get_I1(a,b,fn)
    %Trapezoidal part
    I1=(fn(b)+fn(a)).*(b-a)./2;
end
function I2=get_I2(a,b,fn,I1)
    %Combined midpoint and trapezoidal rules
    I2=I1./2+fn((a+b)./2)*(b-a)./2;
end