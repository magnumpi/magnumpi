classdef GK9_bisection <handle
%{
    Recursive method with coefficients from 9-point Gauss Kronrod method
    Here the error is estimated using the bisection method.
%}
    properties (Constant)
        is_embedded=false;
        
        %points
        x=[...
            -0.9765602507375731115345;...
            -0.861136311594052575224;...
            -0.6402862174963099824047;...
            -0.3399810435848562648027;...
            0;...
            0.3399810435848562648027;...
            0.6402862174963099824047;...
            0.861136311594052575224;...
            0.9765602507375731115345...
            ];
        %weights
        w_K=[...
            0.06297737366547301476549;...
            0.1700536053357227268027;...
            0.2667983404522844480328;...
            0.326949189601451629558;...
            0.346442981890136361681;...
            0.3269491896014516295585;...
            0.2667983404522844480328;...
            0.1700536053357227268027;...
            0.0629773736654730147655;...
            ];
        
        scheme_order=3*4+1;
        name='GK9_bisection';
    end
    
    methods (Static)
        function [S1,S2,partial_lower,partial_upper] = integrate(a,b,fn,previous_estimate)
            if isempty(previous_estimate)
                S1=GK9_bisection.integrate_interval(a,b,fn);
            else
                %{
                After a bisection the fine estimate becomes the course
                estimate for the next level down the recursion tree.
                This saves at best 33% of function evaluations. 
                %}
                S1=previous_estimate;
            end
            partial_lower=GK9_bisection.integrate_interval(a,(a+b)/2,fn);
            partial_upper=GK9_bisection.integrate_interval((a+b)/2,b,fn);
            S2=partial_lower+partial_upper;
        end
        function S =integrate_interval(a,b,fn)
            %Transform to the interval [-1,1]
            rm=(b-a)/2;
            rp=(b+a)/2;
            
            n=length(GK9_bisection.w_K);
            fn_vals=zeros(n,1);
            for i=1:n
                fn_vals(i)=fn(rm*GK9_bisection.x(i)+rp);
            end
            I2=0;
            
            for i=1:n
                I2=I2+fn_vals(i)*GK9_bisection.w_K(i);
            end
            S=I2*rm;            
            
        end
    end
end

