classdef integrand <handle
%{
    integrand class to keep track of number of function evaluations.
    Can be extended to keep track of other metrics.
%}
    properties
        N_function_evals
        func
    end
    
    methods
        function obj=integrand(fn)
            obj.func=fn;
            obj.N_function_evals=0;
        end
        function value=get(obj,x)
            obj.N_function_evals=obj.N_function_evals+length(x);
            value=obj.func(x);
            return
        end
        
    end
end

