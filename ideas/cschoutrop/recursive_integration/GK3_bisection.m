classdef GK3_bisection <handle
    %{
    Recursive method with coefficients from 9-point Gauss Kronrod method
    Here the error is estimated using the bisection method.
    %}
    properties (Constant)
        is_embedded=false;
        
        scheme_order=1*3+1;
        name='GK3_bisection';
        
        %points
        x=[...
            -sqrt(3/5);...
            0;...
            sqrt(3/5);...
            ];
        %Kronrod weights
        w_K=[...
            5/9;...
            8/9;...
            5/9;...
            ];
    end
    
    methods (Static)
        function [S1,S2,partial_lower,partial_upper] = integrate(a,b,fn,previous_estimate)
            if isempty(previous_estimate)
                S1=GK3_bisection.integrate_interval(a,b,fn);
            else
                %{
                After a bisection the fine estimate becomes the course
                estimate for the next level down the recursion tree.
                This saves at best 33% of function evaluations.
                %}
                S1=previous_estimate;
            end
            partial_lower=GK3_bisection.integrate_interval(a,(a+b)/2,fn);
            partial_upper=GK3_bisection.integrate_interval((a+b)/2,b,fn);
            S2=partial_lower+partial_upper;
        end
        function S =integrate_interval(a,b,fn)
            %Transform to the interval [-1,1]
            rm=(b-a)/2;
            rp=(b+a)/2;
            
            n=length(GK3_bisection.w_K);
            fn_vals=zeros(n,1);
            for i=1:n
                fn_vals(i)=fn(rm*GK3_bisection.x(i)+rp);
            end
            I2=0;
            
            for i=1:n
                I2=I2+fn_vals(i)*GK3_bisection.w_K(i);
            end
            S=I2*rm;
            
        end
    end
end

