classdef Simpson_bisection <handle
    %{
    Recursive method with coefficients from 9-point Gauss Kronrod method
    Here the error is estimated using the bisection method.
    %}
    properties (Constant)
        is_embedded=false;
        
        scheme_order=4;
        name='Simpson_bisection';
    end
    
    methods (Static)
        function [S1,S2,partial_lower,partial_upper] = integrate(a,b,fn,previous_estimate)
            if isempty(previous_estimate)
                S1=Simpson_bisection.integrate_interval(a,b,fn);
            else
                %{
                After a bisection the fine estimate becomes the course
                estimate for the next level down the recursion tree.
                This saves at best 33% of function evaluations. 
                %}
                S1=previous_estimate;
            end
            partial_lower=Simpson_bisection.integrate_interval(a,(a+b)/2,fn);
            partial_upper=Simpson_bisection.integrate_interval((a+b)/2,b,fn);
            S2=partial_lower+partial_upper;
        end
        function S =integrate_interval(a,b,fn)
            %Trapezoidal part
            S1=(fn(b)+fn(a)).*(b-a)./2;
            %Combined midpoint and trapezoidal rules
            S2=S1./3+fn((a+b)./2)*(b-a)*(2./3);
            
            S=S2;
            
        end        
    end
end

