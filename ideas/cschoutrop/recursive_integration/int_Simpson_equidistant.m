function [N,val]=int_Simpson_equidistant(a,b,fn,N)
    %Simpsons rule for equidistant sampling of fn
    %See Atkinson equation 5.1.16
    
    if mod(N,2)==0
        N=N+1;
    end
    
    dx=(b-a)./(N-1);
    x=linspace(a,b,N);
    val=fn(x(1));
    for iter=2:2:N-1
        val=val+4*fn(x(iter));
    end
    for iter=3:2:N-2
        val=val+2*fn(x(iter));
    end    
    val=val+fn(x(end));
    val=val.*dx./3;
end
