function save_to_pdf( figure_handle,filename )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
set(figure_handle,'Units','Inches');
pos = get(figure_handle,'Position');
set(figure_handle,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)])
print(figure_handle,filename,'-dpdf','-r0')

end

