
#Plot settings
set terminal pdf
filename1="recursive_1.pdf"
filename2="recursive_2.pdf"
filename3="recursive_3.pdf"
filename4="recursive_4.pdf"

#Make plot 1
#%quadrature_rules={Fractal,G1,G2,G3,G4,G5}; %Gaussian quadratures
set output filename1
set font 'Arial-Bold'
plot    'results_recursive_formatted_c9b0e081.txt'  using 1:2 linestyle 1 with lines title 'Fractal',\
        'results_recursive_formatted_c9b0e081.txt'  using 3:4 linestyle 2 with lines title '1-Point Gaussian',\
        'results_recursive_formatted_c9b0e081.txt'  using 5:6 linestyle 3 with lines title '2-Point Gaussian',\
        'results_recursive_formatted_c9b0e081.txt'  using 7:8 linestyle 4 with lines title '3-Point Gaussian',\
        'results_recursive_formatted_c9b0e081.txt'  using 9:10 linestyle 5 with lines title '4-Point Gaussian',\
        'results_recursive_formatted_c9b0e081.txt'  using 11:12 linestyle 6 with lines title '5-Point Gaussian'
set logscale y
set logscale x
set autoscale fix
set xlabel 'Number of function evaluations' font 'Arial Bold,15'
set ylabel 'Error' font 'Arial Bold,15'
set xtics font 'Arial Bold,15'
set ytics font 'Arial Bold,15'
set format x "10^{%L}";
set format y "10^{%L}";
set style line 1 lc rgb 'red'  linewidth 1.5
set style line 2 lc rgb 'black' linewidth 1.5
set style line 3 lc rgb 'blue'  linewidth 1.5
set style line 4 lc rgb 'magenta' linewidth 1.5
set style line 5 lc rgb 'orange'  linewidth 1.5
set style line 6 lc rgb '#3CB371'  linewidth 1.5
set border linewidth 2
set key box linewidth 2 font 'Arial Bold,12'
set output filename1
replot

#Make plot 2
#%quadrature_rules={Fractal,GK3,GK9,GK15}; %Gauss-Kronrod with embedded error estimate
set output filename2
set font 'Arial-Bold'
plot    'results_recursive_formatted_c9b0e081.txt'  using 1:2 linestyle 1 with lines title 'Fractal',\
        'results_recursive_formatted_c9b0e081.txt'  using 13:14 linestyle 2 with lines title '3-Point Gauss-Kronrod embedded',\
        'results_recursive_formatted_c9b0e081.txt'  using 15:16 linestyle 3 with lines title '9-Point Gauss-Kronrod embedded',\
        'results_recursive_formatted_c9b0e081.txt'  using 17:18 linestyle 4 with lines title '15-Point Gauss-Kronrod embedded'
set logscale y
set logscale x
set autoscale fix
set xlabel 'Number of function evaluations' font 'Arial Bold,15'
set ylabel 'Error' font 'Arial Bold,15'
set xtics font 'Arial Bold,15'
set ytics font 'Arial Bold,15'
set format x "10^{%L}";
set format y "10^{%L}";
set style line 1 lc rgb 'red'  linewidth 1.5
set style line 2 lc rgb 'black' linewidth 1.5
set style line 3 lc rgb 'blue'  linewidth 1.5
set style line 4 lc rgb 'magenta' linewidth 1.5
set style line 5 lc rgb 'orange'  linewidth 1.5
set style line 6 lc rgb '#3CB371'  linewidth 1.5
set border linewidth 2
set key box linewidth 2 font 'Arial Bold,12'
set output filename2
replot

#Make plot 3
#%quadrature_rules={Fractal,GK3_bisection,GK9_bisection,GK15_bisection}; %Gauss-Kronrod with bisection error estimate
set output filename3
set font 'Arial-Bold'
plot    'results_recursive_formatted_c9b0e081.txt'  using 1:2 linestyle 1 with lines title 'Fractal',\
        'results_recursive_formatted_c9b0e081.txt'  using 19:20 linestyle 2 with lines title '3-Point Gauss-Kronrod bisection',\
        'results_recursive_formatted_c9b0e081.txt'  using 21:22 linestyle 3 with lines title '9-Point Gauss-Kronrod bisection',\
        'results_recursive_formatted_c9b0e081.txt'  using 23:24 linestyle 4 with lines title '15-Point Gauss-Kronrod bisection'
set logscale y
set logscale x
set autoscale fix
set xlabel 'Number of function evaluations' font 'Arial Bold,15'
set ylabel 'Error' font 'Arial Bold,15'
set xtics font 'Arial Bold,15'
set ytics font 'Arial Bold,15'
set format x "10^{%L}";
set format y "10^{%L}";
set style line 1 lc rgb 'red'  linewidth 1.5
set style line 2 lc rgb 'black' linewidth 1.5
set style line 3 lc rgb 'blue'  linewidth 1.5
set style line 4 lc rgb 'magenta' linewidth 1.5
set style line 5 lc rgb 'orange'  linewidth 1.5
set style line 6 lc rgb '#3CB371'  linewidth 1.5
set border linewidth 2
set key box linewidth 2 font 'Arial Bold,12'
set output filename3
replot

#Make plot 4
#%quadrature_rules={Fractal,Simpson,Simpson_bisection,'Clenshaw_Curtis','Simpson_equidistant'}; %Other quadratures
set output filename4
set font 'Arial-Bold'
plot    'results_recursive_formatted_c9b0e081.txt'  using 1:2 linestyle 1 with lines title 'Fractal',\
        'results_recursive_formatted_c9b0e081.txt'  using 25:26 linestyle 2 with lines title 'Simpson embedded',\
        'results_recursive_formatted_c9b0e081.txt'  using 27:28 linestyle 3 with lines title 'Simpson bisection',\
        'results_recursive_formatted_c9b0e081.txt'  using 29:30 linestyle 4 with lines title 'Clenshaw Curtis',\
        'results_recursive_formatted_c9b0e081.txt'  using 31:32 linestyle 5 with lines title 'Simpson equidistant'
set logscale y
set logscale x
set autoscale fix
set xlabel 'Number of function evaluations' font 'Arial Bold,15'
set ylabel 'Error' font 'Arial Bold,15'
set xtics font 'Arial Bold,15'
set ytics font 'Arial Bold,15'
set format x "10^{%L}";
set format y "10^{%L}";
set style line 1 lc rgb 'red'  linewidth 1.5
set style line 2 lc rgb 'black' linewidth 1.5
set style line 3 lc rgb 'blue'  linewidth 1.5
set style line 4 lc rgb 'magenta' linewidth 1.5
set style line 5 lc rgb 'orange'  linewidth 1.5
set style line 6 lc rgb '#3CB371'  linewidth 1.5
set border linewidth 2
set key box linewidth 2 font 'Arial Bold,12'
set output filename4
replot