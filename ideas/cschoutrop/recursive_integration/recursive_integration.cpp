
#include <iostream>
#include <array>
#include <cmath>
#include <algorithm>
#include <iomanip>
#include <chrono>

template<typename IntegrationMethod>
class Integrator
{
	public:
		double m_x_min;
		double m_x_max;
		double m_length;
		double m_tolerance = 1e-12;
		const double use_local_tolerance = true;
		Integrator(double x_min, double x_max, double tolerance): m_x_min(x_min), m_x_max(x_max), m_tolerance(tolerance)
		{
			m_length = m_x_max - m_x_min;
		}
		Integrator() {}
		void set_range(double x_min, double x_max)
		{
			m_x_min = x_min;
			m_x_max = x_max;
			m_length = m_x_max - m_x_min;
		}
		void set_tolerance(double tolerance)
		{
			m_tolerance = tolerance;
		}
		template<typename L>
		double integrate(L fn)
		{
			if constexpr(IntegrationMethod::is_embedded && IntegrationMethod::is_recycling == false)
			{
				return integrate_recursively_embedded(fn, m_x_min, m_x_max);
			}
			else if constexpr(IntegrationMethod::is_embedded == false && IntegrationMethod::is_recycling == false)
			{
				return integrate_recursively(fn, m_x_min, m_x_max, NAN);
			}
			else if constexpr(IntegrationMethod::is_embedded == true && IntegrationMethod::is_recycling == true)
			{
				return integrate_recursively_recycling_embedded(fn, m_x_min, m_x_max, NAN, NAN);
			}


		}
	private:
		template<typename L>
		double integrate_recursively(L fn, double a, double b, double previous_estimate)
		{
			/*
			        These methods use S1 (the integral approximation over [a,b]; previous_estimate) as the "inaccurate estimate"
			        S2 is the integral approximation over [a,(a+b)/2] + the approximation over [(a+b)/2,b].
			        The error estimate for the integral approximated by S2 on [a,b] is then |S1-S2|.

			        An example of this method is the 2-Point Gaussian method.
			*/
			double S1, S2, partial_lower, partial_upper;

			//Integrate from a to b
			IntegrationMethod::integrate(fn, a, b, S1, S2, partial_lower, partial_upper, previous_estimate);

			//Estimate the error in the integral on [a,b]
			double error = estimate_error(S1, S2, a, b);
			if (error > m_tolerance)
			{
				//Subdivide the interval if the error is too big.
				double lower_subdivision = integrate_recursively(fn, a, (a + b) / 2.0, partial_lower);
				double upper_subdivision = integrate_recursively(fn, (a + b) / 2.0, b, partial_upper);
				return lower_subdivision + upper_subdivision;
			}
			else
			{
				//If the error is small enough, return the most accurate estimate.
				return S2;
			}
		};
		template<typename L>
		double integrate_recursively_embedded(L fn, double a, double b)
		{
			/*
			        These methods have a built-in error estimate.
			        The most accurate estimate should be given by S2, S1 the least accurate estimate.
			        The error estimate for the integral approximated by S2 on [a,b] is then |S1-S2|.

			        An example of this method is the 15-Point Gauss-Kronrod method, which is a 15-Point Kronrod method
			        with embedded 7-Point Gaussian quadrature.
			*/
			double S1, S2;

			//Integrate from a to b
			IntegrationMethod::integrate(fn, a, b, S1, S2);

			//Estimate the error in the integral on [a,b]
			double error = estimate_error(S1, S2, a, b);
			if (error > m_tolerance)
			{
				//Subdivide the interval if the error is too big.
				double lower_subdivision = integrate_recursively_embedded(fn, a, (a + b) / 2.0);
				double upper_subdivision = integrate_recursively_embedded(fn, (a + b) / 2.0, b);
				return lower_subdivision + upper_subdivision;
			}
			else
			{
				//If the error is small enough, return the most accurate estimate.
				return S2;
			}
		};
		template<typename L>
		double integrate_recursively_recycling_embedded(L fn, double a, double b, double fn_a, double fn_b)
		{
			/*
			        These methods have a built-in error estimate.
			        The most accurate estimate should be given by S2, S1 the least accurate estimate.
			        The error estimate for the integral approximated by S2 on [a,b] is then |S1-S2|.

			        An example of this method is the Simpson's rule. The idea is that upon a bisection, only the new midpoint has to be calculated.
			*/
			if (std::isnan(fn_a))
			{
				fn_a = fn(a);
				fn_b = fn(b);
			}
			double fn_mid = fn((a + b) / 2.0);
			double S1, S2;
			//Integrate from a to b
			IntegrationMethod::integrate(fn, a, b, S1, S2, fn_a, fn_b, fn_mid);

			//Estimate the error in the integral on [a,b]
			double error = estimate_error(S1, S2, a, b);
			if (error > m_tolerance)
			{
				//Subdivide the interval if the error is too big.
				double lower_subdivision = integrate_recursively_recycling_embedded(fn, a, (a + b) / 2.0, fn_a, fn_mid);
				double upper_subdivision = integrate_recursively_recycling_embedded(fn, (a + b) / 2.0, b, fn_mid, fn_b);
				return lower_subdivision + upper_subdivision;
			}
			else
			{
				//If the error is small enough, return the most accurate estimate.
				return S2;
			}
		};
		double estimate_error(double S1, double S2, double a, double b)
		{
			double error = std::abs(S2 - S1);
			if (use_local_tolerance)
			{
				//Error estimate in this specific interval
				return error;
			}
			else
			{
				//Extrapolate how much the global error is
				return error * m_length / std::abs(b - a);
			}
		};
};

class GK15
{
	public:
		static const bool is_embedded = true;
		static const bool is_recycling = false;
		static const int scheme_order = 7 * 3 + 1;
		static const std::array<double, 15> nodes;
		static const std::array<double, 15> weights_Kronrod;
		static const std::array<double, 7> weights_Gauss;
		template<typename L>
		static void integrate(L fn, double a, double b, double& S1, double& S2)
		{
			double rm = (b - a) / 2.0;
			double rp = (b + a) / 2.0;
			const int N_points = GK15::nodes.size();
			std::array<double, 15> fn_vals;

			S2 = 0.0;
			//Kronrod quadrature
			for (int i = 0; i < N_points; ++i)
			{
				fn_vals[i] = fn(rm * GK15::nodes[i] + rp);
				S2 += fn_vals[i] * GK15::weights_Kronrod[i];
			}
			S2 = S2 * rm;

			//Embedded Gaussian quadrature
			int i_G = 0;
			S1 = 0.0;
			for (int i = 1; i < N_points; i += 2)
			{
				S1 += fn_vals[i] * GK15::weights_Gauss[i_G];
				i_G++;
			}
			S1 = S1 * rm;

		}
};
const std::array<double, 15> GK15::nodes{-0.9914553711208126392069, -0.9491079123427585245262, -0.8648644233597690727897, -0.7415311855993944398639, -0.5860872354676911302941, -0.4058451513773971669066, -0.2077849550078984676007, 0, 0.2077849550078984676007, 0.4058451513773971669066, 0.5860872354676911302941, 0.7415311855993944398639, 0.8648644233597690727897, 0.9491079123427585245262, 0.9914553711208126392069};
const std::array<double, 15> GK15::weights_Kronrod{0.0229353220105292249637, 0.0630920926299785532907, 0.1047900103222501838399, 0.140653259715525918745, 0.1690047266392679028266, 0.1903505780647854099133, 0.2044329400752988924142, 0.209482141084727828013, 0.2044329400752988924142, 0.190350578064785409913, 0.1690047266392679028266, 0.1406532597155259187452, 0.10479001032225018384, 0.0630920926299785532907, 0.02293532201052922496373};
const std::array<double, 7> GK15::weights_Gauss{0.129484966168869693271, 0.279705391489276667901, 0.3818300505051189449504, 0.4179591836734693877551, 0.3818300505051189449504, 0.2797053914892766679015, 0.1294849661688696932706};

class GK9
{
	public:
		static const bool is_embedded = true;
		static const bool is_recycling = false;
		static const int scheme_order = 4 * 3 + 1;
		static const std::array<double, 9> nodes;
		static const std::array<double, 9> weights_Kronrod;
		static const std::array<double, 4> weights_Gauss;
		template<typename L>
		static void integrate(L fn, double a, double b, double& S1, double& S2)
		{
			double rm = (b - a) / 2.0;
			double rp = (b + a) / 2.0;
			const int N_points = GK9::nodes.size();
			std::array<double, 9> fn_vals;

			S2 = 0.0;
			//Kronrod quadrature
			for (int i = 0; i < N_points; ++i)
			{
				fn_vals[i] = fn(rm * GK9::nodes[i] + rp);
				S2 += fn_vals[i] * GK9::weights_Kronrod[i];
			}
			S2 = S2 * rm;

			//Embedded Gaussian quadrature
			int i_G = 0;
			S1 = 0.0;
			for (int i = 1; i < N_points; i += 2)
			{
				S1 += fn_vals[i] * GK9::weights_Gauss[i_G];
				i_G++;
			}
			S1 = S1 * rm;

		}
};
const std::array<double, 9> GK9::nodes{-0.9765602507375731115345, -0.861136311594052575224, -0.6402862174963099824047, -0.3399810435848562648027, 0, 0.3399810435848562648027, 0.6402862174963099824047, 0.861136311594052575224, 0.9765602507375731115345,};
const std::array<double, 9> GK9::weights_Kronrod{0.06297737366547301476549, 0.1700536053357227268027, 0.2667983404522844480328, 0.326949189601451629558, 0.346442981890136361681, 0.3269491896014516295585, 0.2667983404522844480328, 0.1700536053357227268027, 0.0629773736654730147655,};
const std::array<double, 4> GK9::weights_Gauss{0.347854845137453857373, 0.6521451548625461426269, 0.6521451548625461426269, 0.347854845137453857373,};


class G2
{
	public:
		static const bool is_embedded = false;
		static const bool is_recycling = false;
		static const int scheme_order = 2 * 2 + 1;
		static const std::array<double, 2> nodes;
		static const std::array<double, 2> weights;
		template<typename L>
		static double integrate_interval(L fn, const double a, const double b)
		{
			//Transform the integration interval to [-1,1]
			const double rm = (b - a) / 2.0;
			const double rp = (b + a) / 2.0;
			const int N_points = G2::nodes.size();
			/*
			        There probably is a way to do this with std::accumulate,
			        but it'll be confusing.
			*/
			double I2 = 0.0;
			for (int i = 0; i < N_points; ++i)
			{
				I2 += fn(rm *  G2::nodes[i] + rp) *  G2::weights[i];
			}

			return I2 * rm;
		}
		template<typename L>
		static void integrate(L fn, double a, double b, double& S1, double& S2, double& partial_lower, double& partial_upper, double previous_estimate = NAN)
		{
			if (std::isnan(previous_estimate))
			{
				S1 = G2::integrate_interval(fn, a, b);
			}
			else
			{
				S1 = previous_estimate;
			}
			partial_lower = G2::integrate_interval(fn, a, (a + b) / 2.0);
			partial_upper = G2::integrate_interval(fn, (a + b) / 2.0, b);
			S2 = partial_lower + partial_upper;
		}
};
const std::array<double, 2> G2::nodes{-1 / std::sqrt(3), +1 / std::sqrt(3)};
const std::array<double, 2> G2::weights{1, 1};

class FractalEmbedded
{
	public:
		static const bool is_embedded = true;
		static const bool is_recycling = false;
		template<typename L>
		static void integrate(L fn, double a, double b, double& S1, double& S2)
		{
			//Trapezoidal rule
			double trapezoidal_estimate = (fn(b) + fn(a)) * (b - a) / 2.0;
			S1 = trapezoidal_estimate;

			//Midpoint rule (1-Point Gaussian quadrature)
			double midpoint_estimate = fn((a + b) / 2.0) * (b - a);

			//Combined
			S2 = trapezoidal_estimate / 2.0 + midpoint_estimate / 2.0;
		}
};
class FractalEmbeddedRecycle
{
	public:
		static const bool is_embedded = true;
		static const bool is_recycling = true;
		template<typename L>
		static void integrate(L fn, double a, double b, double& S1, double& S2, double fn_a, double fn_b, double fn_mid)
		{
			//Trapezoidal rule
			double trapezoidal_estimate = (fn_a + fn_b) * (b - a) / 2.0;
			S1 = trapezoidal_estimate;

			//fn_mid = fn((a + b) / 2.0);
			//Midpoint rule (1-Point Gaussian quadrature)
			double midpoint_estimate = fn_mid * (b - a);

			//Combined
			S2 = trapezoidal_estimate / 2.0 + midpoint_estimate / 2.0;
		}
};
class FractalBisection
{
	public:
		static const bool is_embedded = false;
		static const bool is_recycling = false;
		template<typename L>
		static double integrate_interval(L fn, const double a, const double b)
		{
			//Trapezoidal
			double trapezoidal_estimate = (fn(b) + fn(a)) * (b - a) / 2.0;

			//Midpoint rule (1-Point Gaussian quadrature)
			double midpoint_estimate = fn((a + b) / 2.0) * (b - a);

			//Combined
			return trapezoidal_estimate / 2.0 + midpoint_estimate / 2.0;
		}
		template<typename L>
		static void integrate(L fn, double a, double b, double& S1, double& S2, double& partial_lower, double& partial_upper, double previous_estimate = NAN)
		{
			if (std::isnan(previous_estimate))
			{
				S1 = FractalBisection::integrate_interval(fn, a, b);
			}
			else
			{
				S1 = previous_estimate;
			}
			partial_lower = FractalBisection::integrate_interval(fn, a, (a + b) / 2.0);
			partial_upper = FractalBisection::integrate_interval(fn, (a + b) / 2.0, b);
			S2 = partial_lower + partial_upper;
		}
};

class SimpsonEmbedded
{
	public:
		static const bool is_embedded = true;
		static const bool is_recycling = false;
		template<typename L>
		static void integrate(L fn, double a, double b, double& S1, double& S2)
		{
			//Trapezoidal
			double trapezoidal_estimate = (fn(b) + fn(a)) * (b - a) / 2.0;
			S1 = trapezoidal_estimate;

			//Midpoint rule (1-Point Gaussian quadrature)
			double midpoint_estimate = fn((a + b) / 2.0) * (b - a);

			//Combined
			S2 = trapezoidal_estimate / 3.0 + 2.0 * midpoint_estimate / 3.0;
		}
};
class SimpsonBisect
{
	public:
		static const bool is_embedded = false;
		static const bool is_recycling = false;
		template<typename L>
		static double integrate_interval(L fn, const double a, const double b)
		{
			//Trapezoidal
			double trapezoidal_estimate = (fn(b) + fn(a)) * (b - a) / 2.0;

			//Midpoint rule (1-Point Gaussian quadrature)
			double midpoint_estimate = fn((a + b) / 2.0) * (b - a);

			//Combined
			return trapezoidal_estimate / 3.0 + 2.0 * midpoint_estimate / 3.0;
		}
		template<typename L>
		static void integrate(L fn, double a, double b, double& S1, double& S2, double& partial_lower, double& partial_upper, double previous_estimate = NAN)
		{
			if (std::isnan(previous_estimate))
			{
				S1 = SimpsonBisect::integrate_interval(fn, a, b);
			}
			else
			{
				S1 = previous_estimate;
			}
			partial_lower = SimpsonBisect::integrate_interval(fn, a, (a + b) / 2.0);
			partial_upper = SimpsonBisect::integrate_interval(fn, (a + b) / 2.0, b);
			S2 = partial_lower + partial_upper;
		}
};
class SimpsonEmbeddedRecycle
{
	public:
		static const bool is_embedded = true;
		static const bool is_recycling = true;
		template<typename L>
		static void integrate(L fn, double a, double b, double& S1, double& S2, double fn_a, double fn_b, double fn_mid)
		{
			//Trapezoidal rule
			double trapezoidal_estimate = (fn_a + fn_b) * (b - a) / 2.0;
			S1 = trapezoidal_estimate;

			//fn_mid = fn((a + b) / 2.0);
			//Midpoint rule (1-Point Gaussian quadrature)
			double midpoint_estimate = fn_mid * (b - a);

			//Combined
			S2 = trapezoidal_estimate / 3.0 + 2.0 * midpoint_estimate / 3.0;
		}
};

template<typename T>
void run_test(T integrator)
{
	auto fn = [](double x)
	{
		return std::sin(M_PI * std::log(x));
	};
	auto primitive = [](double x)
	{
		return x * (std::sin(M_PI * std::log(x)) - M_PI * std::cos(M_PI * std::log(x))) / (1 + M_PI * M_PI);
	};
	double x_min = 1e-8;
	double x_max = 2;
	double exact_value = primitive(x_max) - primitive(x_min);

	std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
	integrator.set_range(x_min, x_max);
	integrator.set_tolerance(1e-10);
	double value = integrator.integrate(fn);
	std::cout << std::setprecision(std::numeric_limits<long double>::digits10 + 1) << value << std::endl;
	std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
	std::chrono::duration<double> time_span = std::chrono::duration_cast<std::chrono::duration<double>>
		(t2 - t1);
	std::cout << "Solver time: "  << std::setprecision(5) << time_span.count() << " seconds" << std::endl;
	std::cout << "Error: " << std::abs(value - exact_value) << std::endl << std::endl;
}

int main()
{
	{
		std::cout << "2-point Gauss" << std::endl;
		Integrator<G2> I;
		run_test(I);
	}
	{
		std::cout << "15-point Gauss Kronrod" << std::endl;
		Integrator<GK15> I;
		run_test(I);
	}
	{
		std::cout << "9-point Gauss Kronrod" << std::endl;
		Integrator<GK9> I;
		run_test(I);
	}
	{
		std::cout << "Embedded 'Fractal' method" << std::endl;
		Integrator<FractalEmbedded> I;
		run_test(I);
	}
	{
		std::cout << "Bisection 'Fractal' method" << std::endl;
		Integrator<FractalBisection> I;
		run_test(I);
	}
	{
		std::cout << "Embedded Simpson method" << std::endl;
		Integrator<SimpsonEmbedded> I;
		run_test(I);
	}
	{
		std::cout << "Bisection Simpson method" << std::endl;
		Integrator<SimpsonBisect> I;
		run_test(I);
	}
	{
		std::cout << "Embedded Recycling 'Fractal' method" << std::endl;
		Integrator<FractalEmbeddedRecycle> I;
		run_test(I);
	}
	{
		std::cout << "Embedded Recycling Simpson method" << std::endl;
		Integrator<SimpsonEmbeddedRecycle> I;
		run_test(I);
	}

	/*
	        Observations and conclusions:
	        Gauss Kronrod 15 is the fastest, and most accurate method overall.
		Gauss Kronrod 15 is the fastest, and most accurate embedded method.
		2-point Gauss is the fastest and most accurate bisection method.
		GK9 and GK15 are very similar in performance, GK15 is usually faster.

		Embedded Fractal is slightly better than Bisection Fractal.
		Bisection Simpson is better than Embedded Simpson.
		Both non-recycling Simpson methods are better than Fractal.
		Recycling embedded Simpson is more accurate than embedded recycling Fractal

		Recycling methods are better than non-recycling methods.
	*/

	return 0;
}