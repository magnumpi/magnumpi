classdef integrator <handle
    
    properties
        %input
        func
        x_min
        x_max
        
        %settings
        quadrature_rule
        use_richardson
        tolerance
        use_local_tolerance
        
        %derived
        L
        integral_value
        integrand_function
        scheme_order
        
        %diagnostics
        N_function_evals
        N_intervals
    end
    
    methods
        function obj = integrator(func,x_min,x_max,tolerance_type,tolerance,quadrature_rule,use_richardson)
            obj.func=func;
            obj.x_min=x_min;
            obj.x_max=x_max;
            assert(x_max>x_min,'x_max must be greater than x_min')
            obj.L=abs(x_max-x_min);
            
            obj.tolerance=tolerance;
            if strcmp('local',tolerance_type)
                obj.use_local_tolerance=true;
            elseif strcmp('global',tolerance_type)
                obj.use_local_tolerance=false;
            else
                error('unknown setting for tolerance_type')
            end
            obj.quadrature_rule=quadrature_rule;
            obj.use_richardson=use_richardson;
            
            N_intervals=1;
            
            I=integrand(obj.func);
            obj.integrand_function= @(x) I.get(x);
            obj.quadrature_rule=quadrature_rule;
            
            if obj.quadrature_rule.is_embedded && use_richardson
                warning('Richardson extrapolation cannot be used with embedded rules')
            end
            obj.scheme_order=obj.quadrature_rule.scheme_order;
            
            if quadrature_rule.is_embedded
                [obj.integral_value,obj.N_intervals]=obj.integrate_recursively_embedded(obj.x_min,obj.x_max,1);
            else
                [obj.integral_value,obj.N_intervals]=obj.integrate_recursively(obj.x_min,obj.x_max,1,[]);
            end
            obj.N_function_evals=I.N_function_evals;
        end
        
        function [integral_value,N_intervals] = integrate_recursively(obj,a,b,N_intervals,previous_estimate)
            [S1,S2,partial_lower,partial_upper]=obj.quadrature_rule.integrate(a,b,obj.integrand_function,previous_estimate);
            %assert(partial_lower+partial_upper==S2,'partial_lower+partial_upper !=S2')
            adjusted_error=obj.estimate_error(S1,S2,a,b);
            
            if adjusted_error>obj.tolerance
%                 [lower_subdivision,N_lower]=integrate_recursively(obj,a,(a+b)/2,N_intervals,[]);
%                 [upper_subdivision,N_upper]=integrate_recursively(obj,(a+b)/2,b,N_intervals,[]);
                [lower_subdivision,N_lower]=integrate_recursively(obj,a,(a+b)/2,N_intervals,partial_lower);
                [upper_subdivision,N_upper]=integrate_recursively(obj,(a+b)/2,b,N_intervals,partial_upper);                
                integral_value=lower_subdivision+upper_subdivision;
                N_intervals=N_intervals+N_lower+N_upper;
            else
                if obj.use_richardson
                    %This only holds for schemes that use bisection, not
                    %for embedded rules like Gauss-Kronrod.
                    integral_value=(2.^obj.scheme_order.*S2-S1)./(2.^obj.scheme_order-1);
                else
                    integral_value=S2;
                end
                N_intervals=N_intervals;
            end
            
        end
        
        function [integral_value,N_intervals] = integrate_recursively_embedded(obj,a,b,N_intervals)
            [S1,S2]=obj.quadrature_rule.integrate(a,b,obj.integrand_function);
            %assert(partial_lower+partial_upper==S2,'partial_lower+partial_upper !=S2')
            adjusted_error=obj.estimate_error(S1,S2,a,b);
            
            if adjusted_error>obj.tolerance
                [lower_subdivision,N_lower]=integrate_recursively_embedded(obj,a,(a+b)/2,N_intervals);
                [upper_subdivision,N_upper]=integrate_recursively_embedded(obj,(a+b)/2,b,N_intervals);
                integral_value=lower_subdivision+upper_subdivision;
                N_intervals=N_intervals+N_lower+N_upper;
            else
                integral_value=S2;
                N_intervals=N_intervals;
            end
            
        end
        
        function adjusted_error=estimate_error(obj,S1,S2,a,b)
            error=abs(S2-S1);
            if obj.use_local_tolerance
                adjusted_error=error;
            else
                adjusted_error=error.*obj.L./abs(b-a);
                %This estimate is very, very pessimistic
            end
        end
        
        
        
        function integrand_value=evaluate_integrand(obj,x)
            obj.N_function_evals=obj.N_function_evals+length(x);
            integrand_value=obj.func(x);
        end
        
        
    end
end

