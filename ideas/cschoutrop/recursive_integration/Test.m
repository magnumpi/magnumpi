%% Test Gaussian quadratures

assert(length(G1.x)==length(G1.w_G))
assert(length(G2.x)==length(G2.w_G))
assert(length(G3.x)==length(G3.w_G))
assert(length(G4.x)==length(G4.w_G))
assert(length(G5.x)==length(G5.w_G))
assert(almost_equal(sum(G1.w_G),2))
assert(almost_equal(sum(G2.w_G),2))
assert(almost_equal(sum(G3.w_G),2))
assert(almost_equal(sum(G4.w_G),2))
assert(almost_equal(sum(G5.w_G),2))

clear all
%% Test Kronrod quadratures

assert(length(GK3_bisection.x)==length(GK3_bisection.w_K))
assert(length(GK9_bisection.x)==length(GK9_bisection.w_K))
assert(length(GK15_bisection.x)==length(GK15_bisection.w_K))
assert(length(GK3.x)==length(GK3.w_K))
assert(length(GK9.x)==length(GK9.w_K))
assert(length(GK15.x)==length(GK15.w_K))
assert(almost_equal(sum(GK3_bisection.w_K),2))
assert(almost_equal(sum(GK9_bisection.w_K),2))
assert(almost_equal(sum(GK15_bisection.w_K),2))
assert(almost_equal(sum(GK3.w_K),2))
assert(almost_equal(sum(GK9.w_K),2))
assert(almost_equal(sum(GK15.w_K),2))
assert(almost_equal(sum(GK3.w_G),2))
assert(almost_equal(sum(GK9.w_G),2))
assert(almost_equal(sum(GK15.w_G),2))


clear all
%% Test integrand

[ x_min,x_max,fn,result_exact ] = test_functions(1);
I=integrand(fn);
func= @(x) I.get(x);
func([1,2,3,4]);
assert(I.N_function_evals==4);
func([1,2]);
assert(I.N_function_evals==6);
func(18)
assert(I.N_function_evals==7);

clear all

%% Test known results

x_min=-pi;
x_max=+pi;
N=10;
func=@(x) x+1;
expected_value=2*pi;
[N,val]=int_Clenshaw_Curtis(x_min,x_max,func,N);
assert(almost_equal(val,expected_value));
[N,val]=int_Simpson_equidistant(x_min,x_max,func,N);
assert(almost_equal(val,expected_value));
[S1,S2]=Fractal.integrate(x_min,x_max,func);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));

[S1,S2]=GK3.integrate(x_min,x_max,func);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));
[S1,S2]=GK9.integrate(x_min,x_max,func);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));
[S1,S2]=GK15.integrate(x_min,x_max,func);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));

[S1,S2]=G1.integrate(x_min,x_max,func,[]);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));
[S1,S2]=G2.integrate(x_min,x_max,func,[]);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));
[S1,S2]=G3.integrate(x_min,x_max,func,[]);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));
[S1,S2]=G4.integrate(x_min,x_max,func,[]);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));
[S1,S2]=G5.integrate(x_min,x_max,func,[]);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));

[S1,S2]=GK3_bisection.integrate(x_min,x_max,func,[]);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));
[S1,S2]=GK9_bisection.integrate(x_min,x_max,func,[]);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));
[S1,S2]=GK15_bisection.integrate(x_min,x_max,func,[]);
assert(almost_equal(S1,expected_value)); assert(almost_equal(S2,expected_value));

clear all
%% Test integrator

tol=1e-4;
x_min=-pi;
x_max=+pi;
fn= @(x) x+1;

I1=integrator(fn,x_min,x_max,'local',tol,GK15,false);
assert(I1.N_function_evals==15);
assert(I1.N_intervals==1);
assert(almost_equal(I1.integral_value,2*pi));
assert(almost_equal(I1.L,2*pi));

I1=integrator(fn,x_min,x_max,'local',tol,G4,false);
assert(I1.N_function_evals==12);
assert(I1.N_intervals==1);
assert(almost_equal(I1.integral_value,2*pi));
assert(almost_equal(I1.L,2*pi));

I1=integrator(fn,x_min,x_max,'local',0,G3,true);
assert(mod(I1.N_function_evals,3)==0);
assert(mod(I1.N_intervals-1,2)==0);
assert(almost_equal(I1.integral_value,2*pi));
assert(almost_equal(I1.L,2*pi));

I1=integrator(fn,x_min,x_max,'local',0,G2,true);
assert(mod(I1.N_function_evals,2)==0);
assert(mod(I1.N_intervals-1,2)==0);
assert(almost_equal(I1.integral_value,2*pi));
assert(almost_equal(I1.L,2*pi));

I1=integrator(fn,x_min,x_max,'local',tol,Fractal,false);
assert(I1.N_function_evals==3);
assert(I1.N_intervals==1);
assert(almost_equal(I1.integral_value,2*pi));
assert(almost_equal(I1.L,2*pi));

clear all

function val=almost_equal(a,b)

    if abs(a-b)<1000*eps*min(abs(a),abs(b))
        val=true;
    else
        val=false;
        a
        b
    end
end