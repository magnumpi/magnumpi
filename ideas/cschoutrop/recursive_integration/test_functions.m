function [ x_min,x_max,fn,result_exact ] = test_functions(n)
result_exact=NaN;
x_min=1e-8;
x_max=2;

switch n
    case 1
        fn=@(x) sin(pi.*log(x));
        result_exact=0.480776177070095659743229;
    case 2
        fn=@(x) x.^5;
    case 3
        fn=@(x) exp(-10.*(x-1).^2).*sin(10.*x);
        result_exact=-0.02503077892405439;
    case 4
        fn=@(x) exp(-10.*(x-1).^2).*sin(20.*x);
        result_exact=0.00002420789647992333;
    case 5
        fn=@(x) rect(0.1,0.7,x);
        result_exact=0.6;
    case 6
        fn=@(x) sin(15.*x);
    case 7
        fn=@(x) sin(5.*x.^2);
    case 8
        fn=@(x) 1./x;  
        result_exact=19.11382792451232;
    case 9
        fn=@(x) sin(1./x);
        result_exact=1.136635156015669;
    case 10
        fn=@(x) exp(x);        
    case 11
        fn=@(x) sin(10.*sqrt(x)); 
    case 12
        fn=@(x) (1-1./x.^2-(1./x.^12-1./x.^6)).^(-0.5);
        x_min=1+1e-8;
        x_max=2;
    otherwise
        error('Unknown test function')
end

if isnan(result_exact)
    result_exact=integral(@(x) fn(x),x_min,x_max,'RelTol',1e-16,'AbsTol',1e-16);
end




end

