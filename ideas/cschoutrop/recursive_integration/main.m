clear all
close all
format compact
format long g

tic
%{
The goal of this script is to compare the recursive integration strategies.
Here the midpoint and trapezoidal rules are combined. Firstly with the
coefficients [1/2,1/2] as suggested in Colonna 2008. Second with
coefficients [2/3,1/3]; the Simpson's rule.
These methods are then applied recursively, such that the local error estimates
are below a given tolerance.
A non-recursive variant of the Simpson's rule is included for comparison.

The test-integrand; sin(pi.*log(x)) has a primitive:
x.*(-pi.*cos(log(x))+sin(pi*log(x)))./(1+pi.^2).
%}

%Integrand
[ x_min,x_max,fn,result_exact ] = test_functions(1);
tol=logspace(-12,-2,100);
%{
    simpson_equidistant and Clenshaw_Curtis uses same number of intervals as
    the first quadrature rule stated.
%}

%quadrature_rules={Fractal,G1,G2,G3,G4,G5}; %Gaussian quadratures
%quadrature_rules={Fractal,GK3,GK9,GK15}; %Gauss-Kronrod with embedded error estimate
%quadrature_rules={Fractal,GK3_bisection,GK9_bisection,GK15_bisection}; %Gauss-Kronrod with bisection error estimate
quadrature_rules={Fractal,Simpson,Simpson_bisection,'Clenshaw_Curtis','Simpson_equidistant'}; %Other quadratures

%quadrature_rules={Fractal,G1,G2,G3,G4,G5,GK3,GK9,GK15,GK3_bisection,GK9_bisection,GK15_bisection,Simpson,Simpson_bisection,'Clenshaw_Curtis','Simpson_equidistant'};



N_rules=length(quadrature_rules);
N_tol=length(tol);
N_intervals=cell(N_rules,1);
N_evals=cell(N_rules,1);
result=cell(N_rules,1);

for iter_rules=1:N_rules
    N_intervals{iter_rules}=NaN(N_tol,1);
    result{iter_rules}=NaN(N_tol,1);
    N_evals{iter_rules}=NaN(N_rules,1);
end


%Loop over tolerance for all integration rules
for iter_tol=1:N_tol
    disp(['iter: ',num2str(iter_tol) '/',num2str(N_tol)]);
    
    %Loop over all integration rules
    for iter_rules=1:N_rules
        
        
        %{
        The Simpson equidistant and Clenshaw Curtis methods do not have an error
        criterion built in, but use a fixed number of intervals.
        %}
        if strcmp(quadrature_rules{iter_rules},'Simpson_equidistant')
            I=integrand(fn);
            func= @(x) I.get(x);
            
            fc=str2func(['int_',quadrature_rules{iter_rules}]);
            [N, val]=...
                fc(x_min,x_max,func,N_intervals{1}(iter_tol));
            result{iter_rules}(iter_tol)=val;
            N_evals{iter_rules}(iter_tol)=N;
            N_intervals{iter_rules}(iter_tol)=N_intervals{1}(iter_tol);
        elseif strcmp(quadrature_rules{iter_rules},'Clenshaw_Curtis')
            I=integrand(fn);
            func= @(x) I.get(x);
            
            fc=str2func(['int_',quadrature_rules{iter_rules}]);
            [N, val]=...
                fc(x_min,x_max,func,N_intervals{1}(iter_tol));
            result{iter_rules}(iter_tol)=val;
            N_evals{iter_rules}(iter_tol)=N;
            N_intervals{iter_rules}(iter_tol)=N_intervals{1}(iter_tol);
        else
            I1=integrator(fn,x_min,x_max,'local',tol(iter_tol),quadrature_rules{iter_rules},false);
            N_intervals{iter_rules}(iter_tol)=I1.N_intervals;
            result{iter_rules}(iter_tol)=I1.integral_value;
            N_evals{iter_rules}(iter_tol)=I1.N_function_evals;
            
        end
        
        
        
        %
        % I1
        % return
        
        clear I N val
    end
end



%{
Note that the Fractal rule, Simpsons rule can recycle points in between
subdivisions since they place an additional point halfway. The same for the
Clenshaw Curtis rule if it would be applied recursively.
This cannot be done for the Gauss-Kronrod rules or the Gauss-Legendre rules.
%}

%Determine order as function of number of function evaluations
%{
The order as function of number of function evaluations
is very similar to the order as function of number of intervals, since
one basically multiplies N with a constant. This only shifts the resulting
log log plot, the slope is unchanged. Approximately.
%}
%{
If recycling is taken into account then:
For the Simpson, Simpson_bisection_error and Fractal rules the optimal
number of function evaluations is equal to the number of intervals +1.
For Gauss-Kronrod rules the number is the same as without recycling.
For bisection Gauss-Kronrod rules the number of function evaluations could
be reduced by 1/3 at most?.
%}
for iter_rules=1:N_rules
    N=N_evals{iter_rules};
    %N=N_intervals{iter_rules};
    
    p = polyfit(log(N),log(abs(result_exact-result{iter_rules})), 1);
    try
        disp(['Order of ',quadrature_rules{iter_rules}.name,':'])
    catch
        disp(['Order of ',quadrature_rules{iter_rules},':'])
    end
    disp(num2str(p(1),10))
end

%Plot the error as function of tolerance
h=figure(1);
for iter_rules=1:N_rules
    try
        loglog(tol(:),abs(result_exact-result{iter_rules}),'displayname',quadrature_rules{iter_rules}.name,'linewidth',1.5)
    catch
        loglog(tol(:),abs(result_exact-result{iter_rules}),'displayname',quadrature_rules{iter_rules},'linewidth',1.5)
    end
    hold on
end
xlabel('Set tolerance');ylabel('Error');
l=legend('-Dynamiclegend','location','northeast');set(l,'Interpreter', 'none');
axis('tight');set(gca,'linewidth',1.5);
save_to_pdf(h,'integral_comparison_tolerance');

%Plot the error as function of number of intervals
h=figure(2);
for iter_rules=1:N_rules
    try
        loglog(N_intervals{iter_rules},abs(result_exact-result{iter_rules}),'displayname',quadrature_rules{iter_rules}.name,'linewidth',1.5)
    catch
        loglog(N_intervals{iter_rules},abs(result_exact-result{iter_rules}),'displayname',quadrature_rules{iter_rules},'linewidth',1.5)
    end
    hold on
end
xlabel('N_{intervals}');ylabel('Error');
l=legend('-Dynamiclegend','location','northeast');set(l,'Interpreter', 'none');
axis('tight');set(gca,'linewidth',1.5);
save_to_pdf(h,'integral_comparison_N');

%Plot the error as function of number of minimum number of function evaluations
%Possible recycling is not taken into account.
h=figure(3);
for iter_rules=1:N_rules
    try
        loglog(N_evals{iter_rules},abs(result_exact-result{iter_rules}),'displayname',quadrature_rules{iter_rules}.name,'linewidth',1.5)
    catch
        loglog(N_evals{iter_rules},abs(result_exact-result{iter_rules}),'displayname',quadrature_rules{iter_rules},'linewidth',1.5)
    end
    hold on
end
xlabel('N_{function evaluations}');ylabel('Error');
l=legend('-Dynamiclegend','location','northeast');set(l,'Interpreter', 'none');
axis('tight');set(gca,'linewidth',1.5);
save_to_pdf(h,'integral_fn_eval_estimate');

diary off
diary('output.txt')
for iter_rules=1:N_rules
    N=N_evals{iter_rules};
    %N=N_intervals{iter_rules};
    
    p = polyfit(log(N),log(abs(result_exact-result{iter_rules})), 1);
    try
        disp(['Order of ',quadrature_rules{iter_rules}.name,':'])
    catch
        disp(['Order of ',quadrature_rules{iter_rules},':'])
    end
    disp(num2str(p(1),10))
end
for iter_rules=1:N_rules
    try
        quadrature_rules{iter_rules}.name
    catch
        quadrature_rules{iter_rules}
    end
    [N_evals{iter_rules} abs(result_exact-result{iter_rules})]
end



toc