classdef GK3 <handle
    %{
    Recursive method with coefficients from 9-point Gauss Kronrod method
    Here the error is estimated using the bisection method.
    %}
    properties (Constant)
        is_embedded=true;
        
        scheme_order=3*3+1;
        name='GK3';
        
        %points
        x=[...
            -sqrt(3/5);...
            0;...
            sqrt(3/5);...
            ];
        %Kronrod weights
        w_K=[...
            5/9;...
            8/9;...
            5/9;...
            ];
        %Gaussian weights
        w_G=[...
            2;...
            ];
    end
    
    methods (Static)
        function [S1,S2] = integrate(a,b,fn)
            %Transform to the interval [-1,1]
            rm=(b-a)/2;
            rp=(b+a)/2;
    
            n=3;
            fn_vals=zeros(n,1);
            for i=1:n
                fn_vals(i)=fn(rm*GK3.x(i)+rp);
            end

            S1=0;
            i_G=1;
            for i=2:2:n
                S1=S1+fn_vals(i)*GK3.w_G(i_G);
                i_G=i_G+1;
            end
            S1=S1*rm;

            S2=0;
            for i=1:n
                S2=S2+fn_vals(i)*GK3.w_K(i);
            end
            S2=S2*rm;
        end
        
    end
end

