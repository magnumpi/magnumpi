#include "magnumpi/vector_sort.h"
#include "ideas/cschoutrop/interpolation_test/boost_spline.h"
#include <iostream>
#include <cassert>

namespace tk
{
	void boost_spline::set_points(const std::vector<double>& x, const std::vector<double>& y)
	{
		/*
		        Set the points, only the nodes and values at the nodes
		        are available.
		*/
		//Sanity checks
		assert(x.size() == y.size());
		assert(x.size() > 4);

		//Compute an estimate for the first and second derivatives
		std::vector<double> dydx, d2ydx2;
		compute_derivatives(x, y, dydx);
		compute_derivatives(x, dydx, d2ydx2);
		set_points(x, y, dydx, d2ydx2);
	}
	void boost_spline::set_points(const std::vector<double>& x, const std::vector<double>& y,
		const std::vector<double>& dydx)
	{
		/*
		        Set the points, only the nodes, values at the nodes
		        and first derivatives are available.
		*/
		//Sanity checks
		assert(x.size() == y.size());
		assert(x.size() == dydx.size());
		assert(x.size() > 4);

		//Compute an estimate for the second derivative
		std::vector<double> d2ydx2;
		compute_derivatives(x, dydx, d2ydx2);
		set_points(x, y, dydx, d2ydx2);
	}
	void boost_spline::set_points(const std::vector<double>& x, const std::vector<double>& y,
		const std::vector<double>& dydx, const std::vector<double>& d2ydx2)
	{
		/*
		        Set the points, the nodes, values at the nodes,
		        first derivatives and second derivatives are available.
		*/
		//Sanity checks
		assert(x.size() == y.size());
		assert(x.size() == dydx.size());
		assert(x.size() == d2ydx2.size());
		assert(x.size() > 4);

		//Check if the x-array is sorted to be sure everything is ok
		if (!std::is_sorted(x.begin(), x.end()))
		{
			//Sort m_x and keep corresponding x and y points together
			vector_sort permutation(x, std::less<double>());

			//Make a copy as we may not modify the inputs
			std::vector<double> x_local(x.begin(), y.end());
			std::vector<double> y_local(y.begin(), y.end());
			std::vector<double> dydx_local(dydx.begin(), dydx.end());
			std::vector<double> d2ydx2_local(d2ydx2.begin(), d2ydx2.end());

			//Permute x and y such that x is sorted
			permutation.permute(x_local);
			permutation.permute(y_local);

			//Recompute the derivatives, as we shuffled x and y
			//these have likely become invalid.
			compute_derivatives(x_local, y_local, dydx_local);
			compute_derivatives(x_local, dydx_local, d2ydx2_local);

			//Construct the spline
			construct_spline(x_local, y_local, dydx_local, d2ydx2_local);
			return;
		}

		//Everything went well, construct the spline
		construct_spline(x, y, dydx, d2ydx2);
		return;
	}

	void boost_spline::construct_spline(std::vector<double> x, std::vector<double> y,
		std::vector<double> dydx, std::vector<double> d2ydx2)
	{
		/*
		        Construct the spline
		        Note that the spline object requires some sort of move which
		        makes invalidates the inputs, so we cannot take in the vectors
		        by reference.
		*/
		//Store the values at the first and last nodes,
		//these are needed to extrapolate.
		m_x.first = x.front();
		m_x.second = x.back();
		m_y.first = y.front();
		m_y.second = y.back();
		m_dydx.first = dydx.front();
		m_dydx.second = dydx.back();
		m_d2ydx2.first = d2ydx2.front();
		m_d2ydx2.second = d2ydx2.back();

		//Construct the spline
		m_spline = std::make_shared<boost::constants::interpolators::quintic_hermite<std::vector<double>>>
			(std::move(x), std::move(y), std::move(dydx), std::move(d2ydx2));
	}

	double boost_spline::operator() (const double x) const
	{
		/*
			Interpolated value at x, when extrapolating use a Taylor
			series; y(x)\approx y(xp)+y'(xp)*(x-xp)+0.5*y''(xp)*(x-xp)^2.
			Where xp is either the left-most or right-most node.
		*/
		double interpol;

		if (x >= m_x.first && x <= m_x.second)
		{
			interpol = m_spline->operator()(x);
		}
		else if (x < m_x.first)
		{
			//Extrapolation to the left
			interpol = m_y.first + (x - m_x.first) * (m_dydx.first + 0.5 * (x - m_x.first) *
					m_d2ydx2.first);
		}
		else //x>m_x[n-1]
		{
			//Extrapolation to the right
			interpol = m_y.second + (x - m_x.second) * (m_dydx.second + 0.5 * (x - m_x.second) *
					m_d2ydx2.second);
		}

		return interpol;
	}

	double boost_spline::deriv1(double x) const
	{
		/*
			Interpolated first derivative at x, when extrapolating
			use a Taylor series; y'(x)\approx y'(xp)+y''(xp)*(x-xp).
			Where xp is either the left-most or right-most node.
		*/
		double interpol;

		//std::cout<<"???"<<std::endl;
		//std::cout << *m_spline << std::endl;
		if (x >= m_x.first && x <= m_x.second)
		{
			interpol = m_spline->prime(x);
		}
		else if (x < m_x.first)
		{
			//Extrapolation to the left
			interpol = m_dydx.first + m_d2ydx2.first * (x - m_x.first);
		}
		else //x>m_x[n-1]
		{
			//Extrapolation to the right
			interpol = m_dydx.second + m_d2ydx2.second * (x - m_x.second);
		}

		return interpol;
	}

	double boost_spline::deriv2(double x) const
	{
		/*
			Interpolated first derivative at x, when extrapolating
			use a Taylor series; y''(x)\approx y''(xp).
			Where xp is either the left-most or right-most node.
		*/
		double interpol;

		if (x >= m_x.first && x <= m_x.second)
		{
			interpol = m_spline->double_prime(x);
		}
		else if (x < m_x.first)
		{
			//Extrapolation to the left
			interpol = m_d2ydx2.first;
		}
		else //x>m_x[n-1]
		{
			//Extrapolation to the right
			interpol = m_d2ydx2.second;
		}

		return interpol;
	}

	double boost_spline::compute_derivative(const double x, const double x0, const double x1,
		const double x2,
		const double y0, const double y1, const double y2) const
	{
		/*
			Estimates dy/dx at x given 3 coordinates (x0,y0),(x1,y1),(x2,y2)
		*/
		//Compute dy/dx by first interpolating y(x) using Lagrange interpolating polynomials;
		//Compute the derivative analytically

		//Derivatives of the Lagrange interpolated polynomials
		double l0_dx, l1_dx, l2_dx;

		l0_dx = (x - x2) / (x0 - x1) / (x0 - x2) + (x - x1) / (x0 - x2) / (x0 - x1);
		l1_dx = (x - x2) / (x1 - x0) / (x1 - x2) + (x - x0) / (x1 - x2) / (x1 - x0);
		l2_dx = (x - x1) / (x2 - x0) / (x2 - x1) + (x - x0) / (x2 - x1) / (x2 - x0);

		return y0 * l0_dx + y1 * l1_dx + y2 * l2_dx;
	}

	void boost_spline::compute_derivatives(const std::vector<double>& x, const std::vector<double>& y,
		std::vector<double>& dydx) const
	{
		/*
			Compute an estimate of dy/dx
		*/
		//Resize dydx to match the size of x
		dydx.resize(x.size());

		//Compute an estimate of dydx
		for (size_t i = 1; i < x.size() - 1; ++i)
		{
			dydx[i] = compute_derivative(x[i], x[i - 1], x[i], x[i + 1], y[i - 1], y[i], y[i + 1]);
		}

		//Handle the first and last point separately
		//dydx at x=x[0]
		dydx[0] = compute_derivative(x[0], x[0], x[1], x[2], y[0], y[1], y[2]);

		//dydx at x=x[end]
		size_t i = x.size() - 1;
		dydx[i] = compute_derivative(x[i], x[i - 2], x[i - 1], x[i], y[i - 2], y[i - 1], y[i]);
	}
}