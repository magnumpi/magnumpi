#ifndef H_TK_BOOST_SPLINE_H
#define H_TK_BOOST_SPLINE_H

#include <vector>
#include "boost/math/interpolators/quintic_hermite.hpp"
#include <memory>

/*
        TODO:
        -Figure out why make_unique and new/delete don't work for m_spline.
        -Upgrade boost to a version that includes quintic_hermite to prevent
        the spam of gitlab emails.
*/

namespace tk
{
	class boost_spline
	{
		public:
			//Set the interpolation points
			void set_points(const std::vector<double>& x, const std::vector<double>& y);
			void set_points(const std::vector<double>& x, const std::vector<double>& y,
				const std::vector<double>& dydx);
			void set_points(const std::vector<double>& x, const std::vector<double>& y,
				const std::vector<double>& dydx, const std::vector<double>& d2ydx2);

			//Interpolated value at x
			double operator() (double x) const;
			//Interpolated first derivative at x
			double deriv1(double x) const;
			//Interpolated second derivative at x
			double deriv2(double x) const;
		private:
			//Coordinates of the left and right-most nodes
			std::pair<double, double> m_x;
			//Values at the left and right-most nodes
			std::pair<double, double> m_y;
			//Estimate of the derivative at the left and right-most nodes
			std::pair<double, double> m_dydx;
			//Estimate of the second derivative the left and right-most nodes
			std::pair<double, double> m_d2ydx2;
			//Spline object from boost
			std::shared_ptr<boost::constants::interpolators::quintic_hermite<std::vector<double>>> m_spline;

			void construct_spline(std::vector<double> x, std::vector<double> y, std::vector<double> dydx,
				std::vector<double> d2ydx2);

			void compute_derivatives(const std::vector<double>& x, const std::vector<double>& y,
				std::vector<double>& dydx) const;
			double compute_derivative(const double x, const double x0, const double x1, const double x2,
				const double y0, const double y1, const double y2) const;


	};
} // namespace tk
#endif // H_TK_BOOST_SPLINE_H
