#include <iostream>
#include <vector>
#include <iomanip>
#include "magnumpi/spline.h"
#include "ideas/cschoutrop/interpolation_test/boost_spline.h"

struct Interpol_data
{
	Interpol_data(const std::vector<double>& x_, const std::vector<double>& y_,
		const std::vector<double>& xi_): x(x_), y(y_), xi(xi_)
	{

	}
	//Input data
	std::vector<double> x;
	std::vector<double> y;
	//Points to be interpolated at
	std::vector<double> xi;

	//Output data
	//Interpolated y-values at xi
	std::vector<double> yi;
	//First derivative
	std::vector<double> yi_dx;
	//Second derivative
	std::vector<double> yi_dx2;
};

double calc_dydx(const double x, const double x0, const double x1, const double x2,
	const double y0, const double y1, const double y2)
{
	/*
		Estimates dy/dx at x given 3 coordinates (x0,y0),(x1,y1),(x2,y2)
	*/
	//Compute dy/dx by first interpolating y(x) using Lagrange interpolating polynomials;
	//Compute the derivative analytically

	//Derivatives of the Lagrange interpolated polynomials
	double l0_dx, l1_dx, l2_dx;

	l0_dx = (x - x2) / (x0 - x1) / (x0 - x2) + (x - x1) / (x0 - x2) / (x0 - x1);
	l1_dx = (x - x2) / (x1 - x0) / (x1 - x2) + (x - x0) / (x1 - x2) / (x1 - x0);
	l2_dx = (x - x1) / (x2 - x0) / (x2 - x1) + (x - x0) / (x2 - x1) / (x2 - x0);

	return y0 * l0_dx + y1 * l1_dx + y2 * l2_dx;
}

void compute_derivative(const std::vector<double>& x, const std::vector<double>& y,
	std::vector<double>& dydx)
{
	/*
		Compute an estimate of dy/dx
	*/
	//Resize dydx to match the size of x
	dydx.resize(x.size());

	//Compute an estimate of dydx
	for (size_t i = 1; i < x.size() - 1; ++i)
	{
		dydx[i] = calc_dydx(x[i], x[i - 1], x[i], x[i + 1], y[i - 1], y[i], y[i + 1]);
	}

	//Handle the first and last point separately
	//dydx at x=x[0]
	dydx[0] = calc_dydx(x[0], x[0], x[1], x[2], y[0], y[1], y[2]);

	//dydx at x=x[end]
	size_t i = x.size() - 1;
	dydx[i] = calc_dydx(x[i], x[i - 2], x[i - 1], x[i], y[i - 2], y[i - 1], y[i]);
}

void print_results(const std::vector<double>& x, const std::vector<double>& y,
	const std::vector<double>& y_exact)
{
	/*
	        Prints the true and estimated value
	        Prints the error
	*/
	double error = 0.0;

	for (size_t i = 0; i < x.size(); ++i)
	{
		std::cout << std::left << std::setw(20) << x[i] << std::setw(20) << y[i] << std::setw(
				20) << y_exact[i] << std::endl;
		error += std::abs(y[i] - y_exact[i]);
	}

	std::cout << "1-norm error: " << error << std::endl;
}
void print_results_type(const Interpol_data& data, const std::vector<double>& yi_exact,
	const std::vector<double>& yi_dx_exact, const std::vector<double>& yi_dx2_exact)
{
	//Print the results
	std::cout << std::left << std::setw(20) << "x" << std::setw(20) << "y_interp" << std::setw(
			20) << "y" << std::endl;
	print_results(data.xi, data.yi, yi_exact);
	std::cout << std::left << std::setw(20) << "x" << std::setw(20) << "dydx_interp" << std::setw(
			20) << "dydx" << std::endl;
	print_results(data.xi, data.yi_dx, yi_dx_exact);
	std::cout << std::left << std::setw(20) << "x" << std::setw(20) << "dy2dx2_interp" << std::setw(
			20) << "dy2dx2" << std::endl;
	print_results(data.xi, data.yi_dx2, yi_dx2_exact);
}

std::vector<double> LinearSpacedArray(double a, double b, std::size_t N = 100)
{
	/*
	        Linear interpolation following MATLAB linspace
	*/
	double h = (b - a) / static_cast<double>(N - 1);
	std::vector<double> xs(N);
	std::vector<double>::iterator x;
	double val;

	for (x = xs.begin(), val = a; x != xs.end(); ++x, val += h)
	{
		*x = val;
	}

	return xs;
}


void interpolate_boost(Interpol_data& data)
{
	/*
	        Computes the values yi,yi_dx,yi_dx2 in the Interpol_data struct
	        This is done using quintic_hermite from Boost.
	*/

	//The derivatives have to be obtained from the data if they're not given
	//If these can be obtained analytically, the error in the interpolation
	//is massively reduced. Getting the first derivative right seems to have
	//the largest impact.
	std::vector<double> dydx;
	std::vector<double> d2ydx2;
	compute_derivative(data.x, data.y, dydx);
	compute_derivative(data.x, dydx, d2ydx2);

	//Exact derivatives:
	// for(size_t i=0;i<x.size();++i){
	// 	dydx[i]=std::cos(x[i]);
	// 	d2ydx2[i]=-std::sin(x[i]);
	// }

	//Construct the spline
	using boost::constants::interpolators::quintic_hermite;
	auto spline = quintic_hermite<std::vector<double>>(std::move(data.x), std::move(data.y),
			std::move(dydx),
			std::move(d2ydx2));

	//Set up the interpolated results
	std::vector<double> xi = data.xi;
	std::vector<double> yi(xi.size());
	std::vector<double> yi_dx(xi.size());
	std::vector<double> yi_dx2(xi.size());

	//Interpolation
	std::generate(yi.begin(), yi.end(), [ix = 0, xi, spline]() mutable {return spline(xi[ix++]);});
	//First derivative
	std::generate(yi_dx.begin(), yi_dx.end(), [ix = 0, xi, spline]() mutable {return spline.prime(xi[ix++]);});
	//Second derivative
	std::generate(yi_dx2.begin(), yi_dx2.end(), [ix = 0, xi, spline]() mutable {return spline.double_prime(xi[ix++]);});

	//Store in data struct
	data.yi = yi;
	data.yi_dx = yi_dx;
	data.yi_dx2 = yi_dx2;
}
void interpolate_boost_spline(Interpol_data& data)
{
	/*
	        Computes the values yi,yi_dx,yi_dx2 in the Interpol_data struct
	        This is done using quintic_hermite from Boost spline
			implementation with the wrapper.
	*/
	tk::boost_spline spline;
	spline.set_points(data.x, data.y);

	//Set up the interpolated results
	std::vector<double> xi = data.xi;
	std::vector<double> yi(xi.size());
	std::vector<double> yi_dx(xi.size());
	std::vector<double> yi_dx2(xi.size());

	//Interpolation
	std::generate(yi.begin(), yi.end(), [ix = 0, xi, spline]() mutable {return spline(xi[ix++]);});
	//First derivative
	std::generate(yi_dx.begin(), yi_dx.end(), [ix = 0, xi, spline]() mutable {return spline.deriv1(xi[ix++]);});
	//Second derivative
	std::generate(yi_dx2.begin(), yi_dx2.end(), [ix = 0, xi, spline]() mutable {return spline.deriv2(xi[ix++]);});

	//Store in data struct
	data.yi = yi;
	data.yi_dx = yi_dx;
	data.yi_dx2 = yi_dx2;
}
void interpolate_magnum(Interpol_data& data)
{
	/*
	        Computes the values yi,yi_dx,yi_dx2 in the Interpol_data struct
	        This is done using cubic splines from MagnumPI
	*/
	tk::spline QS;
	QS.set_points(data.x, data.y);

	std::vector<double> xi = data.xi;
	std::vector<double> yi(xi.size());
	std::vector<double> yi_dx(xi.size());
	std::vector<double> yi_dx2(xi.size());
	std::generate(xi.begin(), xi.end(), [ix = 0, xi]() mutable {return 5.0 * (ix++) / xi.size();});
	std::generate(yi.begin(), yi.end(), [ix = 0, xi, QS]() mutable {return QS(xi[ix++]);});
	std::generate(yi_dx.begin(), yi_dx.end(), [ix = 0, xi, QS]() mutable {return QS.deriv1(xi[ix++]);});
	std::generate(yi_dx2.begin(), yi_dx2.end(), [ix = 0, xi, QS]() mutable {return QS.deriv2(xi[ix++]);});

	//Store in data struct
	data.yi = yi;
	data.yi_dx = yi_dx;
	data.yi_dx2 = yi_dx2;
}

void sin_example()
{
	/*
	        Example interpolation of sin(x)
	*/
	//Define the function and the derivatives
	auto exact_f = [](double x)
	{
		return std::sin(x);
	};
	auto exact_fdx = [](double x)
	{
		return std::cos(x);
	};
	auto exact_f2dx2 = [](double x)
	{
		return -std::sin(x);
	};

	//Input data x
	std::vector<double> x{-0.1, 0.2, 0.3, 0.9, 1.5, 2.0, 2.6, 2.9, 3.5, 4, 5};

	//Input data s
	std::vector<double> y(x.size());
	std::generate(y.begin(), y.end(), [ix = 0, x, exact_f]() mutable {return exact_f(x[ix++]);});

	//Points at which the interpolation should take place
	std::vector<double> xi;
	xi = LinearSpacedArray(0, 5, 13);

	Interpol_data data_boost(x, y, xi);
	Interpol_data data_magnum(x, y, xi);
	Interpol_data data_boost_spline(x, y, xi);
	interpolate_boost(data_boost);
	interpolate_boost_spline(data_boost_spline);
	interpolate_magnum(data_magnum);

	//Set up the exact results for comparison
	std::vector<double> yi_exact(xi.size());
	std::vector<double> yi_dx_exact(xi.size());
	std::vector<double> yi_dx2_exact(xi.size());
	std::generate(yi_exact.begin(), yi_exact.end(), [ix = 0, xi, exact_f]() mutable {return exact_f(xi[ix++]);});
	std::generate(yi_dx_exact.begin(), yi_dx_exact.end(), [ix = 0, xi, exact_fdx]() mutable {return exact_fdx(xi[ix++]);});
	std::generate(yi_dx2_exact.begin(), yi_dx2_exact.end(), [ix = 0, xi, exact_f2dx2]() mutable {return exact_f2dx2(xi[ix++]);});

	std::cout << "Results Boost:\n";
	print_results_type(data_boost, yi_exact, yi_dx_exact, yi_dx2_exact);
	std::cout << "Results Boost spline:\n";
	print_results_type(data_boost, yi_exact, yi_dx_exact, yi_dx2_exact);
	std::cout << "Results Magnum:\n";
	print_results_type(data_magnum, yi_exact, yi_dx_exact, yi_dx2_exact);
}

double calc_error(std::vector<double> v1, std::vector<double> v2)
{
	double error = 0.0;

	for (size_t i = 0; i < v1.size(); ++i)
	{
		//error += std::abs(v1[i] - v2[i]);
		if (std::abs(v1[i] - v2[i]) > error)
		{
			error = std::abs(v1[i] - v2[i]);
		}
	}

	//return error / v1.size();
	return error;
}

void test_scaling()
{
	/*
	        Example interpolation of a Lennard-Jones potential
	*/
	//Define the function and the derivatives
	auto exact_f = [](double x)
	{
		return 4.0 * (std::pow(x, -12) - std::pow(x, -6));
	};
	auto exact_fdx = [](double x)
	{
		return 4.0 * (-12.0 * std::pow(x, -13) + 6.0 * std::pow(x, -7));
	};
	auto exact_f2dx2 = [](double x)
	{
		return 4.0 * (12.0 * 13.0 * std::pow(x, -14) - 6.0 * 7.0 * std::pow(x, -8));
	};

	std::vector<double> error_boost_y;
	std::vector<double> error_boost_dydx;
	std::vector<double> error_boost_d2ydx2;
	std::vector<double> error_boost_spline_y;
	std::vector<double> error_boost_spline_dydx;
	std::vector<double> error_boost_spline_d2ydx2;
	std::vector<double> error_magnum_y;
	std::vector<double> error_magnum_dydx;
	std::vector<double> error_magnum_d2ydx2;

	unsigned Nmin = 10;
	unsigned Nmax = 1e4;

	for (size_t N = Nmin; N < Nmax; N *= 2)
	{

		//Input data x
		std::vector<double> x;
		x = LinearSpacedArray(0.8, 2.0, N);

		//Input data s
		std::vector<double> y(x.size());
		std::generate(y.begin(), y.end(), [ix = 0, x, exact_f]() mutable {return exact_f(x[ix++]);});

		//Points at which the interpolation should take place
		std::vector<double> xi;
		xi = LinearSpacedArray(0.81, 1.99, 2 * N);

		Interpol_data data_boost(x, y, xi);
		Interpol_data data_boost_spline(x, y, xi);
		Interpol_data data_magnum(x, y, xi);
		interpolate_boost(data_boost);
		interpolate_boost_spline(data_boost_spline);
		interpolate_magnum(data_magnum);

		//Set up the exact results for comparison
		std::vector<double> yi_exact(xi.size());
		std::vector<double> yi_dx_exact(xi.size());
		std::vector<double> yi_dx2_exact(xi.size());
		std::generate(yi_exact.begin(), yi_exact.end(), [ix = 0, xi, exact_f]() mutable {return exact_f(xi[ix++]);});
		std::generate(yi_dx_exact.begin(), yi_dx_exact.end(), [ix = 0, xi, exact_fdx]() mutable {return exact_fdx(xi[ix++]);});
		std::generate(yi_dx2_exact.begin(), yi_dx2_exact.end(), [ix = 0, xi, exact_f2dx2]() mutable {return exact_f2dx2(xi[ix++]);});

		//Compute the error for a given N
		error_boost_y.push_back(calc_error(yi_exact, data_boost.yi));
		error_boost_dydx.push_back(calc_error(yi_dx_exact, data_boost.yi_dx));
		error_boost_d2ydx2.push_back(calc_error(yi_dx2_exact, data_boost.yi_dx2));
		error_boost_spline_y.push_back(calc_error(yi_exact, data_boost.yi));
		error_boost_spline_dydx.push_back(calc_error(yi_dx_exact, data_boost.yi_dx));
		error_boost_spline_d2ydx2.push_back(calc_error(yi_dx2_exact, data_boost.yi_dx2));
		error_magnum_y.push_back(calc_error(yi_exact, data_magnum.yi));
		error_magnum_dydx.push_back(calc_error(yi_dx_exact, data_magnum.yi_dx));
		error_magnum_d2ydx2.push_back(calc_error(yi_dx2_exact, data_magnum.yi_dx2));
	}

	//Print the results
	std::cout << "Error in boost interpolation" << std::endl;
	std::cout << std::left << std::setw(20) << "N" << std::setw(20) << "y" << std::setw(
			20) << "dydx" << std::setw(20) << "d2ydx2" << std::endl;
	size_t N = Nmin;

	for (size_t i = 0; i < error_boost_y.size(); ++i)
	{
		std::cout << std::left << std::setw(20) << N << std::setw(20) << error_boost_y[i] << std::setw(
				20) << error_boost_dydx[i] << std::setw(20) << error_boost_d2ydx2[i] << std::endl;
		N *= 2;
	}

	std::cout << "Error in boost_spline interpolation" << std::endl;
	std::cout << std::left << std::setw(20) << "N" << std::setw(20) << "y" << std::setw(
			20) << "dydx" << std::setw(20) << "d2ydx2" << std::endl;
	N = Nmin;

	for (size_t i = 0; i < error_boost_y.size(); ++i)
	{
		std::cout << std::left << std::setw(20) << N << std::setw(20) << error_boost_spline_y[i] <<
			std::setw(
				20) << error_boost_spline_dydx[i] << std::setw(20) << error_boost_spline_d2ydx2[i] << std::endl;
		N *= 2;
	}

	std::cout << "Error in magnum interpolation" << std::endl;
	std::cout << std::left << std::setw(20) << "N" << std::setw(20) << "y" << std::setw(
			20) << "dydx" << std::setw(20) << "d2ydx2" << std::endl;
	N = Nmin;

	for (size_t i = 0; i < error_magnum_y.size(); ++i)
	{
		std::cout << std::left << std::setw(20) << N << std::setw(20) << error_magnum_y[i] << std::setw(
				20) << error_magnum_dydx[i] << std::setw(20) << error_magnum_d2ydx2[i] << std::endl;
		N *= 2;
	}
}


int main()
{
	/*
	        Program to investigate the boost interpolation methods for use in MagnumPI
	        Documentation on the boost interpolation methods can be found at:
	        https://www.boost.org/doc/libs/master/libs/math/doc/html/interpolation.html

	        Methods:
	        -Cardinal Cubic B-spline interpolation
	        -Cardinal Quadratic B-spline interpolation
	        -Cardinal Quintic B-spline interpolation
	        -Whittaker-Shannon interpolation
	        -Barycentric Rational Interpolation
	        -Vector-valued Barycentric Rational Interpolation
	        -Catmull-Rom Splines
	        -Cardinal Trigonometric interpolation
	        -Cubic Hermite interpolation
	        -Modified Akima interpolation
	        -PCHIP interpolation
	        -Quintic Hermite interpolation

	        Methods that work with non-equidistant data:
	        -Barycentric Rational Interpolation
	        -Cubic Hermite interpolation
	        -Modified Akima interpolation
	        -PCHIP interpolation
	        -Quintic Hermite interpolation

	        Methods that are also C2 continuous
	        (we need second derivative for the orbiting detection,
		and good first derivative for the dchi/db), this leaves:
	        -Quintic Hermite interpolation
	*/

	//Set up some example data
	std::vector<double> x{1, 2, 3, 4, 5, 6};
	std::vector<double> y{7, 8, 9, 10, 11, 12};

	//The derivatives have to be estimated from the data if they're not known
	std::vector<double> dydx;
	std::vector<double> d2ydx2;
	compute_derivative(x, y, dydx);
	compute_derivative(x, dydx, d2ydx2);

	//Set up the interpolating spline
	using boost::constants::interpolators::quintic_hermite;
	auto spline = quintic_hermite<std::vector<double>>(std::move(x), std::move(y), std::move(dydx),
			std::move(d2ydx2));

	//Evaluate at a point
	std::cout << spline(3.4) << "\n";
	//Estimate the first derivative at a point
	std::cout << spline.prime(3.4) << "\n";
	//Estimate the second derivative at a point
	std::cout << spline.double_prime(3.4) << "\n";
	//Print the info stored in the spline
	std::cout << spline << "\n";
	//std::cout << spline(0.8)<<std::endl;

	/*
		Possible optimization:
		We do not need to reconstruct the interpolation if new points get added
		The interpolator can be updated in constant time using circular_buffer
		Hence we can use boost::circular_buffer to do real-time interpolation.
		See:
		https://www.boost.org/doc/libs/master/libs/math/doc/html/math_toolkit/quintic_hermite.html
		for more details.
	*/

	//Test example for a more complicated function:
	sin_example();
	test_scaling();
}
