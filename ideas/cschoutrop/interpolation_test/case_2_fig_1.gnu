set terminal svg enhanced size 500,500
set key bottom right

set output 'case_2_fig_1.svg'
set xlabel "x"
set ylabel "y"
set title "1.0 / (1.0 + exp(-25 * x))"

set style line 1 lt rgb "red" lw 2
set style line 2 lt rgb "orange" lw 2
set style line 3 lt rgb "yellow" lw 2
set style line 4 lt rgb "green" lw 2
set style line 5 lt rgb "cyan" lw 2
set style line 6 lt rgb "blue" lw 2
set style line 7 lt rgb "violet" lw 2

plot "case2_start_data.txt" ls 2 pt 7 ps 1, "case2_interpolated_spline.txt" with lines ls 1, "case2_real.txt" with lines ls 6
