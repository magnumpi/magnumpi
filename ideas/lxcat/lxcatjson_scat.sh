#!/usr/bin/env bash

cnf=$MAGNUMPI_INPUTDATA_DIR/input/scat.json

pot=$MAGNUMPI_INPUTDATA_DIR/ideas/lxcat/He_He+/input/heplus_he_gerade_inline_int.json#.interaction.potential
ext=lennard-jones-inline
$MAGNUMPI_DIR/app/calc_scat_colonna $cnf $pot $ext

pot=$MAGNUMPI_INPUTDATA_DIR/ideas/lxcat/He_He+/input/heplus_he_gerade_external_int.json#.interaction.potential
ext=lennard-jones-remote
$MAGNUMPI_DIR/app/calc_scat_colonna $cnf $pot $ext
