from cmath import pi
from re import A
import numpy
from scipy import interpolate
import math
from matplotlib import pyplot
import sys

def add_dcs_section(dcs,chi,b,i_low,i_high,resolution, chi_grid):
    print("Considering subrange: " + str(b[i_low]) + "\t" + str(b[i_high]) + " m")
    #sanity check: make sure any zeros at the end of the chi vector are removed
    index = i_high
    while (chi[index]) == 0 and index >= i_low:
        index -= 1
    if index == i_low or index - i_low < 3:
        return

    #set up spline fit
    if chi[i_low] < chi[i_high]:
        spline = interpolate.CubicSpline(chi[i_low:i_high],b[i_low:i_high],bc_type='clamped')
    else:
        spline = interpolate.CubicSpline(chi[i_high:i_low:-1],b[i_high:i_low:-1],bc_type='clamped')
    max_chi = max(chi[i_low],chi[i_high])
    min_chi = min(chi[i_low],chi[i_high])
    #loop over all intervals of 0-180 until all angles have been added
    min_mult = min_chi / 180
    min_mult = int(-numpy.floor(numpy.abs(min_mult)) if min_mult < 0 else numpy.floor(min_mult))
    max_mult = max_chi / 180
    max_mult = int(-numpy.ceil(numpy.abs(max_mult)) if max_mult < 0 else numpy.ceil(max_mult))
    for mult in range(min_mult,max_mult):
        angle1 = mult * 180
        angle2 = (mult + 1) * 180
        flip = numpy.mod(numpy.abs(angle1),360) >= 180
        min_angle = max(min_chi,angle1)
        max_angle = min(max_chi,angle2)
        #determine indices of chi_grid that should be considered
        if flip == False:
            #consider range from 0 - 180 in this order
            ind = (chi_grid >= min_angle + angle1) & (chi_grid <= max_angle + angle1)
        elif flip == True:
            #consider range from 180-0 in reversed order
            ind = numpy.flip((chi_grid >= min_angle + angle1) & (chi_grid <= max_angle + angle1))
        #calculate DCS contribution
        b_temp = spline(chi_grid[ind])
        derivative_db_dchi = spline.derivative(1)
        db_dchi = derivative_db_dchi(chi_grid[ind])
        dcs_temp = numpy.abs(b_temp / numpy.sin(numpy.deg2rad(chi_grid[ind])) * db_dchi)
        dcs[ind] += dcs_temp

def calc_dcs(chi_grid, data, b_col, chi_col):
    b = data[:,b_col]
    #convert to degrees
    chi = data[:,chi_col] * 180 / math.pi
    #check whether we need to flip the vectors
    if b[-1] < b[0]:
        b = numpy.flip(b)
        chi = numpy.flip(chi)
    resolution = chi_grid[1]-chi_grid[0]
    #initialize dcs
    dcs = numpy.zeros(len(chi_grid))
    #find monotonic sections chi
    sign = -1
    interval_low = 0
    interval_high = 0
    i = 1
    while i < len(data):
        if sign == -1 and chi[i]-chi[i-1] >= 0:
            interval_high = i - 1
            if interval_high - interval_low >= 3:
                add_dcs_section(dcs,chi,b,interval_low,interval_high, resolution, chi_grid)
            interval_low = i - 1
            if chi[i] - chi[i-1] == 0:
                sign = 0
            else:
                sign = 1
        elif sign == 1 and chi[i]-chi[i-1] <= 0:
            interval_high = i - 1
            if interval_high - interval_low >= 3:
                add_dcs_section(dcs,chi,b,interval_low,interval_high, resolution, chi_grid)
            interval_low = i - 1
            if chi[i] - chi[i-1] == 0:
                sign = 0
            else:
                sign = -1
        elif sign == 0 and numpy.abs(chi[i] - chi[i-1]) > 0:
            if chi[i] - chi[i-1] > 0:
                sign = 1
            else:
                sign = -1
            interval_low = i-1
            interval_high = i-1
        i += 1
    #handle last section
    if interval_low < len(data) -1:
        interval_high = len(data) - 1
        if interval_high - interval_low >= 3:
            add_dcs_section(dcs,chi,b,interval_low,interval_high, resolution, chi_grid)
    return dcs

def integrate(chi_grid, dcs, l):
    cs = 0
    for i in range(len(chi_grid)):
        cs += 2 * numpy.pi * dcs[i] * numpy.sin(numpy.deg2rad(chi_grid[i])) * (1 - numpy.cos(numpy.deg2rad(chi_grid[i]))**l)
    return cs

def main(filename, b_col, chi_col, plot_dcs):
    data = numpy.loadtxt(filename,delimiter='\t',skiprows=1)

    if plot_dcs == 1:
        pyplot.figure(1)
        pyplot.plot(data[:,b_col], data[:,chi_col])

    resolution = 1
    num_bins = int(180/resolution)
    chi_grid = numpy.linspace(resolution/2,180-resolution/2,num_bins)

    print("Mesh size\t" + str(len(chi_grid)))

    dcs1 = calc_dcs(chi_grid, data, b_col, chi_col)
    fileout = "python_dcs.dat"
    with open(fileout,"w") as f:
        for i in range(len(chi_grid)):
            f.write(str(chi_grid[i]) + "\t" + str(dcs1[i]) + "\n")

    #report integral value
    sigma = integrate(chi_grid, dcs1, 1)
    print("Cross section: " + f"{sigma:1.4e}" + "*m^2")

    if plot_dcs == 1:
        pyplot.figure(2)
        pyplot.plot(chi_grid, dcs1)
        pyplot.show()

if __name__ == "__main__":
    print("Usage: python3 dcs.py <filename=LJ_b_vs_chi.txt> <b column=0> <chi column=2> <plot dcs=1>")
    if len(sys.argv) >= 2:
        filename = sys.argv[1]
    else:
        filename = "LJ_b_vs_chi.txt"
    if len(sys.argv) >= 3:
        b_col = int(sys.argv[2])
    else:
        b_col = 0
    if len(sys.argv) >= 4:
        chi_col = int(sys.argv[3])
    else:
        chi_col = 2
    if len(sys.argv) >= 5:
        plot_dcs = int(sys.argv[4])
    else:
        plot_dcs = 1
    main(filename, b_col, chi_col, plot_dcs)
