import numpy as np
from scipy import interpolate
import math
from matplotlib import pyplot
import sys
import os
import subprocess

def call_cpp(energy, Directory, potential, prefix):
    fileout = os.path.join(Directory, "dcs_input.json")
    with open(fileout,'w') as f:
        f.write("{\n")
        f.write("\t\"cc\": {\n")
        f.write("\t\t\"lower\": 6,\n")
        f.write("\t\t\"upper\": 14\n")
        f.write("\t},\n")
        f.write("\t\"energy_range\": {\n")
        f.write("\t\t\"max\": {\"value\": " + str(energy) + ", \"unit\": \"eV\"},\n")
        f.write("\t\t\"min\": {\"value\": " + str(energy) + ", \"unit\": \"eV\"},\n")
        f.write("\t\"points_per_decade\": 20\n")
        f.write("\t},\n")
        f.write("\t\"r\": {\n")
        f.write("\t\t\"Vmax\": {\"value\": 20, \"unit\": \"eV\"},\n")
        f.write("\t\t\"Vmin\": {\"value\": 1e-3, \"unit\": \"eV\"},\n")
        f.write("\t\t\"del\": {\"value\": 1e-14, \"unit\": \"m\"},\n")
        f.write("\t\t\"rmax\": {\"value\": 1e-9, \"unit\": \"m\"},\n")
        f.write("\t\t\"rmin\": {\"value\": 2e-10, \"unit\": \"m\"}\n")
        f.write("\t},\n")
        f.write("\t\"dcs\": {\n")
        f.write("\t\t\"points_per_degree\": {\"value\": 1, \"unit\": \"1\"}\n")
        f.write("\t},\n")
        f.write("\t\"tolerance\": 1e-3\n")
        f.write("}")
    file_exe = os.path.join(Directory,"..","..","..","build","app","calc_dcs")
    subprocess.run([file_exe,fileout,potential,prefix])

def call_python(Directory):
    file_python = os.path.join(Directory,"dcs.py")
    file_b_chi = os.path.join(Directory,"b_chi.txt")
    subprocess.run(["python3",file_python, file_b_chi,"0","1","0"]) 

def make_plots(Directory,prefix):
    #load b vs chi data
    file_b_chi = os.path.join(Directory,"b_chi.txt")
    with open(file_b_chi,'r') as f:
        data = f.readlines()
    b = np.zeros(len(data))
    chi = np.zeros(len(data))
    for i in range(len(data)):
        row = data[i]
        s = row.split()
        b[i] = float(s[0])
        chi[i] = float(s[1])
    pyplot.figure()
    pyplot.plot(b,chi)
    pyplot.xlabel("b(m)")
    pyplot.ylabel("chi(rad)")
    pyplot.xlim([0, 1e-9])

    #load data from cpp script
    if len(prefix) > 0:
        cpp_fname = prefix + "_dcs.dat"
    else:
        cpp_fname = "dcs.dat"
    dcs_cpp = os.path.join(Directory, cpp_fname)
    with open(dcs_cpp,'r') as f:
        dcs_cpp_input = f.readlines()
    #count number of entries
    count = 0
    for i in range(len(dcs_cpp_input)):
        if len(dcs_cpp_input[i].strip()) > 0 and dcs_cpp_input[i][0] != "#":
            count += 1
    dcs_c = np.zeros(count)
    angle_c = np.zeros(count)
    count = 0
    for i in range(len(dcs_cpp_input)):
        if len(dcs_cpp_input[i].strip()) > 0 and dcs_cpp_input[i][0] != "#":
            row = dcs_cpp_input[i].strip()
            s = row.split()
            dcs_c[count] = float(s[1])
            angle_c[count] = float(s[0])
            count += 1
    #load data from python script
    file_p = os.path.join(Directory, "python_dcs.dat")
    with open(file_p,"r") as f:
        dcs_p_input = f.readlines()
    angle_p = np.zeros(len(dcs_p_input))
    dcs_p = np.zeros(len(dcs_p_input))
    for i in range(len(dcs_p_input)):
        row = dcs_p_input[i].strip()
        s = row.split()
        angle_p[i] = float(s[0])
        dcs_p[i] = float(s[1])

    pyplot.figure()
    pyplot.plot(angle_c, dcs_c)
    pyplot.plot(angle_p, dcs_p)
    pyplot.xlabel("chi(degrees)")
    pyplot.ylabel("DCS (m^2)")
    pyplot.yscale('log')
    pyplot.legend(("CPP","python"))

    pyplot.show()

if __name__ == "__main__":
    print("Usage: python3 compare_dcs.py <energy (in eV)> <potential type>")
    Directory = os.path.dirname(os.path.realpath(__file__))
    if len(sys.argv) >= 2:
        energy = float(sys.argv[1])
    if len(sys.argv) >= 3:
        ptype = sys.argv[2]
    if ptype == "LennardJones":
        potential = os.path.join(Directory,"..","..","..","input","colonna_fig4a_lj_pot.json#.potential")
        prefix = "lj"
    else:
        print("Unknown potential type: " + ptype)
        exit()
    #generate b_chi.txt. We need debug mode enabled in the cpp code
    call_cpp(energy, Directory, potential, prefix)
    #call python executable and calculate the DCS as well
    call_python(Directory)
    #plot DCS result cpp in python plot too
    make_plots(Directory, prefix)

