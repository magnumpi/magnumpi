#ifndef H_MAGNUMPI_HISTOGRAM_H
#define H_MAGNUMPI_HISTOGRAM_H

#include <iostream>
#include <vector>

namespace magnumpi {

class histogram
{
	public:
		histogram(unsigned num_cells);
		unsigned size() const
		{
			return m_data.size();
		}
		void add_interval(unsigned index, double b1, double b2);
		void get_interval(unsigned ind_angle, unsigned ind_db, double& b1, double& b2) const;
		void add(unsigned index);
		void reset(unsigned index);
		unsigned number(unsigned index) const;
		void print_histogram() const;
		void print_intervals() const;
	private:
		class interval
		{
			public:
				interval(double low, double high);
				void get(double& low, double& high) const;
			private:
				double m_lower, m_higher;
		};
		class hdata
		{
			public:
				hdata();
				void add_interval(double b1, double b2);
				void get_interval(unsigned index, double& b1, double& b2) const;
				void add()
				{
					++m_count;
				};
				void reset()
				{
					m_count = 0;
				};
				unsigned number() const
				{
					return m_count;
				};
				unsigned size() const
				{
					return m_step.size();
				};
			private:
				unsigned m_count;
				std::vector<interval> m_step;
		};
		std::vector<hdata> m_data;
		std::vector<interval> m_indices;

};

} // namespace magnumpi

#endif // H_MAGNUMPI_HISTOGRAM_H
