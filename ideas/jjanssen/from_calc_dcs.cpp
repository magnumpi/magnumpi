/*  This file contains snippets of unused code from calc_dcs.cpp,
 *  related to the sampling of angles.
 */

// dbdchi is no calculated as part of the evaluate_DCS function

void calc_dcs_base::calculate_dbdchi(unsigned n,
		int mult,
		const std::vector<double>& b,
		const std::vector<double>&chi,
		std::vector<double>&dbdchi) const
{
	unsigned s=b.size()/n;
	double left=0.0;
	double right=0.0;
	for (unsigned i=0; i<n; i += mult)
	{
		for (unsigned j=0;j < s ; ++j)
		{
			if (i+mult>=n || (chi[i+j*n+mult] - chi[i+j*n])==0.0)
				right = dbdchi[i+j*n]=0.0;
			else
				right = (b[i+j*n+mult] - b[i+j*n]) / (chi[i+j*n+mult] - chi[i+j*n]);

			if (i-mult<0 || i-mult>=n || (chi[i+j*n] - chi[i+j*n-mult])==0.0)
				left = dbdchi[i+j*n]=0.0;
			else
				left = (b[i+j*n] - b[i+j*n-mult]) / (chi[i+j*n] - chi[i+j*n-mult]);
			dbdchi[i+j*n] = (right + left) /2.0;
		}
	}
}

void calc_dcs_base::calculate_dbdchi(unsigned n,
		int mult,
		const std::vector<double>& b_u,
		const std::vector<double>&chi_u,
		std::vector<double>&dbdchi_u,
		const std::vector<double>&b_g,
		const std::vector<double>&chi_g,
		std::vector<double>&dbdchi_g) const
{
	//ungerade
	unsigned s=b_u.size()/n;
	double left=0.0;
	double right=0.0;
	for (unsigned i=0; i<n; i += mult)
	{
		for (unsigned j=0;j < s ; ++j)
		{
			if (i+mult>=n || (chi_u[i+j*n+mult] - chi_u[i+j*n])==0.0)
				right = dbdchi_u[i+j*n]=0.0;
			else
				right = (b_u[i+j*n+mult] - b_u[i+j*n]) / (chi_u[i+j*n+mult] - chi_u[i+j*n]);

			if (i-mult<0 || i-mult>=n || (chi_u[i+j*n] - chi_u[i+j*n-mult])==0.0)
				left = dbdchi_u[i+j*n]=0.0;
			else
				left = (b_u[i+j*n] - b_u[i+j*n-mult]) / (chi_u[i+j*n] - chi_u[i+j*n-mult]);
			dbdchi_u[i+j*n] = (right + left) /2.0;
		}
	}
	//gerade
	s=b_g.size()/n;
	for (unsigned i=0; i<n; i += mult)
	{
		for (unsigned j=0; j < s; ++j)
		{
			if (i+mult>=n || (chi_g[i+j*n+mult] - chi_g[i+j*n])==0.0)
				right = dbdchi_g[i+j*n]=0.0;
			else
				right = (b_g[i+j*n+mult] - b_g[i+j*n]) / (chi_g[i+j*n+mult] - chi_g[i+j*n]);

			if (i-mult<0 || i-mult>=n ||(chi_g[i+j*n] - chi_g[i+j*n-mult])==0.0)
				left = dbdchi_g[i+j*n]=0.0;
			else
				left = (b_g[i+j*n] - b_g[i+j*n-mult]) / (chi_g[i+j*n] - chi_g[i+j*n-mult]);
			dbdchi_g[i+j*n] = (right + left) /2.0;
		}
	}
}

void calc_dcs_base::analyze_chi_binning(std::vector<double>& chi, std::vector<double>& b, histogram& hist) const
{
	int sum = 0.0;
	double rad_to_deg = 180.0 / Constant::pi;
	int index_prev = hist.size()-1;
	double bmin=0.0, bmax=0.0;
	double bzero=0.0;

	for (unsigned i=0; i<chi.size(); ++i)
	{
		if (std::fabs(chi[i]) == 0.0 || std::isnan(chi[i])==1)
		{
			if (bmax>bzero && std::isnan(b[i])==0)
				bzero = b[i];
			continue;
		}
		else
		{
			double temp = std::fmod(std::fabs(chi[i])*rad_to_deg,360);
			if (temp > 180.0)
				temp = 360.0 - temp;
			int index = std::round(temp);
			hist.add(index);
			++sum;

			//store interval related to scattering angle
			if (index == index_prev)
			{
				bmax = b[i];
			}
			else
			{
				//predict boundary
				double boundary = (bmax+b[i])/ 2.0;

				//store interval
				hist.add_interval(index_prev, bmin, boundary);

				//reset parameters
				bmin = boundary;
				bmax = boundary;
			}
			index_prev = index;
		}
	}
	//add last interval and use bzero as bmax
	hist.add_interval(index_prev,bmin,bzero);
	/*
	//display results
	for (unsigned i=0; i<histogram.size(); ++i)
	{
		std::cout << i << "\t" << histogram[i] << "\t" << histogram[i] / sum << std::endl;
	}
	// */
}

unsigned calc_dcs_base::count_non_zeros(std::vector<double>& chi, unsigned& max_index) const
{
	unsigned num = 0;
	for (unsigned i=0; i<chi.size(); ++i)
	{
		if (std::isnan(chi[i]) == 0 && chi[i]!=0.0)
		{
			max_index = i;
			++num;
		}
	}

	if (max_index<chi.size()-1)
		++max_index;
	return num;
}

void calc_dcs_base::increase_resolution(const potential* p, double energy, std::vector<double> b, std::vector<double> chi, std::vector<double> eta, std::vector<double> dbdchi, std::vector<Ql>& DCS_new, std::vector<Ql>& DCS_old) const
{
	//make copy of input data
	unsigned n_low = pow(2,m_integrator.lower()) + 1;
	unsigned n = pow(2,m_integrator.upper()) + 1;
	unsigned s = b.size() / n;
	int mult = pow(2,m_integrator.upper()-m_integrator.lower());
	unsigned ind_max, non_zeros;
	non_zeros = count_non_zeros(chi, ind_max);
	std::vector<double> b_copy(non_zeros,0.0);
	std::vector<double> chi_copy(non_zeros,0.0);
	std::vector<double> eta_copy(non_zeros,0.0);
	std::vector<double> dbdchi_copy(non_zeros,0.0);
	unsigned count = 0;

	for (unsigned j=0; j<s; ++j)
	{
		for (unsigned i=0; i<n_low; ++i)
		{

			if (std::isnan(chi[i+j*s])==0 && chi[i+j*s] != 0.0)
			{
				b_copy[count] = b[j*n + i * mult];
				chi_copy[count] = chi[j*n + i * mult];
				eta_copy[count] = eta[j*n + i * mult];
				dbdchi_copy[count] = dbdchi[j*n + i * mult];
				++count;
				//std::cout << b[i+j*s] << "\t" << chi[i+j*s] << std::endl;
			}
		}
	}
	histogram hist(DCS_new[0].size_DCS());
	analyze_chi_binning(chi, b, hist);
	hist.print_histogram();
	hist.print_intervals();
	//minimum number of points per cell
	unsigned min_points = 4;
	double residue = 1.0;
	unsigned iter = 0;

	///*
	while (residue > m_tolerance)
	{
		//determine size for new vectors
		unsigned required_per_cell = min_points * pow(2, iter);
		//determine which cells violate the requirement
		unsigned extra_cells = 0;
		for (unsigned i=0;i<hist.size();++i)
		{
			if (hist.number(i) < required_per_cell)
				extra_cells += required_per_cell - hist.number(i);
		}

		//rescale output vectors
		unsigned size = b.size() + extra_cells;
		b.resize(size);
		chi.resize(size);
		eta.resize(size);
		dbdchi.resize(size);
/*
		//fill vectors with previous results and add new contributions
		count = 0;
		for (unsigned i=0;i<hist.size();++i)
		{
			double blow, bhigh;
			unsigned ind_angle, indb;
			hist.get_indices(i, ind_angle, indb);
			hist.get_interval(ind_angle,indb, blow, bhigh);
			//check whether b values are in indicated interval
			if (hist.number()>=required_per_cell)
			{
				//copy all data points that correspond to this interval
				while (b_copy[count]<=hist)
				{

				}
			}
		}
*/

		residue = 0.0;
	}
	// */
}

