#include <string>
#include <iostream>
#include <stdexcept>
#include <iomanip>
#include <chrono>
#include "ideas/scat_vl/scat_vl.h"
#include "magnumpi/json.h"
#include "magnumpi/consts.h"
#include "magnumpi/potential.h"


int main(int argc, char *argv[])
{
	try {
		using namespace magnumpi;
		//set output to scientific
		std::cout << std::scientific;
		//std::setprecision(16);
		const std::chrono::time_point<std::chrono::system_clock> start(std::chrono::system_clock::now());

		if (argc!=2)
		{
			throw std::runtime_error("Usage: calc_scat <inputfile>");
		}
		const json_type cnf(magnumpi::read_json_from_file(argv[1]));

		const std::unique_ptr<const potential> pot(potential::create(cnf.at("interaction").at("potential")));

		//consider these clenshaw curtis quadratures
		const int lower_cc=cnf.at("cc").at("lower");
		const int upper_cc=cnf.at("cc").at("upper");

		double rmin=in_si(cnf.at("r").at("min"),units::length);
		double rmax=in_si(cnf.at("r").at("max"),units::length);
		const double dr=in_si(cnf.at("r").at("del"),units::length);

		// adjust rmin,rmax if necessary
		const double Vmin=in_si(cnf.at("r").at("Vmin"),units::energy);
		const double Vmax=in_si(cnf.at("r").at("Vmax"),units::energy);
		rmin_input_evaluation(rmin,pot.get(),Vmin);
		rmax_input_evaluation(rmax,pot.get(),Vmax);

		//determine whether scat_Colonna or scat_Viehland is used for estimating scattering angles
		const int use_scat_Colonna=1;
		//set tolerance and interpolation order
		const int interp=3;
		const double tolerance = cnf.at("tolerance");

		const double energy = in_si(cnf.at("energy"),units::energy);

	        //determine scattering angle as a function of impact parameter
	        int index_cc=upper_cc;
		std::cout << "Calculating scattering angle as a function of impact parameter" << std::endl;

		data_set chi_b_data = 
			get_scat_vs_b(lower_cc, upper_cc, index_cc,energy,rmin,rmax,dr,use_scat_Colonna,interp,tolerance,pot.get());
		chi_b_data.write("scat_vs_b.new");

		//print the elapsed time
		const std::chrono::time_point<std::chrono::system_clock> end(std::chrono::system_clock::now());
		const std::chrono::duration<double> dt(end - start);
		std::cout << "Run time\t" << dt.count() << "s" << std::endl;
	}
	catch (const std::exception& exc)
	{
		std::cerr << "Error: " << exc.what() << std::endl;
		return 1;
	}
	return 0;
}
