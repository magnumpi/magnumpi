#include <string>
#include <vector>
#include <limits>
#include "ideas/scat_vl/scat_vl.h"
#include "magnumpi/potential.h"
#include "magnumpi/scat_colonna.h"
#include "magnumpi/scat_viehland.h"
#include "magnumpi/consts.h"
#include "magnumpi/data_set.h"
#include "plmath/clencurt.h"

namespace magnumpi {

data_set get_scat_vs_b(int lower_cc, int upper_cc, int index_cc, double energy,double rmin,double rmax,double dr,
	int use_scat_Col,int interpol,double tol,const potential* pot)
{
	std::cout << "Calculating chi(b)" << std::endl;
	//make a scattering calculation object: use arbitrary values for E_low
	calc_scat cs(lower_cc,upper_cc,1e-4,1,
		rmin,rmax,dr,use_scat_Col,interpol,tol,pot);
	return cs.get_scat_vs_b(index_cc,energy);
}

calc_scat::calc_scat(int lower, int upper,
		double E_low,
		int l_max, double rmin,double rmax,double dr,
		int use_scat_Col,int interpol,
		double tol,const potential* pot)
 : m_rmin(rmin),m_rmax(rmax),
   m_use_scat_Colonna(use_scat_Col),
   m_interp(interpol),
   m_tolerance(tol),
   m_integrator(-1.0,1.0,lower,upper),
   m_pot(pot),
   m_triplets(pot, rmin, rmax, dr, E_low)
{
}

data_set calc_scat::get_scat_vs_b(int index_cc,double energy) const
{
	data_set chi_b_data(
		"chi(b)", units::angle, "rad",
		{"b", units::length, "m"},
		{"species pair"} );
	chi_b_data.add_column("pair"); // todo: replace with the interaction pair's name

	const triplet_data::case_data data{m_triplets,energy};
	switch (data.case_id)
	{
		case 1:
			Viehland_impact_case_1_get_scat(index_cc,energy,data,chi_b_data);
		break;
		case 2:
			Viehland_impact_case_2_get_scat(index_cc,energy,data,chi_b_data);
		break;
		case 3:
			Viehland_impact_case_3_get_scat(index_cc,energy,data,chi_b_data );
		break;
	}
	return chi_b_data;
}

void calc_scat::Viehland_impact_case_1_get_scat(int index_cc,double energy, const triplet_data::case_data& data, data_set& chi_b_data) const
{
	//determine number of orbiting impact parameters
	const std::vector<double>& b_vec = data.b_vec;
	const std::vector<double>& r_vec = data.r_vec;

	std::cout << "case 1" << std::endl;

	//part 1: y=1 -> b=0	y=-1 -> b=b_vec
	for (int i=0;i<pow(2,index_cc)+1;++i)
	{
		double y=m_integrator.cc_position(index_cc,i);
		//omit orbiting calculation
		if (y>-1)
		{
			double b;
			double scat;
			case1_part1 (y,energy,&b,&scat,r_vec[0],b_vec[0]);
			chi_b_data.add_row(b, {scat});
		}
	}
	//part 2
	if (b_vec.size()>1)
	{
		for (int j=0; b_vec.size()-1; ++j)
		{
			//part 1: y=1 -> r=r_avr  y=-1 -> r=r_vec_j
			for (int i=pow(2,index_cc)+1;i>=0;--i)
			{
				double y=m_integrator.cc_position(index_cc,i);
				if (y>-1)
				{
					double b;
					double scat;
					//skip r_vec (y=-1)
					case1_part2_1 (y,energy,&b,&scat,r_vec[j],r_vec[j+1]);
					chi_b_data.add_row(b,{scat});
				}
			}
			//part 2: y=1 -> r=r_avr  y=-1 -> r=r_vec_j+1
			for (int i=0; i<pow(2,index_cc)+1;++i)
			{
				double y=m_integrator.cc_position(index_cc,i);
				if (y>-1 && y<1)
				{
					double b;
					double scat;
					//skip r_vec (y=1) and r_avr was already calculated
					case1_part2_2 (y,energy,&b,&scat,r_vec[j],r_vec[j+1]);
					chi_b_data.add_row(b,{scat});
				}
			}
		}
	}
	//part 3: y=1 -> r=r_vec_j  y=-1 -> r=Infinite (skip y=-1 which gives r=Inf)
	for (int i=0; i<pow(2,index_cc);++i)
	{
		double y=m_integrator.cc_position(index_cc,i);
		double b;
		double scat;
		case1_part3 (y,energy,&b,&scat,r_vec[r_vec.size()-1]);
		chi_b_data.add_row(b, {scat} );
	}
}

void calc_scat::case1_part1 (double y,double energy, double *b,double *scat, double r_vec,double b_vec) const
{
	//calculate impact parameter and scattering angle for part 1 of case 1
	if (y==-1.0)
	{
		*b= b_vec;
		*scat=-std::numeric_limits<double>::infinity();	
	}
	else if (y<1)
	{
		//discretization of impact parameter
		*b= b_vec *cos(constants::pi *(y+1.0)/4.0);
		//if (m_use_scat_Colonna==1)
		//{
			*scat=scat_Colonna(m_pot,*b,energy,m_tolerance,m_interp);
		//}
		/*
			else
		{
		//use last found r0 as a lower boundary
		//initialize with lowest r0 value: *(rvec+0)
		double r0=find_r0_case_1(*m_pot,energy,b,r_vec);
		*scat=scat_viehland::scat1(m_integrator,*m_pot,energy,*b,r0,coeff);
		}
		*/
	}
	else if (y==1.0)
	{
		*b=0.0;
		*scat=constants::pi;
	}
}

void calc_scat::case1_part2_1(double y,double energy,double *b,double *scat,double r_vec1,double r_vec2) const
{
	//average radius, eq 26
	if (y<1)
	{
		double r1;
		//part r1
		if (y==-1)
		{
			r1=r_vec1;
		}
		else
		{
			//average radius, eq 26
			double r_avr=(r_vec1+r_vec2)/2.0;
			//eq 27-29
			double xi=4.0/constants::pi*asin(r_vec1/r_avr)-1.0;
			double x1=( (1.0-xi)*y+xi+1.0)/2.0;
			r1=r_avr*sin(constants::pi/4.0*(x1+1.0));
		}
		*b=r1*sqrt(1.0-m_pot->value(r1)/energy);
		//always use scat_Colonna here
		*scat=scat_Colonna(m_pot,*b,energy,m_tolerance,m_interp);
	}
	/// \todo What if y>=1 ?
}

void calc_scat::case1_part2_2(double y,double energy,double *b,double *scat,double r_vec1,double r_vec2) const
{
	double r2;

	if (y==1)
	{
		r2=r_vec2;
	}
	else
	{
		//average radius, eq 26
		double r_avr=(r_vec1+r_vec2)/2.0;
		//eq30-32
		double xi=4.0/constants::pi*acos(r_avr/ r_vec2)-1.0;
		double x2=( (1.0+xi)*y+xi-1.0)/2.0;
		r2=r_vec2*cos(constants::pi/4.0*(x2+1.0));
	}
	*b=r2*sqrt(1.0-m_pot->value(r2)/energy);
	//always use scat_Colonna here
	*scat=scat_Colonna(m_pot,*b,energy,m_tolerance,m_interp);
}

void calc_scat::case1_part3(double y,double energy,double *b,double *scat,double r_vec) const
{
	double r3=r_vec/sin(constants::pi/4.0*(1.0+y));
	*b=r3*sqrt(1.0-m_pot->value(r3)/energy);

	if (m_use_scat_Colonna==1)
	{
		*scat=scat_Colonna(m_pot,*b,energy,m_tolerance,m_interp);
	}
	else
	{
		adjust_b(*m_pot,energy,b,r3);
		*scat=scat_viehland::scat1(m_integrator,*m_pot,energy,*b,r3,m_tolerance);
	}
}

void calc_scat::Viehland_impact_case_2_get_scat(int index_cc,double energy,const triplet_data::case_data& data, data_set& chi_b_data) const
{
	//determine number of orbiting impact parameters
	std::cout << "case 2" << std::endl;

	//part 1: y=-1 -> r=r0_tilde; y=1 -> r=rc_tilde
	//handle y=-1 -> r=r0_tilde separately
	chi_b_data.add_row(0.0, {constants::pi} );
	for (int i=pow(2,index_cc)-1;i>=0;--i)
	{
		double y=m_integrator.cc_position(index_cc,i);
		double b;
		double scat;
		case2_part1 (y,energy,&b,&scat,data.rc_tilde,data.r0_tilde);
		chi_b_data.add_row(b, {scat});
	}
	//part 2: y=1 -> r=rc_tilde; y=-1 -> r=infinity
	for (int i=0; i<pow(2,index_cc)+1;++i)
	{
		double y=m_integrator.cc_position(index_cc,i);
		//rc_tilde already calculated, skip r=infinity
		if (y>-1 && y<1)
		{
			double b;
			double scat;
			case2_part2 (y,energy,&b,&scat,data.rc_tilde);
			chi_b_data.add_row(b, {scat});
		}
	}
}

void calc_scat::case2_part1(double y,double energy,double *b,double *scat,double rc_tilde,double r0_tilde) const
{
	//discretization of the distance of closest approach
	double r4=((rc_tilde-r0_tilde)*y+rc_tilde+r0_tilde)/2.0;
	*b=r4*sqrt(1.0-m_pot->value(r4)/energy);

	if (m_use_scat_Colonna==1)
	{
		*scat=scat_Colonna(m_pot,*b,energy,m_tolerance,m_interp);
	}
	else
	{
		adjust_b(*m_pot,energy,b,r4);
		*scat=scat_viehland::scat1(m_integrator,*m_pot,energy,*b,r4,m_tolerance);
	}
}

void calc_scat::case2_part2(double y,double energy,double *b,double *scat,double rc_tilde) const
{
	double r5=rc_tilde/sin(constants::pi*(1.0+y)/4.0);
	*b=r5*sqrt(1.0-m_pot->value(r5)/energy);

	if (m_use_scat_Colonna==1)
	{
		*scat=scat_Colonna(m_pot,*b,energy,m_tolerance,m_interp);
	}
	else
	{
		adjust_b(*m_pot,energy,b,r5);
		*scat=scat_viehland::scat1(m_integrator,*m_pot,energy,*b,r5,m_tolerance);
	}
}

void calc_scat::Viehland_impact_case_3_get_scat(int index_cc,double energy, const triplet_data::case_data& data, data_set& chi_b_data) const
{
	//part 1: y=1 -> b=b_split ;y=-1 -> b=0
	for (int i=pow(2,index_cc);i>=0;--i)
	{
		double y=m_integrator.cc_position(index_cc,i);
		double b;
		double scat;
		case3_part1 (y,energy,&b,&scat,data.b_split);
		chi_b_data.add_row(b, {scat});
	}
	//part 2:y=1 -> b=b_split; y=-1 -> b=infinity
	for (int i=0; i<pow(2,index_cc)+1;++i)
	{
		double y=m_integrator.cc_position(index_cc,i);
		//already included b_split and skip infinity
		if (y>-1 && y<1)
		{
			double b;
			double scat;
			case3_part2 (y,energy,&b,&scat,data.b_split);
			chi_b_data.add_row(b, {scat});
		}
	}
}

void calc_scat::case3_part1 (double y,double energy,double *b,double *scat,double b_split) const
{

	if (y>-1)
	{
		*b=b_split/2.0*(y+1.0);
		if (m_use_scat_Colonna==1)
		{
			*scat=scat_Colonna(m_pot,*b,energy,m_tolerance,m_interp);
		}
		else
		{
			double r0=scat_viehland::find_r0_case_3(*m_pot,m_rmin,m_rmax,energy,b);
			*scat=scat_viehland::scat1(m_integrator,*m_pot,energy,*b,r0,m_tolerance);
		}
	}
	else
	{
		*b=0.0;
		*scat=constants::pi;
	}
}

void calc_scat::case3_part2(double y,double energy,double *b,double *scat,double b_split) const
{
	if (y>-1)
	{
		*b=2.0*b_split/(y+1.0);
		if (m_use_scat_Colonna==1)
		{
			*scat=scat_Colonna(m_pot,*b,energy,m_tolerance,m_interp);
		}
		else
		{
			double r0=scat_viehland::find_r0_case_3(*m_pot,m_rmin,m_rmax,energy,b);
			*scat=scat_viehland::scat1(m_integrator,*m_pot,energy,*b,r0,m_tolerance);
		}
	}
	else
	{
		//avoid division by 0 for y=-1
		*scat=0.0;
		*b=b_split;
	}
}

} // namespace magnumpi
