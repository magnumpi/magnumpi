The code in this directory aims to create tables chi(b) for a given energy E,
using the Viehland algorithm. At present is uses a mix of Viehland and Colonna
calculations, and the setting 'use Colonna' setting is not always respected.
The code also uses a b-grid as if the result is going to be used for a cross
section calculation (the b-points are the positions of a Clemshaw-Curtis
integration). This may be unexpected for a user who specifies a b-range in the
iput file.
