/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_IDEAS_SCAT_VL_SCAT_VL_H
#define H_MAGNUMPI_IDEAS_SCAT_VL_SCAT_VL_H

#include <algorithm>
#include "magnumpi/viehland_common.h"
#include "magnumpi/potential.h"
#include "magnumpi/data_set.h"
#include "plmath/clencurt.h"

namespace magnumpi {

/** Create and return a table of chi(b) for a particular \a energy value.
 */
data_set get_scat_vs_b(
	int lower_cc,
	int upper_cc,
	int index_cc,
	double energy,
	double rmin,
	double rmax,
	double dr,
	int use_scat_Col,
	int interpol,
	double tol,
	const potential* pot);

class calc_scat
{
  public:
	calc_scat(int lower, int upper,
		double E_low,
		int l_max, double rmin,double rmax,double dr,
		int use_scat_Col,int interpol,
		double tol,const potential* pot);
	//get scattering angle as a function of b
	data_set get_scat_vs_b(int index_cc,double energy) const;
  private:
	using integrator_type = plasimo::clencurt::integrator;
	//****************************
	//define functions that call the structures that are defined below
	void Viehland_impact_case_1_get_scat(int index_cc,double energy,
		const triplet_data::case_data& data, data_set& chi_b_data) const;
	void Viehland_impact_case_2_get_scat(int index_cc,double energy,
		const triplet_data::case_data& data, data_set& chi_b_data) const;
	void Viehland_impact_case_3_get_scat(int index_cc,double energy,
		const triplet_data::case_data& data, data_set& chi_b_data) const;
	//define functions for acquiring scattering angle as a function of b
	void case1_part1 (double y,double energy,double *b,double *scat,double r_vec,double b_vec) const;
	void case1_part2_1 (double y,double energy,double *b,double *scat,double r_vec1,double r_vec2) const;
	void case1_part2_2 (double y,double energy,double *b,double *scat,double r_vec1,double r_vec2) const;
	void case1_part3 (double y,double energy,double *b,double *scat,double r_vec) const;
	void case2_part1 (double y,double energy,double *b,double *scat,double rc_tilde,double r0_tilde) const;
	void case2_part2 (double y,double energy,double *b,double *scat,double rc_tilde) const;
	void case3_part1 (double y,double energy,double *b,double *scat,double b_split) const;
	void case3_part2 (double y,double energy,double *b,double *scat,double b_split) const;

	const double m_rmin;
	const double m_rmax;
	//choose scattering model (1=yes,otherwise=no)
	const int m_use_scat_Colonna;
	//choose interpolation order for Colonna model
	const int m_interp;
	//desired relative accuracy
	const double m_tolerance;
	const integrator_type m_integrator;
	const potential* m_pot;
	triplet_data m_triplets;
};

} // namespace magnumpi

#endif // H_MAGNUMPI_IDEAS_SCAT_VL_SCAT_VL_H
