#ifndef H_MAGNUMPI_CALC_DCS_H
#define H_MAGNUMPI_CALC_DCS_H

#include <algorithm>
#include <utility>
#include "magnumpi/viehland_common.h"
#include "magnumpi/potential.h"
#include "plmath/clencurt.h"
#include "magnumpi/consts.h"

namespace magnumpi {

class calc_dcs_base
{
  public:
	//constructor DCS: difference, two pointers to potentials are provided
	calc_dcs_base(int lower,int upper,
		double E_low,
		int l_max, double rmin,double rmax,
		int use_scat_Col,int interpol,
		double tol,double red_mass);
  protected:
	using integrator_type = plasimo::clencurt::integrator;
	//store DCS data in this class
	class Ql
	{
	  public:
		//constructor
		Ql(int number_points, double dchi, unsigned l);
		unsigned size_DCS() const { return num_p; }
		//function assumes that no l dependency is required from here on
		double calc_Ql() const
		{
			double Q=0.0;
			for (unsigned i=0; i!=DCS.size();++i)
			{
				const double dchi = (DCS[i].bnd_up()-DCS[i].bnd_low()) * constants::deg;
				Q += DCS[i].dcs() * dchi;
			}
			return Q;
		}

		//declare structure for keeping track of angle and DCS
		struct struct_DCS
		{
			/** constructor
			 *  \todo It seems that chi is always in the centre of the interval.
			 *        Then we could also expect chi and Dchi and construct the
			 *        interval as [chi-Dchi/2,chi+Dchi/2].
			 */
			struct_DCS(double chi,double bnd_low,double bnd_up)
			 : m_chi(chi),m_bnd_low(bnd_low),m_bnd_up(bnd_up), m_dcs(0.0)
			{}
			double chi() const { return m_chi; }
			double dcs() const { return m_dcs; }
			double bnd_low() const { return m_bnd_low; }
			double bnd_up() const { return m_bnd_up; }
			void set_DCS(double dcs) { m_dcs=dcs; }
			void add(double dcs) { m_dcs += dcs; }
		private:
			const double m_chi;
			const double m_bnd_low;
			const double m_bnd_up;
			double m_dcs;
		};
		std::vector<struct_DCS> DCS;
	  private:
		const unsigned num_p;
	};

	void Viehland_DCS_case_1_sub(
		const potential* p,
		double y,
		double energy,
		int i,
		int mult,
		const std::vector<double>& b_vec,
		const std::vector<double>& r_vec,
		std::vector<double>& b,
		std::vector<double>& chi,
		std::vector<double>& eta,
		std::vector<double>& dbdchi) const;
	void Viehland_DCS_case_2_sub(
		const potential* p,
		double y,
		double energy,
		int i,
		int mult,
		int n,
		double rc_tilde,
		double r0_tilde,
		std::vector<double>& b,
		std::vector<double>& chi,
		std::vector<double>& eta,
		std::vector<double>& dbdchi ) const;
	void Viehland_DCS_case_3_sub(
		const potential* p,
		double y,
		double energy,
		int i,
		int mult,
		int n,
		double b_split,
		std::vector<double>& b,
		std::vector<double>& chi,
		std::vector<double>& eta,
		std::vector<double>& dbdchi ) const;
	const double m_rmin;
	const double m_rmax;
	//choose scattering model (1=yes,otherwise=no)
	const int m_use_scat_Colonna;
	//choose interpolation order for Colonna model
	const int m_interp;
	//desired relative accuracy
	const double m_tolerance;
	const integrator_type m_integrator;
	//number of cross sections
	const unsigned m_l_max;
	const double m_reduced_mass;
};

class calc_dcs : public calc_dcs_base
{
  public:
	calc_dcs(int lower,int upper,
		double E_low,
		int l_max, double rmin,double rmax,double dr,
		int use_scat_Col,int interpol,
		double tol,double red_mass, const potential* pot);
	// return value: dcs data set, cs data set
	std::pair<data_set,data_set> DCS(const std::vector<double>& energy_vals, unsigned num_chi) const;
  private:
	void Viehland_DCS_cases(double energy, std::vector<Ql>& DCS_new, std::vector<Ql>& DCS_old) const;
	double evaluate_DCS(int index_cc, int s, std::vector<Ql>& DCS_new, std::vector<Ql>& DCS_old, std::vector<double>& b,std::vector<double>& chi, std::vector<double>& dbchi) const;
	double DCS_residue(const std::vector<Ql>& DCS_new, const std::vector<Ql>& DCS_old) const;
	double verify_Ql(const std::vector<double>& b, const std::vector<double>& chi,int l, int index_cc) const;
	const potential* m_pot;
	const triplet_data m_triplets;
};

class calc_dcs_rce : public calc_dcs_base
{
  public:
	//constructor DCS: difference, two pointers to potentials are provided
	calc_dcs_rce(int lower,int upper,
		double E_low,
		int l_max, double rmin,double rmax,double dr,
		int use_scat_Col,int interpol,
		double tol,double red_mass, const potential* pot_gerade,const potential* pot_ungerade);
	// index 0: total cross section. Others: l=1..l_max
	std::vector<data_set> DCS_RCE(const std::vector<double>& energy_vals, unsigned num_chi) const;
  private:
	class QlRCE : public Ql
	{
	public:
		QlRCE(int number_points, double dchi, unsigned l);
		std::vector<double> coef;
	private:
		double permutation(unsigned n,unsigned k) const;
	};
	void Viehland_DCS_RCE_cases(double energy, std::vector<QlRCE>& DCS_new, std::vector<QlRCE>& DCS_old) const;
	double evaluate_DCS(int index_cc, int s, double energy, std::vector<QlRCE>& DCS_new, std::vector<QlRCE>& DCS_old, std::vector<double>& b,std::vector<double>& chig,
		std::vector<double>& dbchig, std::vector<double>& bu, std::vector<double>& chiu, std::vector<double>& dbdchiu, std::vector<double>& delta_eta) const;
	double RCEDCS_residue(const std::vector<QlRCE>& DCS_new, const std::vector<QlRCE>& DCS_old) const;
	double verify_Ql(const std::vector<double>& bg, const std::vector<double>& chig, const std::vector<double>& bu, const std::vector<double>&chiu, const std::vector<double>& delta_eta, int l,int index_cc) const;
	double verify_Ql_DCS(const std::vector<double>& b, const std::vector<double>& chi, const std::vector<double>& dbdchi, int l, int index_cc) const;

	const potential* m_pot_u;
	const potential* m_pot_g;
	const triplet_data m_triplets_u;
	const triplet_data m_triplets_g;
};

} // namespace magnumpi

#endif // H_MAGNUMPI_CALC_DCS_H
