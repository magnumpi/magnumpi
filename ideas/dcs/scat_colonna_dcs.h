#ifndef H_MAGNUMPI_SCAT_COLONNA_DCS_H
#define H_MAGNUMPI_SCAT_COLONNA_DCS_H

#include "magnumpi/potential.h"

namespace magnumpi {

//DCS version that also calculates phase shift and dchi/db
void scat_Colonna_DCS(double& scat,
		double& dchi_db,
		double& eta,
		const potential* V,
		double b,
		double eps,
		double tolerance,
		int n,
		double reduced_mass);

//calculate phase shift
double calc_eta(const potential* Vu,
		const potential* Vg,
		double reduced_mass,
		double b,
		double eps,
		double tolerance,
		int n);

} // namespace magnumpi

#endif // H_MAGNUMPI_SCAT_COLONNA_DCS_H
