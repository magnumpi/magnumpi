add_library(magnumpi_dcs SHARED
	dcs.cpp
	scat_colonna_dcs.cpp
)
target_link_libraries(magnumpi_dcs magnumpi)

add_executable(calc_dcs_ideas calc_dcs.cpp)
target_link_libraries(calc_dcs_ideas magnumpi_dcs)

add_executable(calc_dcs_rce calc_dcs_rce.cpp)
target_link_libraries(calc_dcs_rce magnumpi_dcs)


