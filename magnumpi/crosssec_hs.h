/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_CROSSSEC_HS_H
#define H_MAGNUMPI_CROSSSEC_HS_H

#include "magnumpi/crosssec.h"
#include "magnumpi/consts.h"

namespace magnumpi {

/** A hard-sphere cross section. This is fully specified by the interaction's
 *  collision diameter. This cross section type supports all l-values and the
 *  result is energy-independent. The cross section value is given by the
 *  relation Q^(l) = pi*d^2*f(l). The function f(l) is unity for odd l values,
 *  and equal to l/(l+1) for even l values.
 *
 *  \author Jan van Dijk
 *  \date   February 2019
 *
 *  \todo Provide a reference for the expressions for Q^(l).
 */
class cross_section_hard_sphere : public cross_section
{
  public:
	/** Construct a hard-sphere cross section from the collision
	 *  diameter \a d. The value of Q^(1) = pi*d*d is calculated
	 *  and stored in a private data member. Values for other
	 *  l-values can be produced from l and Q^(1).
	 */
	cross_section_hard_sphere(double d)
	 : m_Q1(constants::pi*d*d)
	{}
	/** Return Q_hs^(l). This is equal to Q_hs^(1) times an l-dependent
	 *  factor that is equal to 1 for odd l and to l/(l+1.0) for even l.
	 */
	double Q_hs(unsigned l) const
	{
		this->check_l(l);
		const double Ql_Q1 = (l%2) ? 1.0 : l/(l+1.0);
		return m_Q1 * Ql_Q1;
	}
  protected:
	/** Override the base class get_value interface to calculate
	 *  and return the cross section for the given \a l and \a eps.
	 *  This is done by calling function Q_hs(l).
	 */
	virtual double get_value(unsigned l, double eps) const
	{
		return Q_hs(l);
	}
	/** Implement the base class get_supports_l interface. This cross
	 *  section type supports all positive l values.
	 */
	virtual bool get_supports_l(unsigned l) const
	{
		return l!=0;
	}
  private:
	/** The hard-sphere cross section for l=1. For an interaction with
	 *  collision diameter d this is given by pi*d*d. The values for
	 *  other l-values can be obtained by an l-dependent scaling.
	 */
	const double m_Q1;
};


} // namespace magnumpi

#endif // H_MAGNUMPI_CROSSSEC_HS_H
