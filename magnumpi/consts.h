/** \file
 *
 * Copyright (C) 2001-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_CONSTS_H
#define H_MAGNUMPI_CONSTS_H

#include "plmath/physconst.h"

namespace magnumpi {

/** Namespace constants provides the mathematical constant pi and the numerical
 *  values of a number of physical constants, such as the electron mass and the
 *  Planck constant, expressed in SI base units. All constants are of type
 *  double and have been borrowed from PLASIMO's namespace plasimo::constants and
 *  plasimo::CODATA2016.
 */
namespace constants = plasimo::constants;

} // namespace magnumpi

#endif // H_MAGNUMPI_CONSTS_H

