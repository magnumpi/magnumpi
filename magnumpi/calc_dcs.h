/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_CALC_DCS_H
#define H_MAGNUMPI_CALC_DCS_H

#include "magnumpi/potential.h"
#include "magnumpi/viehland_common.h"
#include "magnumpi/data_set.h"
#include "plmath/clencurt.h"
#include "external/tk_spline/spline.h"
#include <memory>
#include <vector>

namespace magnumpi {

data_set calc_diff_cross_sec(int points_per_degree, unsigned lower_cc, unsigned upper_cc, double logE_low,double logE_high,int logE_pdecade,
	double rmin,double rmax,double dr,int use_scat_Col,int interpol,double tol,const potential* pot);

data_set calc_diff_cross_sec(const json_type& cnf, const potential* pot);

data_set calc_diff_cross_sec(const json_type& cnf, const json_type& pot);

data_set calc_diff_cross_sec(const json_type& cnf);

class calc_dcs
{
	public:
		static std::unique_ptr<const calc_dcs> create(const json_type& cnf, const potential* pot);
		calc_dcs(int points_per_degree,
			 unsigned lower_cc,
			 unsigned upper_cc,
			 double E_low,
			 double rmin,
			 double rmax,
			 double dr,
			 int use_scat_Col,
			 int interpol,
			 double tol,
			 const potential* pot);
		data_set integrate(const std::vector<double>& energy_vals) const;

		data_set integrate(double logE_low, double logE_high, unsigned logE_pdecade) const;
		double integrate_DCS_fraction(std::vector<double>& DCS_temp, unsigned l) const;
		void update_DCS(std::vector<double>& DCS, std::vector<double>& DCS_temp) const;
		void update_DCS_fraction(std::vector<double>& DCS_temp, std::vector<double>& b_vec, std::vector<double>& chi_vec, bool reset) const;
		void update_DCS_monotonic_fraction(std::vector<double>& DCS_temp, std::vector<double>& b_vec, std::vector<double>& chi_vec) const;
		void Viehland_DCS_case_1(std::vector<double>& DCS, double energy, const triplet_data::case_data& data) const;
		void Viehland_DCS_case_2(std::vector<double>& DCS, double energy, const triplet_data::case_data& data) const;
		void Viehland_DCS_case_3(std::vector<double>& DCS, double energy, const triplet_data::case_data& data) const;
		std::vector<double> integrate(double energy) const;
	private:
		const unsigned m_points_per_degree;
		double m_resolution;
		double m_dchi;
		const unsigned m_lower;
		const unsigned m_upper;
		using integrator_type = plasimo::clencurt::integrator;
		const integrator_type m_integrator;
		const double m_rmin;
		const double m_rmax;
		//choose scattering model (1=yes,otherwise=no)
		const int m_use_scat_Colonna;
		//choose interpolation order for Colonna model
		const int m_interp;
		//desired relative accuracy
		const double m_tolerance;
		const potential& m_pot;
		const triplet_data m_triplets;
		//store angles
		std::vector<double> m_angles;
		std::vector<double> m_angles_rad;
};

} //namespace magnumpi

#endif // H_MAGNUMPI_CALC_DCS_H
