/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_CALC_CS_H
#define H_MAGNUMPI_CALC_CS_H

#include "magnumpi/potential.h"
#include "magnumpi/viehland_common.h"
#include "magnumpi/data_set.h"
#include "plmath/clencurt.h"
#include <memory>
#include <vector>

namespace magnumpi {

/** Create and return a cross section table, containing sigma_l(eps) for
 *  l between 1 and l_max (inclusing) and the indicated energy range.
 *
 *  \todo document parameters
 */
data_set calc_cross_sec(int lower_cc, int upper_cc, double logE_low,double logE_high,int logE_pdecade,unsigned l_max,
	double rmin,double rmax,double dr,int use_scat_Col,int interpol,double tol,const potential* pot);

/** Create and return a cross section table, containing sigma_l(eps).
 *
 *  \todo document the expected structore of \a cnf and document \a pot.
 */
data_set calc_cross_sec(const json_type& cnf, const potential* pot);

/** Create and return a cross section table, containing sigma_l(eps).
 *
 *  \todo document the expected structore of \a cnf
 */
data_set calc_cross_sec(const json_type& cnf);

/** Create and return a cross section table, containing sigma_l(eps).
 *
 *  \todo document the expected structore of \a cnf and \a pot
 */
data_set calc_cross_sec(const json_type& cnf, const json_type& pot);

/** A class for calculating cross sections from potentials.
 *
 *  \author Jesper Janssen and Jan van Dijk
 *  \date   May 2015
 */
class calc_cs
{
  public:
	static std::unique_ptr<const calc_cs> create(const json_type& cnf, const potential* pot);
	calc_cs(int lower,
		int upper,
		double E_low,
		unsigned l_max,
		double rmin,
		double rmax,
		double dr,
		int use_scat_Col,
		int interpol,
		double tol,
		const potential* pot);
	std::vector<double> integrate(double energy) const;
	data_set integrate(const std::vector<double>& energy_vals) const;
	data_set integrate(double logE_low, double logE_high, unsigned logE_pdecade) const;
  private:
	using integrator_type = plasimo::clencurt::integrator;
	const integrator_type m_integrator;
	const double m_rmin;
	const double m_rmax;
	//choose scattering model (1=yes,otherwise=no)
	const int m_use_scat_Colonna;
	//choose interpolation order for Colonna model
	const int m_interp;
	//desired relative accuracy
	const double m_tolerance;
	const potential& m_pot;
	//number of cross sections
	const unsigned m_l_max;
	const triplet_data m_triplets;
	// the integrands that describe Viehland's cases 1, 2 and 3;
	class Viehland_case_1_sub;
	class Viehland_case_2_sub;
	class Viehland_case_3_sub;
};

} // namespace magnumpi

#endif // H_MAGNUMPI_CALC_CS_H
