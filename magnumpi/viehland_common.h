/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_VIEHLAND_COMMON_H
#define H_MAGNUMPI_VIEHLAND_COMMON_H

#include "magnumpi/potential.h"
#include <vector>

namespace magnumpi {

/** calculates and returns 1 - (b/r)^2 - V/energy.
 */
double r0_test(const potential& pot, double energy,double b,double r);

/** make sure b can be calculated from r*sqrt(1-V/E)
 *  make sure b matches eq 24
 *
 *  \todo clarify comments
 *
 *  \author Jesper Janssen
 *  \date   May 2015
 */
void adjust_b(const potential& pot,double energy,double* b,double r0);

/** A class for managing triplet intervals
 *
 *  \todo Explain the purpose and interface of this class.
 *
 *  \author Jesper Janssen
 *  \date   May 2015
 */
class triplet_data
{
  public:
	struct Range
	{
		Range(double l,double h)
		 :low(l),high(h)
		{}
		double low;
		double high;
	};
	//generate triplets (r0,b0,E0): This information is used to aid in the integration process
	triplet_data(const potential* pot, double rmin, double rmax, double dr, double E_low);
	//obtain list of b and r values for a specific energy where orbiting occurs
	void get_orbiting_b(std::vector<double>* b_vec,std::vector<double>* r_vec,double energy) const;
	//calculate rc and r0 (Viehland case 2)
	void get_rctilde_r0tilde(double* rc_tilde,double* r0_tilde,double energy) const;
	double min_range_b() const { return min_range(b_orbit); }
	double max_range_b() const { return max_range(b_orbit); }

	class case_data;
  private:
	double min_range(const std::vector<Range>& X_in) const;
	double max_range(const std::vector<Range>& X_in) const;
	// structure for memorizing peaks and wells
	struct peak
	{
		peak(double pos,double pot):r(pos),V(pot){}
		double r,V;
	};
  public:
  //private:
	const potential* m_pot;
	const double m_rmin;
	const double m_rmax;
	std::vector<Range> E_orbit;
	std::vector<Range> b_orbit;
	std::vector<Range> r_orbit;
	//store Ed,Ec,bd,bc,rc
	double Ed,Ec,bd,bc,m_rc;
	//store whether obiting conditions are met (1=yes,0=no)
	int m_orbiting;
};

/** Class case_data accepts a triplet_data object, an rmin value and an energy
 *  and analyzes which case we are dealing with.
 *
 *  This code was previously duplicated in calc_dcs (for one potential object)
 *  and twice in calc_dcs_rce (for the gerade and ungerade potentials), in calc_cs
 *  and in calc_scat.
 *
 *  \todo This class has a thick interface, just like the original code: not all
 *        members are used in all cases. We could decide to use boost::variant
 *        and use visitors to 'call' the correct case functions to fix that.
 */
class triplet_data::case_data
{
  public:
	case_data(const triplet_data& triplets, double energy);
	int case_id;
	std::vector<double> b_vec;
	std::vector<double> r_vec;
	double b_split;
	double rc_tilde, r0_tilde;
};

} // namespace magnumpi

#endif // H_MAGNUMPI_VIEHLAND_COMMON_H
