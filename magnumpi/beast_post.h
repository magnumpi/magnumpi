/** \file
 *
 * Copyright (C) 2020,2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_BEAST_POST_H
#define H_MAGNUMPI_BEAST_POST_H

#include "magnumpi/json.h"
#include <string>

namespace magnumpi {

json_type post_request(
	const std::string& host,
	const std::string& port,
	const std::string& target,
	const std::string& req_body,
	int version=11);

} // namespace magnumpi

#endif // H_MAGNUMPI_BEAST_POST_H
