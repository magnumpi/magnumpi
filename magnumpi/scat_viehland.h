/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_SCAT_VIEHLAND_H
#define H_MAGNUMPI_SCAT_VIEHLAND_H

#include "magnumpi/potential.h"
#include "plmath/clencurt.h"

namespace magnumpi {

/** class scat_viehland provides a number of building blocks that are used
 *  for calculations of scattering angles using Viehland's algorithm. These
 *  can be used by code that wants to calculate scettering angles and by code
 *  that needs scattering angles to calculate e.g. cross sections.
 *
 *  \sa calc_cs
 *
 *  \author Jesper Janssen and Jan van Dijk
 *  \date   April 2015
 */
class scat_viehland
{
  public:
	using integrator_type = plasimo::clencurt::integrator;
	/// scattering angles Viehland case 1, eq 61-66
	static double scat1(const integrator_type& integrator,
			const potential& pot, double energy,double b,double r0, double tolerance);
	/// scattering angles Viehland case 2, eq 67-72
	static double scat2(const integrator_type& integrator,
			const potential& pot, double energy,double b,double r_tilde,double r0, double tolerance);
	//find r0
	static double find_r0_case_1(const potential& pot, double energy,double* b,double r0_prev);
	//find r0 for case 3
	static double find_r0_case_3(const potential& pot, double rmin, double rmax, double energy,const double* b);
  private:
	class scat1_integrand;
	class scat2_integrand;
};

} // namespace magnumpi

#endif // H_MAGNUMPI_SCAT_VIEHLAND_H
