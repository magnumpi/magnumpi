/** \file
 *
 * Copyright (C) 2016-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_INTERPOLATION_H
#define H_MAGNUMPI_INTERPOLATION_H

#include <vector>

namespace magnumpi {

/** Linear interpolation, provide x1 and y1 -> obtain y_param for x_param
 *  In case of a matrix y1, pos sets the column to interpolate
 *
 *  \todo Improve documentation
 *
 *  \author Jesper Janssen
 *  \date  September 2016
 */
void single_linear_interp1(
		const std::vector<double>& x1,
		const std::vector<std::vector<double>>& y1,
		double x_param,
		double* y_param,
		int column);

/** linear interpolation for vectors: provide x1 and y1 -> obtain y_param for x_param
 *  In case of a matrix y1, pos sets the column to interpolate
 *
 *  \todo Improve documentation
 *
 *  \author Jesper Janssen
 *  \date  September 2016
 */
void vector_linear_interp1(
		const std::vector<double>& x1,
		const std::vector<double>& y1,
		const std::vector<double>& x_param,
		std::vector<double>& y_param,
		unsigned int mult,
		unsigned int last);

} // namespace magnumpi

#endif // H_MAGNUMPI_INTERPOLATION_H
