/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_CROSSSEC_H
#define H_MAGNUMPI_CROSSSEC_H

#include <stdexcept>

namespace magnumpi {

/** Class cross_section defines the interface for energy-dependent elastic cross
 *  sections \f$\sigma_l(\epsilon)\f$.
 *
 *  Such objects may support the calculation of cross sections for one or more
 *  values of the parameter l (note that l=1 corresponds to the momentum cross
 *  section, while for l=2 the viscosity cross section is obtained).
 *
 *  In order to find out if a cross section object supports a particular l-value
 *  the return values of member function supports_l(l) can be checked.
 *  In order to obtain the cross section value for a given parameter l and for a
 *  given energy you can use the function operator, passing the arguments in
 *  this order. This will throw a runtime_error if the l-value is not supported.
 *
 *  The function operator and member supports_l delegate their tasks to the
 *  protected members get_supports_l and get_value, respectively. These members
 *  have been declared pure virtual and must be overridden by derived cross
 *  section types.
 *
 *  A cross_section object is used as input by the code for the calculation of
 *  collision integrals. In MagnumPI, cross section data are typically the
 *  result of a calculation for a given potential, in which case the values are
 *  obtained from tabulated results (see class cross_section_from_dataset_log).
 *  Cross sections of other types can be used for testing the code for
 *  integrating the data into colision integrals. The derived cross section
 *  class cross_section_hard_sphere provides such opportunity.
 *
 *  Note that the energy argument is assumed to be in J and that the cross
 *  section is returned in m^2.
 *
 *  \author Jan van Dijk
 *  \date   February 2019
 *
 *  \sa cross_section_hard_sphere
 *  \sa cross_section_from_dataset_log
 *
 *  \todo It may be useful to support the creation of cross_section objects
 *  from json_type objects using a factory function, as is the case for
 *  potential objects.
 */
class cross_section
{
  public:
	/** virtual destructor. This is needed for 'delete c' to destroy
	 *  a cross section object of the actual derived type when c is a
	 *  pointer to the cross_section base class.
	 */
	virtual ~cross_section(){}
	/** Return the value of the cross section for paramater \a l and
	 *  energy \a eps. If the requested \a l value is not supported,
	 *  a std::runtime_error exception is thrown. The energy must be
	 *  specified in J. Also the return value is in SI base units (m^2).
	 */
	double operator()(unsigned l, double eps) const
	{
		check_l(l);
		return get_value(l,eps);
	}
	/** Return true if the requested \a l value is supported, false
	 *  otherwise. (No exception is thrown if the value is unsupported.)
	 */
	bool supports_l(unsigned l) const
	{
		return get_supports_l(l);
	}
  protected:
	/** Must be overridden to return the value of the cross section for
	 *  paramater \a l and energy \a eps.
	 */
	virtual double get_value(unsigned l, double eps) const=0;
	/** Must be overridden to return true if the requested \a l value is
	 *  supported, false otherwise.
	 */
	virtual bool get_supports_l(unsigned l) const=0;
	/** If parameter \a l is supported, d nothing. Otherwise, throw
	 *  an exception.
	 */
	void check_l(unsigned l) const
	{
		if (!supports_l(l))
		{
			throw std::runtime_error("Unsupported l-value in cross section calculation.");
		}
	}
};

} // namespace magnumpi

#endif // H_MAGNUMPI_CROSSSEC_H
