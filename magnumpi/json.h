/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#ifndef H_MAGNUMPI_JSON_H
#define H_MAGNUMPI_JSON_H

#include "nlohmann/json.hpp"
#include <iostream>
#include <list>
#include <string>

namespace magnumpi {

/** Make type json_type available in namespace magnumpi. We use the 'nlohmann'
 *  JSON implementation.
 */
using json_type = nlohmann::json;

/** Helper function for creating a json object from stream \a is.
 *
 *  \author Jan van Dijk
 *  \date   February 2019
 */
json_type read_json_from_stream(std::istream& is);

/** Helper function for creating a json object from file \a fname.
 *  Environment variables (of the form $VAR) are substituted in the string
 *  \a fname. A std::runtime_error is thrown if the file cannot be opened
 *  for reading (it does not exist or the user has insufficient permissions).
 *  If \a fname contains a hash, the characters until the hash are
 *  interpreted as the file name, and the characters after the hash are
 *  interpreted as a JSON path. The path functionality is very minimalistic,
 *  it must be of a form like /interaction/potential. In this example,
 *  the unique element 'potential' that resides in the unique element
 *  'interaction' is extracted from the JSOn object and returned to the
 *  caller.
 *
 *  \author Jan van Dijk
 *  \date   February 2019
 */
json_type read_json_from_file(const std::string& fname);

/** Helper function for writing json object \a data to file \a fname.
 *  Environment variables (of the form $VAR) are substituted in the string
 *  \a fname. A std::runtime_error is thrown if the file cannot be opened
 *  for writing. Optional argument \a indent controls the indentation that
 *  is used in the output file. The default value -1 means that the data will
 *  be written in a single-line compact format.
 *
 *  \author Jan van Dijk
 *  \date   February 2019
 */
void write_json_to_file(const json_type& data, const std::string& fname, int indent=-1);

/** A minimalistic implementation of a JSON Path that works with nlohmann::json.
 *
 *  For an explanation of the syntax of a JSON Path, see the documentation of
 *  a full implementation at https://github.com/json-path/JsonPath
 *  Class json_path only supports a single language element: recursibely
 *  accessing subsections of am nlohmann JSON object based on its name.
 *  A path object can be constructed with a string that must be of the form
 *  ".section_name[.subsection_name[.subsubsection_name]]". When member
 *  get_element is called, the indicated subsection of the argument is looked
 *  up and returned. When any section does not exist or is not unique, a
 *  std::runtime_error is produced.
 *
 *  \author Jan van Dijk
 *  \date   December 2021
 */
class json_path
{
	using string_list = std::list<std::string>;
public:
	/// The size type of the representation of the path
	using size_type = string_list::size_type;
	/** Construct an default path, which refers to the entire target
	 *  object.
	 */
	json_path();
	/** Return the size of the path (the number of section identifiers).
	 *  As an example, for path ".section.subsection" the result is two.
	 */
	size_type size() const { return m_path.size(); }
	/** Construct a path from string \a s. This string must be
	 *  of the form ".section_name[.subsection_name[.subsubsection_name]]".
	 *  The string "." refers to the entire object. An runeime_error is
	 *  produced when the string does not start with a "." or when any
	 *  subsection identifier is empty, as in ".section..subsubsection".
	 */
	json_path(const std::string& s);
	/*  Look up (recursively) the subsection of \a json that is specified
	 *  by the path object and return a reference to the result. When any
	 *  section does not exist or is not unique, a std::runtime_error is
	 *  produced.
	 */
	const json_type& get_element(const json_type& json) const;
	/** See the other overload for details. This version accepts a mutable
	 *  JSON object and returns a non-constant reference to the result.
	 */
	json_type& get_element(json_type& json) const;
private:
	/** The internal representation of a path: a list of (non-empty)
	 *  subsection names.
	 */
	string_list m_path;
};

}; // namespace magnumpi

#endif // H_MAGNUMPI_JSON_H
