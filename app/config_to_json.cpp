/** \file
 *
 * Copyright (C) 2018-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

/* This application reads an old-style MagnumPI configuration file
 * from standard input, converts it into a JSON object and writes
 * the result to standard output.
 */

#include "magnumpi/units.h"
#include "plutil/environment.h"
#include <string>
#include <iostream>
#include <map>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <cstdlib>
#include <algorithm>

namespace magnumpi {

/** A class for managing key-value pairs. This was preciously used in MagnumPI
 *  to read and query configuration files. It is now used only for the purpose
 *  if reading such files and converting them to JSON format.
 *
 *  \author Jan van Dijk
 *  \date   October 2018
 */
class config_data
{
  public:
	/** Constructs and returns a config_data object
	 *  that is created by parsing the string \a str.
	 */
	static config_data parse(const std::string& str);

	/** Creates a default (empty) config_data object:
	 *  it as no items and an empty value string.
	 */
	explicit config_data(){}
	/** Copy constructor.
	 */
	config_data(const config_data& other);
	/** Constructs a config_data object that is created
	 *  by parsing characters from the stream \a is.
	 */
	explicit config_data(std::istream& is);
private:
	// NOTE: made private, since the alternative
	// (nlohmann::json) does unchecked access for
	// this operator.
	const config_data& operator[](const std::string& path) const;
	const config_data& operator[](const char* path) const
	{
		return (*this)[std::string(path)];
	}
public:
	/** cnf.at(key) is an alternative spelling for cnf[key].
	 *  NOTE that hlohmann::json is different: at does bounds
	 *  checking, whereas operator[] does not. We therefore
	 *  recommend to use at throughout the code, since there
	 *  is no performance penalty involved in doing bounds
	 *  checking in the present applications.
	 *
	 *  This is for now enforced by making operator[] private.
	 */
	const config_data& at(const std::string& path) const { return (*this)[path]; }
	const config_data& at(const char* path) const { return (*this)[path]; }
	bool operator==(const config_data& arg) const
	{
		return m_value==arg.m_value && m_items==arg.m_items;
	}

	operator std::string() const
	{
		return get_value(); 
	}
	bool operator==(const std::string& val) const
	{
		return get_value()==val;
	}
	bool operator==(const char* val) const
	{
		return get_value()==val;
	}

	operator int() const
	{
		return convert_to<int>(get_value()); 
	}
	bool operator==(int val) const
	{
		return static_cast<int>(*this)==val;
	}

	operator double() const
	{
		return convert_to<double>(get_value()); 
	}
	bool operator==(double val) const
	{
		return static_cast<double>(*this)==val;
	}

	operator bool() const
	{
		return convert_to<bool>(get_value()); 
	}
	bool operator==(bool val) const
	{
		return static_cast<bool>(*this)==val;
	}

	template <class T>
	T get() const
	{
		return convert_to<T>(get_value()); 
	}

	void write(std::ostream& os) const;
	void write_json(std::ostream& os, int indent=0, char indent_char=' ') const;
	std::string dump(int indent, char indent_char) const;
  private:
	const std::string& get_value() const
	{
		if (m_value.empty())
		{
			throw std::runtime_error("Need a string, but have is an object.");
		}
		return m_value; 
	}
	const config_data* find_item(const std::string& path) const;
	void add_item(const std::string& key, const std::string& value);
	void read(std::istream& is);
	void write(std::ostream& os, const std::string& prefix) const;
	void write_json(std::ostream& os, const std::string& key, int indent=0, char indent_char=' ') const;
	using section_map_type = std::map<std::string,config_data>;
	section_map_type m_items;
	std::string m_value;
};

config_data read_config_from_file(const std::string& fname)
{
	std::ifstream is(plasimo::substitute_env_vars_copy(fname,true));
	if (!is)
	{
		throw std::runtime_error("Could not open file '"
			+ fname + "' for reading.");
	}
	try {
		return config_data(is);
	}
	catch (std::exception& exc)
	{
		throw std::runtime_error("Error reading file '"
			+ fname + "':\n" + std::string(exc.what()));
	}
}

config_data config_data::parse(const std::string& str)
{
	std::stringstream ss(str);
	return config_data(ss);
}

config_data::config_data(const config_data& other)
 : m_items(other.m_items),
   m_value(other.m_value)
{
}

config_data::config_data(std::istream& is)
{
	m_value=std::string{};
	read(is);
}

void config_data::write(std::ostream& os) const
{
	write(os,std::string());
}

void config_data::write(std::ostream& os, const std::string& prefix) const
{
	if (!m_value.empty())
	{
		os << prefix << '=' << m_value << '\n';
	}
	else
	{
		for (const auto& s : m_items)
		{
			s.second.write(os, prefix.empty() ? s.first : prefix + '.' + s.first);
		}
	}
}

void config_data::write_json(std::ostream& os, int indent, char indent_char) const
{
	write_json(os,std::string(),indent,indent_char);
	os << std::endl;
}

void config_data::write_json(std::ostream& os, const std::string& key, int indent, char indent_char) const
{
	unsigned nwrite = m_items.size();

	if (!m_value.empty())
	{
		const std::string quote = (is_number(m_value) || is_bool(m_value)) ? "" : "\"";
		os << std::string(indent,indent_char) << '"' << key << "\": " << quote << m_value << quote;
	}
	else
	{
		os << std::string(indent,indent_char) << (key.empty() ? "" : "\"" + key + "\": ") << "{\n";
		for (const auto& s : m_items)
		{
			s.second.write_json(os, s.first, indent+1,indent_char);
			if (--nwrite!=0)
			{
				os << ',';
			}
			os << '\n';
		}
		os << std::string(indent,indent_char) << "}";
	}
}

std::string config_data::dump(int indent, char indent_char) const
{
	std::stringstream ss;
	write_json(ss,indent,indent_char);
	return ss.str();
}

const config_data& config_data::operator[](const std::string& path) const
{
	const config_data* sec = find_item(path);
	if (sec==nullptr)
	{
		throw std::runtime_error("Subsection '" + path + "' not found.");
	}
	return *sec;
}

const config_data* config_data::find_item(const std::string& path) const
{
	section_map_type::const_iterator i = m_items.find(path);
	if (i==m_items.end())
	{
		return nullptr;
	}
	return &i->second;
}

void config_data::add_item(const std::string& key, const std::string& value)
{
	const std::string::size_type pos_dot=key.find('.');
	if (pos_dot==std::string::npos)
	{
		config_data item;
		item.m_value = value;
		m_items.insert({key,item});
	}
	else
	{
		const std::string key0 = key.substr(0,pos_dot);
		const std::string key_rest = key.substr(pos_dot+1);
		m_items[key0].add_item(key_rest,value);
	}
}

void config_data::read(std::istream& is)
try
{
	while(!is.eof())
	{
		std::string line;
		std::getline(is,line);
		std::string::size_type pos_first=0;
		// remove leading whitespace
		while (line[pos_first]==' ' || line[pos_first]=='\t')
		{
			++pos_first;
		}
		line = line.substr(pos_first);
		// ignore empty lines
		if (line.empty())
		{
			continue;
		}
		if (line[0]=='#')
		{
			// ignore comment line
			continue;
		}
		// parse line, add key-value pair
		const std::string::size_type pos_equal=line.find('=');
		if (pos_equal==0 || pos_equal==std::string::npos)
		{
			throw std::runtime_error("Syntax error on line '"
				+ line + "'. Expected <key>=[value].");
		}
		const std::string key = line.substr(0,pos_equal);
		const std::string val = line.substr(pos_equal+1);
		add_item(key,val);
	}
}
catch (std::exception& exc)
{
	throw std::runtime_error("Error reading stream: "
		+ std::string(exc.what()));
}

} // namespace magnumpi

int main()
try
{
	magnumpi::config_data cnf(std::cin);
	cnf.write_json(std::cout,0,' ');
	return 0;
}
catch (const std::exception& exc)
{
	std::cerr << exc.what() << std::endl;
	return 1;
}
