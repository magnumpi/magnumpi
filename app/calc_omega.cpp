/** \file
 *
 * Copyright (C) 2015-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <string>
#include <iostream>
#include <stdexcept>
#include <chrono>
#include "magnumpi/json.h"
#include "magnumpi/calc_cs.h"
#include "magnumpi/calc_omega.h"
#include "magnumpi/data_set.h"
#include "magnumpi/crosssec_dataset.h"

int main(int argc, char *argv[])
try
{
	using namespace magnumpi;

	const std::chrono::time_point<std::chrono::system_clock> start(std::chrono::system_clock::now());
	if (argc!=4)
	{
		throw std::runtime_error("Usage: calc_omega <settings file> <interaction file> <outputfile-prefix>");
	}
	const json_type cnf(magnumpi::read_json_from_file(argv[1]));
	const json_type inter(magnumpi::read_json_from_file(argv[2]));
	const std::string prefix = argv[3];

	// part 1: calculate cross sections Q^(l)(eps)

	const data_set Qtable = calc_cross_sec(cnf,inter.at("potential"));
	Qtable.write(prefix+"cs.txt");
	magnumpi::write_json_to_file(Qtable.write_json(),prefix+"cs.json",2);

	// part 2: calculate the collision integrals Omega^(l,s)(T)

	/// \todo this assumes that Qtable has columns eps Q^(1) Q^(2) ... Q^(lmax)
	const unsigned l_max = Qtable.col_axis().size();
	cross_section_from_dataset_log Q(Qtable.write_json());

	const double reduced_mass = in_si(inter.at("reduced_mass"),units::mass);
	const double tolerance = cnf.at("tolerance");
	const data_set omega_data = calculate_ci(Q,cnf.at("ci"),l_max,reduced_mass,tolerance);
	omega_data.write(prefix+"ci.txt");
	write_json_to_file(omega_data.write_json(),prefix+"ci.json",2);

	//print the elapsed time
	const std::chrono::time_point<std::chrono::system_clock> end(std::chrono::system_clock::now());
	const std::chrono::duration<double> dt(end - start);
	std::cout << "Elapsed time: " << dt.count() << "s" << std::endl;
	return 0;
}
catch (const std::exception& exc)
{
	std::cerr << "Error: " << exc.what() << std::endl;
	return 1;
}
