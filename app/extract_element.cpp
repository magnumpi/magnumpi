/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/json.h"
#include "magnumpi/potential.h"
#include "plutil/split_string.h"
#include <iostream>
#include <vector>
#include <memory>
#include <stdexcept>

int main(int argc, const char* argv[])
try
{
	using namespace magnumpi;

	if (argc!=2)
	{
		throw std::runtime_error(
			"Usage: extract_element <JSON path>.\n"
			"This file reads a JSON file from stdin and writes\n"
			"the section that is indicated by the path to stdout.\n"
			"A path is of the form . or .section.subsection...\n");
	}
	const json_type j(read_json_from_stream(std::cin));
	const json_path p(argv[1]);
	std::cout << p.get_element(j).dump(2) << std::endl;
	return 0;
}
catch(std::exception& exc)
{
	std::cerr << exc.what() << std::endl;
	return 1;
}
