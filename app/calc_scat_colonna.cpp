/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <string>
#include <iostream>
#include <stdexcept>
#include "magnumpi/scat_colonna.h"

using namespace magnumpi;

void run_case(const json_type& cnf, const json_type& pot, const std::string& prefix)
{
	data_set chi = get_scat_vs_b_vs_E(cnf,pot);
	chi.write(prefix+"scat_vs_b_vs_E.dat");
	write_json_to_file(chi.write_json(),prefix+"scat_vs_b_vs_E.json",2);
}

int main(int argc, char *argv[])
try
{
	if (argc!=4)
	{
		throw std::runtime_error("Usage: calc_scat_colonna <config file> "
			"<potential file> <outputfile-prefix>");
	}
	const json_type cnf = read_json_from_file(argv[1]);
	// we assume that the file is an interaction file with a potential
	// element inside.
	const json_type pot = read_json_from_file(argv[2]);
	run_case(cnf,pot,argv[3]);
	return 0;
}
catch (const std::exception& exc)
{
	std::cerr << "Error calculating chi(b,E): " << exc.what() << std::endl;
	return 1;
}
