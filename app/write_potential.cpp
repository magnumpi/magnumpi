/** \file
 *
 * Copyright (C) 2019-2021 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include "magnumpi/json.h"
#include "magnumpi/potential.h"
#include <iostream>
#include <memory>
#include <stdexcept>

const char* usage_str =
	"Usage: write_potential <--xny|--json> <rmin/m> <rmax/m> <npoints>.\n"
	"This file reads a JSON potential section from stdin and writes\n"
	"a potential table to stdout.\n\n"
	"The following example uses extract_element to extract a potential from a JSON file.\n"
	"   ./app/extract_element .interaction.potential < ../input/heplus_he_gerade_int.json "
	" | ./app/write_potential --json 1e-12 1e-9 1000";

int main(int argc, const char* argv[])
try
{
	using namespace magnumpi;

	if (argc!=5)
	{
		throw std::runtime_error(usage_str);
	}
	const json_type cnf(read_json_from_stream(std::cin));
	const std::unique_ptr<const potential> pot(potential::create(cnf));
	const double rmin = convert_to<double>(argv[2]);
	const double rmax = convert_to<double>(argv[3]);
	const std::size_t npoints = convert_to<std::size_t>(argv[4]);
	if (argv[1]==std::string("--xny"))
	{
		pot->write_xny(rmin,rmax,npoints).write(std::cout);
	}
	else if (argv[1]==std::string("--json"))
	{
		std::cout << pot->write_json(rmin,rmax,npoints).dump(2) << std::endl;
	}
	else
	{
		throw std::runtime_error(usage_str);
	}
	return 0;
}
catch(std::exception& exc)
{
	std::cerr << exc.what() << std::endl;
	return 1;
}
