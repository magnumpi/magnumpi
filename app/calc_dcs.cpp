/** \file
 *
 * Copyright (C) 2019-2025 Eindhoven University of Technology.
 *
 * This code is free software, you can redistribute it and/or modify it under
 * the terms of the GNU General Public License; either version 3.0 of the
 * License, or (at your option) any later version. See COPYING-GPL3 for details.
 *
 * This code is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <string>
#include <iostream>
#include <stdexcept>
#include "magnumpi/scat_colonna.h"
#include "magnumpi/calc_dcs.h"

using namespace magnumpi;

void run_case(const json_type& cnf, const json_type& pot, const std::string& prefix)
{
	data_set dcs = calc_diff_cross_sec(cnf,pot);
	std::string extra = "";
	if (prefix.size() > 0)
	{
		//add _ at the end when it is not already there
		if (std::strcmp(&prefix.back(),"_") != 0)
		{
			extra = "_";
		}
	}
	dcs.write(prefix+extra+"dcs.dat");
	write_json_to_file(dcs.write_json(),prefix+extra+"dcs.json",2);
}

int main(int argc, char *argv[])
try
{
	if (argc!=4)
	{
		throw std::runtime_error("Usage: calc_dcs <config file> "
			"<potential file> <outputfile-prefix>");
	}
	const json_type cnf = read_json_from_file(argv[1]);
	// we assume that the file is an interaction file with a potential
	// element inside.
	const json_type pot = read_json_from_file(argv[2]);
	run_case(cnf,pot,argv[3]);
	return 0;
}
catch (const std::exception& exc)
{
	std::cerr << "Error calculating the differential cross section: " << exc.what() << std::endl;
	return 1;
}
