if HAVE_PDFLATEX
DOCTARGET_PDF=$(LATEX_DOC_NAME).pdf
endif
if MAKE_HTLATEX
DOCTARGET_XML=$(LATEX_DOC_NAME)-xml
DOCTARGET_NEWHTML=$(LATEX_DOC_NAME)-html
endif

EXTRA_TEXINPUTS ?=

FORCE:

LATEX_INTERACTION_MODE ?= -interaction batchmode
HTLATEX_SPLIT_LEVEL ?= 3
DOC_INST_DIR ?= "doc/"

DOC_TARGETS=$(DOCTARGET_PDF) $(DOCTARGET_HTML) $(DOCTARGET_XML) $(DOCTARGET_NEWHTML)

all-docs: $(DOC_TARGETS)
	echo $(DOC_TARGETS)


%.aux: $(LATEX_DOC_SRC)
	TEXINPUTS=.:$(TEXINPUTS):$(srcdir):$(top_srcdir):$(EXTRA_TEXINPUTS) pdflatex $(LATEX_INTERACTION_MODE) $(srcdir)/$(LATEX_DOC_NAME)

%.bbl: %.aux
	BIBINPUTS=".:$(BIBINPUTS):$(srcdir):$(top_srcdir)" BSTINPUTS=".:$(BSTINPUTS):$(srcdir):$(top_srcdir)" bibtex $(LATEX_DOC_NAME)

%.pdf: %.bbl FORCE
	TEXINPUTS=.:$(TEXINPUTS):$(srcdir):$(top_srcdir):$(EXTRA_TEXINPUTS) pdflatex $(LATEX_INTERACTION_MODE) $(srcdir)/$(LATEX_DOC_NAME)
	TEXINPUTS=.:$(TEXINPUTS):$(srcdir):$(top_srcdir):$(EXTRA_TEXINPUTS) pdflatex $(LATEX_INTERACTION_MODE) $(srcdir)/$(LATEX_DOC_NAME)

clean-aux: FORCE
	rm -rf *.toc *.aux *.log *.dvi *.blg *.bbl *.out

clean-local:
	rm -rf	*.toc *.lof *.lot *.aux *.log *.blg *.bbl *.out \
		*.4ct  *.4tc  *.dvi  *.idv  *.lg  *.tmp  *.xref \
		*.idx *.ndx *.ilg $(LATEX_DOC_NAME).pdf $(LATEX_DOC_NAME).ps \
		$(LATEX_DOC_NAME)-html	\
		$(LATEX_DOC_NAME)-xml	\
		$(MORE_CLEAN_FILES)

install-data-local: all-docs
	$(INSTALL) -d -m755 $(DESTDIR)$(prefix)/$(DOC_INST_DIR)
if HAVE_PDFLATEX
	$(INSTALL_DATA) $(LATEX_DOC_NAME).pdf $(DESTDIR)$(prefix)/$(DOC_INST_DIR)/$(LATEX_DOC_NAME).pdf
endif
if MAKE_HTLATEX
	$(INSTALL) -d -m755 $(DESTDIR)$(prefix)/$(DOC_INST_DIR)/$(LATEX_DOC_NAME)-xml
	for FILE in `ls $(LATEX_DOC_NAME)-xml`; do \
	  $(INSTALL_DATA) $(LATEX_DOC_NAME)-xml/$$FILE $(DESTDIR)$(prefix)/$(DOC_INST_DIR)/$(LATEX_DOC_NAME)-xml; \
	done
	$(INSTALL) -d -m755 $(DESTDIR)$(prefix)/$(DOC_INST_DIR)/$(LATEX_DOC_NAME)-html
	for FILE in `ls $(LATEX_DOC_NAME)-html`; do \
	  $(INSTALL_DATA) $(LATEX_DOC_NAME)-html/$$FILE $(DESTDIR)$(prefix)/$(DOC_INST_DIR)/$(LATEX_DOC_NAME)-html; \
	done
endif

%-xml: %.bbl
	rm -rf $(LATEX_DOC_NAME)-xml && \
	mkdir $(LATEX_DOC_NAME)-xml && \
	cd $(LATEX_DOC_NAME)-xml && \
	TEXINPUTS=.:$(TEXINPUTS):..:$(abs_srcdir):$(abs_top_srcdir) htlatex $(abs_srcdir)/$(LATEX_DOC_NAME) "xhtml,mozilla,$(HTLATEX_SPLIT_LEVEL),next" "-cmozhtf" "" "$(LATEX_INTERACTION_MODE)"
#	TEXINPUTS=.:$(TEXINPUTS):..:$(srcdir):$(top_srcdir) htlatex $(srcdir)/$(LATEX_DOC_NAME) "xhtml,mozilla,$(HTLATEX_SPLIT_LEVEL),uni-html4" " -cmozhtf -cunihtf -utf8" "" "$(LATEX_INTERACTION_MODE)"
	
%-html: %.bbl
	rm -rf $(LATEX_DOC_NAME)-html && \
	mkdir $(LATEX_DOC_NAME)-html && \
	cd $(LATEX_DOC_NAME)-html && \
	TEXINPUTS=.:$(TEXINPUTS):..:$(abs_srcdir):$(abs_top_srcdir) htlatex $(abs_srcdir)/$(LATEX_DOC_NAME) "xhtml,$(HTLATEX_SPLIT_LEVEL),next" "" "" "$(LATEX_INTERACTION_MODE)"
	
