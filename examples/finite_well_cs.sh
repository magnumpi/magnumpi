#!/bin/bash

cnf=$MAGNUMPI_INPUTDATA_DIR/input/cs.json
pot=$MAGNUMPI_INPUTDATA_DIR/input/finite_well_pot.json#.potential
ext=finite-well-
$MAGNUMPI_DIR/app/calc_cs $cnf $pot $ext
