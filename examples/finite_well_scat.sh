#!/bin/bash

cnf=$MAGNUMPI_INPUTDATA_DIR/input/scat.json
pot=$MAGNUMPI_INPUTDATA_DIR/input/finite_well_pot.json#.potential
ext=finite-well-
$MAGNUMPI_DIR/app/calc_scat_colonna $cnf $pot $ext
