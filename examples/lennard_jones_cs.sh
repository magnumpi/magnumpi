#!/bin/bash

cnf=$MAGNUMPI_INPUTDATA_DIR/input/cs.json
pot=$MAGNUMPI_INPUTDATA_DIR/input/colonna_fig4a_lj_pot.json#.potential
ext=lennard-jones-
$MAGNUMPI_DIR/app/calc_cs $cnf $pot $ext
