#!/bin/bash

cnf=$MAGNUMPI_INPUTDATA_DIR/input/scat.json
pot=$MAGNUMPI_INPUTDATA_DIR/input/colonna_fig4a_lj_pot.json#.potential
ext=lennard-jones-
$MAGNUMPI_DIR/app/calc_scat_colonna $cnf $pot $ext
