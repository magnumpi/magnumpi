#!/bin/bash

# Rollup generates code which does not work inside web worker like usage of document and window globals
perl -pi -e 's!document.baseURI!self.location.href!g' dist/assets/calculator.worker.*.js
perl -pi -e 's!document\.currentScript\&\&document\.currentScript\.src\|\|!!g' dist/assets/calculator.worker.*.js
perl -pi -e 's!window.location!self.location!g' dist/assets/calculator.worker.*.js

# WASM files is not included in dist/ dir by vite so do it manually.
SRC_WASM=$(echo "console.log(require.resolve('@magnumpi/wasm/dist/magnumpicalc.wasm'))" | node )
DST_WASM=$(perl -n -e 'print $1 if /(magnumpicalc\w*.wasm)/' dist/assets/calculator.worker.*.js)
cp ${SRC_WASM} dist/assets/${DST_WASM}