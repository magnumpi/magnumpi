import { tsvParseRows } from "d3-dsv";
import parseDataURL from "data-urls";

export function dcsRowConvert(d) {
  return {
    chi: +d[0],
    energy: +d[1],
    dcs: +d[2],
  };
}

export function csOddEvenRowConvert(d) {
  const r = {
    energy: +d.shift(),
  };
  d.forEach((v, i) => {
    r["l_" + (i + 1)] = +v;
  });
  return r;
}

export function dcsRceRowConvert(d) {
  const r = {
    chi: +d.shift(),
  };
  r.energy = +d.shift();
  d.forEach((v, i) => {
    r["dcs_" + (i + 1)] = +v;
  });
  return r;
}

export function lutRowConvert(d) {
  return [+d[0], +d[1]];
}

export function readDCS(FS) {
  const dcstxt = FS.readFile("DCS.txt", { encoding: "utf8" });
  FS.unlink("DCS.txt");
  return tsvParseRows(dcstxt, dcsRowConvert);
}

export function readTotalCS(FS) {
  const cs_odd_eventxt = FS.readFile("cs_odd_even.txt", { encoding: "utf8" });
  FS.unlink("cs_odd_even.txt");
  return tsvParseRows(cs_odd_eventxt, csOddEvenRowConvert);
}

export function readDCSRCE(FS) {
  const dcstxt = FS.readFile("DCS.txt", { encoding: "utf8" });
  FS.unlink("DCS.txt");
  return tsvParseRows(dcstxt, dcsRceRowConvert);
}

export function readLUT(dataUrl) {
  const blob = parseDataURL(dataUrl);
  const text = blob.body.toString();
  return tsvParseRows(text, lutRowConvert);
}
