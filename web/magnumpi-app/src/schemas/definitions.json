{
  "Length": {
    "type": "object",
    "properties": {
      "value": {
        "type": "number",
        "title": "Value",
        "minimum": 0
      },
      "unit": {
        "type": "string",
        "title": "Unit",
        "default": "m",
        "enum": ["m", "a0"]
      }
    },
    "required": ["value", "unit"],
    "additionalProperties": false
  },
  "Temperature": {
    "type": "object",
    "properties": {
      "value": {
        "type": "number",
        "title": "Value",
        "minimum": 0
      },
      "unit": {
        "type": "string",
        "title": "Unit",
        "default": "K",
        "enum": ["K", "eVT"]
      }
    },
    "required": ["value", "unit"],
    "additionalProperties": false
  },
  "Energy": {
    "type": "object",
    "properties": {
      "value": {
        "type": "number",
        "title": "Value",
        "minimum": 0
      },
      "unit": {
        "type": "string",
        "title": "Unit",
        "default": "eV",
        "enum": ["eV", "J"]
      }
    },
    "required": ["value", "unit"],
    "additionalProperties": false
  },
  "Mass": {
    "type": "object",
    "properties": {
      "value": {
        "type": "number",
        "title": "Value",
        "minimum": 0
      },
      "unit": {
        "type": "string",
        "title": "Unit",
        "default": "amu",
        "enum": ["amu", "kg"]
      }
    },
    "required": ["value", "unit"],
    "additionalProperties": false
  },
  "Area": {
    "type": "object",
    "properties": {
      "value": {
        "type": "number",
        "title": "Value",
        "minimum": 0
      },
      "unit": {
        "type": "string",
        "title": "Unit",
        "default": "m^2",
        "enum": ["m^2"]
      }
    },
    "required": ["value", "unit"],
    "additionalProperties": false
  },
  "VolumeVelocity": {
    "type": "object",
    "properties": {
      "value": {
        "type": "number",
        "title": "Value",
        "minimum": 0
      },
      "unit": {
        "type": "string",
        "title": "Unit",
        "default": "m^3/s",
        "enum": ["m^3/s"]
      }
    },
    "required": ["value", "unit"],
    "additionalProperties": false
  },
  "Jm6": {
    "type": "object",
    "properties": {
      "value": {
        "type": "number",
        "title": "Value",
        "minimum": 0
      },
      "unit": {
        "type": "string",
        "title": "Unit",
        "default": "J*m^6",
        "enum": ["J*m^6", "eV*m^6"]
      }
    },
    "required": ["value", "unit"],
    "additionalProperties": false
  },
  "FiniteWell": {
    "properties": {
      "r_s": {
        "$ref": "#/definitions/Length"
      },
      "V_s": {
        "$ref": "#/definitions/Energy"
      }
    },
    "required": ["r_s", "V_s"],
    "type": "object"
  },
  "HardSphere": {
    "properties": {
      "re": {
        "$ref": "#/definitions/Length"
      }
    },
    "required": ["re"],
    "type": "object"
  },
  "LennardJones": {
    "properties": {
      "epsilon": {
        "$ref": "#/definitions/Energy"
      },
      "sigma": {
        "$ref": "#/definitions/Length"
      }
    },
    "required": ["epsilon", "sigma"],
    "type": "object"
  },
  "HulburtHirschfelder": {
    "properties": {
      "alpha": {
        "type": "number"
      },
      "beta": {
        "type": "number"
      },
      "gamma": {
        "type": "number"
      },
      "phi0": {
        "$ref": "#/definitions/Energy"
      },
      "re": {
        "$ref": "#/definitions/Length"
      }
    },
    "required": ["alpha", "beta", "gamma", "phi0", "re"],
    "type": "object"
  },
  "ESMSV": {
    "description": "Brunetti 1981",
    "properties": {
      "eps": {
        "$ref": "#/definitions/Energy"
      },
      "rm": {
        "$ref": "#/definitions/Length"
      },
      "betam": {
        "type": "number"
      },
      "A": {
        "type": "number"
      },
      "alpha": {
        "type": "number"
      },
      "beta": {
        "$ref": "#/definitions/Area"
      },
      "a1": {
        "type": "number"
      },
      "a2": {
        "type": "number"
      },
      "a3": {
        "type": "number"
      },
      "a4": {
        "type": "number"
      },
      "gamma": {
        "type": "number"
      },
      "x1": {
        "type": "number"
      },
      "x2": {
        "type": "number"
      },
      "x3": {
        "type": "number"
      },
      "x4": {
        "type": "number"
      },
      "b1": {
        "type": "number"
      },
      "b2": {
        "type": "number"
      },
      "b3": {
        "type": "number"
      },
      "b4": {
        "type": "number"
      },
      "C6": {
        "$ref": "#/definitions/Jm6"
      }
    },
    "required": [
      "eps",
      "rm",
      "betam",
      "A",
      "alpha",
      "beta",
      "a1",
      "a2",
      "a3",
      "a4",
      "gamma",
      "x1",
      "x2",
      "x3",
      "x4",
      "b1",
      "b2",
      "b3",
      "b4",
      "C6"
    ],
    "type": "object"
  },
  "BuckinghamCorner": {
    "properties": {
      "A": {
        "$ref": "#/definitions/Energy"
      },
      "alpha": {
        "type": "number"
      },
      "rm": {
        "$ref": "#/definitions/Length"
      },
      "C6": {
        "$ref": "#/definitions/Jm6"
      },
      "beta": {
        "$ref": "#/definitions/Area"
      },
      "gamma": {
        "type": "number"
      }
    },
    "required": ["A", "alpha", "rm", "C6", "beta", "gamma"],
    "type": "object"
  },
  "ExponentialRepulsive": {
    "properties": {
      "A": {
        "$ref": "#/definitions/Energy"
      },
      "rho": {
        "$ref": "#/definitions/Length"
      }
    },
    "required": ["A", "rho"],
    "type": "object"
  },
  "MS_12_4": {
    "description": "Mason Schamp 1958 Ann. Phys. p233",
    "properties": {
      "epsilon": {
        "$ref": "#/definitions/Energy"
      },
      "rm": {
        "$ref": "#/definitions/Length"
      }
    },
    "required": ["epsilon", "rm"],
    "type": "object"
  },
  "Morse": {
    "properties": {
      "epsilon": {
        "$ref": "#/definitions/Energy"
      },
      "re": {
        "$ref": "#/definitions/Length"
      },
      "sigma": {
        "$ref": "#/definitions/Length"
      },
      "C": {
        "type": "number"
      }
    },
    "required": ["epsilon", "re", "sigma", "C"],
    "type": "object"
  },
  "RSCP": {
    "description": "Repulsive screened coulomb potential",
    "properties": {
      "epsilon": {
        "$ref": "#/definitions/Energy"
      },
      "rho": {
        "$ref": "#/definitions/Length"
      }
    },
    "required": ["epsilon", "rho"],
    "type": "object"
  },
  "LUT": {
    "description": "Lookup table",
    "properties": {
      "is_neutral": {
        "title": "Is neutral",
        "type": "boolean"
      },
      "file": {
        "title": "File",
        "type": "string",
        "description": "Tab delimited file radius (m) and potential (eV) columns",
        "format": "data-url"
      }
    },
    "required": ["is_neutral", "file"],
    "type": "object"
  },
  "LXCatJSONInline": {
    "description": "LXCat JSON",
    "properties": {},
    "required": ["record"],
    "type": "object"
  },
  "LXCatJSONExtern": {
    "description": "LXCat JSON file",
    "properties": {
      "file": {
        "title": "File",
        "type": "string",
        "description": "JSON file formatted with LXCat JSON schema",
        "format": "data-url"
      }
    },
    "required": ["file"],
    "type": "object"
  },
  "LXCatJSONOnline": {
    "description": "LXCat Online",
    "properties": {
      "endpoint": {
        "title": "Endpoint",
        "type": "string",
        "description": "Root of LXCat web service",
        "default": "http://localhost:3000"
      },
      "is_neutral": {
        "title": "Is neutral",
        "type": "boolean",
        "default": false
      },
      "body": {
        "type": "object",
        "properties": {
          "states": {
            "title": "States",
            "type": "array",
            "items": {
              "type": "string"
            },
            "minItems": 2,
            "maxItems": 2
          },
          "parity": {
            "title": "Parity",
            "type": "string",
            "enum": ["gerade", "ungerade"]
          }
        },
        "required": ["states", "parity"]
      }
    },
    "required": ["endpoint", "is_neutral", "body"],
    "type": "object"
  },
  "Potential": {
    "type": "object",
    "title": "Potential",
    "properties": {
      "potentialType": {
        "type": "string",
        "title": "Type",
        "enum": [
          "FiniteWell",
          "LennardJones",
          "HardSphere",
          "HulburtHirschfelder",
          "ESMSV",
          "BuckinghamCorner",
          "ExponentialRepulsive",
          "MS_12_4",
          "Morse",
          "RSCP",
          "LUT",
          "LXCatJSONInline",
          "LXCatJSONExtern",
          "LXCatJSONOnline"
        ],
        "default": "HardSphere"
      }
    },
    "required": ["potentialType"],
    "dependencies": {
      "potentialType": {
        "oneOf": [
          {
            "properties": {
              "potentialType": {
                "enum": ["FiniteWell"]
              },
              "potentialSettings": {
                "title": "FiniteWell settings",
                "$ref": "#/definitions/FiniteWell"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["LennardJones"]
              },
              "potentialSettings": {
                "title": "LennardJones settings",
                "$ref": "#/definitions/LennardJones"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["HardSphere"]
              },
              "potentialSettings": {
                "title": "HardSphere settings",
                "$ref": "#/definitions/HardSphere"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["HulburtHirschfelder"]
              },
              "potentialSettings": {
                "title": "HulburtHirschfelder settings",
                "$ref": "#/definitions/HulburtHirschfelder"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["ESMSV"]
              },
              "potentialSettings": {
                "title": "ESMSV settings",
                "$ref": "#/definitions/ESMSV"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["BuckinghamCorner"]
              },
              "potentialSettings": {
                "title": "BuckinghamCorner settings",
                "$ref": "#/definitions/BuckinghamCorner"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["ExponentialRepulsive"]
              },
              "potentialSettings": {
                "title": "ExponentialRepulsive settings",
                "$ref": "#/definitions/ExponentialRepulsive"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["MS_12_4"]
              },
              "potentialSettings": {
                "title": "MS_12_4 settings",
                "$ref": "#/definitions/MS_12_4"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["Morse"]
              },
              "potentialSettings": {
                "title": "Morse settings",
                "$ref": "#/definitions/Morse"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["RSCP"]
              },
              "potentialSettings": {
                "title": "RSCP settings",
                "$ref": "#/definitions/RSCP"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["LUT"]
              },
              "potentialSettings": {
                "title": "LUT settings",
                "$ref": "#/definitions/LUT"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["LXCatJSONInline"]
              },
              "potentialSettings": {
                "title": "LXCatJSONInline settings",
                "$ref": "#/definitions/LXCatJSONInline"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["LXCatJSONExtern"]
              },
              "potentialSettings": {
                "title": "LXCatJSONExtern settings",
                "$ref": "#/definitions/LXCatJSONExtern"
              }
            },
            "required": ["potentialSettings"],
            "additionalProperties": false,
            "type": "object"
          },
          {
            "properties": {
              "potentialType": {
                "enum": ["LXCatJSONOnline"]
              },
              "potentialSettings": {
                "title": "LXCatJSONOnline settings",
                "$ref": "#/definitions/LXCatJSONOnline"
              }
            }
          }
        ]
      }
    }
  },
  "ClenshawCurtisQuadrature": {
    "title": "Clenshaw Curtis Quadrature",
    "type": "object",
    "properties": {
      "lower": {
        "type": "integer"
      },
      "upper": {
        "type": "integer"
      }
    },
    "required": ["lower", "upper"],
    "additionalProperties": false
  },
  "EnergyRange": {
    "type": "object",
    "title": "Energy range",
    "properties": {
      "min": {
        "$ref": "#/definitions/Energy"
      },
      "max": {
        "$ref": "#/definitions/Energy"
      },
      "pointsPerDecade": {
        "title": "points per decade",
        "type": "integer"
      }
    },
    "required": ["min", "max", "pointsPerDecade"],
    "additionalProperties": false
  },
  "LengthRange": {
    "type": "object",
    "title": "Length range",
    "properties": {
      "min": {
        "$ref": "#/definitions/Length"
      },
      "max": {
        "$ref": "#/definitions/Length"
      },
      "points": {
        "type": "integer"
      }
    },
    "required": ["min", "max", "points"],
    "additionalProperties": false
  },
  "CollisionIntegral": {
    "type": "object",
    "title": "Collision integral",
    "properties": {
      "s_max": {
        "type": "integer"
      },
      "clenshawCurtisQuadrature": {
        "$ref": "#/definitions/ClenshawCurtisQuadrature"
      },
      "temperatureRange": {
        "type": "object",
        "title": "Temperature range",
        "properties": {
          "min": {
            "$ref": "#/definitions/Temperature"
          },
          "max": {
            "$ref": "#/definitions/Temperature"
          },
          "step": {
            "$ref": "#/definitions/Temperature"
          }
        },
        "required": ["min", "max", "step"],
        "additionalProperties": false
      }
    },
    "required": ["s_max", "clenshawCurtisQuadrature", "temperatureRange"],
    "additionalProperties": false
  },
  "ReducedMass": {
    "$ref": "#/definitions/Mass",
    "title": "Reduced mass"
  },
  "LMax": {
    "type": "integer"
  },
  "Tolerance": {
    "title": "Tolerance",
    "type": "number"
  },
  "R": {
    "title": "R",
    "type": "object",
    "properties": {
      "min": {
        "$ref": "#/definitions/Length"
      },
      "max": {
        "$ref": "#/definitions/Length"
      },
      "del": {
        "$ref": "#/definitions/Length"
      },
      "Vmin": {
        "$ref": "#/definitions/Energy"
      },
      "Vmax": {
        "$ref": "#/definitions/Energy"
      }
    },
    "required": ["min", "max", "del", "Vmin", "Vmax"],
    "additionalProperties": false
  }
}
