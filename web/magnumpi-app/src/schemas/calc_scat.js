import definitions from "./definitions.json";
import LXCatJSONSchema from "./interaction-potential.json";

export const schema = {
  $schema: "http://json-schema.org/draft-07/schema#",
  $id: "http://magnumpi.org/schemas/calc_scat.json",
  definitions: {
    ...definitions,
    ...LXCatJSONSchema.definitions,
  },
  properties: {
    interaction: {
      type: "object",
      properties: {
        potential: {
          $ref: "#/definitions/Potential",
        },
      },
      required: ["potential"],
    },
    bRange: {
      $ref: "#/definitions/LengthRange",
    },
    energyRange: {
      $ref: "#/definitions/EnergyRange",
    },
    tolerance: {
      $ref: "#/definitions/Tolerance",
    },
  },
  required: ["interaction", "energyRange", "bRange", "tolerance"],
  additionalProperties: false,
  type: "object",
};

schema.definitions.LXCatJSONInline.properties.record = LXCatJSONSchema;
