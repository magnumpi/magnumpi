import PropTypes from "prop-types";
import React, { Component } from "react";
import { VegaLite } from "react-vega";

import { DownloadButton } from "./DownloadButton";
import { tcs2tsv } from "./writers";

export class TotalCrossSectionChart extends Component {
  render() {
    const values = this.format4vega();
    const tsv = tcs2tsv(this.props.data.values);
    const spec = {
      title: "Total cross section",
      width: 800,
      height: 600,
      mark: "point",
      encoding: {
        x: { field: "energy", type: "quantitative", scale: { type: "log" } },
        y: { field: "dcs", type: "quantitative", axis: { format: ".4e" } },
        color: { field: "l", type: "nominal" },
      },
      selection: {
        grid: {
          type: "interval",
          bind: "scales",
        },
      },
      usermeta: {
        embedOptions: {
          actions: false,
        },
      },
      data: { name: "values" },
    };
    return (
      <div>
        <VegaLite spec={spec} data={{ values }} renderer="canvas" />
        <DownloadButton
          filename="tcs.txt"
          mime_type="text/tab-separated-values"
          blob={tsv}
        />
      </div>
    );
  }

  format4vega() {
    const values = [];
    this.props.data.values.forEach((d) => {
      const energy = d.energy;
      Object.keys(d)
        .filter((l) => l.startsWith("l_"))
        .forEach((l) => {
          values.push({
            energy,
            dcs: d[l],
            l: l.replace("l_", ""),
          });
        });
    });
    return values;
  }
}
TotalCrossSectionChart.propTypes = {
  data: PropTypes.any.isRequired,
};
