import Form from "@rjsf/core";
import React, { Component } from "react";
import Alert from "react-bootstrap/lib/Alert";
import Button from "react-bootstrap/lib/Button";
import ButtonGroup from "react-bootstrap/lib/ButtonGroup";
import Glyphicon from "react-bootstrap/lib/Glyphicon";
import ProgressBar from "react-bootstrap/lib/ProgressBar";

import CalculatorWorker from "./calculator.worker.js?worker";
import { defaultPotentialSettings } from "./defaultPotentialSettings";
import { DifferentialCrossSectionChart } from "./DifferentialCrossSectionChart";
import { FormSerializer } from "./FormSerializer";
import { schema } from "./schemas/calc_dcs";
import { TotalCrossSectionChart } from "./TotalCrossSectionChart";

export class CalcDifferentialCrossSection extends Component {
  constructor(props) {
    super(props);
    this.formRef = React.createRef();
    this.state = {
      request: {
        potential: {
          potentialType: "LennardJones",
          potentialSettings: defaultPotentialSettings,
        },
        reducedMass: {
          value: 2.0013,
          unit: "amu",
        },
        clenshawCurtisQuadrature: {
          lower: 6,
          upper: 14,
        },
        energyRange: {
          min: {
            value: 1e-4,
            unit: "eV",
          },
          max: {
            value: 1e5,
            unit: "eV",
          },
          pointsPerDecade: 20,
        },
        l_max: 5,
        r: {
          min: {
            value: 2e-10,
            unit: "m",
          },
          max: {
            value: 1e-9,
            unit: "m",
          },
          del: {
            value: 1e-14,
            unit: "m",
          },
          Vmin: {
            value: 1e-3,
            unit: "eV",
          },
          Vmax: {
            value: 20,
            unit: "eV",
          },
        },
        tolerance: 1e-3,
      },
      results: {
        dcs: { values: [] },
        total: { values: [] },
      },
      progress: {
        progress: 0,
        detail: "Not started",
      },
    };
  }

  render() {
    let main = (
      <div>
        No calculation results available. Fill form and press Calculate button
        below.
      </div>
    );
    if (this.state.progress.error) {
      main = (
        <Alert bsStyle="danger">
          <h2>Calculation failed</h2>
          <p>{this.state.progress.error}</p>
        </Alert>
      );
    } else if (
      this.state.progress.progress > 0 &&
      this.state.progress.progress < 100
    ) {
      main = (
        <div>
          <span>Calculating: {this.state.progress.detail}</span>
          <ProgressBar now={this.state.progress.progress} />
          <Button onClick={this.cancel}>
            <Glyphicon glyph="remove" /> Cancel
          </Button>
          <p>Follow log of calculation in console log of DevTools (F12)</p>
        </div>
      );
    } else if (this.state.progress.progress >= 100) {
      main = (
        <div>
          <DifferentialCrossSectionChart data={this.state.results.dcs} />
          <TotalCrossSectionChart data={this.state.results.total} />
        </div>
      );
    }
    return (
      <div>
        <div className="app">
          <div className="sidebar">
            <Form
              schema={schema}
              formData={this.state.request}
              onSubmit={(f) => this.calculate(f)}
              onChange={(f) => this.onChange(f)}
              ref={this.formRef}
            >
              <ButtonGroup>
                <Button bsStyle="primary" type="submit">
                  <Glyphicon glyph="cog" /> Calculate
                </Button>
                <FormSerializer
                  form={this.formRef}
                  filename="calc_dcs"
                  onLoad={this.updateRequest}
                />
              </ButtonGroup>
            </Form>
          </div>
          <div className="main">{main}</div>
        </div>
      </div>
    );
  }

  calculate({ formData }) {
    this.setState({ request: formData });
    this.worker = new CalculatorWorker();

    this.worker.onmessage = (event) => {
      switch (event.data.type) {
        case "RESULTS":
          this.setState({
            progress: {
              progress: 100,
            },
            results: event.data.results,
          });
          this.worker.terminate();
          break;
        case "PROGRESS":
          this.setState({
            progress: event.data,
          });
          break;
        case "ERROR":
          this.setState({
            progress: {
              progress: 100,
              error: event.data.message,
            },
          });
          break;
        default:
          console.error(event);
      }
    };

    this.worker.onerror = (error) => {
      this.setState({
        progress: {
          progress: 100,
          error,
        },
      });
      this.worker.terminate();
    };

    this.worker.postMessage({
      type: "CALC_DCS",
      formData,
    });
    window.scrollTo({ top: 0, behavior: "smooth" });
  }

  updateRequest(request) {
    this.setState({ request });
  }

  onChange(event) {
    this.setState({ request: event.formData });
  }

  cancel() {
    if (this.worker) {
      this.worker.terminate();
    }
    this.setState({
      progress: {
        progress: 0,
        detail: "Not started",
      },
    });
  }
}
