import "./App.css";

import React from "react";
import { BrowserRouter as Router, NavLink, Route } from "react-router-dom";

import { CalcCrossSection } from "./CalcCrossSection";
// import { CalcDifferentialCrossSection } from "./CalcDifferentialCrossSection";
// import { CalcResonantChargeExchange } from "./CalcResonantChargeExchange";
import { CalcScatteringAngle } from "./CalcScatteringAngle";

function Home() {
  return (
    <div>
      <h2>Home</h2>
    </div>
  );
}

export default function App() {
  return (
    <Router>
      <div>
        <h1>Magnum PI</h1>
        <ul>
          <li>
            <NavLink exact to="/">
              Home
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/calc_scat">
              Calculate scattering angle
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/calc_cs">
              Calculate cross section and collision integral
            </NavLink>
          </li>
          {/* TODO re-enable dcs once calculation implemented */}
          {/* <li>
            <NavLink exact to="/calc_dcs">
              Calculate differential cross section
            </NavLink>
          </li>
          <li>
            <NavLink exact to="/calc_dcs_rce">
              Calculate resonant charge exchange
            </NavLink>
          </li> */}
        </ul>
        <Route exact path="/" component={Home} />
        <Route path="/calc_scat" component={CalcScatteringAngle} />
        <Route path="/calc_cs" component={CalcCrossSection} />
        {/* <Route path="/calc_dcs" component={CalcDifferentialCrossSection} />
        <Route path="/calc_dcs_rce" component={CalcResonantChargeExchange} /> */}
      </div>
      <footer>
        This software is licensed under{" "}
        <a href="http://www.gnu.org/licenses/">
          GNU General Public License version 3
        </a>{" "}
        and any technical questions can be posted to the{" "}
        <a href="https://gitlab.com/magnumpi/magnumpi/-/issues">
          issue tracker
        </a>
        .
      </footer>
    </Router>
  );
}
