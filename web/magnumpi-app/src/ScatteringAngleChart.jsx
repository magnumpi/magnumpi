import PropTypes from "prop-types";
import React from "react";
import { VegaLite } from "react-vega";

import { DownloadButton } from "./DownloadButton";
import { sa2tsv } from "./writers";

const spec = {
  title: "Scattering angle",
  width: 900,
  height: 700,
  mark: "point",
  padding: { left: 30, top: 5, right: 50, bottom: 20 },
  encoding: {
    x: {
      field: "b",
      type: "quantitative",
      axis: { title: "b (m)", format: ".2e" },
    },
    y: { field: "chi(b,E)", type: "quantitative", axis: { title: "chi(b,E)" } },
    color: { field: "E", type: "nominal", legend: { title: "E (eV)" } },
  },
  selection: {
    grid: {
      type: "interval",
      bind: "scales",
    },
  },
  usermeta: {
    embedOptions: {
      actions: false,
    },
  },
  data: { name: "values" },
};

export const ScatteringAngleChart = ({ data }) => {
  const tsv = sa2tsv(data.values);
  return (
    <div>
      <VegaLite spec={spec} data={data} renderer="canvas" />
      <DownloadButton
        filename="sa.txt"
        mime_type="text/tab-separated-values"
        blob={tsv}
      />
    </div>
  );
};
ScatteringAngleChart.propTypes = {
  data: PropTypes.any.isRequired,
};
