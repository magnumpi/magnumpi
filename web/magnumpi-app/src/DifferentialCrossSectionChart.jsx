import PropTypes from "prop-types";
import React, { Component } from "react";
import { VegaLite } from "react-vega";

import { DownloadButton } from "./DownloadButton";
import { dcs2tsv } from "./writers";

const spec = {
  title: "Differential cross section",
  width: 900,
  height: 700,
  mark: "rect",
  padding: { left: 80, top: 5, right: 5, bottom: 20 },
  encoding: {
    x: { field: "chi", type: "nominal" },
    y: { field: "energy", type: "nominal", scale: { type: "log" } },
    color: {
      field: "dcs",
      type: "quantitative",
      scale: { scheme: "greenblue" },
    },
  },
  selection: {
    grid: {
      type: "interval",
      bind: "scales",
    },
  },
  usermeta: {
    embedOptions: {
      actions: false,
    },
  },
  data: { name: "values" },
};

export class DifferentialCrossSectionChart extends Component {
  render() {
    const tsv = dcs2tsv(this.props.data.values);
    return (
      <div>
        <VegaLite spec={spec} data={this.props.data} renderer="canvas" />
        <DownloadButton
          filename="dcs.txt"
          mime_type="text/tab-separated-values"
          blob={tsv}
        />
      </div>
    );
  }
}
DifferentialCrossSectionChart.propTypes = {
  data: PropTypes.any.isRequired,
};
