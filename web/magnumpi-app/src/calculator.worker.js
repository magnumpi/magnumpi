/* eslint-env worker */
import magnumpicalc from "@magnumpi/wasm";

import { ds2json } from "./ds2json";
import { potentialBuilder } from "./potentialBuilder";
import { readDCS, readDCSRCE, readTotalCS } from "./readers";

function in_si(quantity, dimension, magnumpi) {
  return magnumpi.in_si(
    JSON.stringify(quantity),
    magnumpi.dimensions[dimension]
  );
}

async function calc_cs(formData, magnumpi) {
  postMessage({
    type: "PROGRESS",
    progress: 10,
    detail: "Preparing input",
  });

  const pot = await potentialBuilder(
    magnumpi,
    formData.potential.potentialType,
    formData.potential.potentialSettings
  );

  const lower_cc = formData.clenshawCurtisQuadrature.lower;
  const upper_cc = formData.clenshawCurtisQuadrature.upper;

  const reduced_mass = in_si(formData.reducedMass, "mass", magnumpi);
  const er = formData.energyRange;
  const logE_low = Math.log10(in_si(er.min, "energy", magnumpi));
  const logE_high = Math.log10(in_si(er.max, "energy", magnumpi));
  const E_points_decade = er.pointsPerDecade;

  // max l for which the cross section is calculated
  const l_max = formData.l_max;

  let rmin = in_si(formData.r.min, "length_", magnumpi);
  let rmax = in_si(formData.r.max, "length_", magnumpi);
  const dr = in_si(formData.r.del, "length_", magnumpi);

  // adjust rmin,rmax if necessary
  const Vmin = in_si(formData.r.Vmin, "energy", magnumpi);
  const Vmax = in_si(formData.r.Vmax, "energy", magnumpi);
  rmin = magnumpi.rmin_input_evaluation(rmin, pot, Vmin);
  rmax = magnumpi.rmax_input_evaluation(rmax, pot, Vmax);

  // determine whether scat_Colonna or scat_Viehland is used for estimating scattering angles
  const use_scat_Colonna = 1;
  // set tolerance and interpolation order
  const interp = 3;
  const tolerance = formData.tolerance;

  postMessage({
    type: "PROGRESS",
    progress: 20,
    detail: "Calculating cross section",
  });

  const csc = new magnumpi.CrossSectionCalculator(
    lower_cc,
    upper_cc,
    logE_low,
    l_max,
    rmin,
    rmax,
    dr,
    use_scat_Colonna,
    interp,
    tolerance,
    pot
  );
  const cs_data = csc.integrate(logE_low, logE_high, E_points_decade);
  pot.delete();

  postMessage({
    type: "PROGRESS",
    progress: 80,
    detail: "Calculating omega",
  });

  const ci = formData.ci;
  const s_max = ci.s_max;
  const ci_lower_cc = ci.clenshawCurtisQuadrature.lower;
  const ci_upper_cc = ci.clenshawCurtisQuadrature.upper;
  const tr = ci.temperatureRange;
  const Tmin = in_si(tr.min, "temperature", magnumpi);
  const Tmax = in_si(tr.max, "temperature", magnumpi);
  const Tdel = in_si(tr.step, "temperature", magnumpi);

  const ls_pairs = magnumpi.construct_ls_pairs(l_max, s_max);
  const oc = new magnumpi.OmegaCalculator(
    ls_pairs,
    ci_lower_cc,
    ci_upper_cc,
    tolerance
  );
  const q = new magnumpi.CrossSectionFromDatasetLog(cs_data);
  const omega_data = oc.integrate(q, reduced_mass, Tmin, Tmax, Tdel);
  oc.delete();

  postMessage({
    type: "PROGRESS",
    progress: 90,
    detail: "Gathering output",
  });

  //  console.log("Converting cs and omega results to json");
  const results = {
    cs: { values: ds2json(cs_data) },
    omega: { values: ds2json(omega_data) },
  };
  cs_data.delete();
  omega_data.delete();

  postMessage({
    type: "RESULTS",
    results,
  });
}

async function calc_sa(formData, magnumpi) {
  postMessage({
    type: "PROGRESS",
    progress: 10,
    detail: "Preparing input",
  });

  const pot = await potentialBuilder(
    magnumpi,
    formData.interaction.potential.potentialType,
    formData.interaction.potential.potentialSettings
  );

  const b_low = in_si(formData.bRange.min, "length_", magnumpi);
  const b_high = in_si(formData.bRange.max, "length_", magnumpi);
  const num_b = formData.bRange.points;

  const logE_low = Math.log10(
    in_si(formData.energyRange.min, "energy", magnumpi)
  );
  const logE_high = Math.log10(
    in_si(formData.energyRange.max, "energy", magnumpi)
  );
  const E_pdecade = formData.energyRange.pointsPerDecade;

  // set tolerance and interpolation order
  const interpol = 3;
  const tolerance = formData.tolerance;

  postMessage({
    type: "PROGRESS",
    progress: 20,
    detail: "Calculating scattering angle",
  });

  const data = magnumpi.get_scat_vs_b_vs_E(
    b_low,
    b_high,
    num_b,
    logE_low,
    logE_high,
    E_pdecade,
    interpol,
    tolerance,
    pot
  );

  postMessage({
    type: "PROGRESS",
    progress: 90,
    detail: "Gathering output",
  });

  pot.delete();
  const results = {
    scat: { values: ds2json(data) },
  };
  data.delete();

  postMessage({
    type: "RESULTS",
    results,
  });
}

async function calc_dcs(formData, magnumpi) {
  postMessage({
    type: "PROGRESS",
    progress: 10,
    detail: "Preparing input",
  });
  const pot = await potentialBuilder(
    magnumpi,
    formData.potential.potentialType,
    formData.potential.potentialSettings
  );

  const lower_cc = formData.clenshawCurtisQuadrature.lower;
  const upper_cc = formData.clenshawCurtisQuadrature.upper;

  const reduced_mass = in_si(formData.reducedMass, "mass", magnumpi);
  const er = formData.energyRange;
  const logE_low = Math.log10(in_si(er.min, "energy", magnumpi));
  const logE_high = Math.log10(in_si(er.max, "energy", magnumpi));
  const E_points_decade = er.pointsPerDecade;

  // max l for which the cross section is calculated
  const l_max = formData.l_max;

  let rmin = in_si(formData.r.min, "length_", magnumpi);
  let rmax = in_si(formData.r.max, "length_", magnumpi);
  const dr = in_si(formData.r.del, "length_", magnumpi);

  // adjust rmin,rmax if necessary
  const Vmin = in_si(formData.r.Vmin, "energy", magnumpi);
  const Vmax = in_si(formData.r.Vmax, "energy", magnumpi);
  rmin = magnumpi.rmin_input_evaluation(rmin, pot, Vmin);
  rmax = magnumpi.rmax_input_evaluation(rmax, pot, Vmax);

  // determine whether scat_Colonna or scat_Viehland is used for estimating scattering angles
  const use_scat_Colonna = 1;
  // set tolerance and interpolation order
  const interp = 3;
  const tolerance = formData.tolerance;

  postMessage({
    type: "PROGRESS",
    progress: 20,
    detail: "Calculating differential cross section",
  });

  const dcs = new magnumpi.DcsCalculator(
    lower_cc,
    upper_cc,
    Math.pow(10, logE_low),
    l_max,
    rmin,
    rmax,
    dr,
    use_scat_Colonna,
    interp,
    tolerance,
    reduced_mass,
    pot
  );
  //set angular intervals
  const delta_chi = 1.0;
  dcs.DCS(delta_chi, logE_low, logE_high, E_points_decade);
  dcs.delete();

  postMessage({
    type: "PROGRESS",
    progress: 90,
    detail: "Gathering output",
  });

  const results = {
    dcs: {
      values: readDCS(magnumpi.FS),
    },
    total: {
      values: readTotalCS(magnumpi.FS),
    },
  };

  postMessage({
    type: "RESULTS",
    results,
  });
}

async function calc_rce(formData, magnumpi) {
  postMessage({
    type: "PROGRESS",
    progress: 10,
    detail: "Preparing input",
  });

  const gPot = await potentialBuilder(
    magnumpi,
    formData.geradePotential.potentialType,
    formData.geradePotential.potentialSettings
  );
  const uPot = await potentialBuilder(
    magnumpi,
    formData.ungeradePotential.potentialType,
    formData.ungeradePotential.potentialSettings
  );

  const lower_cc = formData.clenshawCurtisQuadrature.lower;
  const upper_cc = formData.clenshawCurtisQuadrature.upper;

  const reduced_mass = in_si(formData.reducedMass, "mass", magnumpi);
  const er = formData.energyRange;
  const logE_low = Math.log10(in_si(er.min, "energy", magnumpi));
  const logE_high = Math.log10(in_si(er.max, "energy", magnumpi));
  const E_points_decade = er.pointsPerDecade;

  // max l for which the cross section is calculated
  const l_max = formData.l_max;

  let rmin = in_si(formData.r.min, "length_", magnumpi);
  let rmax = in_si(formData.r.max, "length_", magnumpi);
  const dr = in_si(formData.r.del, "length_", magnumpi);

  // adjust rmin,rmax if necessary
  const Vmin = in_si(formData.r.Vmin, "energy", magnumpi);
  const Vmax = in_si(formData.r.Vmax, "energy", magnumpi);
  rmin = magnumpi.rmin_input_evaluation(rmin, gPot, Vmin);
  rmax = magnumpi.rmax_input_evaluation(rmax, gPot, Vmax);
  rmin = magnumpi.rmin_input_evaluation(rmin, uPot, Vmin);
  rmax = magnumpi.rmax_input_evaluation(rmax, uPot, Vmax);

  // determine whether scat_Colonna or scat_Viehland is used for estimating scattering angles
  const use_scat_Colonna = 1;
  // set tolerance and interpolation order
  const interp = 3;
  const tolerance = formData.tolerance;

  postMessage({
    type: "PROGRESS",
    progress: 20,
    detail: "Calculating differential cross section",
  });

  const calculator = new magnumpi.DcsRceCalculator(
    lower_cc,
    upper_cc,
    Math.pow(10, logE_low),
    l_max,
    rmin,
    rmax,
    dr,
    use_scat_Colonna,
    interp,
    tolerance,
    reduced_mass,
    gPot,
    uPot
  );
  //set angular intervals
  const delta_chi = 1.0;
  calculator.DCS_RCE(delta_chi, logE_low, logE_high, E_points_decade);

  calculator.delete();
  gPot.delete();
  uPot.delete();

  postMessage({
    type: "PROGRESS",
    progress: 90,
    detail: "Gathering output",
  });

  const results = {
    dcs: {
      values: readDCSRCE(magnumpi.FS),
    },
    total: {
      values: readTotalCS(magnumpi.FS),
    },
  };

  postMessage({
    type: "RESULTS",
    results,
  });
}

onmessage = async function (e) {
  postMessage({
    type: "PROGRESS",
    progress: 5,
    detail: "Initializing worker",
  });
  const magnumpi = await magnumpicalc();
  try {
    switch (e.data.type) {
      case "CALC_CS":
        await calc_cs(e.data.formData, magnumpi);
        break;
      case "CALC_SA":
        await calc_sa(e.data.formData, magnumpi);
        break;
      case "CALC_DCS":
        await calc_dcs(e.data.formData, magnumpi);
        break;
      case "CALC_RCE":
        await calc_rce(e.data.formData, magnumpi);
        break;
      default:
        postMessage({
          type: "ERROR",
          message: "Unknown calculation type",
        });
    }
  } catch (error) {
    const message = magnumpi.error_message(error);
    postMessage({
      type: "ERROR",
      message,
    });
  }
};
