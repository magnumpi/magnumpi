export function ds2json(ds) {
  const row_axis = ds.row_axis();
  const col_axis = ds.col_axis();
  const nr = row_axis.size();
  const nc = col_axis.size();
  const rows = [];
  const dname = ds.name();
  const cname = col_axis.name();
  const rname = row_axis.name();
  for (var r = 0; r < nr; r++) {
    const rval = row_axis.value(r);
    for (var c = 0; c < nc; c++) {
      const cval = col_axis.label_str(c);
      const cell = {};
      cell[rname] = rval;
      cell[cname] = cval;
      cell[dname] = ds.cell(r, c);
      rows.push(cell);
    }
  }
  row_axis.delete();
  col_axis.delete();
  return rows;
}
