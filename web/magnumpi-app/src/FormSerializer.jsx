import { dump, load } from "js-yaml";
import PropTypes from "prop-types";
import React, { Component } from "react";
import Button from "react-bootstrap/lib/Button";
import ButtonGroup from "react-bootstrap/lib/ButtonGroup";
import Glyphicon from "react-bootstrap/lib/Glyphicon";

export class FormSerializer extends Component {
  constructor(props) {
    super(props);
    this.loadFormFileRef = React.createRef();
  }

  render() {
    return (
      <ButtonGroup>
        <Button
          onClick={() => this.saveForm("json")}
          title="Save form to JSON file"
        >
          <Glyphicon glyph="save" /> JSON
        </Button>
        <Button
          onClick={() => this.saveForm("yaml")}
          title="Save form to YAML file"
        >
          <Glyphicon glyph="save" /> YAML
        </Button>
        <Button onClick={this.loadForm} title="Load form from file">
          <Glyphicon glyph="open" /> Load
        </Button>
        <input
          type="file"
          accept="application/json,.json,application/x-yaml,.yml,.yaml"
          onChange={this.loadFormFileSelected}
          ref={this.loadFormFileRef}
          style={{ display: "none" }}
        />
      </ButtonGroup>
    );
  }

  saveForm(eventKey) {
    const formData = this.props.form.current.state.formData;
    let data;
    let extension;
    let mimetype;
    switch (eventKey) {
      case "json":
        data = JSON.stringify(formData, null, 4);
        extension = ".json";
        mimetype = "application/json";
        break;
      case "yaml":
        data = dump(formData);
        extension = ".yaml";
        mimetype = "application/x-yaml";
        break;
      default:
        throw Error("Unknown format");
    }
    const formDataUrl = "data:" + mimetype + ";base64," + btoa(data);
    const a = document.createElement("a");
    a.href = formDataUrl;
    a.download = this.props.filename + extension;
    a.click();
    a.remove();
  }

  loadForm() {
    this.loadFormFileRef.current.click();
  }

  loadFormFileSelected(event) {
    const file = event.target.files[0];
    const mimetype = file.type;
    const reader = new FileReader();
    reader.onload = (evt) => {
      let request;
      switch (mimetype) {
        case "application/json":
          request = JSON.parse(evt.target.result);
          break;
        case "application/x-yaml":
          request = load(evt.target.result);
          break;
        default:
          throw Error("Unknown format: " + mimetype);
      }
      this.props.onLoad(request);
    };
    reader.readAsText(file);
  }
}
FormSerializer.propTypes = {
  filename: PropTypes.string.isRequired,
  form: PropTypes.any.isRequired,
  onLoad: PropTypes.func.isRequired,
};
