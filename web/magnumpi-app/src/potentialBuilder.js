import parseDataURL from "data-urls";

// function transformLXCatJSONOnline(potentialSettings) {
//   const url = new URL(potentialSettings.endpoint);
//   delete potentialSettings.endpoint;
//   potentialSettings.query = {
//     host: url.hostname,
//     port: url.port,
//     target: "/test/magnumpi/by_state",
//     body: potentialSettings.body,
//   };
//   delete potentialSettings.body;
//   return potentialSettings;
// }

async function fetchPotential(potentialSettings) {
  const url = potentialSettings.endpoint + "/test/magnumpi/by_state";
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(potentialSettings.body),
  });
  if (!response.ok) {
    throw new Error("Web service request failed: " + response.statusText);
  }
  const json = await response.json();
  if (json.length !== 1) {
    throw new Error(
      "Query did not result in a unique potential section, I received " +
        json.length
    );
  }
  const fetched_potential = json[0];
  if (fetched_potential.type === "LUT") {
    return {
      is_neutral: potentialSettings.is_neutral,
      lookup_table_xy: fetched_potential,
      type: fetched_potential.type,
    };
  } else {
    return {
      type: fetched_potential.type,
      ...fetched_potential.data,
    };
  }
}

export async function potentialBuilder(
  magnumpi,
  potentialType,
  potentialSettings
) {
  // LUT (lookup table) requires a file, while form gives data url
  if (potentialType === "LUT") {
    const fn = "lut.txt";
    const blob = parseDataURL(potentialSettings.rvs);
    const text = blob.body.toString();
    magnumpi.FS.writeFile(fn, text);
    const pot = magnumpi.json2potential(
      JSON.stringify({
        type: potentialType,
        ...potentialSettings,
        file: fn,
      })
    );
    magnumpi.FS.unlink(fn);
    return pot;
  }
  if (potentialType === "LXCatJSONExtern") {
    const fn = "lxcat.json";
    const blob = parseDataURL(potentialSettings.file);
    const text = blob.body.toString();
    magnumpi.FS.writeFile(fn, text);
    const pot = magnumpi.json2potential(
      JSON.stringify({
        type: "LXCatJSON",
        file: fn,
      })
    );
    magnumpi.FS.unlink(fn);
    return pot;
  }
  if (potentialType === "LXCatJSONInline") {
    potentialType = "LXCatJSON";
  }
  if (potentialType === "LXCatJSONOnline") {
    // requesting potential in wasm does not work
    // Gives an `[json.exception.type_error.302] type must be number, but is null` error after
    // `magnumpi::potential: found a query section. Will fetch the potential information from elsewhere. Query: ...` print
    // and never talks to ws.
    // transformLXCatJSONOnline(potentialSettings);
    const fetched_potential = await fetchPotential(potentialSettings);
    console.log(fetched_potential);
    return magnumpi.json2potential(JSON.stringify(fetched_potential));
  }
  return magnumpi.json2potential(
    JSON.stringify({
      type: potentialType,
      ...potentialSettings,
    })
  );
}
