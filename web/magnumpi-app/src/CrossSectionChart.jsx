import PropTypes from "prop-types";
import React, { Component } from "react";
import { VegaLite } from "react-vega";

import { DownloadButton } from "./DownloadButton";
import { cs2tsv } from "./writers";

const spec = {
  title: "Cross section",
  width: 900,
  height: 700,
  mark: "point",
  padding: { left: 80, top: 5, right: 50, bottom: 20 },
  encoding: {
    x: { field: "epsilon", type: "quantitative", scale: { type: "log" } },
    y: { field: "sigma_l(eps)", type: "quantitative", axis: { format: ".4e" } },
    color: { field: "l", type: "nominal" },
  },
  selection: {
    grid: {
      type: "interval",
      bind: "scales",
    },
  },
  usermeta: {
    embedOptions: {
      actions: false,
    },
  },
  data: { name: "values" },
};

export class CrossSectionChart extends Component {
  render() {
    const tsv = cs2tsv(this.props.data.values);
    return (
      <div>
        <VegaLite spec={spec} data={this.props.data} renderer="canvas" />
        <DownloadButton
          filename="cs.txt"
          mime_type="text/tab-separated-values"
          blob={tsv}
        />
      </div>
    );
  }
}
CrossSectionChart.propTypes = {
  data: PropTypes.any.isRequired,
};
