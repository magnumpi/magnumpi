import PropTypes from "prop-types";
import React from "react";
import { VegaLite } from "react-vega";

import { DownloadButton } from "./DownloadButton";
import { omega2tsv } from "./writers";

const spec = {
  title: "Collision integral",
  width: 900,
  height: 700,
  mark: "point",
  padding: { left: 80, top: 5, right: 50, bottom: 20 },
  encoding: {
    x: { field: "T", type: "quantitative", scale: { type: "log" } },
    y: { field: "Omega_ls(T)", type: "quantitative", axis: { format: ".4e" } },
    color: { field: "(l,s)", type: "nominal" },
  },
  selection: {
    grid: {
      type: "interval",
      bind: "scales",
    },
  },
  usermeta: {
    embedOptions: {
      actions: false,
    },
  },
  data: { name: "values" },
};

export const OmegaChart = ({ data }) => {
  const tsv = omega2tsv(data.values);
  return (
    <div>
      <VegaLite spec={spec} data={data} renderer="canvas" />
      <DownloadButton
        filename="tcs.txt"
        mime_type="text/tab-separated-values"
        blob={tsv}
      />
    </div>
  );
};
OmegaChart.propTypes = {
  data: PropTypes.any.isRequired,
};
