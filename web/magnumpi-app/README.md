# MagnumPI web application

![screenshot](magnumpi-webapp-screencast.gif "Screenshot")

## Available Scripts

To use the LXCatJSONOnline potential type the LXCat web service must be running on a known endpoint, default is `http://localhost:3000`.

### Dependencies

Installs dependencies of application and build wasm.

```shell
cd ..
# From web/
# Install all dependepencies
npm install
# Build web assembly
npm run build --workspace=assembly
cd -
```

By using [npm packages](https://docs.npmjs.com/cli/v7/using-npm/workspaces) the assembly/ dir is included in the node_modules of magnumpi-app.

### Development mode

```shell
npm run dev
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### Tests

To run unit tests do

```shell
npm run test:unit
```

### Linting & formatting

The code should be formatted using [prettier](https://prettier.io/) and have no [eslint](https://eslint.org/) errors.

Automatic formatting can be done with

```shell
npm run format
```

Linting can be done with

```shell
npm run lint
```

See prettier and eslint websites for how to configure editor to perform automatic linting and formatting.

### Build

The build command requires bash and perl to fix up the generated files.

To make a production build use

```shell
npm run build
```

Builds the app for production to the `dist/` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

For local deployment use

```bash
npm run serve
```

Open [http://localhost:5000](http://localhost:5000) to view it in the browser.

## Docker

The web application can be build & run using Docker

To build from repo root execute following

```bash
cd ../..
docker build -t passingxsams/magnumpi:webapp -f web/magnumpi-app/Dockerfile .
```

To run do

```bash
docker run -p 5000:5000 passingxsams/magnumpi:webapp
```

Will run web application on [http://localhost:5000](http://localhost:5000)

## Credits

This project was bootstrapped with [Vite](https://vitejs.dev/).
