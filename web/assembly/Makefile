EMCC=emcc

# Ran from web/assembly/, but need files from parent dirs
ROOT=../..

CFLAGS= \
   -I$(ROOT) \
   -I$(ROOT)/external \
   -I$(ROOT)/external/plasimo \
   -Isrc \
   -std=c++14 \

EMFLAGS = \
  --bind \
  -s USE_BOOST_HEADERS=1 \
  -s 'EXPORTED_RUNTIME_METHODS=["FS"]' \
  -s EXPORT_NAME=magnumpicalc \
  -s DISABLE_EXCEPTION_CATCHING=0 \
  -s ASSERTIONS=1 \
  -s MODULARIZE=1

EMFLAGS_OPTIMIZED= \
    -O2 \
	# -flto \
	# --closure 1

SOURCES=\
	$(ROOT)/external/tk_spline/src/spline.cpp \
	$(ROOT)/external/plasimo/plmath/src/clencurt.cpp	\
	$(ROOT)/external/plasimo/plmath/src/clencurt_coefs.cpp	\
	$(ROOT)/src/beast_post.cpp \
	$(ROOT)/src/calc_cs.cpp \
	$(ROOT)/src/calc_omega.cpp \
	$(ROOT)/src/crosssec_dataset.cpp \
	$(ROOT)/src/data_set.cpp \
	$(ROOT)/src/interpolation.cpp \
	$(ROOT)/src/json.cpp \
	$(ROOT)/src/potential.cpp \
	$(ROOT)/src/scat_colonna.cpp \
	$(ROOT)/src/scat_viehland.cpp \
	$(ROOT)/src/units.cpp \
	$(ROOT)/src/utility.cpp \
	$(ROOT)/src/viehland_common.cpp

# the DCS components are disabled at present
DCS_SOURCES= \
	$(ROOT)/src/scat_colonna_dcs.cpp \
	$(ROOT)/src/calc_dcs.cpp

BINDING = src/wa.cpp

all: wasmjs

.PHONY: clean

wasmjs: dist/magnumpicalc.cjs dist/magnumpicalc.mjs

dist:
	mkdir dist

dist/magnumpicalc.mjs: $(BINDING) $(SOURCES) dist
	$(EMCC) $(CFLAGS) $(EMFLAGS) $(EMFLAGS_OPTIMIZED) $(SOURCES) $(BINDING) -s EXPORT_ES6=1 -s ENVIRONMENT=worker,web -o $@

dist/magnumpicalc.cjs: $(BINDING) $(SOURCES) dist
	$(EMCC) $(CFLAGS) $(EMFLAGS) $(EMFLAGS_OPTIMIZED) $(SOURCES) $(BINDING) -s ENVIRONMENT=node -o $@

clean:
	$(RM) -r dist
