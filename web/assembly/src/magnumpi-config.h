#ifndef _MAGNUMPI_CONFIG_H
#define _MAGNUMPI_CONFIG_H 1
 
/* magnumpi-config.h. Generated automatically at end of configure. */
/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Defined if the requested minimum BOOST version is satisfied */
#ifndef MAGNUMPI_HAVE_BOOST
#define MAGNUMPI_HAVE_BOOST 1
#endif

/* once: _MAGNUMPI_CONFIG_H */
#endif
