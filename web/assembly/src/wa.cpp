#include <iostream>
#include <chrono>
#include "magnumpi/json.h"
#include "magnumpi/consts.h"
#include "magnumpi/potential.h"
#include "magnumpi/calc_cs.h"
#include "magnumpi/calc_omega.h"
#include "magnumpi/scat_colonna.h"
#include "magnumpi/data_set.h"
#include "magnumpi/crosssec_dataset.h"
//#include "ideas/dcs.h"

#include <emscripten/bind.h>

// Normally thrown exceptions will be a long in js-space
// the long is a function pointer which in c++-space can be asked for the what message
std::string error_message(intptr_t pointer) {
	auto error = reinterpret_cast<std::runtime_error *>(pointer);
	return error->what();
}

// convert JSON string to json_type
const magnumpi::potential* json2potential(const std::string cnf) {
	return magnumpi::potential::create(magnumpi::json_type::parse(cnf)).release();
}

// convert JSON string to json_type
double in_si(const std::string quantity, magnumpi::units::dimension d) {
	return magnumpi::units::in_si(magnumpi::json_type::parse(quantity), d);
}

using namespace emscripten;

EMSCRIPTEN_BINDINGS(magnumpi)
{
	function("error_message", &error_message, allow_raw_pointers());
	class_<magnumpi::potential>("Potential");
	function("json2potential", &json2potential, allow_raw_pointers());
	function("rmin_input_evaluation", &magnumpi::rmin_input_evaluation, allow_raw_pointers());
	function("rmax_input_evaluation", &magnumpi::rmax_input_evaluation, allow_raw_pointers());
	class_<magnumpi::cross_section>("CrossSection");
	class_<magnumpi::cross_section_from_dataset_log, base<magnumpi::cross_section>>("CrossSectionFromDatasetLog")
		.constructor<magnumpi::data_set>();
	;
	class_<magnumpi::calc_cs>("CrossSectionCalculator")
		.constructor<int, int, double, int, double, double, double, int, int, double, const magnumpi::potential*>()
		.function("integrate", select_overload<magnumpi::data_set(double, double, unsigned)const>(&magnumpi::calc_cs::integrate))
	;
	class_<magnumpi::calc_omega::ls_pair>("ls_pair")
		.constructor<unsigned, unsigned>()
	;
	register_vector<magnumpi::calc_omega::ls_pair>("ls_pairs_type");
	function("construct_ls_pairs", &magnumpi::construct_ls_pairs);
	class_<magnumpi::calc_omega>("OmegaCalculator")
		.constructor<const magnumpi::calc_omega::ls_pairs_type, int, int, double>()
		.function("integrate", select_overload<magnumpi::data_set(const magnumpi::cross_section&,double,double, double, double) const>(&magnumpi::calc_omega::integrate))
	;
	function("get_scat_vs_b_vs_E", select_overload<magnumpi::data_set(double, double, int, double, double, int, int, double, const magnumpi::potential*)>(&magnumpi::get_scat_vs_b_vs_E), allow_raw_pointers());

	// TODO differential scattering: work in progress
	// class_<magnumpi::calc_dcs_base>("BaseDcsCalculator");
	// class_<magnumpi::calc_dcs, base<magnumpi::calc_dcs_base>>("DcsCalculator")
	// 	.constructor<int, int, double, int, double, double, double, int, int, double, double, const magnumpi::potential*>()
	// 	.function("DCS", &magnumpi::calc_dcs::DCS) // writes DCS.txt and cs_odd_even.txt to current working directory
	// ;
	// class_<magnumpi::calc_dcs_rce, base<magnumpi::calc_dcs_base>>("DcsRceCalculator")
	// 	.constructor<int, int, double, int, double, double, double, int, int, double, double, const magnumpi::potential*, const magnumpi::potential*>()
	// 	.function("DCS_RCE", &magnumpi::calc_dcs_rce::DCS_RCE) // writes DCS.txt and cs_odd_even.txt to current working directory
	// ;
	enum_<magnumpi::units::dimension>("dimensions")
		.value("scalar", magnumpi::units::dimension::scalar)
		.value("mass", magnumpi::units::dimension::mass)
		.value("length_", magnumpi::units::dimension::length) // length is reserved word in js, appended `_`
		.value("temperature", magnumpi::units::dimension::temperature)
		.value("energy", magnumpi::units::dimension::energy)
		.value("angle", magnumpi::units::dimension::angle)
		.value("area", magnumpi::units::dimension::area)
		.value("m3_s", magnumpi::units::dimension::m3_s)
	;
	function("in_si", &in_si);
	constant("eV", magnumpi::constants::eV);
	class_<magnumpi::data_info>("DataInfo")
		.function("name", &magnumpi::data_set::axis_type::name)
		.function("unit", &magnumpi::data_set::axis_type::unit)
	;
	class_<magnumpi::data_set::axis_type, base<magnumpi::data_info>>("AxisType")
		.function("size", &magnumpi::data_set::axis_type::size)
		.function("value", &magnumpi::data_set::axis_type::value)
		.function("label_str", &magnumpi::data_set::axis_type::label_str)
	;
	class_<magnumpi::data_set, base<magnumpi::data_info>>("Dataset")
		.function("cell", &magnumpi::data_set::cell)
		.function("col_axis", select_overload<magnumpi::data_set::axis_type&()>(&magnumpi::data_set::col_axis))
		.function("row_axis",select_overload<magnumpi::data_set::axis_type&()>(&magnumpi::data_set::row_axis))
	;
}
