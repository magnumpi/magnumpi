/**
* Potential Integrator
* No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
*
* OpenAPI spec version: 0.1.0
* 
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/


#include "MS124.h"

namespace magnumpi {
namespace ws {
namespace model {

MS_12_4::MS_12_4()
{
    m_Type = "";
    
}

MS_12_4::~MS_12_4()
{
}

void MS_12_4::validate()
{
    // TODO: implement validation
}

nlohmann::json MS_12_4::toJson() const
{
    nlohmann::json val = nlohmann::json::object();

    val["type"] = ModelBase::toJson(m_Type);
    val["epsilon"] = ModelBase::toJson(m_Epsilon);
    val["rm"] = ModelBase::toJson(m_Rm);
    

    return val;
}

void MS_12_4::fromJson(const nlohmann::json& val)
{
    setType(val.at("type"));
    
}


std::string MS_12_4::getType() const
{
    return m_Type;
}
void MS_12_4::setType(std::string const& value)
{
    m_Type = value;
    
}
Energy MS_12_4::getEpsilon() const
{
    return m_Epsilon;
}
void MS_12_4::setEpsilon(Energy const& value)
{
    m_Epsilon = value;
    
}
Length MS_12_4::getRm() const
{
    return m_Rm;
}
void MS_12_4::setRm(Length const& value)
{
    m_Rm = value;
    
}

}
}
}

