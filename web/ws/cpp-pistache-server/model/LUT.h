/**
* Potential Integrator
* No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
*
* OpenAPI spec version: 0.1.0
* 
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/
/*
 * LUT.h
 *
 * Lookup table
 */

#ifndef LUT_H_
#define LUT_H_


#include "Potential.h"

#include <string>

namespace magnumpi {
namespace ws {
namespace model {

/// <summary>
/// Lookup table
/// </summary>
class  LUT
    : public Potential
{
public:
    LUT();
    virtual ~LUT();

    /////////////////////////////////////////////
    /// ModelBase overrides

    void validate() override;

    nlohmann::json toJson() const override;
    void fromJson(const nlohmann::json& json) override;

    /////////////////////////////////////////////
    /// LUT members

    /// <summary>
    /// 
    /// </summary>
    std::string getType() const;
    void setType(std::string const& value);
        /// <summary>
    /// Tab delimited file radius (m) and potential (eV) columns
    /// </summary>
    std::string getFile() const;
    void setFile(std::string const& value);
        /// <summary>
    /// 
    /// </summary>
    bool isIsNeutral() const;
    void setIsNeutral(bool const value);
    
protected:
    std::string m_Type;

    std::string m_file;

    bool m_Is_neutral;

};

}
}
}

#endif /* LUT_H_ */
