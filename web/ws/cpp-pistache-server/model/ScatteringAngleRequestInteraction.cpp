/**
* Potential Integrator
* No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
*
* OpenAPI spec version: 0.1.0
* 
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/


#include "ScatteringAngleRequestInteraction.h"
// #include "HardSphere.h"

namespace magnumpi {
namespace ws {
namespace model {

ScatteringAngleRequest_interaction::ScatteringAngleRequest_interaction()
{
    
}

ScatteringAngleRequest_interaction::~ScatteringAngleRequest_interaction()
{
}

void ScatteringAngleRequest_interaction::validate()
{
    // TODO: implement validation
}

nlohmann::json ScatteringAngleRequest_interaction::toJson() const
{
    nlohmann::json val = nlohmann::json::object();

    val["potential"] = ModelBase::toJson(m_Potential);
    

    return val;
}

void ScatteringAngleRequest_interaction::fromJson(const nlohmann::json& val)
{
    // const std::string type(val.at("potential").at("val").get<std::string>());
    // if (type=="HardSphere") {
    //     HardSphere pot;
    //     pot.fromJson(val.at("potential"));
    //     setPotential(pot);
    // } else
    //     throw std::runtime_error("Unknown potential model '" + type + "'.");
    // }
}

Potential ScatteringAngleRequest_interaction::getPotential() const
{
    return m_Potential;
}
void ScatteringAngleRequest_interaction::setPotential(Potential const& value)
{
    m_Potential = value;
    
}

}
}
}

