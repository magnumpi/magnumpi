#include "ModelBase.h"

namespace magnumpi
{
namespace ws
{
namespace model
{

class Potential
    : public ModelBase
{
  public:
    Potential();
    virtual ~Potential();

    /////////////////////////////////////////////
    /// ModelBase overrides

    void validate() override;

    nlohmann::json toJson() const override;
    void fromJson(const nlohmann::json &json) override;
  protected:
    std::string m_Type;    
};

} // namespace model
} // namespace ws
} // namespace magnumpi