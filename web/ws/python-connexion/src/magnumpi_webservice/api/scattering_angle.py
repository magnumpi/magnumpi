from magnumpi import get_scattering_angles


def post(body):
    """Calculate scattering angle"""
    return get_scattering_angles(body)
