import logging

import connexion
from connexion import RestyResolver

logging.basicConfig(level=logging.INFO)
app = connexion.FlaskApp(__name__)
resolver = RestyResolver('magnumpi_webservice.api')
app.add_api('openapi.yml', resolver=resolver, validate_responses=True)

if __name__ == '__main__':
    app.run(port=8888, debug=True)
