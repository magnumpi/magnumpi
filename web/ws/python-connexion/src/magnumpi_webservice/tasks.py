from os import getenv

from celery import Celery

from magnumpi.calc_cs import calc_cs
from magnumpi.calc_omega import calc_omega
from magnumpi.dsutils import ds2json

app = Celery(
    'tasks',
    broker=getenv('CELERY_BROKER_URL', 'redis://localhost:6379'),
    backend=getenv('CELERY_RESULT_BACKEND', 'redis://localhost:6379'),
)

PROGRESS = 'PROGRESS'


@app.task(bind=True)
def calc_cross_section(self, request):
    if not self.request.called_directly:
        self.update_state(state=PROGRESS,
                          meta={
                              'progress': 20,
                              'detail': 'Calculating cross section'
                          })

    cs_data = calc_cs(request)

    if not self.request.called_directly:
        self.update_state(state=PROGRESS,
                          meta={
                              'progress': 80,
                              'detail': 'Calculating omega'
                          })

    omega_data = calc_omega(request, cs_data)

    if not self.request.called_directly:
        self.update_state(state=PROGRESS,
                          meta={
                              'progress': 90,
                              'detail': 'Gathering output'
                          })

    return {'cs': cs_data, 'omega': ds2json(omega_data)}


@app.task(bind=True)
def calc_differential_cross_section(self, request):
    # TODO implement
    return {'dcs': {}, 'total': {}}


def get_result(jobid):
    job = app.AsyncResult(jobid)
    result = {'status': {'state': job.state}}
    job.maybe_throw()
    if job.successful():
        result['result'] = job.get()
    elif job.state == PROGRESS:
        result['status']['progress'] = job.info['progress']
        result['status']['detail'] = job.info['detail']
    return result


def delete_calculation(jobid):
    job = app.AsyncResult(jobid)
    job.revoke(terminate=True)
    job.forget()
