openapi: 3.0.0
info:
  title: Potential Integrator
  license:
    name: GPLv3
    url: https://www.gnu.org/licenses/gpl-3.0.txt
  version: 0.1.0
paths:
  /scattering_angle:
    post:
      summary: Calculate scattering angle
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ScatteringAngleRequest'
      responses:
       '200':
         description: Scattering angle
         content:
           application/json:
             schema:
               $ref: '#/components/schemas/ScatteringAngleResponse'
  /cross_section:
    post:
      summary: Submit cross section calculation
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CrossSectionRequest'
      responses:
        '201':
          description: Calculation submitted, follow location for status/result
          headers:
            Location:
              schema:
                type: string
                format: uri
  /cross_section/{jobid}:
    get:
      summary: Cross section calculation status and when completed a result
      parameters:
        - in: path
          name: jobid
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Cross section calculation result
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/CrossSectionResponse'
    delete:
      summary: Delete pending, running or completed calculation
      parameters:
        - in: path
          name: jobid
          schema:
            type: string
          required: true
      responses:
        '204':
          description: Deletion success

  /differential_cross_section:
    post:
      summary: Submit differential cross section calculation
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DifferentialCrossSectionRequest'
      responses:
        '201':
          description: Calculation submitted, follow location for status/result
          headers:
            Location:
              schema:
                type: string
                format: uri
  /differential_cross_section/{jobid}:
    get:
      summary: Differential cross section calculation status and when completed a result
      parameters:
        - in: path
          name: jobid
          schema:
            type: string
          required: true
      responses:
        '200':
          description: Differential cross section calculation result
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DifferentialCrossSectionResponse'
    delete:
      summary: Delete pending, running or completed calculation
      parameters:
        - in: path
          name: jobid
          schema:
            type: string
          required: true
      responses:
        '204':
          description: Deletion success

components:
  schemas:
    Area:
      additionalProperties: false
      properties:
        unit:
          default: m^2
          enum: [m^2]
          title: Unit
          type: string
        value: {minimum: 0, title: Value, type: number}
      required: [value, unit]
      type: object
    Energy:
      additionalProperties: false
      properties:
        unit:
          default: eV
          enum: [eV, J]
          title: Unit
          type: string
        value: {minimum: 0, title: Value, type: number}
      required: [value, unit]
      type: object
    EnergyRange:
      additionalProperties: false
      properties:
        max: {$ref: '#/components/schemas/Energy'}
        min: {$ref: '#/components/schemas/Energy'}
        points_per_decade: {title: Points per decade, type: integer}
      required: [min, max, points_per_decade]
      title: Energy range
      type: object
    Jm6:
      additionalProperties: false
      properties:
        unit:
          default: J*m^6
          enum: [J*m^6, eV*m^6]
          title: Unit
          type: string
        value: {minimum: 0, title: Value, type: number}
      required: [value, unit]
      type: object
    Length:
      additionalProperties: false
      properties:
        unit:
          default: m
          enum: [m, a0]
          title: Unit
          type: string
        value: {minimum: 0, title: Value, type: number}
      required: [value, unit]
      type: object
    LengthRange:
      additionalProperties: false
      properties:
        max: {$ref: '#/components/schemas/Length'}
        min: {$ref: '#/components/schemas/Length'}
        points: {type: integer}
      required: [min, max, points]
      title: Length range
      type: object
    Mass:
      additionalProperties: false
      properties:
        unit:
          default: amu
          enum: [amu, kg]
          title: Unit
          type: string
        value: {minimum: 0, title: Value, type: number}
      required: [value, unit]
      type: object
    Temperature:
      additionalProperties: false
      properties:
        unit:
          default: K
          enum: [K, eVT]
          title: Unit
          type: string
        value: {minimum: 0, title: Value, type: number}
      required: [value, unit]
      type: object
    VolumeVelocity:
      additionalProperties: false
      properties:
        unit:
          default: m^3/s
          enum: [m^3/s]
          title: Unit
          type: string
        value: {minimum: 0, title: Value, type: number}
      required: [value, unit]
      type: object
    BuckinghamCorner:
      properties:
        type: {type: string, enum: [BuckinghamCorner]}
        A: {$ref: '#/components/schemas/Energy'}
        C6: {$ref: '#/components/schemas/Jm6'}
        alpha: {type: number}
        beta: {$ref: '#/components/schemas/Area'}
        gamma: {type: number}
        rm: {$ref: '#/components/schemas/Length'}
        annotation:
          type: string
      required: [type, A, alpha, rm, C6, beta, gamma]
      additionalProperties: false
      type: object
    ESMSV:
      description: Brunetti 1981
      properties:
        type: {type: string, enum: [ESMSV]}
        A: {type: number}
        C6: {$ref: '#/components/schemas/Jm6'}
        a1: {type: number}
        a2: {type: number}
        a3: {type: number}
        a4: {type: number}
        alpha: {type: number}
        b1: {type: number}
        b2: {type: number}
        b3: {type: number}
        b4: {type: number}
        beta: {$ref: '#/components/schemas/Area'}
        betam: {type: number}
        eps: {$ref: '#/components/schemas/Energy'}
        gamma: {type: number}
        rm: {$ref: '#/components/schemas/Length'}
        x1: {type: number}
        x2: {type: number}
        x3: {type: number}
        x4: {type: number}
        annotation:
          type: string
      required: [type, eps, rm, betam, A, alpha, beta, a1, a2, a3, a4, gamma, x1, x2, x3, x4,
        b1, b2, b3, b4, C6]
      additionalProperties: false
      type: object
    ExponentialRepulsive:
      properties:
        type: {type: string, enum: [ExponentialRepulsive]}
        A: {$ref: '#/components/schemas/Energy'}
        rho: {$ref: '#/components/schemas/Length'}
        annotation:
          type: string
      required: [type, A, rho]
      additionalProperties: false
      type: object
    FiniteWell:
      properties:
        type:
          type: string
          enum: ['FiniteWell']
        V_s: {$ref: '#/components/schemas/Energy'}
        r_s: {$ref: '#/components/schemas/Length'}
        annotation:
          type: string
      required: [type,r_s, V_s]
      additionalProperties: false
      type: object
    HardSphere:
      properties:
        type:
          type: string
          enum: ['HardSphere']
        re: {$ref: '#/components/schemas/Length'}
        annotation:
          type: string
      required: [type,re]
      additionalProperties: false
      type: object
    HulburtHirschfelder:
      properties:
        type: {type: string, enum: [HulburtHirschfelder]}
        alpha: {type: number}
        beta: {type: number}
        gamma: {type: number}
        phi0: {$ref: '#/components/schemas/Energy'}
        re: {$ref: '#/components/schemas/Length'}
        annotation:
          type: string
      required: [type, alpha, beta, gamma, phi0, re]
      additionalProperties: false
      type: object
    LUT:
      description: Lookup table
      properties:
        type: {type: string, enum: [LUT]}
        file: {description: Tab delimited file radius (m) and potential (eV) columns,
          format: url, title: File, type: string}
        is_neutral: {title: Is neutral, type: boolean}
        annotation:
          type: string
      required: [type, is_neutral, file]
      additionalProperties: false
      type: object
    LennardJones:
      properties:
        type: {type: string, enum: [LennardJones]}
        epsilon: {$ref: '#/components/schemas/Energy'}
        sigma: {$ref: '#/components/schemas/Length'}
        annotation:
          type: string
      required: [type, epsilon, sigma]
      additionalProperties: false
      type: object
    MS_12_4:
      description: Mason Schamp 1958 Ann. Phys. p233
      properties:
        type: {type: string, enum:[MS_12_4]}
        epsilon: {$ref: '#/components/schemas/Energy'}
        rm: {$ref: '#/components/schemas/Length'}
        annotation:
          type: string
      required: [type, epsilon, rm]
      additionalProperties: false
      type: object
    Morse:
      properties:
        type: {type: string, enum:[Morse]}
        C: {type: number}
        epsilon: {$ref: '#/components/schemas/Energy'}
        re: {$ref: '#/components/schemas/Length'}
        sigma: {$ref: '#/components/schemas/Length'}
        annotation:
          type: string
      required: [type, epsilon, re, sigma, C]
      additionalProperties: false
      type: object
    RSCP:
      description: Repulsive screened coulomb potential
      properties:
        type: {type: string, enum:[RSCP]}
        epsilon: {$ref: '#/components/schemas/Energy'}
        rho: {$ref: '#/components/schemas/Length'}
        annotation:
          type: string
      required: [type, epsilon, rho]
      additionalProperties: false
      type: object
    LXCatAxis:
      additionalProperties: false
      properties:
        label:
          type: string
        unit:
          type: string
      required:
      - label
      - unit
      type: object
    Process:
      additionalProperties: false
      properties:
        Comment:
          type: string
        Parameters:
          items:
            type: string
          minItems: 0
          type: array
        Species:
          items:
            type: string
          minItems: 1
          type: array
        Updated:
          format: date-time
          type: string
        Values:
          additionalProperties: false
          properties:
            Axes:
              items:
                $ref: '#/components/schemas/LXCatAxis'
              type: array
              maxItems: 2
              minItems: 2
            Data:
              description: Rows of database
              items:
                description: Columns of database, each column is described in the Axes
                  property
                items:
                  type: number
                maxItems: 2
                minItems: 2
                type: array
              type: array
          required:
          - Axes
          - Data
          type: object
      required:
      - Species
      - Parameters
      - Comment
      - Updated
      - Values
      type: object
    LXCatRecord:
      properties:
        '@created':
          format: date-time
          type: string
        '@message':
          type: string
        '@version':
          default: '1.1'
          enum: ['1.1']
          type: string
        Database:
          additionalProperties: false
          properties:
            '@id':
              type: string
            '@name':
              type: string
            Contact:
              type: string
            Description:
              type: string
            Groups:
              additionalProperties: false
              properties:
                Group:
                  additionalProperties: false
                  properties:
                    '@id':
                      type: string
                    Description:
                      type: string
                    Processes:
                      items:
                        $ref: '#/components/schemas/Process'
                      minItems: 1
                      type: array
                  required:
                  - '@id'
                  - Description
                  - Processes
                  type: object
              required:
              - Group
              type: object
            HowToReference:
              type: string
            LXCatUrl:
              format: uri
              type: string
            Permlink:
              format: uri
              type: string
          required:
          - '@name'
          - '@id'
          - Permlink
          - Description
          - Contact
          - HowToReference
          - LXCatUrl
          - Groups
          type: object
        References:
          additionalProperties: false
          properties:
            Reference:
              items:
                type: string
              minItems: 1
              type: array
            Source:
              type: string
          required:
          - Source
          - Reference
          type: object
      required:
      - '@version'
      - '@created'
      - '@message'
      - References
      - Database
      title: LXCat interaction potential database
      type: object
      additionalProperties: false
    LXCatJSONInline:
      description: LXCat JSON
      properties:
        type:
          type: string
          enum: [LXCatJSONInline]
        record:
          $ref: '#/components/schemas/LXCatRecord'
        annotation:
          type: string
      required: [type, record]
      additionalProperties: false
      type: object
    LXCatJSONExtern:
      description: LXCat JSON file
      properties:
        type:
          type: string
          enum: [LXCatJSONExtern]
        file:
          type: string
          description: JSON file formatted with LXCat JSON schema
          format: uri
        annotation:
          type: string
      required: [type, file]
      additionalProperties: false
      type: object
    CollisionIntegral:
      additionalProperties: false
      properties:
        cc: {$ref: '#/components/schemas/ClenshawCurtisQuadrature'}
        s_max: {type: integer}
        temperature_range:
          additionalProperties: false
          properties:
            max: {$ref: '#/components/schemas/Temperature'}
            min: {$ref: '#/components/schemas/Temperature'}
            step: {$ref: '#/components/schemas/Temperature'}
          required: [min, max, step]
          title: Temperature range
          type: object
      required: [s_max, cc, temperature_range]
      title: Collision integral
      type: object
    Potential:
      oneOf:
        - $ref: '#/components/schemas/HardSphere'
        - $ref: '#/components/schemas/FiniteWell'
        - $ref: '#/components/schemas/BuckinghamCorner'
        - $ref: '#/components/schemas/ESMSV'
        - $ref: '#/components/schemas/ExponentialRepulsive'
        - $ref: '#/components/schemas/HulburtHirschfelder'
        - $ref: '#/components/schemas/LUT'
        - $ref: '#/components/schemas/LennardJones'
        - $ref: '#/components/schemas/MS_12_4'
        - $ref: '#/components/schemas/Morse'
        - $ref: '#/components/schemas/RSCP'
        - $ref: '#/components/schemas/LXCatJSONInline'
        - $ref: '#/components/schemas/LXCatJSONExtern'
      title: Potential
      type: object
      discriminator:
        propertyName: type
    ClenshawCurtisQuadrature:
      additionalProperties: false
      properties:
        lower: {type: integer}
        upper: {type: integer}
      required: [lower, upper]
      title: Clenshaw Curtis Quadrature
      type: object
    R:
      additionalProperties: false
      properties:
        Vmax: {$ref: '#/components/schemas/Energy'}
        Vmin: {$ref: '#/components/schemas/Energy'}
        del: {$ref: '#/components/schemas/Length'}
        max: {$ref: '#/components/schemas/Length'}
        min: {$ref: '#/components/schemas/Length'}
      required: [min, max, del, Vmin, Vmax]
      title: R
      type: object
    LMax: {type: integer}
    ReducedMass:
      $ref: '#/components/schemas/Mass'
    Tolerance: {title: Tolerance, type: number}
    ScatteringAngleRequest:
      additionalProperties: false
      properties:
        b_range: {$ref: '#/components/schemas/LengthRange'}
        energy_range: {$ref: '#/components/schemas/EnergyRange'}
        interaction:
          properties:
            potential: {$ref: '#/components/schemas/Potential'}
          required: [potential]
          type: object
          additionalProperties: false
        tolerance: {$ref: '#/components/schemas/Tolerance'}
      required: [interaction, energy_range, b_range, tolerance]
      type: object
    CrossSectionRequest:
      additionalProperties: false
      properties:
        cc: {$ref: '#/components/schemas/ClenshawCurtisQuadrature'}
        l_max: {$ref: '#/components/schemas/LMax'}
        r: {$ref: '#/components/schemas/R'}
        energy_range: {$ref: '#/components/schemas/EnergyRange'}
        interaction:
          properties:
            potential: {$ref: '#/components/schemas/Potential'}
            reduced_mass: {$ref: '#/components/schemas/ReducedMass'}
          required: [potential, reduced_mass]
          type: object
          additionalProperties: false
        ci: {$ref: '#/components/schemas/CollisionIntegral'}
        tolerance: {$ref: '#/components/schemas/Tolerance'}
      required: [cc, l_max, r, interaction, energy_range, ci, tolerance]
      type: object
    DifferentialCrossSectionRequest:
      additionalProperties: false
      properties:
        interaction:
          properties:
            potential: {$ref: '#/components/schemas/Potential'}
            reduced_mass: {$ref: '#/components/schemas/ReducedMass'}
          required: [potential, reduced_mass]
          type: object
          additionalProperties: false
        cc: {$ref: '#/components/schemas/ClenshawCurtisQuadrature'}
        energy_range: {$ref: '#/components/schemas/EnergyRange'}
        l_max: {$ref: '#/components/schemas/LMax'}
        r: {$ref: '#/components/schemas/R'}
        tolerance: {$ref: '#/components/schemas/Tolerance'}
      required: [interaction, cc, energy_range, l_max, r, tolerance]
      type: object
    LabelledAxis:
      type: object
      properties:
        labels:
          type: array
          items:
            type: string
        title:
          type: string
      required: [labels, title]
      additionalProperties: false
    NumericAxis:
      type: object
      properties:
        data:
          type: object
          properties:
            unit:
              type: string
            values:
              type: array
              items:
                type: number
          required: [unit, values]
          additionalProperties: false
        title:
          type: string
      required: [data, title]
      additionalProperties: false
    Axis:
      oneOf:
        - $ref: '#/components/schemas/LabelledAxis'
        - $ref: '#/components/schemas/NumericAxis'
    DataSet:
      type: object
      properties:
        column_axis:
          $ref: '#/components/schemas/Axis'
        columns:
          type: array
          items:
            type: array
            items:
              type: number
        row_axis:
          $ref: '#/components/schemas/Axis'
        title:
          type: string
        unit:
          type: string
      required: [column_axis, columns, row_axis, title, unit]
      additionalProperties: false
    JobStatus:
      type: object
      additionalProperties: false
      required: [state]
      properties:
        state:
          type: string
          enum: [PENDING, SUCCESS, FAILURE, PROGRESS, REVOKED]
        progress:
          description: Progress in percentage.
          type: integer
          minimum: 0
          maximum: 100
        detail:
          description: When state==FAILURE is error message. When state==PROGRESS is progress message.
          type: string
    ScatteringAngleResponse:
      $ref: '#/components/schemas/DataSet'
    CrossSectionResponse:
      type: object
      additionalProperties: false
      required: [status]
      properties:
        status:
          $ref: '#/components/schemas/JobStatus'
        result:
          description: Result of calculation. Present when status.state==SUCCESS
          type: object
          additionalProperties: false
          required: [cs, omega]
          properties:
            cs:
              $ref: '#/components/schemas/DataSet'
            omega:
              $ref: '#/components/schemas/DataSet'
    DifferentialCrossSectionResponse:
      type: object
      additionalProperties: false
      required: [status]
      properties:
        status:
          $ref: '#/components/schemas/JobStatus'
        result:
          description: Result of calculation. Present when status.state==SUCCESS
          type: object
          additionalProperties: false
          required: [dcs, total]
          properties:
            dcs:
              $ref: '#/components/schemas/DataSet'
            total:
              $ref: '#/components/schemas/DataSet'
