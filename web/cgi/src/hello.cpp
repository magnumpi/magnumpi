#include <string>
#include <iostream>
#include <nlohmann/json.hpp>

int main(int argc, char *argv[])
{
  std::cout << "Content-type: application/json" << std::endl << std::endl;
  nlohmann::json request(nlohmann::json::parse(std::cin));
  nlohmann::json response;
  response["message"] = "Hello World!";
  response["name"] = request["name"];
  std::cout << response.dump() << std::endl;
  return 0;
}