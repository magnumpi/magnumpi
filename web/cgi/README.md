# Calculators as CGI scripts

## Hello world

Compile

```sh
g++ -I../../pi_support -std=c++14 src/hello.cpp -o cgi-bin/hello
```

Test Directly

```sh
echo '{"name":"me"}' | ./cgi-bin/hello
Content-type: application/json
{"message":"Hello World!","name":"me"}
```

```sh
docker build -t magnumpi:cgi .
docker run -p 8888:80 magnumpi:cgi
curl -X POST -H "Content-Type: application/json" -d '{"name":"me"}' http://localhost:8888/cgi-bin/hello
```

## Scattering angle

Compile

```sh
g++ -std=c++14 -I../../pi_support -I../.. -o cgi-bin/calc_scat_colonna ../../src/beast_post.cpp ../../src/json_nlohmann.cpp ../../src/units.cpp ../../src/data_set.cpp ../../src/spline.cpp ../../src/potential.cpp ../../src/scat_colonna.cpp ../../src/scat_colonna_dcs.cpp src/calc_scat_colonna.cpp
```

Test directly

```sh
cat ../../input/He_He+/input/gerade_scat_inline.json | cgi-bin/calc_scat_colonna
```

Against Docker container:

```sh
curl -X POST -H "Content-Type: application/json" -d @../../input/He_He+/input/gerade_scat_inline.json http://localhost:8888/cgi-bin/calc_scat_colonna
```
