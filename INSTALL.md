# Quick compilation instructions

## I. Requirements

To compile the code you need

* [cmake](https://cmake.org/)
* C++ compiler like [GCC](https://gcc.gnu.org/)
* (optional) [pybind11](https://pybind11.readthedocs.io/) for Python bindings
* (optional) [emscripten](https://emscripten.org/) for WebAssembly module
* (optional) pdflatex, doxygen and/or tex4ht for doc generation

## II. After cloning, create a build directory, cmake and make

```shell
cd /path/to/magnumpi
mkdir build
cd build
cmake ..
make
```

## III. Prepare the environment and run one of the example scripts:

```shell
. set-magnumpi-local
app/calc_scat ../input/scat_hs.json
```

NOTES:

* Step II can be repeated if you want to support multiple builds.
  As an example, a debug and doc build can be set up as follows:

  ```shell
  cd /path/to/magnumpi
  mkdir build
  cd build
  cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -Dwith-doc=1 ..
  make
  ```

* After updating, repeat the 'make' step in your build directories.

* The steps above assume that you run the code in the build directory.
  If you plan to install the code, add a --prexix option to configure
  and type 'make install' after make. In the installation directory,
  use bin/set-magnumpi instead of set-magnumpi-local to prepare the
  environment.

* Various bits of documentation can be built in the doc/ subdirectory
  (in the build directory). To that end, install pdflatex, doxygen
  and/or tex4ht, reconfigure and do a make inside doc/. The LaTeX
  compilation may silently fail if not all its required sub-packages are
  installed. In order to get the LateX errors, go to the LaTeX directory
  (e.g. doc/paper1) and type 'make LATEX_INTERACTION_MODE='. Without
  that environment setting, 'make' will instruct LaTeX to ignore errors
  and bail out. tex4ht will create xml/html and xml/mathml versions of
  the documents. When doxygen is available, source code documentation
  will be extracted from annotations in the code. The documentation will
  appear in doc/doxygen.
