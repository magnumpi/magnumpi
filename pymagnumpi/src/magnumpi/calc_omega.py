from ._libmagnumpi import (
    in_si,
    Dimension,
    construct_ls_pairs,
    OmegaCalculator,
    CrossSectionFromDatasetLog,
)


def calc_omega(request, cs_data):
    nonsi_reduced_mass = request["interaction"]["reduced_mass"]
    mu = in_si(nonsi_reduced_mass, Dimension.MASS)
    tolerance = request["tolerance"]
    l_max = request["l_max"]
    ci = request["ci"]
    s_max = ci["s_max"]
    ci_lower_cc = ci["cc"]["lower"]
    ci_upper_cc = ci["cc"]["upper"]
    tr = ci["temperature_range"]
    tmin = in_si(tr["min"], Dimension.TEMPERATURE)
    tmax = in_si(tr["max"], Dimension.TEMPERATURE)
    tdel = in_si(tr["step"], Dimension.TEMPERATURE)

    ls_pairs = construct_ls_pairs(l_max, s_max)
    q = CrossSectionFromDatasetLog(cs_data)
    oc = OmegaCalculator(ls_pairs, ci_lower_cc, ci_upper_cc, tolerance)
    omega_data = oc.integrate(q, mu, tmin, tmax, tdel)

    return omega_data
