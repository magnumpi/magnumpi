import argparse
import json
import csv
import sys

from . import get_scattering_angles
from .calc_cs import calc_cs
from .potential_builder import build_potential


def get_cs(infile, outfile):
    request = json.load(infile)
    cs_data = calc_cs(request)
    json.dump(cs_data, outfile, indent=2)


def add_cs(subparsers):
    parser = subparsers.add_parser(
        "cs",
        help="""Calculate cross section. It prints the requested cross sections
        `Q_l(eps)` for user-selected values `l` in `[0,l_max)` as a function
        of a set of user-specified energy values.""",
    )
    parser.add_argument(
        "infile",
        type=argparse.FileType("r"),
        help="""Cross section request JSON file.
        For example `../input/cs.json`. Use `-` for stdin.""",
    )
    parser.add_argument(
        "outfile",
        nargs="?",
        default=sys.stdout,
        type=argparse.FileType("w"),
        help="Output JSON file. Default is stdout.",
    )
    parser.set_defaults(func=get_cs)


def get_scat(infile, outfile):
    request = json.load(infile)
    result = get_scattering_angles(request)
    json.dump(result, outfile, indent=2)


def add_scat(subparsers):
    parser = subparsers.add_parser(
        "scat",
        help="""Calculate scattering angle. It calculates
        the scattering angle chi(b,E) for user-specified impact parameter
        (b) and energy (E) ranges. """,
    )
    parser.add_argument(
        "infile",
        type=argparse.FileType("r"),
        help="""Scattering angle request JSON file.
        For example `../input/scat_rscp_colonna.json`. Use `-` for stdin.""",
    )
    parser.add_argument(
        "outfile",
        nargs="?",
        default=sys.stdout,
        type=argparse.FileType("w"),
        help="Output JSON file. Default is stdout.",
    )
    parser.set_defaults(func=get_scat)


def get_pot(rmin, rmax, rdel, infile, outfile):
    request = json.load(infile)

    V = build_potential(request["potential"])

    writer = csv.writer(outfile)
    headers = ["#distance [m]", "V [V]", "dV/dr [V/m]", "d2V/dr2 [V/m^2]"]
    writer.writerow(headers)
    r = rmin
    while r <= rmax:
        writer.writerow([r, V.value(r), V.dVdr(r), V.d2Vdr2(r)])
        r += rdel


def add_pot(subparsers):
    parser = subparsers.add_parser(
        "pot",
        help="""Prints a table of values of the
        potential V(r) and its first and second derivatives for various
        values of the inter-molecular separation r""",
    )
    parser.add_argument(
        "--rmin",
        type=float,
        default=2e-10,
        help="Minimum distance (default: %(default)s)",
    )
    parser.add_argument(
        "--rmax",
        type=float,
        default=1e-9,
        help="Maximum distance (default: %(default)s)",
    )
    parser.add_argument(
        "--rdel",
        type=float,
        default=1e-14,
        help="Distance step size (default: %(default)s)",
    )
    parser.add_argument(
        "infile",
        type=argparse.FileType("r"),
        help="""Potential request JSON file.
        For example `../input/HH.json`. Use `-` for stdin.""",
    )
    parser.add_argument(
        "outfile",
        nargs="?",
        default=sys.stdout,
        type=argparse.FileType("w"),
        help="Output CSV file. Default is stdout.",
    )
    parser.set_defaults(func=get_pot)


def make_parser():
    """Creates a parser with sub commands

    Returns:
        argparse.ArgumentParser: parser with sub commands
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("--version", action="version", version="0.0.1")
    subparsers = parser.add_subparsers(dest="subcommand")

    add_cs(subparsers)
    add_scat(subparsers)
    add_pot(subparsers)

    return parser


def main(argv=sys.argv[1:]):
    parser = make_parser()
    args = parser.parse_args(argv)
    fargs = vars(args)
    if "func" in fargs:
        # Call function inside subparser
        # See https://docs.python.org/3/library/argparse.html#sub-commands
        func = args.func
        del fargs["subcommand"]
        del fargs["func"]
        func(**fargs)
    else:
        if "subcommand" in args:
            parser.parse_args([args.subcommand, "--help"])
        else:
            parser.print_help()
