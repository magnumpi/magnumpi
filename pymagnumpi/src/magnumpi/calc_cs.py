from math import log10
from ._libmagnumpi import in_si, Dimension, CrossSectionCalculator
from .potential_builder import build_potential
from .dsutils import ds2json


def calc_cs(request):
    """Calulate crossection

    Args:
        request (dict): Dictionary with potential and energy_range.
    """
    pot = build_potential(request["interaction"]["potential"])
    log_e_low = log10(in_si(request["energy_range"]["min"], Dimension.ENERGY))
    log_e_high = log10(in_si(request["energy_range"]["max"], Dimension.ENERGY))
    points_per_decade = request["energy_range"]["points_per_decade"]

    csc = CrossSectionCalculator.create(request, pot)
    cs_data = csc.integrate(log_e_low, log_e_high, points_per_decade)
    return ds2json(cs_data)
