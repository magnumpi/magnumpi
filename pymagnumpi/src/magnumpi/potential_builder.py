from os import unlink, path
from tempfile import NamedTemporaryFile
from urllib.request import urlopen

from ._libmagnumpi import Potential


# NOTE: this version differs from that in
#       wew/ws/python-connexion/magnumpi_webservice/potential_builder.py
#       in two ways:
#       - the temporary file is given extension ".json", because the LUT
#         potential constructor will parse it as a plain two-column ASCII
#         file otherwise. (If the url itself appears to be a json file.)
#       - environment variables are expanded. This means that after
#         'set-magnumpi-local' has been sourced, a file like
#         file://$MAGNUMPI_INPUTDATA_DIR/input/He+_He_ungerade.json
#         can be found.
def save_url(url):
    url = path.expandvars(url)
    if url.find(".json") != -1:
        target = NamedTemporaryFile(suffix=".json", delete=False)
    else:
        target = NamedTemporaryFile(suffix="", delete=False)
    target_fn = target.name
    with urlopen(url) as source:
        target.write(source.read())
    target.close()
    return target_fn


def build_potential(potential):
    if potential["type"] == "LUT":
        potential["file"] = save_url(potential["file"])
        pot = Potential.create(potential)
        unlink(potential["file"])
        return pot
    if potential["type"] == "LXCatJSONExtern":
        potential["type"] = "LXCatJSON"
        potential["file"] = save_url(potential["file"])
        pot = Potential.create(potential)
        unlink(potential["file"])
        return pot
    if potential["type"] == "LXCatJSONInline":
        potential["type"] = "LXCatJSON"
    return Potential.create(potential)
