.. MagnumPI documentation master file, created by
   sphinx-quickstart on Fri Jul 30 11:10:20 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MagnumPI's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   cli
   API reference <api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
