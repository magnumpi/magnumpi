# Develop

Documentation for developers of the pymagnumpi package.

## Non-Python requirements

* C++ compiler
* make
* cmake
* [pybind11](https://pybind11.readthedocs.io/)
* boost

### Install with conda

The requirements can be installed using [miniconda](https://docs.conda.io/en/latest/miniconda.html) with

```shell
conda install -n base -c conda-forge mamba
mamba create -y -n magnumpi -c conda-forge pybind11 cmake compilers boost-cpp make
conda activate magnumpi
```

(Boost should not be installed in OS. Otherwise import problems)

## Compile shared library

```shell
# In root
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -Dwith-doc=0  ..
make -j 4
cd pymagnumpi
make install
# installs *.so into /pymagnumpi/src/magnumpi
```

## Install Python requirements

```shell
# In pymagnumpi/
pip install -e .[dev]
```

## Tests

The tests in `tests/` folder can be run with

```shell
# In pymagnumpi/
pytest
```

To get test coverage (obviously will only cover Python code, not C++ code)

```shell
pytest --cov magnumpi --cov-branch --cov-report=html --cov-report=term --cov-report=xml
```

### Lint

Lint with pycodestyle

```shell
pycodestyle src/magnumpi
```

## Docs

Docs for CLI, API including _libmagnumpi.so can be build with

```shell
# In pymagnumpi/
cd docs
make html
xdg-open _build/html/index.html
```

TODO host docs on [readthedocs.io](https://readthedocs.io)

## Build

To build binary wheel for current OS.
Needs [shared library](#compile-shared-library) to have been compiled.

```shell
# In pymagnumpi/
python -m build --wheel .
```

TODO make for multiple OS / python versions, maybe use https://github.com/pypa/cibuildwheel or multiple conda environments.

## Upload to PyPI

```shell
# In pymagnumpi/
twine upload dist/*
```
