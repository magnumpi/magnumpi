import json

from magnumpi.potential_builder import build_potential
from pytest import raises, fixture


@fixture
def lxcat_record():
    return {
        "@version": "1.1",
        "@created": "2019-07-09T21:23:14Z",
        "@message":
        "ATTENTION: downloading in xml format in under development. Do not use this file.",  # noqa
        "References": {
            "Source":
            "LXCat, www.lxcat.net. Generated on 09 Jul 2019. All rights reserved.",  # noqa
            "Reference": [
                "- Viehland database, www.lxcat.net, retrieved on July 9, 2019.",  # noqa
                "Be aware that some databases and solvers can additionally have instructions how to reference corresponding data. Please check below in the headers of databases."  # noqa
            ]
        },
        "Database": {
            "@name": "Viehland database",
            "@id": "Viehland",
            "Permlink": "http://www.lxcat.net/Viehland",
            "Description":
            "The data here are from the Gaseous Ion Transport and Rate Coefficient Database, Software Release 4.1 (March, 2006), as extended and updated on a regular basis.",  # noqa
            "Contact": "Larry A. Viehland, Viehland@Chatham.edu",
            "HowToReference":
            "L. A. Viehland and C. C. Kirkpatrick, Int. J. Mass Spectrom. Ion Proc. 149/150 (1995) 555.",  # noqa
            "LXCatUrl": "http://www.lxcat.net/contributors/#d26",
            "Groups": {
                "Group": {
                    "@id":
                    "Rare Gas Ions in Their Parent Gas",
                    "Description":
                    "Ab Initio Potentials of Wright et al.",
                    "Processes": [{
                        "Species": ["He^+(2S1/2)", "He"],
                        "Parameters": [],
                        "Comment":
                        "2-SIGMA-g-+ Potential of L. A. Viehland, R. Johnsen, B. R. Gray and T. G. Wright,  J. Chem. Phys. 144 (2016) 074306.",  # noqa
                        "Updated": "2018-11-02T15:59:09Z",
                        "Values": {
                            "Axes": [{
                                "label": "r",
                                "unit": "Bohr"
                            }, {
                                "label": "V",
                                "unit": "Hartree"
                            }],
                            "Data": [[1.511781e+0, 6.713898e-1],
                                     [1.606267e+0, 5.718335e-1],
                                     [1.700754e+0, 4.865126e-1],
                                     [1.795240e+0, 4.144149e-1],
                                     [1.889726e+0, 3.535758e-1],
                                     [1.984212e+0, 3.022067e-1],
                                     [2.078699e+0, 2.587806e-1],
                                     [2.173185e+0, 2.220148e-1],
                                     [2.267671e+0, 1.908362e-1],
                                     [2.362158e+0, 1.643492e-1],
                                     [2.456644e+0, 1.418060e-1],
                                     [2.551130e+0, 1.225822e-1],
                                     [2.645617e+0, 1.061561e-1],
                                     [2.834589e+0, 8.002471e-2],
                                     [3.023562e+0, 6.070958e-2],
                                     [3.212534e+0, 4.631288e-2],
                                     [3.401507e+0, 3.549497e-2],
                                     [3.590480e+0, 2.730462e-2],
                                     [3.779452e+0, 2.106159e-2],
                                     [4.157397e+0, 1.258728e-2],
                                     [4.535343e+0, 7.523100e-3],
                                     [4.913288e+0, 4.465823e-3],
                                     [5.291233e+0, 2.613008e-3],
                                     [5.669178e+0, 1.492006e-3],
                                     [6.141610e+0, 6.974311e-4],
                                     [6.614041e+0, 2.847102e-4],
                                     [7.086473e+0, 7.710910e-5],
                                     [7.558904e+0, -2.172538e-5],
                                     [8.031336e+0, -6.400657e-5],
                                     [8.503768e+0, -7.772780e-5],
                                     [8.976199e+0, -7.772472e-5],
                                     [9.448631e+0, -7.174501e-5],
                                     [1.039349e+1, -5.546422e-5],
                                     [1.133836e+1, -4.110913e-5],
                                     [1.228322e+1, -3.040523e-5],
                                     [1.322808e+1, -2.275027e-5],
                                     [1.417295e+1, -1.728694e-5],
                                     [1.511781e+1, -1.334429e-5],
                                     [1.606267e+1, -1.045673e-5],
                                     [1.700754e+1, -8.308403e-6],
                                     [1.795240e+1, -6.684929e-6],
                                     [1.889726e+1, -5.439960e-6],
                                     [2.078699e+1, -3.710824e-6],
                                     [2.267671e+1, -2.618124e-6],
                                     [2.456644e+1, -1.899952e-6],
                                     [2.645617e+1, -1.412090e-6],
                                     [2.834589e+1, -1.071256e-6],
                                     [3.023562e+1, -8.273170e-7],
                                     [3.212534e+1, -6.490250e-7],
                                     [3.401507e+1, -5.162730e-7],
                                     [3.590480e+1, -4.158000e-7],
                                     [3.779452e+1, -3.386250e-7],
                                     [4.251884e+1, -2.113620e-7],
                                     [4.724315e+1, -1.386720e-7],
                                     [5.196747e+1, -9.473200e-8],
                                     [5.669178e+1, -6.690800e-8],
                                     [7.558904e+1, -2.123600e-8],
                                     [9.448631e+1, -8.758000e-9],
                                     [1.133836e+2, -4.280000e-9],
                                     [1.322808e+2, -2.359000e-9],
                                     [1.511781e+2, -1.428000e-9],
                                     [1.700754e+2, -9.310000e-10],
                                     [1.889726e+2, -6.470000e-10],
                                     [2.834589e+2, -2.140000e-10]]
                        }
                    }]
                }
            }
        }
    }


class TestBuild_potential:
    def test_hardsphere(self):
        request = {"type": "HardSphere", "re": {"value": 42, "unit": "m"}}
        pot = build_potential(request)
        assert pot is not None

    def test_lut_dataurl_wrongformat(self):
        request = {
            "type": "LUT",
            "file": "data:,Hello%2C%20World!",
            "is_neutral": False
        }
        with raises(RuntimeError, match=r'Bad data'):
            build_potential(request)

    def test_lut_dataurl(self):
        url = 'data:text/plain;name=He+_He_ungerade.txt;base64,NC41MDAwMDAwODE0NjE5NTY0ZS0xMQkxLjY0Mzc0MDMyMzgxNTYyMzZlKzAxCjQuNzUwMDAwMDg1OTg3MDMzNmUtMTEJMS4zNzE0Nzk3OTg5Njg3MDg1ZSswMQo1LjAwMDAwMDA5MDUxNzQwMDdlLTExCTEuMTM3NzA4NTQ0MTUwMDY4MWUrMDEKNS4yNTAwMDAwOTUwNDI0NzcyZS0xMQk5LjM2MTEyNjQyOTY0NDU5MDZlKzAwCjUuNTAwMDAwMDk5NTY3NTUzN2UtMTEJNy42MTc0NTU4NzUwMDk5NjI4ZSswMAo1Ljc1MDAwMDEwNDA5MjYzMDFlLTExCTYuMTA2NTg0NDQzNTIyNDgwOGUrMDAKNi4wMDAwMDAxMDg2MTc3MDUzZS0xMQk0Ljc5NjM5ODAwOTkwNzUwMTRlKzAwCjYuMjUwMDAwMTEzMTQyNzgzMGUtMTEJMy42NjAzMjcxMzg3MDE1Mzc5ZSswMAo2LjUwMDAwMDExNzY2Nzg1OTVlLTExCTIuNjc2MDYyMDI1MDgyNDgyN2UrMDAKNi42MDAwMDAxMTk0ODAwMDY0ZS0xMQkyLjMyMDUxNzMzNTU2OTYxNThlKzAwCjYuODAwMDAwMTIzMDk5MDA4OGUtMTEJMS42Njg4MTg0Njg0NjUyMzU0ZSswMAo2LjkwMDAwMDEyNDkxMTE1NTZlLTExCTEuMzcwNjc0MDc2MDkxMzA3NmUrMDAKNy4wMDAwMDAxMjY3MjMzMDM4ZS0xMQkxLjA4OTc2NDc5Nzg4NzY2NjFlKzAwCjcuMTAwMDAwMTI4NTMwMTU5M2UtMTEJOC4yNTIyNzM2MTU3OTAwMzM0ZS0wMQo3LjIwMDAwMDEzMDM0MjMwNjJlLTExCTUuNzYyNDQ1ODQyMDQwODYzMWUtMDEKNy4zMDAwMDAxMzIxNTQ0NTMwZS0xMQkzLjQyMDQyMjQ0MDg1MTc5NDdlLTAxCjcuNDAwMDAwMTMzOTYxMzA4NWUtMTEJMS4yMTg4NjI1MDI0NTEzNzE5ZS0wMQo3LjUwMDAwMDEzNTc3MzQ1NTRlLTExCS04LjQ5MTk5MjM3ODk2NjU3NThlLTAyCjguMDAwMDAwMTQ0ODIzNjA4M2UtMTEJLTkuNDA3MzAzODg3Nzk3NjMzNGUtMDEKOC41MDAwMDAxNTM4NzM3NjEzZS0xMQktMS41NDkyNTgwODU3NjE0NTA0ZSswMAo5LjAwMDAwMDE2MjkyOTIwNTZlLTExCS0xLjk2NjM4MTQ4MzQ1ODg3OThlKzAwCjkuNTAwMDAwMTcxOTc5MzU3MmUtMTEJLTIuMjM1OTg1NDkxMjc2MTY5NGUrMDAKMS4wMDAwMDAwMTgxMDI5NTExZS0xMAktMi4zOTI2Mzk4NzYyOTA4MDk3ZSswMAoxLjA1MDAwMDAxOTAwNzk2NjNlLTEwCS0yLjQ2MzYwMDI3Njk4Mzg4MjVlKzAwCjEuMTAwMDAwMDE5OTEzNTEwN2UtMTAJLTIuNDcwMzQxMDc5NTA1MjE1NmUrMDAKMS4xNTAwMDAwMjA4MTg1MjYwZS0xMAktMi40Mjk3NDk2OTU5MDA2ODkyZSswMAoxLjIwMDAwMDAyMTcyMzU0MTFlLTEwCS0yLjM1NTA2NzAwNDM5ODI0NDBlKzAwCjEuMjUwMDAwMDIyNjI4NTU2NmUtMTAJLTIuMjU2NjMyNzQ4ODkzMjY2N2UrMDAKMS4zMDAwMDAwMjM1MzQxMDA4ZS0xMAktMi4xNDI0Nzg2OTI5MDAxODYyZSswMAoxLjM1MDAwMDAyNDQzOTExNjFlLTEwCS0yLjAxODgwMTQzMzQ4NDg5MDVlKzAwCjEuNDAwMDAwMDI1MzQ0MTMxNGUtMTAJLTEuODkwMzM5NjA5MTMxNTI1OWUrMDAKMS41MDAwMDAwMjcxNTQ2OTExZS0xMAktMS42MzI0NzU3NDM4NDM2MDQ0ZSswMAoxLjYwMDAwMDAyODk2NDcyMTdlLTEwCS0xLjM4NzY4NjM5MzU3NDA0MjJlKzAwCjEuNzAwMDAwMDMwNzc1MjgxNGUtMTAJLTEuMTY1NDU4NjcxNjkwNTQ0MGUrMDAKMS44MDAwMDAwMzI1ODUzMTIwZS0xMAktOS42OTYyNjEwNTgwNTg1NzI5ZS0wMQoxLjkwMDAwMDAzNDM5NTg3MTRlLTEwCS04LjAwNjc3NzE0MTUxNzcyNThlLTAxCjIuMDAwMDAwMDM2MjA1OTAyM2UtMTAJLTYuNTcyMTMyODA3MzcxMzg5MmUtMDEKMi4yMDAwMDAwMzk4MjY0OTIzZS0xMAktNC4zNjkxMzMyMDI2NDU1MzcxZS0wMQoyLjQwMDAwMDA0MzQ0NzA4MjFlLTEwCS0yLjg3MDU2MTk3OTE2Mzc2NzFlLTAxCjIuNjAwMDAwMDQ3MDY3NjcyN2UtMTAJLTEuODc0ODg2Mzc1NTU0NTg2MWUtMDEKMi44MDAwMDAwNTA2ODgyNjI3ZS0xMAktMS4yMjMzOTk5Mzg2NjA1MjM4ZS0wMQozLjAwMDAwMDA1NDMwODg1MzNlLTEwCS04LjAwOTk5MjY5ODYxMTU1NTJlLTAyCjMuMjUwMDAwMDU4ODM0NDU4NGUtMTAJLTQuNzY5MDczNzQwMzA4MTMyMmUtMDIKMy41MDAwMDAwNjMzNjA1OTI4ZS0xMAktMi44OTQwNDY1MzkwMjI1NjgwZS0wMgozLjc1MDAwMDA2Nzg4NjE5ODRlLTEwCS0xLjgwMjczMjY2ODM4MDA0NzRlLTAyCjQuMDAwMDAwMDcyNDExODA0NmUtMTAJLTEuMTYwMTQyMTk4MDE0MDA1M2UtMDIKNC4yNTAwMDAwNzY5Mzc5MzkwZS0xMAktNy43NTExNzMyMTQ5MzQwODcxZS0wMwo0LjUwMDAwMDA4MTQ2MzU0NDZlLTEwCS01LjM4ODc1MTQyMDAxODE0NzdlLTAzCjQuNzUwMDAwMDg1OTg5MTUwMmUtMTAJLTMuODk1NTA5ODE5MDgxNjg5M2UtMDMKNS4wMDAwMDAwOTA1MTQ3NTQ4ZS0xMAktMi45MTkyMjc3MDA4Mzc5ODU3ZS0wMwo1LjUwMDAwMDA5OTU2NjQ5NTRlLTEwCS0xLjc5MzYyMjAzNDc1Njg0NTNlLTAzCjYuMDAwMDAwMTA4NjE3NzA2NmUtMTAJLTEuMjAzMDE3ODcxNzY1ODk5OWUtMDMKNi41MDAwMDAxMTc2Njk0NDYxZS0xMAktOC41Mjc3NzkwNDU1NjAwNDc0ZS0wNAo3LjAwMDAwMDEyNjcyMDY1NzNlLTEwCS02LjI2NDIyNTExODAyMzkyMjBlLTA0CjcuNTAwMDAwMTM1NzcyMzk2OWUtMTAJLTQuNzIyMTkzOTA4MjI2NDk2NWUtMDQKOC4wMDAwMDAxNDQ4MjQxMzc1ZS0xMAktMy42MzQ0NTkyNTQ3Mjk2ODI4ZS0wNAo4LjUwMDAwMDE1Mzg3NTM0NzZlLTEwCS0yLjg0NjIwMzMzMTMxOTYzNjFlLTA0CjkuMDAwMDAwMTYyOTI3MDg5M2UtMTAJLTIuMjYyMDM1ODA0MjUxMDE2M2UtMDQKOS41MDAwMDAxNzE5NzgzMDA1ZS0xMAktMS44MjA5MDAwMzUzMTA5MjY3ZS0wNAoxLjAwMDAwMDAxODEwMzAwMzllLTA5CS0xLjQ4MjQwMzcxMzYzMzg5MjRlLTA0CjEuMTAwMDAwMDE5OTEzMjk5MWUtMDkJLTEuMDExNjcwOTM1MzEwMTE2M2UtMDQKMS4yMDAwMDAwMjE3MjM1OTQzZS0wOQktNy4xMzcyMjU3MzYzMjY0NTEwZS0wNQoxLjMwMDAwMDAyMzUzMzg4OTJlLTA5CS01LjE3NzQxODEwMDcxMjYxNDdlLTA1CjEuNDAwMDAwMDI1MzQ0MTg0NGUtMDkJLTMuODQ2MTMwOTIwNDIyMTQ4OWUtMDUKMS41MDAwMDAwMjcxNTQ0Nzk0ZS0wOQktMi45MTY1NzYzMzIxMjY2MDM0ZS0wNQoxLjYwMDAwMDAyODk2NDc3NDZlLTA5CS0yLjI1MTgwNzU4ODA3NTk0NTNlLTA1CjEuNzAwMDAwMDMwNzc1MDY5NWUtMDkJLTEuNzY2MjMxMjcwMjE0NzEwNmUtMDUKMS44MDAwMDAwMzI1ODUzNjQ3ZS0wOQktMS40MDQ4NzIyMTMyMjgxODA4ZS0wNQoxLjkwMDAwMDAzNDM5NTY2MDFlLTA5CS0xLjEzMTQyNzcwNTYxNDkxMDdlLTA1CjIuMDAwMDAwMDM2MjA1OTU1M2UtMDkJLTkuMjE0MjY1NDY5NzUxOTQwMGUtMDYKMi4yNTAwMDAwNDA3MzE3MTk1ZS0wOQktNS43NTEzNzE1NjE1Mjc0NDQyZS0wNgoyLjUwMDAwMDA0NTI1NzQ4MzNlLTA5CS0zLjc3MzQ1NzQ2ODk1MTU2NDdlLTA2CjIuNzUwMDAwMDQ5NzgzMTk1MGUtMDkJLTIuNTc3Nzg5MTIwNzIxNzAwMWUtMDYKMy4wMDAwMDAwNTQzMDg5NTg4ZS0wOQktMS44MjA2NTk0ODY2NDkxNTI3ZS0wNgo0LjAwMDAwMDA3MjQxMTk2MjdlLTA5CS01Ljc3ODYxMDE1OTk5MzAzNTdlLTA3CjUuMDAwMDAwMDkwNTE0OTEzNmUtMDkJLTIuMzgzMTczMjgwMzM2MTc0N2UtMDcKNi4wMDAwMDAxMDg2MTc5MTc1ZS0wOQktMS4xNjQ2NDczNjY5NjAzNTk0ZS0wNwo3LjAwMDAwMDEyNjcyMDg2ODVlLTA5CS02LjQxOTE2NjIxMTgyMTIzMzBlLTA4CjguMDAwMDAwMTQ0ODIzODcyNGUtMDkJLTMuODg1Nzg2MDc0ODExNjY2OWUtMDgKOS4wMDAwMDAxNjI5MjY4Nzc5ZS0wOQktMi41MzMzODAxMzcwMDk1NjY3ZS0wOAoxLjAwMDAwMDAxODEwMjk4MjdlLTA4CS0xLjc2MDU3Njc0Mzk3OTc5NTZlLTA4CjEuNTAwMDAwMDI3MTU0NDc0MmUtMDgJLTUuODIzMjM2ODM0ODAxNzk3NWUtMDkK'  # noqa
        request = {"type": "LUT", "file": url, "is_neutral": False}
        pot = build_potential(request)
        assert pot is not None

    def test_lxcatjsoninline(self, lxcat_record):
        request = {"type": "LXCatJSONInline", "record": lxcat_record}
        pot = build_potential(request)
        assert pot is not None

    def test_lxcatjsonextern(self, lxcat_record, tmp_path):
        fn = tmp_path / 'lxcat.json'
        fn.write_text(json.dumps(lxcat_record))
        request = {"type": "LXCatJSONExtern", "file": 'file://' + str(fn)}
        pot = build_potential(request)
        assert pot is not None
