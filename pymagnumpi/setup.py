from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext

class DummyExtension(Extension):
    def __init__(self, name, sourcedir=""):
        Extension.__init__(self, name, sources=[])

class DummyBuild(build_ext):
    def build_extension(self, ext):
        pass

    def copy_extensions_to_source(self):
        pass

setup(
    ext_modules=[DummyExtension("pymagnumpi")],
    cmdclass={"build_ext": DummyBuild},
    # TODO make for more python versions + arches combis
    package_data={"": ['_libmagnumpi.cpython-39-x86_64-linux-gnu.so', 'libmagnumpi.so']}
)
